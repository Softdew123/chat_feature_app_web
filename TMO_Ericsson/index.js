

import React, { PureComponent } from 'react';
import { View, AppRegistry, SafeAreaView, Text } from 'react-native';
import { StyleApp } from './App/Styles/PageStyle';
import Navigation from './App/Utils/AppNavigation';
import NetInfo from "@react-native-community/netinfo";
import { DeviceInfo, GlobalVariable, Logger, Dialog } from './App/Utils/Utils'
import I18n from './App/Utils/i18n'

export default class App extends PureComponent {
    constructor(props) {
        super(props);
    } 

    componentDidMount() {
        /**	   
        * @desc NetInfo.addEventListener keep checking network connectivity and display alert dialog
        * if no active internet connection available.
        * GlobalVariable.isInternetConnected is a global bool,avaliable across the app and if the balue is true then internet
        * is connection, false means no internet. 
        * @param none
        * @return networkType: object of network type 	 
        */
        // NetInfo.addEventListener('connectionChange', (networkType) => {
        //     GlobalVariable.isInternetConnected = DeviceInfo.validateConnectionType(networkType);
        //     GlobalVariable.networkType = networkType.type;
        //     if (!GlobalVariable.isInternetConnected) {
        //         Dialog.alertDialog(
        //             I18n.t('NO_INTERNET_TITLE'),
        //             I18n.t('NO_INTERNET_MSG'),
        //             I18n.t('ALERT_MESSAGES.OK_BUTTTON'),
        //             false, null
        //         );
        //         Logger.normal(I18n.t('NO_INTERNET'));
        //     }
        // });
    }

    render() {
        return (
            <SafeAreaView style={StyleApp.safeArea}>
                <View style={StyleApp.applicationView}>
                    <Navigation />
                </View>
            </SafeAreaView>
        );
    }
}

AppRegistry.registerComponent('Boilerplate', () => App);