

import React, { PureComponent } from 'react'
import { StatusBar, View } from 'react-native'
import { Colors } from '../Styles/Themes'
import { DeviceInfo } from '../Utils/Utils'

export default class StatusBarComponent extends PureComponent {
	/**
	* @desc Return Status bar component as per mobile OS. Or User can completely hide status bar by setting 
					props isHidden to true.
	* @props isHidden: bool: True hides status bar
	* @return Status bar component.
	*/
	statusBarComponent = () => {
		//Hide status bar if isHidden is true
		if (this.props.isHidden) {
			return (<StatusBar hidden={true} translucent barStyle="light-content" />)
		} else {
			//Return Android Status Bar if is Aandroid returns true;	
			if (DeviceInfo.isAndroid()) {
				return (
					<View style={{ height: StatusBar.currentHeight, backgroundColor: Colors.colorStatusBar}}>
						<StatusBar hidden={this.props.isHidden} translucent backgroundColor={Colors.colorStatusBar} barStyle="light-content" />
					</View>
				)
			} else {
				//Return IOS Status Bar
				return (
					<View style={{ backgroundColor: Colors.colorStatusBar }}>
						<StatusBar hidden={this.props.isHidden} backgroundColor={Colors.colorStatusBar} barStyle="dark-content" />
					</View>
				);
			}
		}
	}

	render() {
		return (
			<View>
				{this.statusBarComponent()}
			</View>
		)
	}
}