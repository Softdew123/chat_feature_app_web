
import React, { Component } from 'react'
import { ActivityIndicator, View, Text, Modal } from 'react-native'
import { StyleLoader } from '../Styles/ComponentStyle'
import { Colors } from '../Styles/Themes'
import { DeviceInfo } from '../Utils/Utils'
 
export default class Loader extends Component {
	/**
	* @desc Return Custom Loader component as per mobile OS. 
	* @props props.msg: string: Message to be displayed loader with loader icon
	* @return Custom Loader component.
	*/
	loader = () => {
		let msg = this.props.msg;
		if (DeviceInfo.isAndroid()) {
			//Android Loader
			return (
				<View style={[StyleLoader.parentContainer]}>
					<View style={StyleLoader.conentContainer}>
						<ActivityIndicator style={StyleLoader.loaderIcon} size={40} color={Colors.blueVarient} hidesWhenStopped={true} />
						<Text style={StyleLoader.loaderText}>
							{((msg && msg).length > 30) ? (((msg).substring(0, 30)) + '...') : msg}
						</Text>
					</View>
				</View>
			)
		} else {
			// IOS Loader
			return (
				<View style={[StyleLoader.parentContainer]}>
					<View style={StyleLoader.conentContainer}>
						<ActivityIndicator style={StyleLoader.loaderIcon} size="large" color={Colors.blueVarient} hidesWhenStopped={true}  />
						<Text style={StyleLoader.loaderText}>
							{((msg && msg).length > 30) ? (((msg).substring(0, 30)) + '...') : msg}</Text>
					</View>
				</View>
			);
		}
	}

	render() {
		return (
			<Modal transparent={true} animationType="none" visible={this.props.hideModel} onRequestClose={() => { }}>
				<View style={StyleLoader.overlay}>
					{this.loader()}
				</View>
			</Modal>
		);
	}
}