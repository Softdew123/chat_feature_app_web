
import React, { PureComponent } from 'react';
import { Text, View } from 'react-native';
import { StyleOfflineBar } from '../Styles/ComponentStyle';
import I18n from '../Utils/i18n';
import NetInfo from "@react-native-community/netinfo";

export default class OfflineBarComponent extends PureComponent {
	/**
	* @desc Return No internet Connection bar if there is no internet connection available.
	* @props None
	* @return Internet bar component.
	*/
	constructor(props) {
		super(props);
		this.state = {
			isConnected: true
		};
	}

	componentDidMount() {
		/** 
		*@desc Add Internet Connectivity Change Event Listener.;
		*/
		NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
	}

	componentWillUnmount() {
		/** 
		*@desc Remove Internet Connectivity Change Event Listener.;
		*/
		NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
	}

	/**
	* @desc Callback function of Network change Event Listener and change state to render no internet bar accordingly.
	* @Param isConnected:bool. If Connected the value must be true and if no internet then value must be false .
	* @return null.
	*/
	handleConnectivityChange = isConnected => {
		this.setState({ isConnected });
	};

	render() {
		if (!this.state.isConnected) {
			return (
				<View style={StyleOfflineBar.offlineContainer}>
					<Text style={StyleOfflineBar.offlineText}>
						{I18n.t('NO_INTERNET_TITLE')}
					</Text>
				</View>
			)
		} else {
			return null;
		}
	}
}