
import React, { PureComponent } from 'react'
import {View, Keyboard,TouchableWithoutFeedback,Text} from 'react-native'
import {Colors} from '../Styles/Themes'
import {StyleToolbar} from '../Styles/ComponentStyle'
import {DeviceInfo} from '../Utils/Utils'
import Constant from '../Utils/Constant';
import Icon from 'react-native-vector-icons/Ionicons';

export default class ToolBarComponent extends PureComponent {
	/**
	 * Method to handle toolbar actions
	 * All other actions are propogated to parent component
	 */
	onActionSelected = (position) => {
		this.props.onActionSelected(position);
	}

	renderToolbarAction() {
		if (this.props.onActionSelected) {
			return (
				<TouchableWithoutFeedback onPress={()=>this.props.onActionSelected(0)}>
				<View style={StyleToolbar.iosToolbarAction}>
					<Text style={StyleToolbar.iosToolbarActionText}>
					{this.props.actionTitle}
					</Text>
				</View>
				</TouchableWithoutFeedback>
			);
		}
		return (
			<View style={[StyleToolbar.toolbar_IOS_buttonCon]}>
				<Text></Text>
			</View>
		);
	}

	renderBackButton() {
		if (this.props.is_IOS_BackBt_Visible) {
			return(
			<View style={[StyleToolbar.toolbar_IOS_buttonCon]}>
				<TouchableWithoutFeedback onPress={this.props.navClick}  style={StyleToolbar.iosBack_button} >
					<Icon name="ios-arrow-back" size={35} color={Colors.blueVarient} style={StyleToolbar.toolbar_IOS_Back} onPress={this.props.navClick}/> 
				</TouchableWithoutFeedback>	
			</View>
			);
		}
		return (
			<View style={[StyleToolbar.toolbar_IOS_buttonCon]}>
				<Text></Text>
			</View>
		);
	}
	 
 	// will return OS specific toolbar			
	toolBarComponent=()=>{ 
		if(DeviceInfo.isAndroid()){
			// android specific status bar
			/**	    
			* @Attribute: props.navIcon navigation icon
			* @Attribute props.navClick click function
			* @Attribute props.title text of navigation 
			* @Attribute props.actionOptions other right hand options	 
			*/
			return ( 
				<Icon.ToolbarAndroid
				navIconName={this.props.navIcon}
				onIconClicked={this.props.navClick}
				style={StyleToolbar.toolbar}
				titleColor={Colors.white}		
				title={this.props.title}
				actions={this.props.actionOptions} 
				onActionSelected={this.onActionSelected}
				overflowIconName={Constant.threeDots}
				/>
			)
		} else{
			// ios specific status bar
			return (
				<View style={StyleToolbar.toolbar_IOS}>
					<View style={StyleToolbar.toolbar_IOS_InnerCon}>
				
						<View style={{flex:1,flexDirection:'row'}}>	
							{this.renderBackButton()}
							{this.props.displayCenterTextIOS&&
							<View style={StyleToolbar.toolbar_IOS_TextCon}>
								<Text style={StyleToolbar.iosTitle} ellipsizeMode='tail' numberOfLines={1}>{this.props.title}</Text>
							</View>
							}
							{this.renderToolbarAction()}
						</View>
						
					</View>
				</View> 
			);
		}  		
	}
	
	/**	    
     * @Attribute:title title of toolbar
	   @Attribute:actionOptions array of options object 	 
     */
  render () { 
	  return( 
	    <View>
			{this.toolBarComponent()}
		</View> 
	  )
  }
}
