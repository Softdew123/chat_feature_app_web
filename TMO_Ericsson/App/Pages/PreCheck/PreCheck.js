import React, { PureComponent,Component } from 'react';
import ToolbarComponent from "../../Components/Toolbar";
import I18n from '../../Utils/i18n';
import { CommonMethods, DeviceInfo, Dialog } from '../../Utils/Utils'
import { WebView,Image,Button,Dimensions, FlatList, RefreshControl, Text, TouchableOpacity, View, KeyboardAvoidingView,Modal, TextInput, Picker, ScrollView, TouchableWithoutFeedback} from 'react-native';
import { StylePreCheck } from '../../Styles/PageStyle';
import { StyleGCSiteLogin } from '../../Styles/PageStyle';
import AutoHeightWebView from 'react-native-autoheight-webview'
// import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

import RadioGroup,{Radio} from "react-native-radio-input";

import StatusBarComponent from '../../Components/StatusBar';        
import OfflineBarComponent from '../../Components/OfflineBar';
import Constant from '../../Utils/Constant';              
import Icon from 'react-native-vector-icons/Ionicons';  
import { Colors } from '../../Styles/Themes';
import Placeholder, { Paragraph, Line, Media } from 'rn-placeholder';
import AsyncStorage from '@react-native-community/async-storage';
import { PostRequest } from '../../Utils/ServiceCalls';
import Loader from '../../Components/Loader';
import ImageZoom from 'react-native-image-pan-zoom';
import { StyleDashboard } from '../../Styles/PageStyle';
import DatePicker from 'react-native-datepicker';
import HTMLView from 'react-native-htmlview'; 
const width = Dimensions.get('window').width;
const height =  Dimensions.get('window').height ;
 
export default class GCSiteLoingScreen extends PureComponent {
    
  precheckImage="";
  constructor(props) {
	super(props);
	this.currentDate=this.getCurrentDate(); 
    this.state = {
      imgUrl:"",
      postCheckUrl:"",
      refresh: true,
      isAppDataAvailable: false,
	  isAppDataAvailables: true,
	  isDayMOP: false,
	  postData: [],
	  loginType:"",
	  fromDate:this.currentDate,
      toDate:this.currentDate,
	  preStatus:"",
	  cressIdds: "",
	  
	  isModalVisible: false,
	  sowArrayDetails:[],
	  SelectedSOWDetails:"1",
    };
  }

  getCurrentDate=()=>{
	var now = new Date(); 
	let currentDateTime = new Date()
	let month = currentDateTime.getMonth() + 1
	if (month < 10) {
		month = '0' + month; 
	}
	
	let day = currentDateTime.getDate()
	if (day < 10) {
		day = '0' + day; 
	}
	
	let year = currentDateTime.getFullYear()
	let fullDate=month + "/" + day + "/" + year;
	return fullDate;
}
gotoWebView_Screen=(data)=>{
	console.log("webview",data)
let params = { item:data}; 
 this.props.navigation.navigate("WebView_Screen",params); 


}
  renderDateComponent=(dateStateTitle)=>{
	console.log(dateStateTitle)
		  
	return(<DatePicker
	  style={StyleDashboard.datePickerContainerStyle}
	  date={this.state[dateStateTitle]}
	  mode="date"
	  showIcon={true}
	  placeholder="select date"
	  format="MM-DD-YYYY"
	  minDate="01-01-2016"
	  maxDate={this.currentDate}
	  confirmBtnText="Confirm"
	  cancelBtnText="Cancel"
	  customStyles={{
		dateIcon: StyleDashboard.datepickerIcon,
		dateInput:StyleDashboard.datepickerInput  
	  }}
	  onDateChange={(selectedDate) => { 
		let dateObj={};
		dateObj[dateStateTitle]=selectedDate; 
		this.setState(dateObj); 
	  }}
	/> )
  }

  componentDidMount() {

	this.getUsersLocalDetails()
	this.getPlannedSOWDetails()
    try
    {
     let params= this.props.navigation.state.params;
   let postCheckUrl = params.preCheckImage; 
   this.state.preStatus = params.view; 
   
   
   console.log("pree  1",postCheckUrl)
   this.state.postCheckUrl = postCheckUrl
   // this.setState({postCheckUrl: postCheckUrl});
   console.log("pree  2",this.state.postCheckUrl)
   this.getPostData()
    }
    catch(error)
    {
        alert(err)
    }
 
   
   }

   getUsersLocalDetails=async()=>{
    console.log("Main 1")
    const userDetails = await AsyncStorage.getItem(Constant.LOCAL_STORAGE.LOGINRESPONSE);

    try
    {
      console.log(JSON.parse(userDetails))
      let userDetailsObject=JSON.parse(userDetails);
      let idss= userDetailsObject.id
      this.setState({cressIdds: idss});
    //   this.setState({contactName:contactName,contactEmail:contactEmail,contactNumber:contactNumber},()=>{
    //       this.callGetSiteApi(); 
    //   })
    }
    catch(err)
    {
       
    }
  }
   getPostData(){
   
   let paramObj={"id":this.state.postCheckUrl};
     let paramObject = {
       path: Constant.Api.POSTCHECKREQUEST.GetPreCheckInfo,
       showErrorMsg: true,
       params: paramObj 
     }
 
     console.log("prrrreeeeee",paramObject)
 
     PostRequest(paramObject).then((responseApisuccess) => {
     this.setState({isLoadervisible:false},()=>{ 
       let response=responseApisuccess.data; 
       if(response==null || response==""){ 
         let alertTitle= I18n.t('GCSITELOGINPAGE.NO_PRECHCKDETAIL_TITLE');
         let alertMessage= I18n.t('GCSITELOGINPAGE.NO_PRECHCKDETAIL_MSG');
         let buttonStr= I18n.t('OK_BUTTON');
         Dialog.alertDialog(alertTitle, alertMessage, buttonStr, true, null); 
         return;
       }
       if(response!=null && response!=""){
         console.log("prrrreeeeee",response)
		 this.state.postData =response.data     
		  
		 if(this.state.preStatus == 0){  
			this.setState({isAppDataAvailable: true,isAppDataAvailables: false})
			
		  }else{ 
			  if(Array.isArray(response.data))     
   this.setState({postData: [...response.data] },()=>{console.log("challaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",response.data[0].Work_Order_No)})
		  }
        
         console.log(this.state.postData)   
        //  let alertTitle= I18n.t('GCSITELOGINPAGE.NO_PRECHCKDETAIL_TITLE');     
        //  let alertMessage= I18n.t('GCSITELOGINPAGE.NO_PRECHCKDETAIL_MSG');
        //  let buttonStr= I18n.t('OK_BUTTON');
        //  Dialog.alertDialog(alertTitle, alertMessage, buttonStr, true, null); 
       }
     })  
   })
   }
 
 
   displayAlertMsg=(title,message,callback)=>{
   Dialog.alertDialog(title,message,I18n.t('OK_BUTTON'),false,null).then(()=>{
     if(callback){
       callback();
     }
   })
 } 
 
         
   postCheckRequest(){
	  let paramObj={"id":this.state.postCheckUrl,
		"crewId":this.state.cressIdds
	};
        let paramObject = {
          path: Constant.Api.POSTCHECKREQUEST.PostCheckReq,
          showErrorMsg: true,
          params: paramObj 
        }
    
        
    
        PostRequest(paramObject).then((responseApisuccess) => {
        this.setState({isLoadervisible:false},()=>{ 
          let response=responseApisuccess.data; 
          if(response==null || response==""){ 
            let alertTitle= I18n.t('GCSITELOGINPAGE.NO_PRECHCKDETAIL_TITLE');
            let alertMessage= I18n.t('GCSITELOGINPAGE.NO_PRECHCKDETAIL_MSG');
            let buttonStr= I18n.t('OK_BUTTON');
            Dialog.alertDialog(alertTitle, alertMessage, buttonStr, true, null); 
            return;
          }
          if(response!=null && response!=""){
            console.log(response)
            this.displayAlertMsg('PostCheckRequest Status',response.message);
          }
        })  
      })
   }
 
   _onMessage = (e) => {
    this.setState({
      webviewHeight: parseInt(e.nativeEvent.data)
    });
  }
   renderAppDataListItem=(item,index)=>{ 
   console.log("prrrreeeeeeeeeee", item.mailInfo )
   
    const htmlContent = item.mailInfo+"";
  let flg=true
		 if(!item.mailInfo || item.mailInfo=="" )
		 flg=false
     
     console.log("htmlll",htmlContent)
       
       
       return( 
         <View   > 
           {/* <View > 
             
             <View > */}
{/* 
                   <Text style={StylePreCheck.preText}>Site Id:        {item.site_id}</Text>
                    <Text style={StylePreCheck.preText}>Work Order No       {item.Work_Order_No}</Text>
                    <Text style={StylePreCheck.preText}>POR       {item.POR}</Text>
					<Text style={StylePreCheck.preText}>Precheck Engineer:        {item.precheck_engineer}</Text>
                    <Text style={StylePreCheck.preText}>Precheck Request Date:        {item.precheckReq}</Text>
                    <Text style={StylePreCheck.preText}>Precheck Deliver Date:        {item.precheck_deliver_date}</Text>

				    <Text style={StylePreCheck.preText}>GSM UP:        {item.gsm_up}</Text>
					<Text style={StylePreCheck.preText}>GSM Alarm:        {item.gsm_alarm}</Text>
                    <Text style={StylePreCheck.preText}>UMTS UP:        {item.umts_up}</Text>
					<Text style={StylePreCheck.preText}>UMTS Alarm:        {item.umts_alarm}</Text>
                    <Text style={StylePreCheck.preText}>LTE UP:        {item.lte_up}</Text>
					<Text style={StylePreCheck.preText}>LTE Alarm:        {item.lte_alarm}</Text>
                    <Text style={StylePreCheck.preText}>5G UP:        {item.five_g_up}</Text>
					<Text style={StylePreCheck.preText}>5G Alarm:        {item.five_g_alarm}</Text>
					<Text style={StylePreCheck.preText}>Precheck Alarm Issue:        {item.precheck_alarm_issue}</Text>
					<Text style={StylePreCheck.preText}>Mail Info:   </Text>  
					<HTMLView
        value={htmlContent}
	 
		 
      /> */}
	 <View style={{flex: 1, flexDirection:'column'}}>

	 <Text style={StylePreCheck.preText}>Site Id:        {item.site_id}</Text>
                    {/* <Text style={StylePreCheck.preText}>Work Order No       {item.Work_Order_No}</Text> */}
					<Text style={StylePreCheck.preText}>Project Code:       {item.Work_Order_No}</Text>
                    {/* <Text style={StylePreCheck.preText}>POR       {item.POR}</Text> */}
					<Text style={StylePreCheck.preText}>Technology:       {item.POR}</Text>
					<Text style={StylePreCheck.preText}>Precheck Engineer:        {item.precheck_engineer}</Text>
                    <Text style={StylePreCheck.preText}>Precheck Request Date:        {item.precheckReq}</Text>
                    <Text style={StylePreCheck.preText}>Precheck Deliver Date:        {item.precheck_deliver_date}</Text>

				    <Text style={StylePreCheck.preText}>GSM UP:        {item.gsm_up}</Text>
					<Text style={StylePreCheck.preText}>GSM Alarm:        {item.gsm_alarm}</Text>
                    <Text style={StylePreCheck.preText}>UMTS UP:        {item.umts_up}</Text>
					<Text style={StylePreCheck.preText}>UMTS Alarm:        {item.umts_alarm}</Text>
                    <Text style={StylePreCheck.preText}>LTE UP:        {item.lte_up}</Text>
					<Text style={StylePreCheck.preText}>LTE Alarm:        {item.lte_alarm}</Text>
                    <Text style={StylePreCheck.preText}>5G UP:        {item.five_g_up}</Text>
					<Text style={StylePreCheck.preText}>5G Alarm:        {item.five_g_alarm}</Text>
					<Text style={StylePreCheck.preText}>Precheck Alarm Issue:        {item.precheck_alarm_issue}</Text>
					<Text style={StylePreCheck.preText}>Mail Info:   </Text>    
				{flg && 	<TouchableOpacity style={StyleDashboard.buttonContainer} onPress={() => this.gotoWebView_Screen(htmlContent)}>
						<Text style={[StyleDashboard.buttonText]}>
						    View Mail Info >>
					
						</Text>
	</TouchableOpacity>    }
					  
					    
 

</View>  
	    

    

					{/* <WebView  
    
	  originWhitelist={['*']}  
	  source={{ html:  htmlContent }}
	  style={{  marginTop: 20,
		maxHeight: 200,
		width: 320,
		flex: 1 }}
	  javaScriptEnabled={true}
	  domStorageEnabled={true}
	  startInLoadingState={true} 
         /> */}
        
					
                    {/* <Text style={StylePreCheck.preText}>scheduleId:        {item.scheduleId}</Text> */}
                   
                   
                   

                    
                   
                    {/* <Text style={StylePreCheck.preText}>Precheck Deliver Date:        {item.precheck_deliver_date}</Text>
                    {/* <Text style={StylePreCheck.preText}>pre_check_alarms:        {item.pre_check_alarms}</Text> */}
                    
                    {/* <Text style={StylePreCheck.preText}>Planned_SoW:        {item.Planned_SoW}</Text>
                    <Text style={StylePreCheck.preText}>tmoEngineer:        {item.tmoEngineer}</Text>
                    <Text style={StylePreCheck.preText}>serviceAffecting:        {item.serviceAffecting}</Text>
                    <Text style={StylePreCheck.preText}>T_Mobile_TicketID:        {item.T_Mobile_TicketID}</Text>
                    <Text style={StylePreCheck.preText}>startTime:        {item.startTime}</Text>
                    <Text style={StylePreCheck.preText}>endTime:        {item.endTime}</Text>
                    <Text style={StylePreCheck.preText}>RF_Approval:        {item.RF_Approval}</Text>
                    <Text style={StylePreCheck.preText}>Day_MOP_Night_MOP:        {item.Day_MOP_Night_MOP}</Text>
                    <Text style={StylePreCheck.preText}>Reason_for_Day_MOP:        {item.Reason_for_Day_MOP}</Text> */}
                

   
             {/* </View> 
           </View> */}
           
           
         </View> 
       ); 
	 }
	 

	 getPlannedSOWDetails=()=>{  
		let paramObject = {
			path: Constant.Api.LCLOGIN.GETSOWDetail,
			showErrorMsg: true 
		};
	  
	   //display loader first
		this.setState({isLoadervisible:true},()=>{
			PostRequest(paramObject).then((responseApisuccess) => {
				this.setState({isLoadervisible:false},()=>{ 
					let response=responseApisuccess.data; 
					// unable to fetch SOW
					if(!response || !response.status || response.status==0 || !response.data){ 
						//if due to ary reason failed to fetch Planned Sow retry again in case of error
						if(this.apiRetryAttempt<3){
							this.apiRetryAttempt++;
							this.getPlannedSOWDetails();
							return;
						}else{
							this.displayAlertMsg(
							I18n.t('GCSITELOGINPAGE.ERROR'),
							I18n.t('GCSITELOGINPAGE.GET_SOW_ERROR'),null
							); 
							return;
						} 
					}
						
					if(response.data.length==0){ 
						this.displayAlertMsg(
							I18n.t('GCSITELOGINPAGE.NO_SOW_TITLE'),
							I18n.t('GCSITELOGINPAGE.NO_SOW_MSG'),null
						); 
						return;
					}
		
					//got accounts
					if(response.data && response.data.length>0){
						this.apiRetryAttempt=0; 
						//  let tempSOWArray= [...this.state.sowArray, ...response.data]; 
						//  this.setState({sowArray:tempSOWArray},()=>{});
		
						this.setState({sowArrayDetails:response.data});
					} 
				}); 
			}, (error) => {
				this.setState({isLoadervisible:false},()=>{});
			}); 
		});
	  } 

	 renderSOWDetailsPicker=()=>{
		let pickerObject=this.state.sowArrayDetails.map((sowObject,i) => {
			let key=i+""+new Date().toLocaleString()+"_sowPickerList"
			return( 
				<Picker.Item label={sowObject.name} value={sowObject.id}  key={key}/>        
			);
		});
		return (pickerObject);
	  } 


	  submitGcLogin= async ()=>{ 
		this.setState({isModalVisible: false})
		// console.log(" 1. Main Id",this.state.selectedMainId)
		// console.log(" 2. WON",this.state.selectedItems)
		// console.log(" 3. POR",this.state.selectedItemsPOR)
		// console.log(" 4. T Mobile",this.state.ttIDMobile)
		// console.log(" 5. Account",this.state.selectedAcount)
		// console.log(" 6. Market",this.state.selectedMarket)
		// console.log(" 7. Planned Sow",this.state.selectedPlannedSOW)
		// console.log(" 8. TOM",this.state.selectedTMO)
		// console.log(" 9. service affecting",this.state.selectedServiceAffected)
		// console.log(" 10. RF start",this.state.fromDate)
		// console.log(" 11. end",this.state.toDate)
		// console.log(" 12. RF",this.state.selectedRFApproval)
		// console.log(" 13. Day check",this.state.selectedDTO)
		// console.log(" 14. comment",this.state.dayMOPComment)
		console.log(" 15. SOW",this.state.SelectedSOWDetails)
		console.log(" 16. sow comment",this.state.SOWComment)
		console.log(" 17. nested",this.state.nestedTime)
		
		
		  let currentTime= new Date().getHours()+":"+new Date().getMinutes();
		  let params=[
			{



			
				"CX_Crew_Ld_E_mail_ID":"Vijaypal.fourbrick2@gmail.com",
				"site_id":this.state.postData[0].site_id,
				"Work_Order_No":this.state.postData[0].Work_Order_No,
				"POR": this.state.postData[0].POR,
				"T_Mobile_TicketID": this.state.postData[0].T_Mobile_TicketID,
				"Account": this.state.postData[0].accountId,
				"Market": this.state.postData[0].marketId,
				"Service_Affecting": this.state.postData[0].serviceAffecting,
				"Day_MOP_Night_MOP": this.state.postData[0].Day_MOP_Night_MOP,
				"Reason_for_Day_MOP": this.state.postData[0].Reason_for_Day_MOP,
				"RF_Approval": this.state.postData[0].RF_Approval,
				"RF_Approved_MW_Time": "2020-03-21 15:00:00",
				"RF_Approved_End_Time": "2020-03-21 05:00:00",
				"TMO_Deployment_Manager": this.state.postData[0].tmoEngineer,
				"Planned_SoW": this.state.postData[0].Planned_SoW,
				"LoginType": this.state.loginType,
				"sowDetails":this.state.SelectedSOWDetails,
				"sowComment":this.state.SOWComment,
				"nestingTime":this.state.nestedTime,
				





	
	
			
	
			
	
		  }
		  ]
		  ;
		  console.log("params",params)
	//
	// { tbl_site_id: 1, tbl_gc_id: 33, m_account_id: 1, m_market_id: 2, service_affecting: 0,
	//    dto: 1,planned_sow:6,planned_sow_details:'none',estimated_time_on_site:6 }
	
		  let paramObject = {
			path: Constant.Api.PreCheckReqNew,
			showErrorMsg: true,
			params: params 
		};
	
		// alert(Constant.Api.LCLOGIN.GCSITELOGIN)
	
		 //display loader first
		 this.setState({isLoadervisible:false},()=>{
			PostRequest(paramObject).then((gciLoginSuccess) => { 
	console.log(gciLoginSuccess)
			  // alert(JSON.stringify(gciLoginSuccess))
				this.setState({isLoadervisible:false},async()=>{ 
					 if(gciLoginSuccess.data.status==1)
					 {
					  let alertTitle= I18n.t('GCSITELOGINPAGE.GCLOGINSUCCESS');
					  let alertMessage= I18n.t('GCSITELOGINPAGE.GCLOGINSUCCESS_MSG');
					  let buttonStr= I18n.t('OK_BUTTON');
	
					  Alert.alert(
						alertTitle,
						alertMessage,
						[
						  
						  {text: buttonStr, onPress: () => {this.props.navigation.goBack();}},
						],
						{cancelable: false},
					  );  
	
					  Toast.show(gciLoginSuccess.data.message, Toast.LONG);
					  await AsyncStorage.setItem("snapDataBase64", "");
					  this.goToPreviousPage();
	
	
					 }else{
					   
					  this.displayAlertMsg(
						"GC Login",
						gciLoginSuccess.data.message,null
					); 
					 }
				});      
			}, (error) => {  
				console.log(error)
			}); 
		}); 
		
	}


	openModals=()=>{
		this.setState({isModalVisible: true})
	}


	getChecked = (value) => {
		// value = our checked value
		console.log(value)
		this.state.loginType = value;
	  }

	
	  
 
 
     render() {
      let url = Constant.PRE_POST_CHECK_URL.URL+this.state.postCheckUrl ;
  
      return(
          <View style={StylePreCheck.container}>
            <StatusBarComponent isHidden={false} />
            {DeviceInfo.isAndroid() &&
            <ToolbarComponent
              title= "PRECHECK"
              navIcon={Constant.ICONS.BACK_ICON}
              navClick={() => this.props.navigation.goBack()}
            />
          }
          {this.state.isAppDataAvailable == false && this.state.isAppDataAvailables == true &&




                <FlatList 
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.onDragRefresh}
//                                 colors={[Colors.orange]}
                                progressViewOffset={20}
                                enabled={true}
                            />
                        } 
//                         ListEmptyComponent={this.appDataList} 
                        data={this.state.postData}
                        extraData={this.state.listRefresh}
                        initialNumToRender={20}
                        keyExtractor={(item, index) => index+""+new Date().toLocaleString()}
				            	renderItem={({item, index}) => this.renderAppDataListItem(item, index)}
                    /> 


     		}


            
          
            
          {this.state.isAppDataAvailable == true && !this.state.isModalVisible &&
            <View style={StyleGCSiteLogin.formContainer}>
				<ScrollView keyboardShouldPersistTaps='always'> 
					<View style={StyleGCSiteLogin.formContainerInner}> 
					{/* MainId */}
						<View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
							<Text style={StyleGCSiteLogin.inputTitle}>
							{I18n.t('GCSITELOGINPAGE.MainId')}
							</Text>
							<View style={StyleGCSiteLogin.dropdownContainer}>
							<Picker 
								selectedValue={this.state.postData[0].site_id}
								style={StyleGCSiteLogin.dropdown}
								enabled = {false}
								
								>
								<Picker.Item label={this.state.postData[0].site_id} value={this.state.postData[0].site_id} />
								{/* {this.renderMainIdPicker()} */}
							</Picker>
							</View>
						</View>
						
						 <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
							<Text style={StyleGCSiteLogin.inputTitle}>
							{/* Work Order Number */}
							Project Code
							</Text>
							<View style={StyleGCSiteLogin.dropdownContainer}>
							<Picker 
								selectedValue={this.state.postData[0].Work_Order_No}
								style={StyleGCSiteLogin.dropdown}
								enabled = {false}
								
								>
								<Picker.Item label={this.state.postData[0].Work_Order_No} value={this.state.postData[0].Work_Order_No} />
								
							</Picker>
							</View>
						</View>
						
						<View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
							<Text style={StyleGCSiteLogin.inputTitle}>
							{/* POR */}
							Technology
							</Text>
							<View style={StyleGCSiteLogin.dropdownContainer}>
							<Picker 
								selectedValue={this.state.postData[0].POR}
								style={StyleGCSiteLogin.dropdown}
								enabled = {false}
								
								>
								<Picker.Item label={this.state.postData[0].POR} value={this.state.postData[0].POR} />
								
							</Picker>
							</View>
						</View>
						
						
						<View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
							<View style={StyleGCSiteLogin.gridColumn}>
								<Text style={StyleGCSiteLogin.inputTitle}>
								T-Mobile TTID
								</Text>
								<View style={StyleGCSiteLogin.dropdownContainer}> 
									<TextInput style={StyleGCSiteLogin.inputField}  
									keyboardType='default'
									returnKeyType="next"
									enabled= {false}
									maxLength = {100}
									value={this.state.postData[0].T_Mobile_TicketID} 
									// onChangeText={(prework) => this.setState({preWorkID:prework})}
									/> 
								</View>
							</View>
							{/* <View style={StyleGCSiteLogin.gridColumn}>
							<Text style={StyleGCSiteLogin.inputTitle}>
							{I18n.t('GCSITELOGINPAGE.ACCOUNT')}
							</Text>
							<Text></Text>
							<View style={StyleGCSiteLogin.dropdownContainer}>
							<Picker 
								selectedValue={this.state.postData[0].workorderNo}
								style={StyleGCSiteLogin.dropdown}
								enabled = {false}
								
								>
								<Picker.Item label={this.state.postData.workorderNo} value={this.state.postData.workorderNo} />
							
							</Picker>
							</View>
							</View> */}
						</View>
						 
						 












						<View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
							<View style={StyleGCSiteLogin.gridColumn}>
								<Text style={StyleGCSiteLogin.inputTitle}>
								{I18n.t('GCSITELOGINPAGE.SERVICE_AFFECTING')}
								</Text>
								<View style={StyleGCSiteLogin.dropdownContainer}> 
								<Picker
									enabled = {false} 
									selectedValue={this.state.postData[0].serviceAffecting}
									style={StyleGCSiteLogin.dropdown}
									>
									<Picker.Item label="Select Service Affect" value="-1" />
									<Picker.Item label="Yes" value="1" />
									<Picker.Item label="No" value="0" />
									<Picker.Item label="No" value="2" />
								</Picker>
							</View>
						</View>
                {/* <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {I18n.t('GCSITELOGINPAGE.DTO')}
                    </Text>
                    <View style={StyleGCSiteLogin.dropdownContainer}> 
                      <Picker
                          enabled = {true} 
                          selectedValue={this.state.selectedDTO}
                          style={StyleGCSiteLogin.dropdown}
                          onValueChange={(selectedDTO, itemIndex) =>
                          this.setState({selectedDTO: selectedDTO})
                        }>
                        <Picker.Item label="Yes" value="1" />
                        <Picker.Item label="No" value="0" />
                      </Picker>
                    </View>
                </View> */}
              </View>




						<View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
							<View style={StyleGCSiteLogin.gridColumn}>
								<Text style={StyleGCSiteLogin.inputTitle}>
								{/* {I18n.t('GCSITELOGINPAGE.SERVICE_AFFECTING')} */}
								RF Approved MW (Start Time)
								</Text>
								
							</View>
							<View style={StyleGCSiteLogin.gridColumn}>
							
								<View style={StyleDashboard.dateConainerParent}>
									<View style={[StyleDashboard.dateConainerChild,StyleDashboard.dateMarginLeft]}>
										
										{this.renderDateComponent("fromDate")}
									</View>
								
								</View>
							</View>
						</View>
						



						<View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
							<View style={StyleGCSiteLogin.gridColumn}>
								<Text style={StyleGCSiteLogin.inputTitle}>
								{/* {I18n.t('GCSITELOGINPAGE.SERVICE_AFFECTING')} */}
								RF Approved MW (end Time)
								</Text>
								
							</View>
							<View style={StyleGCSiteLogin.gridColumn}>
							
								<View style={StyleDashboard.dateConainerParent}>
									<View style={[StyleDashboard.dateConainerChild,StyleDashboard.dateMarginLeft]}>
										
										{this.renderDateComponent("toDate")}
									</View>
								
								</View>
							</View>
						</View>
						


						<View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
							<View style={StyleGCSiteLogin.gridColumn}>
								<Text style={StyleGCSiteLogin.inputTitle}>
								{/* {I18n.t('GCSITELOGINPAGE.SERVICE_AFFECTING')} */}
								RF Approved 
								</Text>
								<View style={StyleGCSiteLogin.dropdownContainer}> 
								<Picker
									enabled = {false} 
									selectedValue={this.state.postData[0].RF_Approval}
									style={StyleGCSiteLogin.dropdown}
									>
									<Picker.Item label="Select RF Approval" value="-1" />
									<Picker.Item label="Yes" value="1" />
									<Picker.Item label="No" value="0" />
								</Picker>
							</View>
							</View>
							<View style={StyleGCSiteLogin.gridColumn}>
								<Text style={StyleGCSiteLogin.inputTitle}>
								{/* {I18n.t('GCSITELOGINPAGE.DTO')} */}
								Day MOP or Night Mop
								</Text>
								<View style={StyleGCSiteLogin.dropdownContainer}> 
								<Picker
									enabled = {true} 
									selectedValue={this.state.postData[0].Day_MOP_Night_MOP}
									style={StyleGCSiteLogin.dropdown}
									onValueChange={(selectedDTO, itemIndex) =>
										this.setState({selectedDTO: selectedDTO},()=>{
										if(selectedDTO == 1){
											this.setState({isDayMOP: true})
										}else{
											this.setState({isDayMOP: false})
										}
										})
									// this.dayMOPVALUEChange(selectedDTO)
									}>
									<Picker.Item label="Select MOP" value="-1" />
									<Picker.Item label="Day MOP" value="1" />
									<Picker.Item label="Night MOP" value="2" />
								</Picker>
								</View>
							</View>
						</View>

						{this.state.isDayMOP &&
							<View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
							<Text style={StyleGCSiteLogin.inputTitle}>
								Day MOP
							</Text>
							<View style={StyleGCSiteLogin.dropdownContainer}>
							<TextInput style={StyleGCSiteLogin.inputField}  
								keyboardType='default'
								returnKeyType="next"
								maxLength = {100}
								value={this.state.postData[0].Reason_for_Day_MOP} 
								enabled={false}
								/>
							</View>


							
						</View>
						}
















						<View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
							<Text style={StyleGCSiteLogin.inputTitle}>
								{I18n.t('GCSITELOGINPAGE.SOW_DETAILS')}
							</Text>
							<View style={StyleGCSiteLogin.dropdownContainer}>
							<Picker
								enabled = {true} 
								selectedValue={this.state.SelectedSOWDetails}
								style={StyleGCSiteLogin.dropdown}
								onValueChange={(selectedSowDetails, itemIndex) =>
								this.setState({SelectedSOWDetails: selectedSowDetails})
								}>  
								{this.renderSOWDetailsPicker()}
							</Picker>
						</View>


						{/* <TextInput style={StyleGCSiteLogin.inputField}  
						keyboardType='default'
						returnKeyType="next" 
						maxLength = {200}
						onChangeText={(sowDetail) => this.setState({SowDetail:sowDetail})}
						value={this.state.SowDetail}  /> */}
             		 </View>
						<View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
							<View style={StyleGCSiteLogin.gridColumn}>
								<Text style={StyleGCSiteLogin.inputTitle}>
								{/* {I18n.t('GCSITELOGINPAGE.PRE_WORK_ID')} */}
								SOW Comment
								</Text>
								<View style={StyleGCSiteLogin.dropdownContainer}> 
									<TextInput style={StyleGCSiteLogin.inputField}  
									keyboardType='default'
									returnKeyType="next"
									maxLength = {100}
									value={this.state.SOWComment} 
									onChangeText={(prework) => this.setState({SOWComment:prework})}
									/> 
								</View>
							</View>
							<View style={StyleGCSiteLogin.gridColumn}>
							<Text style={StyleGCSiteLogin.inputTitle}>
							Nesting Time
							</Text>
							<View style={StyleGCSiteLogin.dropdownContainer}> 
									<TextInput style={StyleGCSiteLogin.inputField}  
									autoCapitalize="none"
									keyboardType='numeric'
									returnKeyType="next" 
									maxLength = {2}
									value={this.state.nestedTime} 
									onChangeText={(prework) => this.setState({nestedTime:prework})}
									/> 
								</View>
							</View>
						</View>
              
						
						<View style={StyleGCSiteLogin.bottomContainer}>   
						<TouchableOpacity style={StyleGCSiteLogin.buttonContainer} onPress={() => this.openModals()}>
							<Text style={StyleGCSiteLogin.buttonText}>
								{I18n.t('GCSITELOGINPAGE.LOGIN')}
							</Text>
						</TouchableOpacity>
					</View> 
				</View>
				</ScrollView>
			</View>


		  }
		{this.state.isModalVisible && 
      		<View style={StyleGCSiteLogin.formContainer}>
      <ScrollView keyboardShouldPersistTaps='always'> 
        <View style={StyleGCSiteLogin.formContainerInner}> 
		
            <Text>Are you!</Text>
			<RadioGroup getChecked={this.getChecked}>
				<Radio iconName={"lens"} label={"CX"} value={"1"}/>
				<Radio iconName={"lens"} label={"IX"} value={"2"}/>
				<Radio iconName={"lens"} label={"Both"} value={"3"}/>
			</RadioGroup>
           
           
      <View style={StyleGCSiteLogin.bottomContainer}>   
                  <TouchableOpacity style={StyleGCSiteLogin.buttonContainer} onPress={() => this.submitGcLogin()}>
                      <Text style={StyleGCSiteLogin.buttonText}>
                          {I18n.t('GCSITELOGINPAGE.LOGIN')}
                      </Text>
                  </TouchableOpacity>
              </View>
           
           
            {/* <Button title="Submit" onPress={() => this.()}/> */}
          
        
        </View> 
        </ScrollView>
        </View>
	  
	  	}


		

            
          
      
                
      
      </View>     
      );
  
    
    }
 }
