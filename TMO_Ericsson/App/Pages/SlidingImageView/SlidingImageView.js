import React, { PureComponent } from 'react';
import {Dimensions,BackHandler,Image, View, TouchableOpacity,Text} from 'react-native';
import { StyleDashboard } from '../../Styles/PageStyle';
const width = Dimensions.get('window').width;
import ToolbarComponent from "../../Components/Toolbar";
import { CommonMethods,DeviceInfo,Dialog,GlobalVariable } from '../../Utils/Utils'
import StatusBarComponent from '../../Components/StatusBar';
import OfflineBarComponent from '../../Components/OfflineBar';
import Constant from '../../Utils/Constant';
import { FlatList } from 'react-native-gesture-handler';

export default class SlidingImageView extends PureComponent {
  
	constructor(props) {
        super(props); 

        this.callFrom = '';
        this.imageUrl =[];
 
		this.state = {
            imgUrlArray: [],
            toolbarTitle:'Photo',
            slides: [],
            currentIndex:1,
            toBeSharedImages:[]
        } 

        const { navigation } = this.props;
        if(navigation.getParam('imageUrl')!='' && navigation.getParam('imageUrl')!=null)
        {
            this.imageUrl = navigation.getParam('imageUrl');
            // for (let i = 0; i < imageUrl.length; i++) {
            //     let imageRawData = imageUrl[i].split(",");
            //     this.imageUrl = [];
            //     for (let index = 0; index < imageRawData.length; index = index + 2) {
            //         this.imageUrl.push(imageRawData[index] + "," + imageRawData[index + 1])
            //     }
            // }
            
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }
  
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    componentDidMount()
    {
        this.setState({imgUrlArray:this.imageUrl},
            ()=> {});
    }

    handleBackButtonClick=()=> {
        if(this.callFrom=='ModalComponent')
        {
            this.callFrom = '';
            const { navigation } = this.props;
            navigation.goBack();
            navigation.state.params.onBackCallback();
        }
        else
        {
            this.callFrom = '';
            this.props.navigation.goBack(null);
        }
      return true;
  }


	render() { 

		return (
			<View style={StyleDashboard.container}>
               <StatusBarComponent isHidden={false} />
                <OfflineBarComponent />
                { DeviceInfo.isAndroid() && 
                    <ToolbarComponent 
                        title= "Images"
                        navIcon={Constant.ICONS.BACK_ICON}
                        navClick={() => this.props.navigation.goBack(null)}
                    /> 
                }
                
                <FlatList 
                    contentContainerStyle = {{paddingBottom: 75}}
                    data={this.state.imgUrlArray}
                    extraData={this.state}
                    numColumns = {2}
                    keyExtractor={(item, index) => index+""+new Date().toLocaleString()}
                    renderItem={({item, index}) => this.renderItem(item, index)}/>
	        </View>
		);
    }

    renderItem=(item,index)=>{
        return(
            <View style = {{width: width / 2, height: width / 2, padding: 5}}>
                <Image style={{width: "100%", height: "100%"}} resizeMode={'center'} source={item} />
            </View>
        )
    }
}