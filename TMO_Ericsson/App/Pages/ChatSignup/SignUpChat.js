import React, { PureComponent }  from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import firebase from 'react-native-firebase';

export default class ChatRegForm extends PureComponent{
    state = {
        email: '',
        password: ''
    }
    signupuser= ()=>{
        const { email, password } = this.state;
        firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(()=> (this.props.navigation.navigate("HomeScreenWindow")))
        .catch(error => console.log(console.error(error)));
    }
    render(){
        return(
            <View style={styles.regform}>
                <Text style={styles.header}>Registration form</Text>
                <TextInput style={styles.textinput} placeholder="Email" underlineColorAndroid={'trasparent'} onChangeText={email => this.setState({email})}></TextInput>
                <TextInput style={styles.textinput} placeholder="Password" underlineColorAndroid={'trasparent'} onChangeText={password => this.setState({password})}></TextInput>
                <TouchableOpacity style={styles.button} onPress={this.signupuser}>
                    <Text style={styles.btntext}>Sign Up</Text>
                </TouchableOpacity>
                <Text onPress={() => this.props.navigation.navigate("ChatLoginForm")}>Move to login</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    regform:{
        flex:1,
        justifyContent: 'center',
        paddingLeft: 60,
        paddingRight: 60,
    },
    header:{
        fontSize: 24,
        borderBottomWidth: 1,
        paddingBottom: 40,
        marginBottom:30,
        borderBottomColor: '#199187',
        borderBottomWidth:1,
    },
    textinput:{
        alignSelf:'stretch',
        height:40,
        marginBottom: 30,
        color: '#000000',
        borderBottomColor: '#f8f8f8',
        borderBottomWidth: 1,
    },
    button:{
        alignSelf: 'stretch',
        alignItems: 'center',
        padding: 20,
        backgroundColor: "#0099bd",
        marginTop: 30,
        marginBottom: 30,
    },
    btntext:{
        color: '#ffff',
        fontWeight:'bold',
    },
    tologinpage:{
        marginTop: 30,
        color: '#0000',
        fontWeight: 'bold',
    }
});