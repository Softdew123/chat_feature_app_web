import React, { PureComponent }  from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import firebase from 'react-native-firebase';

export default class ChatLoginForm extends PureComponent{
    state = {
        email: '',
        password: ''
    }
    signinuser= ()=>{
        const { email, password } = this.state;
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(()=> (this.props.navigation.navigate("HomeScreenWindow")))
        .catch(error => console.log(console.error(error)));
    }
    render(){
        return(
            <View style={styles.regform}>
                <Text style={styles.header}>Login here</Text>
                <TextInput style={styles.textinput} placeholder="Username" underlineColorAndroid={'trasparent'} onChangeText={email => this.setState({email})}></TextInput>
                <TextInput style={styles.textinput} placeholder="Password" underlineColorAndroid={'trasparent'} onChangeText={password => this.setState({password})}></TextInput>
                <TouchableOpacity style={styles.button} onPress={this.signinuser}>
                    <Text style={styles.btntext}>Sign In</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    regform:{
        flex:1,
        justifyContent: 'center',
        paddingLeft: 60,
        paddingRight: 60,
    },
    header:{
        fontSize: 24,
        borderBottomWidth: 1,
        paddingBottom: 40,
        marginBottom:30,
        borderBottomColor: '#199187',
        borderBottomWidth:1,
    },
    textinput:{
        alignSelf:'stretch',
        height:40,
        marginBottom: 30,
        color: '#000000',
        borderBottomColor: '#f8f8f8',
        borderBottomWidth: 1,
    },
    button:{
        alignSelf: 'stretch',
        alignItems: 'center',
        padding: 20,
        backgroundColor: "#0099bd",
        marginTop: 30,
    },
    btntext:{
        color: '#ffff',
        fontWeight:'bold',
    }
});
