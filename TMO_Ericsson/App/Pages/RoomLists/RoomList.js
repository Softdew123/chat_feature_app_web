import React, { PureComponent }  from 'react';
import { StyleSheet, Text, View, TouchableOpacity, FlatList } from 'react-native';
import firebase from 'react-native-firebase';

export default class AllRooms extends PureComponent{
    state = {
        threads: []
    }

    chatroom = (item) =>{
      this.props.navigation.navigate("ChatWindow",{item});
    }

  /**
   * Fetch threads from Firestore
   */

   componentDidMount = () => {
    const subscriber = firebase.firestore()
    .collection("THREADS")
    .orderBy("latestMessage.createdAt", "desc")
    .onSnapshot(querySnapshot => {
        const threads = querySnapshot.docs.map(documentSnapshot => {
          console.log("docsnapshot id", documentSnapshot.id);
            return{
              _id: documentSnapshot.id,
              name: '',
              latestMessage: {
                text:''
              },
              ...documentSnapshot.data()
            };
        });
        this.setState({threads})
    });
    return () => subscriber();
  }

  renderItem = (item) => {
    return(
      <View>
          <TouchableOpacity style={{paddingTop: 10, paddingBottom:10, paddingLeft: 10, paddingRight: 10, marginTop: 1, backgroundColor: "#f5f5f5"}} 
          onPress={() => this.chatroom(item)}>
            <Text style={{color: 'blue', fontSize: 20}}>({item.name})</Text>
            <Text style={{color: 'black', fontSize: 10}}>({item.latestMessage.text})</Text>
          </TouchableOpacity>
      </View>
      // onPress={this.chatroom(item)}
    )
  }

  render(){
    return(
      <View style={styles.listparent}>
        <FlatList
          data={this.state.threads}
          renderItem={({item}) => this.renderItem(item)}
          keyExtractor={(item) => item._id+""+new Date().toLocaleString()}
        />
      </View>
    );
  }
}


const styles = StyleSheet.create({
  listparent:{
    flex:1,
    justifyContent: 'center',
    marginTop: 30,
  }
});
