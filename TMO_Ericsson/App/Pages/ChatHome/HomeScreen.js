import React, { PureComponent }  from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import firebase from 'react-native-firebase';

export default class HomeScreenWindow extends PureComponent{

    logoutUser = () =>{
        firebase.auth().signOut();
        this.props.navigation.navigate("ChatRegForm");
    }

    addRoom = () =>{
        this.props.navigation.navigate("AddnewRoom");
    }

    allRooms = () =>{
        this.props.navigation.navigate("AllRooms");
    }

    render(){
        return(
            <View style={{ flex: 1 , justifyContent: 'center'}}>
                <Text style={{color: 'blue', fontSize: 10}}>Hello {firebase.auth().currentUser.uid}</Text>
                <TouchableOpacity style={styles.button} onPress={this.logoutUser}>
                    <Text style={styles.btntext}>Logout</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={this.addRoom}>
                    <Text style={styles.btntext}>Add Room</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={this.allRooms}>
                    <Text style={styles.btntext}>See All Room</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    button:{
        alignSelf: 'stretch',
        alignItems: 'center',
        padding: 20,
        backgroundColor: "#0099bd",
        marginTop: 30,
        marginBottom: 30,
        marginStart: 50,
        marginEnd: 50,
    },
    btntext:{
        color: '#ffff',
        fontWeight:'bold',
    }
});