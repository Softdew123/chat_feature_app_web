import React, { PureComponent,Component } from 'react';
import ToolbarComponent from "../../Components/Toolbar";
import I18n from '../../Utils/i18n';
import { CommonMethods, DeviceInfo, Dialog } from '../../Utils/Utils'
import {Platform,  WebView,Image,Button,Dimensions, FlatList, RefreshControl, Text, TouchableOpacity, View, KeyboardAvoidingView,Modal, TextInput, Picker, ScrollView, TouchableWithoutFeedback } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';   
import { CopeStyle  } from '../../Styles/PageStyle';
import axios from 'axios'; 
 import AddImg from './../../Images/Cope/addImg.png'           
 import AddNew from './../../Images/Cope/addNew.png'
 import Removeicon from './../../Images/Cope/delete.png'   
import StatusBarComponent from '../../Components/StatusBar';        
import Loader from '../../Components/Loader';
import Constant from '../../Utils/Constant';              
                  
import {Colors} from '../../Styles/Themes'; 	
import AsyncStorage from '@react-native-community/async-storage';	
import GetLocation from 'react-native-get-location'	
import { PermissionsAndroid } from 'react-native';	

import Icon from 'react-native-vector-icons/Entypo';

import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';	
import ImageResizer from 'react-native-image-resizer';	
const RNFS = require("react-native-fs");	


const width = Dimensions.get('window').width;
const height =  Dimensions.get('window').height ;
 
export default class GCSiteLoingScreen extends PureComponent {        
            

  constructor(props) {
  super(props);
  this.requestForLoc()
	this.currentDate=this.getCurrentDate();     
    this.state = {
          isLoading: false,
           value:"",
           dd:[],
           resourcePath: {}, 
           data:[{value:"",path:[AddImg], source:[]}],
           item:{},
           sdata:[],	
           crewId:"",	
           lat: "",	
           long: "",	
        	
          selectFromModal: false
    };
  }      


  async requestForLoc(i,fn){
    const granted = await PermissionsAndroid.check( PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION );
    if (granted) {
      GetLocation.getCurrentPosition({
        enableHighAccuracy: true,
        timeout: 15000,
    })
    .then(location => {
        console.log("location raj",location);
        //latitude
        //longitude
      this.setState({lat:location.latitude,long:location.longitude})
     if(i!=undefined){
       fn()
      let data=[...this.state.data]
      let itm= data[i]
      itm.loc={lat:location.latitude,long:location.longitude}
      this.setState({data:data})
     }
       
        
    })
    .catch(error => {
        const { code, message } = error;

          
        //  console.warn("location raj", message);
        if(i==undefined){
          GetLocation.openGpsSettings();
        }else{
          alert(message)
        }
       
    })
    } 
    else {
      this.requestLocationPermission();
    }
  }
  
 async requestLocationPermission() 
  {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': 'TMOEricsson  App',
          'message': 'TMOEricsson App access to your location '
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
         
      } else {
       

      }
    } catch (err) {
      console.warn(err)
    }
  }

 
  componentDidMount(){
    this.getDropdown()
       
    let params= this.props.navigation.state.params;
    this.setState({item:params.item,crewId:params.crewId}, async ()=>{
      this.getData()
      let key =this.state.item.site_id+2
     
       try{
      let data= await  AsyncStorage.getItem(key);
      console.log("dataaatttt",data)
      data= JSON.parse(data)
      if(Array.isArray(data)){
        this.setState({data:data})

      }
      }      
      catch(err){  

      }
     }
      
      )
     
  }      
  componentWillUnmount(){
    let key =this.state.item.site_id+2
    let data =JSON.stringify(this.state.data)
    
    AsyncStorage.setItem(key,data)
  }






  setSelectFromModalVisibility(visibility)	
  {	
      this.setState({selectFromModal: visibility});  	
  }	

  handleOnNavigateBack = async (refreshPage) => {	
    let returnFrom  = await AsyncStorage.getItem("okCamera");	
    if(returnFrom !== "ok")	
      return;	
      
    let picturePath = await AsyncStorage.getItem("picturePath");
    await AsyncStorage.removeItem("picturePath");	
    this.resize1(picturePath);	
  }	

    openCamera= async()=>{	
      this.setSelectFromModalVisibility(false);	
      // AsyncStorage.setItem("fromCOP", 1);	
      this.props.navigation.navigate('MultipleCapture',{onNavigateBack: this.handleOnNavigateBack});	
    }

    async openGallery()	
    {	
        this.setSelectFromModalVisibility(false);	
        try	
        {		
            ImagePicker.openPicker({
              multiple: true
            }).then(images => {
              console.log(images);
              this.resize2(images)	
            });
        }	
        catch(error1)	
        {	
          console.log(error1);;	
        }	
    }

  //Compress image	
  async resize1(res) {	
    let capturedImage = JSON.parse(res);
    let imageArray  = [];
    let sourceArray = [];
    for (let index = 0; index < capturedImage.length; index++) {
      await ImageResizer.createResizedImage(capturedImage[index], 800, 600, 'JPEG', 50)	
          .then(async response => {	
              await RNFS.readFile(response.uri, "base64").then(async(base64data) => {	
                // let source = null;	
                let splitName = response.name.split(".");	
                let lastIndex = splitName.length - 1;	
                let type = "image/jpeg";	
                if(splitName[lastIndex].toLowerCase() == "png")	
                {	
                  type = "image/png";	
                }
                sourceArray.push({fileName: response.name, type: type, uri:response.uri});	
                let data=[...this.state.data];	
                let itm= data[this.selectedRowIndex];	
                imageArray.push({ uri: 'data:image/jpeg;base64,' + base64data });	
                	
                if(index == capturedImage.length - 1)
                {
                  itm.path   = imageArray;
                  itm.source = sourceArray;
                  this.setState({data: data});
                } 	
          })	
          .catch(err => {	
              console.log(err);	
          });	
      });	
    }
  }		

  async resize2(res) {	
    // let imageArray = [];
    // for (let index = 0; index < res.length; index++) {
    //   ImageResizer.createResizedImage(res[index].path, 800, 600, 'JPEG', 50)	
    //   .then(async response => {	
    //       await RNFS.readFile(response.uri, "base64").then(async(base64data) => {	
    //         let source = {fileName: response.name, type: res.type, uri:response.uri};	
    //         let data=[...this.state.data];	
    //         let itm= data[this.selectedRowIndex];	
    //         imageArray.push({ uri: 'data:image/jpeg;base64,' + base64data });	
    //         itm.source=source;	
    //         if(index == res.length - 1)
    //         {
    //           itm.path = imageArray;
    //           this.setState({data: data});
    //         }
    //       })	
    //       .catch(err => {	
    //           console.log(err);	
    //       });	
    //   });	
    // }
    let capturedImage = [];
    for(let i = 0; i < res.length; i++)
    {
      await capturedImage.push(res[i].path);
    }
    let imageArray = [];
    let sourceArray = [];
    for (let index = 0; index < capturedImage.length; index++) {
      await ImageResizer.createResizedImage(capturedImage[index], 800, 600, 'JPEG', 50)	
          .then(async response => {	
              await RNFS.readFile(response.uri, "base64").then(async(base64data) => {	
                let splitName = response.name.split(".");	
                let lastIndex = splitName.length - 1;	
                let type = "image/jpeg";	
                if(splitName[lastIndex].toLowerCase() == "png")	
                {	
                  type = "image/png";	
                }
                sourceArray.push({fileName: response.name, type: type, uri:response.uri});
                let data=[...this.state.data];	
                let itm= data[this.selectedRowIndex];	
                imageArray.push({ uri: 'data:image/jpeg;base64,' + base64data });	
                if(index == capturedImage.length - 1)
                {
                  itm.path   = imageArray;
                  itm.source = sourceArray;
                  this.setState({data: data});
                } 	
          })	
          .catch(err => {	
              console.log(err);	
          });	
      });	
    }
    
  }










  onRemove=(i)=>{
    console.log("oioioipp",i)
       let data=[...this.state.data];
        data.splice(i,1)
       this.setState({data:data})
  }
  getData= () => {


    let jsn={
      "copType":2 ,
      "schId":this.state.item.Id,
      "siteId":this.state.item.site_id ,
      "crewId":this.state.crewId        
    }  
    
    this.setState({isLoading :false})
      axios( {
        method: "POST",
        url:Constant.Api.ServerDomain+Constant.Api.COPE.PREPOSTCOP,         
        data:  jsn 
      
      })        
       
      .then(response => {
        console.log("upload succes", response);   
        if(response.data && Array.isArray(response.data.data))
        {
          let data= response.data.data 
          console.log("Vijay"+ JSON.stringify(data))
          let flagArray = [];
          for (let index = 0; index < data.length; index++) {
              flagArray.push(data[index].copId);
          }
          arr = flagArray.filter( ( item, index, inputArray )=> { return inputArray.indexOf(item) == index; });
        
          let temp = [];
          for(let i = 0; i < arr.length; i++){
            let tempObj = {value: Number(arr[i]), path:[], source:[]}
            for (let index = 0; index < data.length; index++) {
              if(arr[i] == data[index].copId)
              {
                tempObj.path.push({uri: Constant.Api.COPE.IMAGEBASE + data[index].filePath})
              }
            }
            temp.push(tempObj);
            
         }
         this.setState({sdata:temp})
        } 
        this.setState({isLoading :false});           
      })
        .catch(error => {
        console.log("submit_img", error); 
        console.log("upload error", error);  
        this.setState({isLoading :false})
      
        });
       
        console.log("imagessssss","end"); 
      };   


  getDropdown = () => {


    let jsn={
      "copType":2
  }  
    
                 
    
    console.log("submit_img",Constant.Api.ServerDomain+Constant.Api.COPE.COPLIST) ; 
      
         this.setState({isLoading :false})
      axios( {
        method: "POST",
        url:Constant.Api.ServerDomain+Constant.Api.COPE.COPLIST,         
        data:  jsn 
      
      })
       
        .then(response => {
        console.log("submit_img", response) ; 
        console.log("upload succes", response);   
        if(response.data && Array.isArray(response.data.data)){
          this.setState({dd:response.data.data})

        } 
        this.setState({isLoading :false,})
      
        })
        .catch(error => {
        console.log("submit_img", error); 
        console.log("upload error", error);  
        this.setState({isLoading :false})
      
        });
       
        console.log("imagessssss","end"); 
      };  
      
      


  createFormData = async () => {
    const data = new FormData();
		console.log("imagessssss", data )
    data.append("latitude",this.state.lat)
    data.append("longitude",this.state.long)
    data.append("copType","2")
    data.append("deviceType","1")
    data.append("schId",this.state.item.Id)
    data.append("siteId",this.state.item.site_id)
    data.append("crewId",this.state.crewId)
		console.log("imagessssss", data )
	  // this.state.data.forEach(async(result,i)=>{
      var insertImg=0;   
      for(let i = 0; i < this.state.data.length; i++){
        let result = this.state.data[i];
        
      for (let index = 0; index < result.path.length; index++) {
        data.append("ddl_"+insertImg ,result.value);
        console.log(index+" Vijay Kumar "+JSON.stringify(result.source));
        let photo=result.source;
        let base64data = result.path[index].uri.replace("data:image/jpeg;base64,", "")
        await  data.append("file_"+insertImg, {
            name: photo[index].fileName,
            type: photo[index].type,
            data: base64data, 
            uri:
              Platform.OS === "android" ? photo[index].uri : photo[index].uri.replace("file://", "")
            });
            insertImg++;

            if((i == this.state.data.length - 1) && (index == result.path.length - 1))
            {
              return data;
            }
      }
      insertImg++;
		}
    // )
    
		
  };





    
    handleUploadPhoto = async () => {
      let data = await this.createFormData();
      this.setState({isLoading :true})
      axios( {
        method: "POST",
        url:Constant.Api.ServerDomain+Constant.Api.COPE.SUBMITCOP,         
        data:  data 
      
      })
  
        .then(response => {
        console.log("submit_img", response) ; 
        console.log("upload succes", response);
        this.setState({isLoading :false})
        this.setState({ photo: null });
        if(response.data && response.data.status=="1"){
          let key =this.state.item.site_id+2
     
      
            AsyncStorage.removeItem(key)




          this.setState({
            value:"",
            
            resourcePath: {}, 
            data:[{value:"",path:[AddImg], source:[]}],
             
            sdata:[] 
           },()=>{this.getData()}) 


          alert("Data Uploaded!")
        }else{
          alert("Failed!")
        }
       
       
        })
        .catch(error => {
         
        console.log("submit_img", error); 
        console.log("upload error", error);  
        this.setState({isLoading :false})
        alert("Failed!") 
        });
       
        console.log("imagessssss","end"); 
      };   


       
    
  getCurrentDate=()=>{
	var now = new Date(); 
	let currentDateTime = new Date()
	let month = currentDateTime.getMonth() + 1
	if (month < 10) {
		month = '0' + month; 
	}
	
	let day = currentDateTime.getDate()
	if (day < 10) {
		day = '0' + day; 
	}
	
	let year = currentDateTime.getFullYear()
	let fullDate=month + "/" + day + "/" + year;
	return fullDate;
}
 
 
   displayAlertMsg=(title,message,callback)=>{
   Dialog.alertDialog(title,message,I18n.t('OK_BUTTON'),false,null).then(()=>{
     if(callback){
       callback();
     }
   })
 } 
 
   onValueChange=(value,i)=>{
    let data=[...this.state.data]
     let itm=  data[i]
 itm.value=value;
   this.setState({data: data})
   } 
    
   onAdd=()=>{
    let itm= [...this.state.data]
    itm.push({value:"",path:[AddImg]})
    console.log("data..... 1",itm )
     this.setState({data:itm},()=>{
      console.log("data..... 2",this.state.data)
     })
   }
                     
   submitImages=()=>{
      console.log("submit_img",this.state.data)
      console.log("submit_img",this.state.item)
        let flag=true      
         this.state.data.forEach((itm,i)=>{
           if((!itm.value || itm.value=="")|| (!itm.source || (itm.source==[]))){
             flag=false
           }
         })
          if(flag){
            this.handleUploadPhoto()
          }else{alert("Please fill all fields!")}

   }
     render() {
     
           
  


   let data=this.state.data;
   
  console.log("data.....",data )
      return(
          <View style={CopeStyle .container}>
          
            <StatusBarComponent isHidden={false} />
            {DeviceInfo.isAndroid() &&               
            <ToolbarComponent   
              title= "EOS Post"
              navIcon={Constant.ICONS.BACK_ICON}         
              navClick={() => this.props.navigation.goBack()}
            />

          }
          <ScrollView >    
            
          {this.state.sdata.map((itm,i)=>{
               return(
   
                <View  style={CopeStyle.hv} >     
                <Picker
                 enabled={false}   
             selectedValue={itm.value}
              style={CopeStyle.input} 
             onValueChange={(itemValue, itemIndex) => this.onValueChange(itemValue,i)}
           >
              <Picker.Item label="Select..." value="" />
              {this.state.dd.map((itm,i)=>{
                 return(
                  <Picker.Item label={itm.name} value={itm.Id} />    

                 )
              })}

           </Picker>    
           <TouchableOpacity   >
           {/* <Image       style={CopeStyle.img_pic}  resizeMode={'cover'}     source={itm.path[0]} ></Image>  */}
           <View style = {CopeStyle.img_pic}>
                <Image style={CopeStyle.img_pic} resizeMode={'cover'} source={itm.path[0]} />
                { itm.path[0] != AddImg &&
                  <TouchableOpacity style = {{width: 40, height: 40, position: "absolute", bottom: 0, right: 0, justifyContent: "center", alignItems: "center"}}
                    onPress = {()=> this.props.navigation.navigate("SlidingImageView", {"imageUrl": itm.path})} >
                    <Icon name = {"eye"} size = {20} color = {"#000000"} />
                  </TouchableOpacity>
                }
            </View>
           </TouchableOpacity>
               
                </View>
          
          
          

               );
            })}


            {data.map((itm,i)=>{
               return(

                <View  style={CopeStyle.hv} >     
                <Picker
             selectedValue={itm.value}
              style={CopeStyle.input} 
             onValueChange={(itemValue, itemIndex) => this.onValueChange(itemValue,i)}
           >
              <Picker.Item label="Select..." value="" />
              {this.state.dd.map((itm,i)=>{
                 return(
                  <Picker.Item label={itm.name} value={itm.Id} />    

                 )
              })}

                
           </Picker>    
           <TouchableOpacity  onPress={()=>{	
             this.selectedRowIndex = i; 	
              this.setSelectFromModalVisibility(true);	
              // this.selectFile(i)	
             }} >
           {/* <Image       style={CopeStyle.img_pic}  resizeMode={'cover'}     source={itm.path} ></Image>  */}
           <View style = {CopeStyle.img_pic}>
                <Image style={CopeStyle.img_pic} resizeMode={'cover'} source={itm.path[0]} />
                { itm.path[0] != AddImg &&
                  <TouchableOpacity style = {{width: 40, height: 20, position: "absolute", bottom: 0, right: 0, justifyContent: "center", alignItems: "center"}}
                    onPress = {()=> this.props.navigation.navigate("SlidingImageView", {"imageUrl": itm.path})} >
                    <Icon name = {"eye"} size = {20} color = {"#000000"} />
                  </TouchableOpacity>
                }
            </View>
           </TouchableOpacity>
           {  (data.length!=1) &&      
           <TouchableOpacity  onPress={()=>this.onRemove(i)} >
                  <Image style={CopeStyle.img_add} source={Removeicon} ></Image>
           </TouchableOpacity>    }         
           {(data.length==(i+1)) &&  
           <TouchableOpacity  onPress={()=>this.onAdd()} >
                  <Image style={CopeStyle.img_add} source={AddNew} ></Image>
           </TouchableOpacity>    }         
                </View>
          
          
          

               );
            })}
        
	 
        <TouchableOpacity
               style = {CopeStyle.submitButton}
                onPress={()=>this.submitImages()}
                 >
               <Text style = {CopeStyle.submitButtonText}> Submit </Text>
            </TouchableOpacity>

		  
        </ScrollView>                     
            
          
        {this.state.isLoading   &&  
					<Loader msg={'Uploading...'} />
				} 






        <Modal	
            animationType="none"	
            transparent={true}	
            closeOnClick={true}	
            visible={this.state.selectFromModal}	
            onRequestClose={() => {	
            this.setSelectFromModalVisibility(false)	
            }}>	
            <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'rgba(0,0,0,0.6)'}}>	
            <View style={{width: '90%',backgroundColor: '#FFFFFF', elevation:10, borderTopLeftRadius:8,borderTopEndRadius:8, margin: 5}}>	
                <View style={{ flexDirection: 'row',height:55, backgroundColor: Colors.lightOrange,alignItems:'center'}}>	
                <Text style={{flex: 1, paddingLeft: 7, fontWeight:'bold', fontSize: 17, fontFamily: 'Roboto', color: '#FFFFFF', textAlign: 'left'}}>	
                    {I18n.t('COPPREPOST.SELECT_FROM')}	
                </Text>	
                <View style={{width:80,height:30,justifyContent: 'center',alignSelf:'center',backgroundColor:'#fff',borderRadius:15, marginRight: 10}}>	
                    <TouchableOpacity	
                    onPress={()=>this.setSelectFromModalVisibility(false)}>	
                        <Text style={{ fontWeight:'bold', fontSize: 12, fontFamily: 'Roboto', color: Colors.lightOrange, textAlign: 'center'}}>	
                            {I18n.t('COPPREPOST.DISMISS')}	
                        </Text>	
                    </TouchableOpacity>	
                </View>	
                	
                </View>	
                <View style = {{flexDirection: 'row', height:150,alignSelf: 'center', alignItems:'center', justifyContent: 'center',}}>	
                    <View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>	
                        <TouchableOpacity  style={{width:70,height:70,backgroundColor:'white',alignItems:"center",justifyContent:"center",borderRadius:60, }}	
                        onPress={ this.openCamera.bind(this)}>	
                        <Image 	
                            style={{width:60,height:60, tintColor: Colors.lightOrange}}	
                            resizeMode= "center"	
                            source={require('../../Images/Camera/camera.png')} />	
                        </TouchableOpacity>   	
                        <Text style={{alignSelf:'stretch',padding: 5, fontSize: 15, fontFamily: 'Roboto', fontWeight: 'bold', color: Colors.lightOrange,textAlign:'center'}}>{'Camera'}</Text>     	
                    </View>	
                    	
                    <View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>	
                        <TouchableOpacity  style={{width:70,height:70,backgroundColor:'white',alignItems:"center",justifyContent:"center",borderRadius:60}}	
                        onPress={this.openGallery.bind(this)}>	
                        <Image 	
                            style={{width:60,height:60, tintColor: Colors.lightOrange}}	
                            resizeMode= "center"	
                            source={require('../../Images/Camera/choose_gallery.png')} />	
                        </TouchableOpacity>   	
                        <Text style={{alignSelf:'stretch',padding: 5, fontSize: 15, fontFamily: 'Roboto', fontWeight: 'bold', color: Colors.lightOrange,textAlign:'center'}}>{'Gallery'}</Text>      	
                    </View>     	
                    </View>	
                </View>	
            </View>	
        </Modal>
        
              
      
      </View>     
      );
  
    
    }
 }




























































// import React, { PureComponent,Component } from 'react';
// import ToolbarComponent from "../../Components/Toolbar";
// import I18n from '../../Utils/i18n';
// import { CommonMethods, DeviceInfo, Dialog } from '../../Utils/Utils'
// import {Platform,  WebView,Image,Button,Dimensions, FlatList, RefreshControl, Text, TouchableOpacity, View, KeyboardAvoidingView,Modal, TextInput, Picker, ScrollView, TouchableWithoutFeedback } from 'react-native';
// import ImagePicker from 'react-native-image-crop-picker';   
// import { CopeStyle  } from '../../Styles/PageStyle';
// import axios from 'axios'; 
//  import AddImg from './../../Images/Cope/addImg.png'           
//  import AddNew from './../../Images/Cope/addNew.png'
//  import Removeicon from './../../Images/Cope/delete.png'   
// import StatusBarComponent from '../../Components/StatusBar';        
// import Loader from '../../Components/Loader';
// import Constant from '../../Utils/Constant';              
                  
// import {Colors} from '../../Styles/Themes'; 	
// import AsyncStorage from '@react-native-community/async-storage';	
// import GetLocation from 'react-native-get-location'	
// import { PermissionsAndroid } from 'react-native';	

// import Icon from 'react-native-vector-icons/Entypo';

// import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';	
// import ImageResizer from 'react-native-image-resizer';	
// const RNFS = require("react-native-fs");	


// const width = Dimensions.get('window').width;
// const height =  Dimensions.get('window').height ;
 
// export default class GCSiteLoingScreen extends PureComponent {        
            

//   constructor(props) {
//   super(props);
//   this.requestForLoc()
// 	this.currentDate=this.getCurrentDate();     
//     this.state = {
//           isLoading: false,
//            value:"",
//            dd:[],
//            resourcePath: {}, 
//            data:[{value:"",path:[AddImg], source:[]}],
//            item:{},
//            sdata:[],	
//            sdata:[],	
//            crewId:"",	
//            lat: "",	
//            long: "",	
        	
//           selectFromModal: false
//     };
//   }      


//   async requestForLoc(i,fn){
//     const granted = await PermissionsAndroid.check( PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION );
//     if (granted) {
//       GetLocation.getCurrentPosition({
//         enableHighAccuracy: true,
//         timeout: 15000,
//     })
//     .then(location => {
//         console.log("location raj",location);
//         //latitude
//         //longitude
//       this.setState({lat:location.latitude,long:location.longitude})
//      if(i!=undefined){
//        fn()
//       let data=[...this.state.data]
//       let itm= data[i]
//       itm.loc={lat:location.latitude,long:location.longitude}
//       this.setState({data:data})
//      }
       
        
//     })
//     .catch(error => {
//         const { code, message } = error;

          
//         //  console.warn("location raj", message);
//         if(i==undefined){
//           GetLocation.openGpsSettings();
//         }else{
//           alert(message)
//         }
       
//     })
//     } 
//     else {
//       this.requestLocationPermission();
//     }
//   }
  
//  async requestLocationPermission() 
//   {
//     try {
//       const granted = await PermissionsAndroid.request(
//         PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
//         {
//           'title': 'TMOEricsson  App',
//           'message': 'TMOEricsson App access to your location '
//         }
//       )
//       if (granted === PermissionsAndroid.RESULTS.GRANTED) {
         
//       } else {
       

//       }
//     } catch (err) {
//       console.warn(err)
//     }
//   }

 
//   componentDidMount(){
//     this.getDropdown()
       
//     let params= this.props.navigation.state.params;
//     this.setState({item:params.item,crewId:params.crewId}, async ()=>{
//       this.getData()
//       let key =this.state.item.site_id+2
     
//        try{
//       let data= await  AsyncStorage.getItem(key);
//       console.log("dataaatttt",data)
//       data= JSON.parse(data)
//       if(Array.isArray(data)){
//         this.setState({data:data})

//       }
//       }      
//       catch(err){  

//       }
//      }
      
//       )
     
//   }      
//   componentWillUnmount(){
//     let key =this.state.item.site_id+2
//     let data =JSON.stringify(this.state.data)
    
//     AsyncStorage.setItem(key,data)
//   }






//   setSelectFromModalVisibility(visibility)	
//   {	
//       this.setState({selectFromModal: visibility});  	
//   }	

//   handleOnNavigateBack = async (refreshPage) => {	
//     let returnFrom  = await AsyncStorage.getItem("okCamera");	
//     if(returnFrom !== "ok")	
//       return;	
      
//     let picturePath = await AsyncStorage.getItem("picturePath");
//     await AsyncStorage.removeItem("picturePath");	
//     this.resize1(picturePath);	
//   }	

//     openCamera= async()=>{	
//       this.setSelectFromModalVisibility(false);	
//       // AsyncStorage.setItem("fromCOP", 1);	
//       this.props.navigation.navigate('MultipleCapture',{onNavigateBack: this.handleOnNavigateBack});	
//     }

//     async openGallery()	
//     {	
//         this.setSelectFromModalVisibility(false);	
//         try	
//         {		
//             ImagePicker.openPicker({
//               multiple: true
//             }).then(images => {
//               console.log(images);
//               // alert(JSON.stringify(images))
//               this.resize2(images)	
//             });
//         }	
//         catch(error1)	
//         {	
//           console.log(error1);;	
//         }	
//     }

//   //Compress image	
//   async resize1(res) {	
//     let capturedImage = JSON.parse(res);
//     let imageArray  = [];
//     let sourceArray = [];
//     for (let index = 0; index < capturedImage.length; index++) {
//       await ImageResizer.createResizedImage(capturedImage[index], 800, 600, 'JPEG', 50)	
//           .then(async response => {	
//               await RNFS.readFile(response.uri, "base64").then(async(base64data) => {	
//                 // let source = null;	
//                 let splitName = response.name.split(".");	
//                 let lastIndex = splitName.length - 1;	
//                 let type = "image/jpeg";	
//                 if(splitName[lastIndex].toLowerCase() == "png")	
//                 {	
//                   type = "image/png";	
//                 }
//                 sourceArray.push({fileName: response.name, type: type, uri:response.uri});	
//                 let data=[...this.state.data];	
//                 let itm= data[this.selectedRowIndex];	
//                 imageArray.push({ uri: 'data:image/jpeg;base64,' + base64data });	
                	
//                 if(index == capturedImage.length - 1)
//                 {
//                   itm.path   = imageArray;
//                   itm.source = sourceArray;
//                   this.setState({data: data});
//                 } 	
//           })	
//           .catch(err => {	
//               console.log(err);	
//           });	
//       });	
//     }
//   }		

//   async resize2(res) {	
//     // let imageArray = [];
//     // for (let index = 0; index < res.length; index++) {
//     //   ImageResizer.createResizedImage(res[index].path, 800, 600, 'JPEG', 50)	
//     //   .then(async response => {	
//     //       await RNFS.readFile(response.uri, "base64").then(async(base64data) => {	
//     //         let source = {fileName: response.name, type: res.type, uri:response.uri};	
//     //         let data=[...this.state.data];	
//     //         let itm= data[this.selectedRowIndex];	
//     //         imageArray.push({ uri: 'data:image/jpeg;base64,' + base64data });	
//     //         itm.source=source;	
//     //         if(index == res.length - 1)
//     //         {
//     //           itm.path = imageArray;
//     //           this.setState({data: data});
//     //         }
//     //       })	
//     //       .catch(err => {	
//     //           console.log(err);	
//     //       });	
//     //   });	
//     // }
//     let capturedImage = [];
//     for(let i = 0; i < res.length; i++)
//     {
//       await capturedImage.push(res[i].path);
//     }
//     let imageArray = [];
//     let sourceArray = [];
//     for (let index = 0; index < capturedImage.length; index++) {
//       await ImageResizer.createResizedImage(capturedImage[index], 800, 600, 'JPEG', 50)	
//           .then(async response => {	
//               await RNFS.readFile(response.uri, "base64").then(async(base64data) => {	
//                 let splitName = response.name.split(".");	
//                 let lastIndex = splitName.length - 1;	
//                 let type = "image/jpeg";	
//                 if(splitName[lastIndex].toLowerCase() == "png")	
//                 {	
//                   type = "image/png";	
//                 }
//                 sourceArray.push({fileName: response.name, type: type, uri:response.uri});
//                 let data=[...this.state.data];	
//                 let itm= data[this.selectedRowIndex];	
//                 imageArray.push({ uri: 'data:image/jpeg;base64,' + base64data });	
//                 if(index == capturedImage.length - 1)
//                 {
//                   itm.path   = imageArray;
//                   itm.source = sourceArray;
//                   this.setState({data: data});
//                 } 	
//           })	
//           .catch(err => {	
//               console.log(err);	
//           });	
//       });	
//     }
    
//   }










//   onRemove=(i)=>{
//     console.log("oioioipp",i)
//        let data=[...this.state.data];
//         data.splice(i,1)
//        this.setState({data:data})
//   }
//   getData= () => {


//     let jsn={
//       "copType":2 ,
//       "schId":this.state.item.Id,
//       "siteId":this.state.item.site_id ,
//       "crewId":this.state.crewId        
//     }  
    
//     this.setState({isLoading :false})
//       axios( {
//         method: "POST",
//         url:Constant.Api.ServerDomain+Constant.Api.COPE.PREPOSTCOP,         
//         data:  jsn 
      
//       })        
       
//       .then(response => {
//         console.log("upload succes", response);   
//         if(response.data && Array.isArray(response.data.data))
//         {
//           let data= response.data.data 
//           console.log("Vijay"+ JSON.stringify(data))
//           let flagArray = [];
//           for (let index = 0; index < data.length; index++) {
//               flagArray.push(data[index].copId);
//           }
//           arr = flagArray.filter( ( item, index, inputArray )=> { return inputArray.indexOf(item) == index; });
        
//           let temp = [];
//           for(let i = 0; i < arr.length; i++){
//             let tempObj = {value: Number(arr[i]), path:[], source:[]}
//             for (let index = 0; index < data.length; index++) {
//               if(arr[i] == data[index].copId)
//               {
//                 tempObj.path.push({uri: Constant.Api.COPE.IMAGEBASE + data[index].filePath})
//               }
//             }
//             temp.push(tempObj);
            
//          }
//          this.setState({sdata:temp})
//         } 
//         this.setState({isLoading :false});           
//       })
//         .catch(error => {
//         console.log("submit_img", error); 
//         console.log("upload error", error);  
//         this.setState({isLoading :false})
      
//         });
       
//         console.log("imagessssss","end"); 
//       };   


//   getDropdown = () => {


//     let jsn={
//       "copType":2
//   }  
    
                 
    
//     console.log("submit_img",Constant.Api.ServerDomain+Constant.Api.COPE.COPLIST) ; 
      
//          this.setState({isLoading :false})
//       axios( {
//         method: "POST",
//         url:Constant.Api.ServerDomain+Constant.Api.COPE.COPLIST,         
//         data:  jsn 
      
//       })
       
//         .then(response => {
//         console.log("submit_img", response) ; 
//         console.log("upload succes", response);   
//         if(response.data && Array.isArray(response.data.data)){
//           this.setState({dd:response.data.data})

//         } 
//         this.setState({isLoading :false,})
      
//         })
//         .catch(error => {
//         console.log("submit_img", error); 
//         console.log("upload error", error);  
//         this.setState({isLoading :false})
      
//         });
       
//         console.log("imagessssss","end"); 
//       };  
      
      


//   createFormData = async () => {
//     const data = new FormData();
// 		console.log("imagessssss", data )
//     data.append("latitude",this.state.lat)
//     data.append("longitude",this.state.long)
//     data.append("copType","2")
//     data.append("deviceType","1")
//     data.append("schId",this.state.item.Id)
//     data.append("siteId",this.state.item.site_id)
//     data.append("crewId",this.state.crewId)
// 		console.log("imagessssss", data )
// 	  // this.state.data.forEach(async(result,i)=>{
//       for(let i = 0; i < this.state.data.length; i++){
//         let result = this.state.data[i];
//       var insertImg=0;     
//       for (let index = 0; index < result.path.length; index++) {
//         data.append("ddl_"+insertImg ,result.value);
//         console.log(index+" Vijay Kumar "+JSON.stringify(result.source));
//         let photo=result.source;
//         let base64data = result.path[index].uri.replace("data:image/jpeg;base64,", "")
//         await  data.append("file_"+insertImg, {
//             name: photo[index].fileName,
//             type: photo[index].type,
//             data: base64data, 
//             uri:
//               Platform.OS === "android" ? photo[index].uri : photo[index].uri.replace("file://", "")
//             });
//             insertImg++;

//             if((i == this.state.data.length - 1) && (index == result.path.length - 1))
//             {
//               return data;
//             }
//       }
//       insertImg++;
// 		}
//     // )
    
		
//   };





    
//     handleUploadPhoto = async () => {
//       let data = await this.createFormData();
//       // return;
//       this.setState({isLoading :true})
//       axios( {
//         method: "POST",
//         url:Constant.Api.ServerDomain+Constant.Api.COPE.SUBMITCOP,         
//         data:  data 
      
//       })
  
//         .then(response => {
//         console.log("submit_img", response) ; 
//         console.log("upload succes", response);
//         this.setState({isLoading :false})
//         this.setState({ photo: null });
//         if(response.data && response.data.status=="1"){
//           let key =this.state.item.site_id+2
     
      
//             AsyncStorage.removeItem(key)




//           this.setState({
//             value:"",
            
//             resourcePath: {}, 
//             data:[{value:"",path:[AddImg], source:[]}],
             
//             sdata:[] 
//            },()=>{this.getData()}) 


//           alert("Data Uploaded!")
//         }else{
//           alert("Failed!")
//         }
       
       
//         })
//         .catch(error => {
         
//         console.log("submit_img", error); 
//         console.log("upload error", error);  
//         this.setState({isLoading :false})
//         alert("Failed!") 
//         });
       
//         console.log("imagessssss","end"); 
//       };   


       
    
//   getCurrentDate=()=>{
// 	var now = new Date(); 
// 	let currentDateTime = new Date()
// 	let month = currentDateTime.getMonth() + 1
// 	if (month < 10) {
// 		month = '0' + month; 
// 	}
	
// 	let day = currentDateTime.getDate()
// 	if (day < 10) {
// 		day = '0' + day; 
// 	}
	
// 	let year = currentDateTime.getFullYear()
// 	let fullDate=month + "/" + day + "/" + year;
// 	return fullDate;
// }
 
 
//    displayAlertMsg=(title,message,callback)=>{
//    Dialog.alertDialog(title,message,I18n.t('OK_BUTTON'),false,null).then(()=>{
//      if(callback){
//        callback();
//      }
//    })
//  } 
 
//    onValueChange=(value,i)=>{
//     let data=[...this.state.data]
//      let itm=  data[i]
//  itm.value=value;
//    this.setState({data: data})
//    } 
    
//    onAdd=()=>{
//     let itm= [...this.state.data]
//     itm.push({value:"",path:[AddImg]})
//     console.log("data..... 1",itm )
//      this.setState({data:itm},()=>{
//       console.log("data..... 2",this.state.data)
//      })
//    }
                     
//    submitImages=()=>{
//       console.log("submit_img",this.state.data)
//       console.log("submit_img",this.state.item)
//         let flag=true      
//          this.state.data.forEach((itm,i)=>{
//            if((!itm.value || itm.value=="")|| (!itm.source || (itm.source==[]))){
//              flag=false
//            }
//          })
//           if(flag){
//             this.handleUploadPhoto()
//           }else{alert("Please fill all fields!")}

//    }
//      render() {
     
           
  


//    let data=this.state.data;
   
//   console.log("data.....",data )
//       return(
//           <View style={CopeStyle .container}>
          
//             <StatusBarComponent isHidden={false} />
//             {DeviceInfo.isAndroid() &&               
//             <ToolbarComponent   
//               title= "EOS Post"
//               navIcon={Constant.ICONS.BACK_ICON}         
//               navClick={() => this.props.navigation.goBack()}
//             />

//           }
//           <ScrollView >    
            
//           {this.state.sdata.map((itm,i)=>{
//                return(
   
//                 <View  style={CopeStyle.hv} >     
//                 <Picker
//                  enabled={false}   
//              selectedValue={itm.value}
//               style={CopeStyle.input} 
//              onValueChange={(itemValue, itemIndex) => this.onValueChange(itemValue,i)}
//            >
//               <Picker.Item label="Select..." value="" />
//               {this.state.dd.map((itm,i)=>{
//                  return(
//                   <Picker.Item label={itm.name} value={itm.Id} />    

//                  )
//               })}

//            </Picker>    
//            <TouchableOpacity   >
//            {/* <Image       style={CopeStyle.img_pic}  resizeMode={'cover'}     source={itm.path[0]} ></Image>  */}
//            <View style = {CopeStyle.img_pic}>
//                 <Image style={CopeStyle.img_pic} resizeMode={'cover'} source={itm.path[0]} />
//                 { itm.path[0] != AddImg &&
//                   <TouchableOpacity style = {{width: 40, height: 40, position: "absolute", bottom: 0, right: 0, justifyContent: "center", alignItems: "center"}}
//                     onPress = {()=> this.props.navigation.navigate("SlidingImageView", {"imageUrl": itm.path})} >
//                     <Icon name = {"eye"} size = {20} color = {"#000000"} />
//                   </TouchableOpacity>
//                 }
//             </View>
//            </TouchableOpacity>
               
//                 </View>
          
          
          

//                );
//             })}


//             {data.map((itm,i)=>{
//                return(

//                 <View  style={CopeStyle.hv} >     
//                 <Picker
//              selectedValue={itm.value}
//               style={CopeStyle.input} 
//              onValueChange={(itemValue, itemIndex) => this.onValueChange(itemValue,i)}
//            >
//               <Picker.Item label="Select..." value="" />
//               {this.state.dd.map((itm,i)=>{
//                  return(
//                   <Picker.Item label={itm.name} value={itm.Id} />    

//                  )
//               })}

                
//            </Picker>    
//            <TouchableOpacity  onPress={()=>{	
//              this.selectedRowIndex = i; 	
//               this.setSelectFromModalVisibility(true);	
//               // this.selectFile(i)	
//              }} >
//            {/* <Image       style={CopeStyle.img_pic}  resizeMode={'cover'}     source={itm.path} ></Image>  */}
//            <View style = {CopeStyle.img_pic}>
//                 <Image style={CopeStyle.img_pic} resizeMode={'cover'} source={itm.path[0]} />
//                 { itm.path[0] != AddImg &&
//                   <TouchableOpacity style = {{width: 40, height: 20, position: "absolute", bottom: 0, right: 0, justifyContent: "center", alignItems: "center"}}
//                     onPress = {()=> this.props.navigation.navigate("SlidingImageView", {"imageUrl": itm.path})} >
//                     <Icon name = {"eye"} size = {20} color = {"#000000"} />
//                   </TouchableOpacity>
//                 }
//             </View>
//            </TouchableOpacity>
//            {  (data.length!=1) &&      
//            <TouchableOpacity  onPress={()=>this.onRemove(i)} >
//                   <Image style={CopeStyle.img_add} source={Removeicon} ></Image>
//            </TouchableOpacity>    }         
//            {(data.length==(i+1)) &&  
//            <TouchableOpacity  onPress={()=>this.onAdd()} >
//                   <Image style={CopeStyle.img_add} source={AddNew} ></Image>
//            </TouchableOpacity>    }         
//                 </View>
          
          
          

//                );
//             })}
        
	 
//         <TouchableOpacity
//                style = {CopeStyle.submitButton}
//                 onPress={()=>this.submitImages()}
//                  >
//                <Text style = {CopeStyle.submitButtonText}> Submit </Text>
//             </TouchableOpacity>

		  
//         </ScrollView>                     
            
          
//         {this.state.isLoading   &&  
// 					<Loader msg={'Uploading...'} />
// 				} 






//         <Modal	
//             animationType="none"	
//             transparent={true}	
//             closeOnClick={true}	
//             visible={this.state.selectFromModal}	
//             onRequestClose={() => {	
//             this.setSelectFromModalVisibility(false)	
//             }}>	
//             <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'rgba(0,0,0,0.6)'}}>	
//             <View style={{width: '90%',backgroundColor: '#FFFFFF', elevation:10, borderTopLeftRadius:8,borderTopEndRadius:8, margin: 5}}>	
//                 <View style={{ flexDirection: 'row',height:55, backgroundColor: Colors.lightOrange,alignItems:'center'}}>	
//                 <Text style={{flex: 1, paddingLeft: 7, fontWeight:'bold', fontSize: 17, fontFamily: 'Roboto', color: '#FFFFFF', textAlign: 'left'}}>	
//                     {I18n.t('COPPREPOST.SELECT_FROM')}	
//                 </Text>	
//                 <View style={{width:80,height:30,justifyContent: 'center',alignSelf:'center',backgroundColor:'#fff',borderRadius:15, marginRight: 10}}>	
//                     <TouchableOpacity	
//                     onPress={()=>this.setSelectFromModalVisibility(false)}>	
//                         <Text style={{ fontWeight:'bold', fontSize: 12, fontFamily: 'Roboto', color: Colors.lightOrange, textAlign: 'center'}}>	
//                             {I18n.t('COPPREPOST.DISMISS')}	
//                         </Text>	
//                     </TouchableOpacity>	
//                 </View>	
                	
//                 </View>	
//                 <View style = {{flexDirection: 'row', height:150,alignSelf: 'center', alignItems:'center', justifyContent: 'center',}}>	
//                     <View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>	
//                         <TouchableOpacity  style={{width:70,height:70,backgroundColor:'white',alignItems:"center",justifyContent:"center",borderRadius:60, }}	
//                         onPress={ this.openCamera.bind(this)}>	
//                         <Image 	
//                             style={{width:60,height:60, tintColor: Colors.lightOrange}}	
//                             resizeMode= "center"	
//                             source={require('../../Images/Camera/camera.png')} />	
//                         </TouchableOpacity>   	
//                         <Text style={{alignSelf:'stretch',padding: 5, fontSize: 15, fontFamily: 'Roboto', fontWeight: 'bold', color: Colors.lightOrange,textAlign:'center'}}>{'Camera'}</Text>     	
//                     </View>	
                    	
//                     <View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>	
//                         <TouchableOpacity  style={{width:70,height:70,backgroundColor:'white',alignItems:"center",justifyContent:"center",borderRadius:60}}	
//                         onPress={this.openGallery.bind(this)}>	
//                         <Image 	
//                             style={{width:60,height:60, tintColor: Colors.lightOrange}}	
//                             resizeMode= "center"	
//                             source={require('../../Images/Camera/choose_gallery.png')} />	
//                         </TouchableOpacity>   	
//                         <Text style={{alignSelf:'stretch',padding: 5, fontSize: 15, fontFamily: 'Roboto', fontWeight: 'bold', color: Colors.lightOrange,textAlign:'center'}}>{'Gallery'}</Text>      	
//                     </View>     	
//                     </View>	
//                 </View>	
//             </View>	
//         </Modal>
        
              
      
//       </View>     
//       );
  
    
//     }
//  }




























































// import React, { PureComponent,Component } from 'react';
// import ToolbarComponent from "../../Components/Toolbar";
// import I18n from '../../Utils/i18n';
// import { CommonMethods, DeviceInfo, Dialog } from '../../Utils/Utils'
// import {Platform,  WebView,Image,Button,Dimensions, FlatList, RefreshControl, Text, TouchableOpacity, View, KeyboardAvoidingView,Modal, TextInput, Picker, ScrollView, TouchableWithoutFeedback } from 'react-native';
// // import ImagePicker from 'react-native-image-picker';  
// import ImagePicker from 'react-native-image-crop-picker';   
// import { CopeStyle  } from '../../Styles/PageStyle';
// import axios from 'axios'; 
//  import AddImg from './../../Images/Cope/addImg.png'           
//  import AddNew from './../../Images/Cope/addNew.png'
//  import Removeicon from './../../Images/Cope/delete.png'   
// import StatusBarComponent from '../../Components/StatusBar';        
// import Loader from '../../Components/Loader';
// import Constant from '../../Utils/Constant';              
                  
// import {Colors} from '../../Styles/Themes'; 	
// import AsyncStorage from '@react-native-community/async-storage';	
// import GetLocation from 'react-native-get-location'	
// import { PermissionsAndroid } from 'react-native';

// import Icon from 'react-native-vector-icons/Entypo';	

// import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';	
// import ImageResizer from 'react-native-image-resizer';	
// const RNFS = require("react-native-fs");	

// const width = Dimensions.get('window').width;
// const height =  Dimensions.get('window').height ;
 
// export default class GCSiteLoingScreen extends PureComponent {        
            

//   constructor(props) {
//   super(props);
//   this.requestForLoc()
// 	this.currentDate=this.getCurrentDate();     
//     this.state = {
//            value:"",
//            dd:[],
//            resourcePath: {}, 
//            data:[{value:"",path:[AddImg], source:""}],
//            item:{},
//            sdata:[],	
//            crewId:"",	
//            lat: "",	
//            long: "",	
        	
//           selectFromModal: false
//     };
//   }      


//   async requestForLoc(i,fn){
//     const granted = await PermissionsAndroid.check( PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION );
//     if (granted) {
//       GetLocation.getCurrentPosition({
//         enableHighAccuracy: true,
//         timeout: 15000,
//     })
//     .then(location => {
//         console.log("location raj",location);
//         //latitude
//         //longitude
//       this.setState({lat:location.latitude,long:location.longitude})
//      if(i!=undefined){
//        fn()
//       let data=[...this.state.data]
//       let itm= data[i]
//       itm.loc={lat:location.latitude,long:location.longitude}
//       this.setState({data:data})
//      }
       
        
//     })
//     .catch(error => {
//         const { code, message } = error;

          
//         //  console.warn("location raj", message);
//         if(i==undefined){
//           GetLocation.openGpsSettings();
//         }else{
//           alert(message)
//         }
       
//     })
//     } 
//     else {
//       this.requestLocationPermission();
//     }
//   }
  
//  async requestLocationPermission() 
//   {
//     try {
//       const granted = await PermissionsAndroid.request(
//         PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
//         {
//           'title': 'TMOEricsson  App',
//           'message': 'TMOEricsson App access to your location '
//         }
//       )
//       if (granted === PermissionsAndroid.RESULTS.GRANTED) {
         
//       } else {
       

//       }
//     } catch (err) {
//       console.warn(err)
//     }
//   }


//   componentDidMount(){
//     this.getDropdown()
       
//     let params= this.props.navigation.state.params;
//     this.setState({item:params.item,crewId:params.crewId}, async ()=>{
//       this.getData()
//       let key =this.state.item.site_id+2
     
//        try{
//       let data= await  AsyncStorage.getItem(key);
//       console.log("dataaatttt",data)
//       data= JSON.parse(data)
//       if(Array.isArray(data)){
//         this.setState({data:data})

//       }
//       }      
//       catch(err){  

//       }
//      }
      
//       )
     
//   }       
//   componentWillUnmount(){
//     let key =this.state.item.site_id+2
//     let data =JSON.stringify(this.state.data)
    
//     AsyncStorage.setItem(key,data)
//   }





//   setSelectFromModalVisibility(visibility)	
//     {	
//         this.setState({selectFromModal: visibility});  	
//     }	
//     handleOnNavigateBack = async (refreshPage) => {	
//         let returnFrom  = await AsyncStorage.getItem("okCamera");	
//         if(returnFrom !== "ok")	
//           return;	
        	
//         let picturePath = await AsyncStorage.getItem("picturePath");	
//         await AsyncStorage.removeItem("picturePath");	
//         // this.setState({picturePath: picturePath});	
//         this.resize1(picturePath);	
//       }	
//     openCamera= async()=>{	
//         this.setSelectFromModalVisibility(false);	
//         // AsyncStorage.setItem("fromCOP", 1);	
//         this.props.navigation.navigate('MultipleCapture',{onNavigateBack: this.handleOnNavigateBack});	
//       }	
//     async openGallery()	
//     {	
//         this.setSelectFromModalVisibility(false);	
//         try	
//         {		
//             ImagePicker.openPicker({
//               multiple: true
//             }).then(images => {
//               console.log(images);
//               // alert(JSON.stringify(images))
//               this.resize2(images)	
//             });
//         }	
//         catch(error1)	
//         {	
//           console.log(error1);;	
//         }		
//     }	
//         //Compress image	
//         async resize1(res) {	
//           let capturedImage = JSON.parse(res);
//           let imageArray = [];
//           for (let index = 0; index < capturedImage.length; index++) {
//             ImageResizer.createResizedImage(capturedImage[index], 800, 600, 'JPEG', 50)	
//                 .then(async response => {	
//                     await RNFS.readFile(response.uri, "base64").then(async(base64data) => {	
//                       let source = null;	
//                       let splitName = response.name.split(".");	
//                       let lastIndex = splitName.length - 1;	
//                       let type = "image/jpeg";	
//                       if(splitName[lastIndex].toLowerCase() == "png")	
//                       {	
//                         type = "image/png";	
//                       }
//                       source = await {fileName: response.name, type: type, uri:response.uri};	
//                       let data=[...this.state.data];	
//                       let itm= data[this.selectedRowIndex];	
//                       // itm.path= { uri: 'data:image/jpeg;base64,' + base64data };	
//                       imageArray.push({ uri: 'data:image/jpeg;base64,' + base64data });	
//                       itm.source=source;	
//                       if(index == capturedImage.length - 1)
//                       {
//                         itm.path = imageArray;
//                         this.setState({data: data});
//                       } 	
//                 })	
//                 .catch(err => {	
//                     console.log(err);	
//                 });	
//             });	
//           }
//         }	

//   async resize2(res) {	
//     let imageArray = [];
//     for (let index = 0; index < res.length; index++) {
//       ImageResizer.createResizedImage(res[index].path, 800, 600, 'JPEG', 50)	
//       .then(async response => {	
//           await RNFS.readFile(response.uri, "base64").then(async(base64data) => {	
//             let source = {fileName: response.name, type: res.type, uri:response.uri};	
//             let data=[...this.state.data];	
//             let itm= data[this.selectedRowIndex];	
//             imageArray.push({ uri: 'data:image/jpeg;base64,' + base64data });	
//             itm.source=source;	
//             if(index == res.length - 1)
//             {
//               itm.path = imageArray;
//               this.setState({data: data});
//             }
//           })	
//           .catch(err => {	
//               console.log(err);	
//           });	
//       });	
//     }
//     // ImageResizer.createResizedImage(res.uri, 800, 600, 'JPEG', 50)	
//     //     .then(async response => {	
//     //         await RNFS.readFile(response.uri, "base64").then(async(base64data) => {	
//     //           let source = {fileName: response.name, type: res.type, uri:response.uri};	
//     //           let data=[...this.state.data];	
//     //           let itm= data[this.selectedRowIndex];	
//     //           itm.path= { uri: 'data:image/jpeg;base64,' + base64data };	
//     //           // itm.data = data	
//     //           itm.source=source;	
//     //           this.setState({data: data}) 	
//     //         })	
//     //         .catch(err => {	
//     //             console.log(err);	
//     //         });	
//     // });	
//   }	
            	








//   onRemove=(i)=>{
//     console.log("oioioipp",i)
//        let data=[...this.state.data];
//         data.splice(i,1)
//        this.setState({data:data})
//   }
//   getData= () => {


//     let jsn={
//       "copType":2 ,
//    "schId":this.state.item.Id,
//     "siteId":this.state.item.site_id      
//     , "crewId":this.state.crewId
//   }  
    
                 
    
//     console.log("submit_img",Constant.Api.ServerDomain+Constant.Api.COPE) ; 
      
//          this.setState({isLoading :false})
//       axios( {
//         method: "POST",
//         url:Constant.Api.ServerDomain+Constant.Api.COPE.PREPOSTCOP,         
//         data:  jsn 
      
//       })        
       
//         .then(response => {
//         console.log("submit_img", response) ; 
//         console.log("upload succes", response);   
//         if(response.data && Array.isArray(response.data.data)){
//           let data= response.data.data 
//              if(data.length>0){
//          let tmp=      data.map((item,j)=>{
//                  return({value: Number(item.copId)   ,path:
//                   {uri: Constant.Api.COPE.IMAGEBASE + item.filePath    }
//                      , source:""})
//                })
//                this.setState({sdata:tmp})
//              }
         

//         } 
//         this.setState({isLoading :false,})   
      
//         })
//         .catch(error => {
//         console.log("submit_img", error); 
//         console.log("upload error", error);  
//         this.setState({isLoading :false})
      
//         });
       
//         console.log("imagessssss","end"); 
//       };   


//   getDropdown = () => {


//     let jsn={
//       "copType":2
//   }  
    
                 
    
//     console.log("submit_img",Constant.Api.ServerDomain+Constant.Api.COPE.COPLIST) ; 
      
//          this.setState({isLoading :false})
//       axios( {
//         method: "POST",
//         url:Constant.Api.ServerDomain+Constant.Api.COPE.COPLIST,         
//         data:  jsn 
      
//       })
       
//         .then(response => {
//         console.log("submit_img", response) ; 
//         console.log("upload succes", response);   
//         if(response.data && Array.isArray(response.data.data)){
//           this.setState({dd:response.data.data})

//         } 
//         this.setState({isLoading :false,})
      
//         })
//         .catch(error => {
//         console.log("submit_img", error); 
//         console.log("upload error", error);  
//         this.setState({isLoading :false})
      
//         });
       
//         console.log("imagessssss","end"); 
//       };   
//   createFormData = () => {



// 		const data = new FormData();
// 		console.log("imagessssss", data )
// 	this.state.data.forEach((result,i)=>{
  
//      data.append("ddl_"+(i ),result.value)
//     let photo=result.source
//     let base64data = result.path.uri.replace("data:image/jpeg;base64,", "")
// 			data.append("file_"+(i), {
// 				name: photo.fileName,
//         type: photo.type,
//         data: base64data, 
// 				uri:
// 				  Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
// 			  });
// 		})
//     data.append("latitude",this.state.lat)
//     data.append("longitude",this.state.long)
//       data.append("copType","2")
//       data.append("deviceType","1")  
//       data.append("schId",this.state.item.Id)
//       data.append("siteId",this.state.item.site_id)
//       data.append("crewId",this.state.crewId) 
// 		console.log("imagessssss", data )
// 		return data;
//     };





    
//     handleUploadPhoto = () => {
//       console.log("submit_img",  this.createFormData()) 
      
//          this.setState({isLoading :true})
//       axios( {
//         method: "POST",
//         url:Constant.Api.ServerDomain+Constant.Api.COPE.SUBMITCOP,         
//         data:  this.createFormData()  
      
//       })
  
//         .then(response => {
//         console.log("submit_img", response) ; 
//         console.log("upload succes", response);
//         this.setState({isLoading :false})
//         this.setState({ photo: null });
//         if(response.data && response.data.status=="1"){
//           let key =this.state.item.site_id+2
     
      
//             AsyncStorage.removeItem(key)




//           this.setState({
//             value:"",
            
//             resourcePath: {}, 
//             data:[{value:"",path:[AddImg], source:""}],
             
//             sdata:[] 
//            },()=>{this.getData()}) 


//           alert("Data Uploaded!")
//         }else{
//           alert("Failed!")
//         }
       
       
//         })
//         .catch(error => {
         
//         console.log("submit_img", error); 
//         console.log("upload error", error);  
//         this.setState({isLoading :false})
//         alert("Failed!") 
//         });
       
//         console.log("imagessssss","end"); 
//       };   
  
//   getCurrentDate=()=>{
// 	var now = new Date(); 
// 	let currentDateTime = new Date()
// 	let month = currentDateTime.getMonth() + 1
// 	if (month < 10) {
// 		month = '0' + month; 
// 	}
	
// 	let day = currentDateTime.getDate()
// 	if (day < 10) {
// 		day = '0' + day; 
// 	}
	
// 	let year = currentDateTime.getFullYear()
// 	let fullDate=month + "/" + day + "/" + year;
// 	return fullDate;
// }
 
 
//    displayAlertMsg=(title,message,callback)=>{
//    Dialog.alertDialog(title,message,I18n.t('OK_BUTTON'),false,null).then(()=>{
//      if(callback){
//        callback();
//      }
//    })
//  } 
 
//    onValueChange=(value,i)=>{
//     let data=[...this.state.data]
//      let itm=  data[i]
//  itm.value=value;
//    this.setState({data: data})
//    } 
    
//    onAdd=()=>{
//     let itm= [...this.state.data]
//     itm.push({value:"",path:[AddImg]})
//     console.log("data..... 1",itm )
//      this.setState({data:itm},()=>{
//       console.log("data..... 2",this.state.data)
//      })
//    }
                     
//    submitImages=()=>{
//       console.log("submit_img",this.state.data)
//       console.log("submit_img",this.state.item)
//         let flag=true      
//          this.state.data.forEach((itm,i)=>{
//            if((!itm.value || itm.value=="")|| (!itm.source || (itm.source==""))){
//              flag=false
//            }
//          })
//           if(flag){
//             this.handleUploadPhoto()
//           }else{alert("Please fill all fields!")}

//    }
//      render() {
     
           
  


//    let data=this.state.data;
   
//   console.log("data.....",data )
//       return(
//           <View style={CopeStyle .container}>
          
//             <StatusBarComponent isHidden={false} />
//             {DeviceInfo.isAndroid() &&               
//             <ToolbarComponent   
//               title= "EOS Post"
//               navIcon={Constant.ICONS.BACK_ICON}         
//               navClick={() => this.props.navigation.goBack()}
//             />

//           }
//           <ScrollView >    
            
//           {this.state.sdata.map((itm,i)=>{
//                return(
   
//                 <View  style={CopeStyle.hv} >     
//                 <Picker
//                  enabled={false}   
//              selectedValue={itm.value}
//               style={CopeStyle.input} 
//              onValueChange={(itemValue, itemIndex) => this.onValueChange(itemValue,i)}
//            >
//               <Picker.Item label="Select..." value="" />
//               {this.state.dd.map((itm,i)=>{
//                  return(
//                   <Picker.Item label={itm.name} value={itm.Id} />    

//                  )
//               })}

//            </Picker>    
//            <TouchableOpacity   >
//            <Image       style={CopeStyle.img_pic}  resizeMode={'cover'}     source={itm.path[0]} ></Image> 
//            </TouchableOpacity>
               
//                 </View>
          
          
          

//                );
//             })}


//             {data.map((itm,i)=>{
//                return(

//                 <View  style={CopeStyle.hv} >     
//                 <Picker
//              selectedValue={itm.value}
//               style={CopeStyle.input} 
//              onValueChange={(itemValue, itemIndex) => this.onValueChange(itemValue,i)}
//            >
//               <Picker.Item label="Select..." value="" />
//               {this.state.dd.map((itm,i)=>{
//                  return(
//                   <Picker.Item label={itm.name} value={itm.Id} />    

//                  )
//               })}

                
//            </Picker>    
//            <TouchableOpacity  onPress={()=>{	
//              this.selectedRowIndex = i; 	
//               this.setSelectFromModalVisibility(true);	
//               // this.selectFile(i)	
//              }} >
//            {/* <Image       style={CopeStyle.img_pic}  resizeMode={'cover'}     source={itm.path} ></Image>  */}
//            <View style = {CopeStyle.img_pic}>
//                 <Image style={CopeStyle.img_pic} resizeMode={'cover'} source={itm.path[0]} />
//                 { itm.path[0] != AddImg &&
//                   <TouchableOpacity style = {{width: 40, height: 20, position: "absolute", bottom: 0, right: 0, justifyContent: "center", alignItems: "center"}}
//                     onPress = {()=> this.props.navigation.navigate("SlidingImageView", {"imageUrl": itm.path})} >
//                     <Icon name = {"eye"} size = {20} color = {"#000000"} />
//                   </TouchableOpacity>
//                 }
//             </View>
//            </TouchableOpacity>
//            {  (data.length!=1) &&      
//            <TouchableOpacity  onPress={()=>this.onRemove(i)} >
//                   <Image style={CopeStyle.img_add} source={Removeicon} ></Image>
//            </TouchableOpacity>    }         
//            {(data.length==(i+1)) &&  
//            <TouchableOpacity  onPress={()=>this.onAdd()} >
//                   <Image style={CopeStyle.img_add} source={AddNew} ></Image>
//            </TouchableOpacity>    }         
//                 </View>
          
          
          

//                );
//             })}
        
	 
//         <TouchableOpacity
//                style = {CopeStyle.submitButton}
//                 onPress={()=>this.submitImages()}
//                  >
//                <Text style = {CopeStyle.submitButtonText}> Submit </Text>
//             </TouchableOpacity>

		  
//         </ScrollView>                     
            
          
//         {this.state.isLoading   &&  
// 					<Loader msg={'Uploading...'} />
// 				}  






//         <Modal	
//             animationType="none"	
//             transparent={true}	
//             closeOnClick={true}	
//             visible={this.state.selectFromModal}	
//             onRequestClose={() => {	
//             this.setSelectFromModalVisibility(false)	
//             }}>	
//             <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'rgba(0,0,0,0.6)'}}>	
//             <View style={{width: '90%',backgroundColor: '#FFFFFF', elevation:10, borderTopLeftRadius:8,borderTopEndRadius:8, margin: 5}}>	
//                 <View style={{ flexDirection: 'row',height:55, backgroundColor: Colors.lightOrange,alignItems:'center'}}>	
//                 <Text style={{flex: 1, paddingLeft: 7, fontWeight:'bold', fontSize: 17, fontFamily: 'Roboto', color: '#FFFFFF', textAlign: 'left'}}>	
//                     {I18n.t('COPPREPOST.SELECT_FROM')}	
//                 </Text>	
//                 <View style={{width:80,height:30,justifyContent: 'center',alignSelf:'center',backgroundColor:'#fff',borderRadius:15, marginRight: 10}}>	
//                     <TouchableOpacity	
//                     onPress={()=>this.setSelectFromModalVisibility(false)}>	
//                         <Text style={{ fontWeight:'bold', fontSize: 12, fontFamily: 'Roboto', color: Colors.lightOrange, textAlign: 'center'}}>	
//                             {I18n.t('COPPREPOST.DISMISS')}	
//                         </Text>	
//                     </TouchableOpacity>	
//                 </View>	
                	
//                 </View>	
//                 <View style = {{flexDirection: 'row', height:150,alignSelf: 'center', alignItems:'center', justifyContent: 'center',}}>	
//                     <View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>	
//                         <TouchableOpacity  style={{width:70,height:70,backgroundColor:'white',alignItems:"center",justifyContent:"center",borderRadius:60, }}	
//                         onPress={ this.openCamera.bind(this)}>	
//                         <Image 	
//                             style={{width:60,height:60, tintColor: Colors.lightOrange}}	
//                             resizeMode= "center"	
//                             source={require('../../Images/Camera/camera.png')} />	
//                         </TouchableOpacity>   	
//                         <Text style={{alignSelf:'stretch',padding: 5, fontSize: 15, fontFamily: 'Roboto', fontWeight: 'bold', color: Colors.lightOrange,textAlign:'center'}}>{'Camera'}</Text>     	
//                     </View>	
                    	
//                     <View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>	
//                         <TouchableOpacity  style={{width:70,height:70,backgroundColor:'white',alignItems:"center",justifyContent:"center",borderRadius:60}}	
//                         onPress={this.openGallery.bind(this)}>	
//                         <Image 	
//                             style={{width:60,height:60, tintColor: Colors.lightOrange}}	
//                             resizeMode= "center"	
//                             source={require('../../Images/Camera/choose_gallery.png')} />	
//                         </TouchableOpacity>   	
//                         <Text style={{alignSelf:'stretch',padding: 5, fontSize: 15, fontFamily: 'Roboto', fontWeight: 'bold', color: Colors.lightOrange,textAlign:'center'}}>{'Gallery'}</Text>      	
//                     </View>     	
//                     </View>	
//                 </View>	
//             </View>	
//         </Modal>
                
      
//       </View>     
//       );
  
    
//     }
//  }
