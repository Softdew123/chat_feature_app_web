import React, { PureComponent,Component } from 'react';
import ToolbarComponent from "../../Components/Toolbar";
import I18n from '../../Utils/i18n';
import { CommonMethods, DeviceInfo, Dialog } from '../../Utils/Utils'
import {Platform,  WebView,Image,Button,Dimensions, FlatList, RefreshControl, Text, TouchableOpacity, View, KeyboardAvoidingView,Modal, TextInput, Picker, ScrollView, TouchableWithoutFeedback } from 'react-native';
import ImagePicker from 'react-native-image-picker';   
import { CopeStyle  } from '../../Styles/PageStyle';
import axios from 'axios'; 
 import AddImg from './../../Images/Cope/addImg.png'           
 import AddNew from './../../Images/Cope/addNew.png'
 import Removeicon from './../../Images/Cope/delete.png'   
import StatusBarComponent from '../../Components/StatusBar';        
import Loader from '../../Components/Loader';
import Constant from '../../Utils/Constant';              
              
import AsyncStorage from '@react-native-community/async-storage';    
import GetLocation from 'react-native-get-location'
import { PermissionsAndroid } from 'react-native';
import Dialogs, { DialogContent,DialogTitle } from 'react-native-popup-dialog';
import ImagePickers from 'react-native-image-crop-picker';  
const width = Dimensions.get('window').width;    
const height =  Dimensions.get('window').height ;    
 
export default class GCSiteLoingScreen extends PureComponent {        
    

  constructor(props) {
  super(props);
 
	this.currentDate=this.getCurrentDate();     
    this.state = {
           value:"",
           dd:[],
           resourcePath: {}, 
           data:[],
           item:{},
           sdata:[] 
           ,crewId:''
    };
  }      


  async requestForLoc(i,fn){
    const granted = await PermissionsAndroid.check( PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION );
    if (granted) {
      GetLocation.getCurrentPosition({
        enableHighAccuracy: true,
        timeout: 15000,
    })
    .then(location => {
        console.log("location raj",location);
        //latitude
        //longitude
      this.setState({lat:location.latitude,long:location.longitude})
     if(i!=undefined){
       fn()
      let data=[...this.state.data]
      let itm= data[i]
      itm.loc={lat:location.latitude,long:location.longitude}
      this.setState({data:data})
     }
       
        
    })
    .catch(error => {
        const { code, message } = error;

          
        //  console.warn("location raj", message);
        if(i==undefined){
          GetLocation.openGpsSettings();
        }else{
          alert(message)
        }
       
    })
    } 
    else {
      this.requestLocationPermission();
    }
  }
  
 async requestLocationPermission() 
  {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': 'TMOEricsson  App',
          'message': 'TMOEricsson App access to your location '
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
         
      } else {
         

      }
    } catch (err) {
      console.warn(err)
    }
  }
 
  uloadfromCamera=()=>{
    this.setState({ visible: false });
    const options = {
      title: 'Select Camera',
     
      storageOptions: {
        skipBackup: true,
        path: 'images',
      }, }


      ImagePicker.launchCamera(options, (response) => {
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          const source = { uri: response.uri };
          console.log('ImagePicker....', response);
          // You can also display the image using data:
          // const source = { uri: 'data:image/jpeg;base64,' + response.data };
      
          this.setState({
            avatarSource: response,
          },()=>{
            this.handleUploadPhoto2()
          });
        }
      });
  }
	uploadImg=()=>{



    this.setState({ visible: false });


 let item=this.state.item;

		console.log("imagessssss", item);    
		ImagePickers.openPicker({
			multiple: true
		  }).then(images => {
			console.log("imagessssss", images); 
			   this.handleUploadPhoto(item.site_id,images,item.Id)
			
		  });



       }

       createFormData2=()=>{
  let photo=this.state.avatarSource;
  

        // avatarSource
        const data = new FormData();
        data.append("file0", {
          name: photo.fileName,
          type: photo.type,
          data: photo.data, 
          uri:
            Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
          });
   
          data.append("schId",this.state.item.Id)
          data.append("siteId",this.state.item.site_id)
          data.append("crewId",this.state.crewId) 
          data.append("deviceType","1")  
          return data
       }
      createFormData = (images, body) => {
		const data = new FormData();
		console.log("imagessssss", images )
		images.forEach((photo,i)=>{
			data.append("file"+i, {
				name: "photo"+ photo.modificationDate,
				type: photo.mime,
				uri:
				  Platform.OS === "android" ? photo.path : photo.path.replace("file://", "")
			  });
		})

		Object.keys(body).forEach(key => {
		  data.append(key, body[key]);
		});
		console.log("imagessssss", data )
		return data;
	  };
	  handleUploadPhoto = (id,images,schid) => {
	
		   console.log("imagessssss", Constant.Api.ServerDomain+Constant.Api.DASHBAORD.UPLOADCREWIMAGE   ); 
		this.setState({isLoading :true})
		axios( {
		  method: "POST",
		  url:Constant.Api.ServerDomain+Constant.Api.DASHBAORD.UPLOADCREWIMAGE,         
		  data: this.createFormData(images,   {
			"schId":schid,
			"siteId":id,   
      "crewId":this.state.crewId,
      "deviceType":"1" 
			
			          
           } )    
		})

		  .then(response => {
			console.log("imagessssss", response) ; 
			console.log("upload succes", response);
			this.setState({isLoading :false})
			this.setState({ photo: null });
			if(response.data && response.data.status=="1"){
        alert("Data Uploaded!")
        this.getData()
			  }else{
				alert("Failed!")
			  }
		  })
		  .catch(error => {
			console.log("imagessssss", error); 
			console.log("upload error", error);
			this.setState({isLoading :false})
			alert("Failed!") 
		  });
		 
		  console.log("imagessssss","end"); 
	  };         
    handleUploadPhoto2 = (id,images,schid) => {
	
      console.log("imagessssss", Constant.Api.ServerDomain+Constant.Api.DASHBAORD.UPLOADCREWIMAGE   ); 
   this.setState({isLoading :true})
   axios( {
     method: "POST",
     url:Constant.Api.ServerDomain+Constant.Api.DASHBAORD.UPLOADCREWIMAGE,         
     data: this.createFormData2( )    
   })

     .then(response => {
     console.log("imagessssss", response) ; 
     console.log("upload succes", response);
     this.setState({isLoading :false})
     this.setState({ photo: null });
     if(response.data && response.data.status=="1"){
       alert("Data Uploaded!")
       this.getData()
       }else{
       alert("Failed!")
       }
     })
     .catch(error => {
     console.log("imagessssss", error); 
     console.log("upload error", error);
     this.setState({isLoading :false})
     alert("Failed!") 
     });
    
     console.log("imagessssss","end"); 
   }; 
  componentDidMount(){
  
    let params= this.props.navigation.state.params;
    this.setState({item:params.item,crewId:params.crewId},()=>{
        this.getData()
     }
      
      )

  }      

  getData= () => {


    let jsn={
    
   "schId":this.state.item.Id,
    "siteId":this.state.item.site_id,
      "crewId":this.state.crewId
  }  
    
                 
       
    console.log("get_data",jsn) ; 
      
         this.setState({isLoading :false})
      axios( {
        method: "POST",
        url:Constant.Api.ServerDomain+"GetCrewImage",         
        data:  jsn 
      
      })            
       
        .then(response => {
        console.log("get_data", response) ; 
        console.log("upload succes", response);   
        if(response.data && Array.isArray(response.data.data)){
          let data= response.data.data 
             if(data.length>0){
         let tmp=      data.map((item,j)=>{
                 return({value: ""   ,path:
                  {uri: Constant.Api.COPE.IMAGEBASE + item   }
                     , source:""})
               })    
               this.setState({data:tmp})
             }
         

        } 
        this.setState({isLoading :false,})   
      
        })
        .catch(error => {
        console.log("submit_img", error); 
        console.log("upload error", error);  
        this.setState({isLoading :false})
      
        });
       
        console.log("imagessssss","end"); 
      };   




       
    
  getCurrentDate=()=>{
	var now = new Date(); 
	let currentDateTime = new Date()
	let month = currentDateTime.getMonth() + 1
	if (month < 10) {
		month = '0' + month; 
	}
	
	let day = currentDateTime.getDate()
	if (day < 10) {
		day = '0' + day; 
	}
	
	let year = currentDateTime.getFullYear()
	let fullDate=month + "/" + day + "/" + year;
	return fullDate;
}
 
 
   displayAlertMsg=(title,message,callback)=>{
   Dialog.alertDialog(title,message,I18n.t('OK_BUTTON'),false,null).then(()=>{
     if(callback){
       callback();
     }
   })
 } 
 
    
     render() {
     
           
  


   let data=this.state.data;
   
  console.log("data.....",data )
      return(
          <View style={CopeStyle .container}>
          
            <StatusBarComponent isHidden={false} />
            {DeviceInfo.isAndroid() &&               
            <ToolbarComponent   
              title= "Upload"
              navIcon={Constant.ICONS.BACK_ICON}         
              navClick={() => this.props.navigation.goBack()}
            />

          }
             <TouchableOpacity  style={[CopeStyle.upload_button]}  onPress={()=>this.setState({ visible: true })}  >

             <Text style={[CopeStyle.upload_text]}>
							Upload Images       
						</Text>
           </TouchableOpacity>
          <ScrollView >    
            
       

    
            {data.map((itm,i)=>{
               return(

                <View  style={CopeStyle.hv} >     
           
           <TouchableOpacity  >
           <Image       style={CopeStyle.img_view}  resizeMode={'cover'}     source={itm.path} ></Image> 
           </TouchableOpacity>
             
                </View>
          
          
          

               );
            })}
        
	 

		  
        </ScrollView>                     
            
          
        {this.state.isLoading   &&  
					<Loader msg={'Uploading...'} />
				}  
                
                <View style={CopeStyle.container}>
 
  <Dialogs
    visible={this.state.visible}
    dialogTitle={<DialogTitle title="Choose" />}
    onTouchOutside={() => {
      this.setState({ visible: false });
    }}
    onHardwareBackPress={() => {
      this.setState({ visible: false });
    }}
  >
    <DialogContent>
    <TouchableOpacity  onPress={()=>{this.uloadfromCamera()}} style={CopeStyle.opt}  >
    <Text >
								 open  camera  
								</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{this.uploadImg()}} style={CopeStyle.opt}  >
                <Text  >
								 open  Gallery
								</Text>
                </TouchableOpacity>
    </DialogContent>
  </Dialogs>
</View>
      
      </View>     
      );
  
    
    }
 }
