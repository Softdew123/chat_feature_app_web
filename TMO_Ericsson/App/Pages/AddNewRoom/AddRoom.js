import React, { PureComponent }  from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import firebase from 'react-native-firebase';

export default class AddnewRoom extends PureComponent{
    state = {
        roomName: ''
    }

    handleButtonPressNew = () => {
        const { roomName } = this.state;
        if (roomName.length > 0) {
          var usersRef = firebase.firestore().collection('SITES').doc(roomName);

          usersRef.get()
                  .then((docSnapshot) => {
                          if (docSnapshot.exists) {
                          usersRef.onSnapshot((doc) => {
                          // do stuff with the data
                        });
                    } else {
                        usersRef.set({
                          name: roomName,
                          latestMessage: {
                            text: "You have joined the room "+ roomName,
                            createdAt: new Date().getTime()
                          }
                        }) // create the document

                        firebase.firestore().collection("SITES").doc(roomName).collection("MESSAGES").add({
                          text: "You have joined the room "+ roomName,
                          createdAt: new Date().getTime(),
                          system: true
                        });
                      }
                    });

            // firebase.firestore().collection("SITES").doc(roomName)
            // .set({
            //     name: roomName,
            //     latestMessage: {
            //       text: "You have joined the room "+ roomName,
            //       createdAt: new Date().getTime()
            //     }
            //   });

              // firebase.firestore().collection("SITES").doc(roomName).collection("MESSAGES").add({
              //         text: "You have joined the room "+ roomName,
              //         createdAt: new Date().getTime(),
              //         system: true
              //       });

            //   .then(docRef => {
            //       console.log("docRef -> ",docRef);
            //       console.log("docRef docpath -> ",docRef._documentPath);
            //       console.log("docRef docpath parts -> ",docRef._documentPath._parts);
            //       console.log("docRef docpath parts[1] -> ",docRef._documentPath._parts[1]);
            //     docRef.collection("MESSAGES").add({
            //       text: "You have joined the room "+ roomName,
            //       createdAt: new Date().getTime(),
            //       system: true
            //     });
            //     this.props.navigation.navigate("HomeScreenWindow");
            //   });
        }
    }

    handleButtonPress= ()=>{
        const { roomName } = this.state;
        if (roomName.length > 0) {
          firebase.firestore()
            .collection("THREADS")
            .add({
              name: roomName,
              latestMessage: {
                text: "You have joined the room "+ roomName,
                createdAt: new Date().getTime()
              }
            })
            .then(docRef => {
              docRef.collection("MESSAGES").add({
                text: "You have joined the room "+ roomName,
                createdAt: new Date().getTime(),
                system: true
              });
              this.props.navigation.navigate("HomeScreenWindow");
            });
        }
      }

    render(){
        return(
            <View style={styles.addroomform}>
                <Text style={styles.header}>Add New Room</Text>
                <TextInput style={styles.textinput} placeholder="Add Room Name" underlineColorAndroid={'trasparent'} onChangeText={roomName => this.setState({roomName})}></TextInput>
                <TouchableOpacity style={styles.button} onPress={this.handleButtonPressNew}>
                    <Text style={styles.btntext}>Add Room</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    addroomform:{
        flex:1,
        justifyContent: 'center',
        paddingLeft: 60,
        paddingRight: 60,
    },
    header:{
        fontSize: 24,
        borderBottomWidth: 1,
        paddingBottom: 40,
        marginBottom:30,
        borderBottomColor: '#199187',
        borderBottomWidth:1,
    },
    textinput:{
        alignSelf:'stretch',
        height:40,
        marginBottom: 30,
        color: '#000000',
        borderBottomColor: '#f8f8f8',
        borderBottomWidth: 1,
    },
    button:{
        alignSelf: 'stretch',
        alignItems: 'center',
        padding: 20,
        backgroundColor: "#0099bd",
        marginTop: 30,
        marginBottom: 30,
    },
    btntext:{
        color: '#ffff',
        fontWeight:'bold',
    }
});