
import React, { PureComponent } from 'react';
import { View, Image,Alert, TouchableOpacity, Text, TextInput, KeyboardAvoidingView,ImageBackground} from 'react-native';
import StatusBarComponent from '../../Components/StatusBar'
import { StyleLogin } from '../../Styles/PageStyle';
import AppImages from '../../Images';
// import { CommonMethods,DeviceInfo,Dialog,GlobalVariable } from '../../Utils/Utils'
import I18n from '../../Utils/i18n';
import OfflineBarComponent from '../../Components/OfflineBar'
import {PostRequest} from '../../Utils/ServiceCalls';
import { Dialog,GlobalVariable} from '../../Utils/Utils';
import Constant from '../../Utils/Constant';
import Loader from '../../Components/Loader';
import AsyncStorage from '@react-native-community/async-storage';

export default class LoginScreen extends PureComponent {
    constructor(props) {
        super(props);

        this.state = { 
            mobileNumber:"",
            password:"",
            isLoadervisible:false  
        }; 
    }
    /**	   
	* @desc display alert dialog box
	* @param :title: string title of alert dialog
    * @param :message: string message of alert dialog
    * @param :callback: callback function on ok button click
	* @returns none
	*/
    displayAlertMsg=(title,message,callback)=>{
        Dialog.alertDialog(title,message,I18n.t('OK_BUTTON'),false,null).then(()=>{
            if(callback){
                callback();
            }
        })
    }  

    displayAlertMsg1=(title,message,callback)=>{
        Dialog.alertDialog(title,message,I18n.t('OK_BUTTON'),false,null).then(()=>{
            if(callback){
                callback();
            }
        })
    }
    pendingAlertMsg=(title,message,callback)=>{
        Dialog.alertDialog(title,message,I18n.t('OK_BUTTON'),true,null).then(()=>{
            if(callback){
                callback();
            }
        })
    }
    /**	   
	* @desc validate mobile number and password and display alert if wrong
	* @param :mobile: number mobile number of user
    * @param :password: string password of user 
	* @returns bool if valid credentials true else false
	*/ 
    validattion=(mobile,password)=>{ 
        if(!mobile || mobile.length<10){ 
            this.displayAlertMsg(I18n.t('LOGINPAGE.LOGIN_ERROR_TITLE'),I18n.t('LOGINPAGE.LOGIN_ERROR_MSG'));
            return false;
        }else if(!password){
            this.displayAlertMsg(I18n.t('LOGINPAGE.INVALID_PASSWORD_TITLE'),I18n.t('LOGINPAGE.INVALID_PASSWORD_MSG'));
            return false; 
        }

        return true
    }

    
    /**	   
	* @desc call API end point and authenticate user's credentials
	* @param :none
	* @returns none
    */     
    
    
    authenticate=async ()=>{ 
        let mobile=this.state.mobileNumber;
        let password=this.state.password;
        mobile=mobile.trim();
        password=password.trim();
        let isValidated=this.validattion(mobile,password); 
        //stop code execution due to invalid credentials
        if(!isValidated){
            return;        
        }

        let paramObj = {"contactNo":mobile,"password":password,"tokenId": await AsyncStorage.getItem('fcmToken')};
        let paramObject = {
            path: Constant.Api.LOGINPAGE.LOGIN,
            params: paramObj,
            showErrorMsg: true 
        };

        console.log("helloo",paramObj)
        //display loader first
        this.setState({isLoadervisible:true},()=>{
            PostRequest(paramObject).then((responseApisuccess) => {
                 console.log("postrequest",responseApisuccess)
                this.setState({isLoadervisible:false},()=>{
                    let response=responseApisuccess.data; 
                    //  alert(JSON.stringify(responseApisuccess.data))
                    //user does not exist  

                    if(!response  || !response.data ){
                        this.displayAlertMsg(I18n.t('LOGINPAGE.INVALID_CREDENTIALS_TITLE'),I18n.t('LOGINPAGE.INVALID_CREDENTIALS_MSG'),()=>{
                            // this.setState({mobileNumber:"",password:""});
                        });
                    }

                    //user exist
                    if(response.data){
                        console.log(response.data.crew_lead_mobile_app_access)
                        if(response.data.crew_lead_mobile_app_access == 1){
                            this.authenticationSuccess(response.data, password);
                            return;
                        }
                        else if((response.data.crew_lead_mobile_app_access == 2) ){
                            // this.showAlert()

                            this.pendingAlertMsg('Login Status','Request Rejected want to Request again?',()=>{this.activeAc(response.data.id)});

                        }else if((response.data.crew_lead_mobile_app_access == 3)){
                            // this.showAlert()

                            this.pendingAlertMsg('Login Status',' Request Pending Want to Send Request to approve?',()=>{this.activeAc(response.data.id)});

                        }else{
                            this.displayAlertMsg('Login Status','Not allow to access the app');
                        }

                    }
                });
            }, (error) => {
                this.setState({isLoadervisible:false},()=>{});
            });
        });
    }


     activeAc=(id)=>{

   
        let paramObj = 
        {
            "contactNo":this.state.mobileNumber ,
           
           } 
        
        

           
        let paramObject = {
            path: Constant.Api.LOGINPAGE.ACAprrove,
            params: paramObj,
            showErrorMsg: true 
        };

          console.log("Approve", paramObject)
         //display loader first
         this.setState({isLoadervisible:true},()=>{
            PostRequest(paramObject).then((responseApisuccess) => {
                console.log("Approve" ,responseApisuccess)
                this.setState({isLoadervisible:false},()=>{
                    let response=responseApisuccess.data; 
                    // check if any error during registration
                    if(!response || !response.status ){
                        this.disAlertDialogFailure();    
                    }
                    // user is already registered case
                  else   if(response.message){
                       
                    this.displayAlertMsg('Approve',response.message);
                    }
                        
                    //successfully registered
                 
                });
            }, (error) => {
                this.setState({isLoadervisible:false},()=>{
                     
                    this.disAlertDialogFailure();  
                });
            }); 
        });
    }
    /**
	* @desc called after user is authenticated successfully and save in local storage and redirect.
	* @param :successResponse: json object of response 
	* @returns none
	*/ 
    authenticationSuccess=async (successResponse, password)=>{
        GlobalVariable.USERID=successResponse.id;
        await AsyncStorage.setItem("Password", password);
        GlobalVariable.PASSWORD = password;
        console.log("login response "+successResponse)
        console.log("login market_id "+successResponse.market_id)
        GlobalVariable.MCPSMarketId = successResponse.market_id;
        GlobalVariable.MCPSMARKETID = successResponse.market_id;
        let stringifiedResponse=JSON.stringify(successResponse); 
        await AsyncStorage.setItem(Constant.LOCAL_STORAGE.LOGINRESPONSE, stringifiedResponse); 

        this.props.navigation.navigate("MainApp"); 
    }

    render() {        {console.log("vijay")}        return (
            <KeyboardAvoidingView style={StyleLogin.container}>
                <StatusBarComponent isHidden={true} />
                <OfflineBarComponent />
                {
					this.state.isLoadervisible &&
					<Loader msg={I18n.t('LOADING')} />
				}
                <ImageBackground fadeDuration={100} source={AppImages.SplashBackground} style={StyleLogin.bgImgContainer} resizeMode={'cover'}>
				</ImageBackground> 
                <View style={StyleLogin.formContainer}>
                    <View style={StyleLogin.formContainerInner}>
                        <Image
                            resizeMode="contain"
                            style={StyleLogin.logoImageStyle}
                            source={AppImages.Logo_Login_Image}
                        />
                        {/* <Text style={StyleLogin.logoText}>
                            {I18n.t('SPLASHPAGE.COMPANYNAME')}
                        </Text>
                        <Text style={[StyleLogin.logoText,StyleLogin.addMarginBottom]}>
                            {I18n.t('SPLASHPAGE.COMPANYNAME1')}
                        </Text>  */}
                        <Text style={StyleLogin.inputHeadingTitle}>
                            {I18n.t('LOGINPAGE.MOBILE')}
                        </Text>
                        <TextInput style={StyleLogin.inputField}
                            autoCapitalize="none"
                            keyboardType='numeric'
                            returnKeyType="next" 
                            maxLength = {10}
                            onChangeText={(phNumber) => this.setState({mobileNumber:phNumber})}
                            />
                        <Text style={[StyleLogin.inputHeadingTitle]}>
                            {I18n.t('LOGINPAGE.PASSWORD')}
                        </Text>
                        <TextInput
                            maxLength = {20}
                            style={[StyleLogin.inputField,StyleLogin.addMarginBottom]}
                            onChangeText={(password) => this.setState({password:password})}                              secureTextEntry />
                        <TouchableOpacity style={StyleLogin.buttonContainer} onPress={() => this.authenticate()}>
                            <Text style={StyleLogin.buttonText}>
                                {I18n.t('SPLASHPAGE.LOGIN')}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </KeyboardAvoidingView>
        );
    }
}