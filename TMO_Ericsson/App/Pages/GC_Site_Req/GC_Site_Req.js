import React, { Component } from 'react';
import ToolbarComponent from "../../Components/Toolbar";
import I18n from '../../Utils/i18n';
// import MultiSelect from 'react-native-multiple-select';
// import { MultiSelect } from '@progress/kendo-react-dropdowns';
import MultiSelect from 'react-native-multiple-select';
import { CommonMethods,DeviceInfo,Dialog,GlobalVariable } from '../../Utils/Utils'
import {Alert,FlatList,Button,RefreshControl,Modal,Text,TouchableOpacity,View,KeyboardAvoidingView,TextInput,Picker,ScrollView,TouchableWithoutFeedback, Image} from 'react-native';
import { StyleGCSiteLogin } from '../../Styles/PageStyle';
import { StyleDashboard } from '../../Styles/PageStyle';
import StatusBarComponent from '../../Components/StatusBar';
import OfflineBarComponent from '../../Components/OfflineBar';
import Constant from '../../Utils/Constant';
import Icon from 'react-native-vector-icons/Ionicons';
import {Colors} from '../../Styles/Themes'; 
import Placeholder,{Paragraph,Line, Media } from 'rn-placeholder';
import AsyncStorage from '@react-native-community/async-storage'; 
import {PostRequest} from '../../Utils/ServiceCalls'; 
import Loader from '../../Components/Loader'; 
import Toast from 'react-native-simple-toast';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import { PermissionsAndroid } from 'react-native';
import DatePicker from 'react-native-datepicker';
import RadioGroup,{Radio} from "react-native-radio-input";
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import GetLocation from 'react-native-get-location'
// import Geolocation from '@react-native-community/geolocation';
// import ImagePicker from 'react-native-image-picker';
const RNFS = require("react-native-fs");



export default class GCSiteLoingScreen extends Component {
  apiRetryAttempt=0;
  autoCompleteArrayKey="";
  autoCompleteAarrayFilter=[]; 
  seatIdArray=[];
  // states = { value: [] };
  site_id="";
  autoCompleteQueryStr="";
  autoCompletePlaceholder="";
  tempMarketObject={"id":"-1",name:I18n.t('SIGNUPPAGE.SELECT_MARKET')};
  tempWONObject={};
  tempPORObject={};
  selectedMarketId="-1"
  
  

  constructor(props) {
    super(props);
    this.currentDate=this.getCurrentDate(); 
		this.currentDateMMDDYYYY = new Date(CommonMethods.chaangeDateFormat(this.currentDate));
    this.state = { 
      cressIdds: "",
      isModalVisible: false,
      selectedItems: [],
      selectedPlannedSOW: [],
      selectedItemsPOR: [],
      fromDate:this.currentDate,
      toDate:this.currentDate,
      isLoadervisible:false,
      isAutoCompletePopupVisible:false,
      autoCompleteFilteredArray:[], 
      contactName:"",
      contactEmail:"",
      contactNumber:"", 
      GCCompanyName:"",
      site_id:I18n.t('GCSITELOGINPAGE.ENTER_SITEID'),
      accountArray:[{"id":"-1",name:I18n.t('SIGNUPPAGE.SELECT_ACCOUNT')}],
      tmoArray:[{"id":"-1",name:"Select TMO Deployment Manger"}],
      sowArray:[{"id":"-1",name:I18n.t('GCSITELOGINPAGE.SELECT_SOW')}],
      sowArray:[],
      ttIDMobile:"",
      sowArrayDetails:[],
      marketArray:[this.tempMarketObject], 
      mainIdArray: [{"site_id":"-1","site_name":"Select Main Id","accountId":"-1","marketId":"-1"}],
      WONArray: [],
    PORData: [],
    TempPORData: [],
	  selectedItemsPors:[],
	  PORDatas: [],
      selectedMainId:"-1",
      selectedWON: "-1",
      selectedAcount:"-1", 
      selectedMarket:"-1", 
      SelectedSOW:"-1",
      selectedTMO:"-1",
      SelectedSOWDetails:"",
      selectedRFApproval:"-1",
      selectedServiceAffected:"-1",
      selectedDTO:"-1",
      selectedNFSDLogin:"1",
      estimatedTime:"",
      preWorkID:"",
      emiLoginStr:"",
      SowDetail:"",
      loginType: "",
      AdditionalEmailId:"",
      picturePath: "",
      buttonBackground: "#ed462d",

	  selectFromModal: false,
	  selectFromModal1: false,
      isMainIDData: false,
      isWONData: false,
      isPORData: false,
      isAccountData: false,
      isMarketData: false,
      isDayMOP: false,
      lat: "",
      lan: "",
      nestedTime:"",
      SOWComment: "",
      dayMOP:"",
      dayMOPComment:"",
      RFApproved:"",
	  emailIdUser:"",
	  startHH:"00",
	  startMM:"00",
	  endHH:"00",
	  endMM:"00",

    isParam:false,params:"",
	  
      
      planSowData: [

      {
        id: '1',
        name: 'E911-Call Test',
      },
      {
        id: '2',
        name: 'CX',
      },
      {
        id: '3',
        name: 'IX',
      },
      {
        id: '4',
        name: 'Troubleshooting',
      },

        ]
      
    };

    
  }

  onSelectedItemsChange = selectedItems => {

    this.setState({ selectedItems },()=>{
      let TempPORData  = this.state.PORData.filter((item)=>{
                  for(let it of selectedItems){
                     if(item.name==it){
                        return true
                        }
                  }
                   return false
          })
          console.log( "tttt", this.state.TempPORData )
          this.setState({isPORData: true,TempPORData:TempPORData,selectedItemsPORs:[]})

    });
     

   
    console.log("tttt",selectedItems)
  };



  onSelectedItemsChangePLannedSow = selectedPlannedSOW => {
console.log(selectedPlannedSOW)
    this.setState({ selectedPlannedSOW });
    console.log(selectedPlannedSOW)
  };

  onSelectedItemsChangePOR = selectedItemsPORs => {
    console.log(selectedItemsPORs)
        this.setState({ selectedItemsPORs });
        console.log(selectedItemsPORs)
      };


  // async requestLocationPermission(){
  //  console.log("step 1")
  //   try {
  //     console.log("step 1 1")
  //     const granted = await PermissionsAndroid.request(
        
  //       PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  //       {
  //         'title': 'Example App',
  //         'message': 'Example App access to your location '
  //       }
  //     )
  //     console.log("step 1 2")
  //     if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //       console.log("You can use the location",navigator.geolocation.watchPosition)
  //       navigator.geolocation.watchPosition((position)=>{
  //     console.log(position.coords.latitude)
  //     this.state.lat = position.coords.latitude
  //     this.state.lan = position.coords.longitude
  //     console.log(this.state.lan, this.state.lat)
  //     console.log("step 1 3")
  //     this.getMainId();
    
      
  //   });
  //     } else {
  //       console.log("location permission denied")
  //       alert("Location permission denied");
  //     }
  //   } catch (err) {
  //     console.warn(err)
  //   }
  //   console.log("step 1 222222222222222222222")
  // }
  
  //  async componentDidMount() {
  //    await requestLocationPermission()
  //   }

  

  getDetailData(id){
   
    let paramObj={"id":id};
      let paramObject = {
        path: Constant.Api.POSTCHECKREQUEST.GetPreCheckInfo,
        showErrorMsg: true,
        params: paramObj 
      }
  
      console.log("prrrreeeeee",paramObject)
  
      PostRequest(paramObject).then((responseApisuccess) => {
      this.setState({isLoadervisible:false},()=>{ 
        let response=responseApisuccess.data; 
        if(response==null || response==""){ 
          let alertTitle= I18n.t('GCSITELOGINPAGE.NO_PRECHCKDETAIL_TITLE');
          let alertMessage= I18n.t('GCSITELOGINPAGE.NO_PRECHCKDETAIL_MSG');
          let buttonStr= I18n.t('OK_BUTTON');
          Dialog.alertDialog(alertTitle, alertMessage, buttonStr, true, null); 
          return;
        }
        if(response!=null && response!=""){
          console.log("gccc",response)
      
       this.setState({details:response.data,itemId:id},()=>{
        console.log("gccc d",this.state.details)

       })
       
     
         
       
         //  let alertTitle= I18n.t('GCSITELOGINPAGE.NO_PRECHCKDETAIL_TITLE');
         //  let alertMessage= I18n.t('GCSITELOGINPAGE.NO_PRECHCKDETAIL_MSG');
         //  let buttonStr= I18n.t('OK_BUTTON');
         //  Dialog.alertDialog(alertTitle, alertMessage, buttonStr, true, null); 
        }
      })  
    })
    } 

  async componentDidMount()
  {  
    let params= this.props.navigation.state.params
    console.log("gccc",params.item.Id)
       this.getDetailData(params.item.Id)     
   
//  this.requestForLoc()
//     console.log("step 2")
//     await AsyncStorage.setItem("picturePath", "");
     this.getUsersLocalDetails();
//      this.getAccount()
//      this.getTMO()
      this.getPlannedSOWDetails()
  }
      
 async requestForLoc(){
    const granted = await PermissionsAndroid.check( PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION );
    if (granted) {
      GetLocation.getCurrentPosition({
        enableHighAccuracy: true,
        timeout: 15000,
    })
    .then(location => {
        console.log("location raj",location);
         this.getMainId(location.latitude,location.longitude)
    })
    .catch(error => {
        const { code, message } = error;
  
        console.warn("location raj", message);
        GetLocation.openGpsSettings();
    })
    } 
    else {
      this.requestLocationPermission();
    }
  }
  
 async requestLocationPermission() 
  {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': 'TMOEricsson  App',
          'message': 'TMOEricsson App access to your location '
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
         this.requestForLoc()
      } else {
       

      }
    } catch (err) {
      console.warn(err)
    }
  }
  goToPreviousPage = () =>{
    this.props.navigation.state.params.onNavigateBack();
    this.props.navigation.goBack()
    return true;
  }
  

  getUsersLocalDetails=async()=>{
    console.log("Main 1")
    const userDetails = await AsyncStorage.getItem(Constant.LOCAL_STORAGE.LOGINRESPONSE);

    try
    {
      console.log(JSON.parse(userDetails))
      let userDetailsObject=JSON.parse(userDetails);
      let idss= userDetailsObject.id
      this.setState({cressIdds: idss});
      
    }
    catch(err)
    {
       
    }
  }

  handleOnNavigateBack = async (refreshPage) => {
    let returnFrom  = await AsyncStorage.getItem("okCamera");
    if(returnFrom !== "ok")
      return;
    let picturePath = await AsyncStorage.getItem("picturePath");
    await this.setState({picturePath: picturePath});

    if(picturePath !== null && picturePath !== "")
    {
      this.setState({buttonBackground: "#006600"});
    }
    else
    {
      this.setState({buttonBackground: "#ed462d"});
    }
  }

  setSelectFromModalVisibility(visibility)
  {
    this.setState({selectFromModal: visibility});
  }
  
  openCamera= async()=>{
    this.setSelectFromModalVisibility(false);
    // await AsyncStorage.setItem("snapDataBase64", "");
    this.props.navigation.navigate('CaptureNFSDSnapShot',{onNavigateBack: this.handleOnNavigateBack});
  }

  async openGallery()
  {
    this.setSelectFromModalVisibility(false);
    try
		{
		  DocumentPicker.show( {filetype: [DocumentPickerUtil.images()]},async (error,res) => 
			{
				if(res != null)
				{
          // let imageData = "";
          // await RNFS.readFile(res.uri, "base64").then(async(data) => {
          //   // binary data
          //      imageData = data;
          //   });
          await this.setState({picturePath:res.uri});
          this.setState({buttonBackground: "#006600"});
        }
        // else
        // {
        //   this.setState({buttonBackground: "#ed462d"});
        // }
			});
    }
    catch(error1)
    {
      Toast.show(error1, Toast.LONG);
    }
    
  }


    
  onSelectedItemsChangeSOWDetail  = SelectedSOWDetails => {
    console.log("SelectedSOWDetails",SelectedSOWDetails)
        this.setState({ SelectedSOWDetails });
        console.log(SelectedSOWDetails)  
      };

  callGetSiteApi=()=>{
    console.log("Main 2")
      // let paramObject = {
      //     path: Constant.Api.LCLOGIN.GETSITES,
      //     showErrorMsg: true 
      // };

      //display loader first
      // this.setState({isLoadervisible:true},()=>{
      //     PostRequest(paramObject).then((responseApisuccess) => {
             
      //             let response=responseApisuccess.data; 
      //             // unable to fetch GC SITES ID
      //             if(!response || !response.status || response.status==0 || !response.data){ 
      //                 //if due to ary reason failed to fetch seatid retry again in case of error
      //                 if(this.apiRetryAttempt<3){
      //                     this.apiRetryAttempt++;
      //                     this.callGetSiteApi();
      //                     return;
      //                 }else{
      //                   this.setState({isLoadervisible:false},()=>{ 
      //                     let alertTitle= I18n.t('GCSITELOGINPAGE.ERROR');
      //                     let alertMessage= I18n.t('GCSITELOGINPAGE.GET_SITE_ERROR');
      //                     let buttonStr= I18n.t('OK_BUTTON');
      //                     Dialog.alertDialog(alertTitle, alertMessage, buttonStr, true, null); 
      //                   }); 

      //                   return;
      //                 } 
      //             }
                   
      //             // no seat id available
      //             if(response.data.length==0){ 
      //               let alertTitle= I18n.t('GCSITELOGINPAGE.NO_SITES_TITLE');
      //               let alertMessage= I18n.t('GCSITELOGINPAGE.NO_SITE_MSG');
      //               let buttonStr= I18n.t('OK_BUTTON');
      //               this.setState({isLoadervisible:false},()=>{ 
      //                 Dialog.alertDialog(alertTitle, alertMessage, buttonStr, true, null); 
      //               }) 
      //               return;
      //             }

      //             //got seat id
                 
      //               this.apiRetryAttempt=0; 
      //               this.seatIdArray= response.data;  
      //               // this.getMainId();
      //               // this.getAccount();
      //               // this.getPlannedSOWDetails();
                   
      //     }, (error) => { 
      //         this.setState({isLoadervisible:false},()=>{});
      //     }); 
      // });


      
                    this.getAccount();
                    this.getPlannedSOWDetails();
  }


  getAccount=()=>{  
    let paramObject = {
        path: Constant.Api.SIGNUP.GETACCOUNT,
        showErrorMsg: true 
    };

     //display loader first
     this.setState({isLoadervisible:true},()=>{
        PostRequest(paramObject).then((responseApisuccess) => {
           console.log("Account data")
                let response=responseApisuccess.data; 
                // unable to fetch accounts
                if(!response || !response.status || response.status==0 || !response.data){ 
                    //if due to ary reason failed to fetch account retry again in case of error
                    if(this.apiRetryAttempt<3){
                        this.apiRetryAttempt++;
                        this.getAccount();
                        return;
                    }
                    this.setState({isLoadervisible:false},()=>{ });
                    return;
                }
                  
                if(response.data.length==0){
                  this.setState({isLoadervisible:false},()=>{ 
                      this.displayAlertMsg(
                          I18n.t('SIGNUPPAGE.NO_ACCOUNT_TITLE'),
                          I18n.t('SIGNUPPAGE.NO_ACCOUNT_MSG'),null
                      ); 
                  })
                  return;
                }

                //got accounts 
                this.apiRetryAttempt=0; 
                let tempAccountArray= [...this.state.accountArray, ...response.data]; 
                this.setState({accountArray:tempAccountArray},()=>{
                  this.getPlannedSOW();
                });
                  
             
        }, (error) => {
          //alert(JSON.stringify(error));
            this.setState({isLoadervisible:false},()=>{});
        }); 
    });
}


getTMO=()=>{  
  let paramObject = {
      path: Constant.Api.GetTmoEngineer,
      showErrorMsg: true 
  };

   //display loader first
   this.setState({isLoadervisible:true},()=>{
      PostRequest(paramObject).then((responseApisuccess) => {
         console.log("Account data")
              let response=responseApisuccess.data; 
              // unable to fetch accounts
              if(!response || !response.status || response.status==0 || !response.data){ 
                  //if due to ary reason failed to fetch account retry again in case of error
                  if(this.apiRetryAttempt<3){
                      this.apiRetryAttempt++;
                      this.getTMO();
                      return;
                  }
                  this.setState({isLoadervisible:false},()=>{ });
                  return;
              }
                
              if(response.data.length==0){
                this.setState({isLoadervisible:false},()=>{ 
                    this.displayAlertMsg(
                        I18n.t('SIGNUPPAGE.NO_ACCOUNT_TITLE'),
                        I18n.t('SIGNUPPAGE.NO_ACCOUNT_MSG'),null
                    ); 
                })
                return;
              }

              //got accounts 
              this.apiRetryAttempt=0; 
              let tempAccountArray= [...this.state.tmoArray, ...response.data]; 
              this.setState({tmoArray:tempAccountArray},()=>{
                // this.getPlannedSOW();
              });
                
           
      }, (error) => {
        //alert(JSON.stringify(error));
          this.setState({isLoadervisible:false},()=>{});
      }); 
  });
}


 /**	   
	* @desc display alert dialog box
	* @param :title: string title of alert dialog
    * @param :message: string message of alert dialog
    * @param :callback: callback function on ok button click
	* @returns none
	*/
  displayAlertMsg=(title,message,callback)=>{
      
    Dialog.alertDialog(title,message,I18n.t('OK_BUTTON'),false,null).then(()=>{
        if(callback){
            callback();
        }
    })
}  

displayAlertMsg1=(title,message,callback)=>{
      
  Dialog.alertDialog(title,message,I18n.t('OK_BUTTON'),true,null).then(()=>{
      if(callback){
          callback();
      }
  })
} 
 
getPlannedSOW=()=>{  
    let paramObject = {
        path: Constant.Api.LCLOGIN.GETSOW,
        showErrorMsg: true 
    };
 
   //display loader first
   this.setState({isLoadervisible:true},()=>{
      PostRequest(paramObject).then((responseApisuccess) => {
          this.setState({isLoadervisible:false},()=>{ 
              let response=responseApisuccess.data; 
              // unable to fetch SOW
              if(!response || !response.status || response.status==0 || !response.data){ 
                  //if due to ary reason failed to fetch Planned Sow retry again in case of error
                  if(this.apiRetryAttempt<3){
                      this.apiRetryAttempt++;
                      this.getPlannedSOW();
                      return;
                  }else{
                    this.displayAlertMsg(
                      I18n.t('GCSITELOGINPAGE.ERROR'),
                      I18n.t('GCSITELOGINPAGE.GET_SOW_ERROR'),null
                    ); 
                    return;
                  } 
              }
                
              if(response.data.length==0){ 
                  this.displayAlertMsg(
                      I18n.t('GCSITELOGINPAGE.NO_SOW_TITLE'),
                      I18n.t('GCSITELOGINPAGE.NO_SOW_MSG'),null
                  ); 
                  return;
              }

              //got accounts
              if(response.data && response.data.length>0){
                 this.apiRetryAttempt=0; 
                //  let tempSOWArray= [...this.state.sowArray, ...response.data]; 
                //  this.setState({sowArray:tempSOWArray},()=>{});

                 this.setState({sowArray:response.data});
              } 
          }); 
      }, (error) => {
          this.setState({isLoadervisible:false},()=>{});
      }); 
  });
}

 
getPlannedSOWDetails=()=>{  
  let paramObject = {
      path: Constant.Api.LCLOGIN.GETSOWDetail,
      showErrorMsg: true 
  };

 //display loader first
 this.setState({isLoadervisible:true},()=>{
    PostRequest(paramObject).then((responseApisuccess) => {
        this.setState({isLoadervisible:false},()=>{ 
            let response=responseApisuccess.data; 
            // unable to fetch SOW
            if(!response || !response.status || response.status==0 || !response.data){ 
                //if due to ary reason failed to fetch Planned Sow retry again in case of error
                if(this.apiRetryAttempt<3){
                    this.apiRetryAttempt++;
                    this.getPlannedSOWDetails();
                    return;
                }else{
                  this.displayAlertMsg(
                    I18n.t('GCSITELOGINPAGE.ERROR'),
                    I18n.t('GCSITELOGINPAGE.GET_SOW_ERROR'),null
                  ); 
                  return;
                } 
            }
              
            if(response.data.length==0){ 
                this.displayAlertMsg(
                    I18n.t('GCSITELOGINPAGE.NO_SOW_TITLE'),
                    I18n.t('GCSITELOGINPAGE.NO_SOW_MSG'),null
                ); 
                return;
            }

            //got accounts
            if(response.data && response.data.length>0){
               this.apiRetryAttempt=0; 
              //  let tempSOWArray= [...this.state.sowArray, ...response.data]; 
              //  this.setState({sowArray:tempSOWArray},()=>{});
           let tmp=[]
            if(Array.isArray(response.data)){
               tmp= response.data.map((item,i)=>{
                  return {id:item.Id,name:item.name}
               })
            }            
               this.setState({sowArrayDetails:tmp});
            } 
        }); 
    }, (error) => {
        this.setState({isLoadervisible:false},()=>{});
    }); 
});
}


getMarket=(accountID)=>{ 
  console.log("market 1 : ",accountID)
    if(accountID==-1){
        let tempMarketArray= [this.tempMarketObject];
        this.setState({selectedAcount: accountID,marketArray:tempMarketArray},()=>{}); 
        return;
    } 
     
    let paramObject = {
        path: Constant.Api.SIGNUP.GETMARKET,
        showErrorMsg: true,
        params: {"account_id":accountID}, 
    };

     //display loader first
     this.setState({isLoadervisible:true,selectedAcount:accountID},()=>{
        PostRequest(paramObject).then((responseApisuccess) => { 
            this.setState({isLoadervisible:false},()=>{
                let response=responseApisuccess.data; 
                console.log("market 2",response)
                // unable to fetch market
                if(!response || !response.status || response.status==0 || !response.data){
                    //if due to ary reason failed to fetch market retry again in case of error
                    if(this.apiRetryAttempt<3){
                        this.apiRetryAttempt++;
                        this.getMarket(accountID);
                        return;
                    } 
                    this.makeEmptyMarketArray(); 
                    return;
                }

                if(response.data.length==0){ 
                    this.makeEmptyMarketArray();  
                    this.displayAlertMsg(
                        I18n.t('SIGNUPPAGE.NO_MARKET_TITLE'),
                        I18n.t('SIGNUPPAGE.NO_MARKET_MSG'),null
                    );
                    return;
                }
                    
                //got accounts
                if(response.data && response.data.length>0){ 
                    this.apiRetryAttempt=0; 
                    response.data.unshift(this.tempMarketObject); 
                    let tempMarketArray= response.data;
                    this.setState({marketArray:tempMarketArray},()=>{});
                } 
            });      
        }, (error) => {  
            this.makeEmptyMarketArray(); 
        }); 
    });
}

makeEmptyMarketArray=()=>{
    let tempMarketArray= [this.tempMarketObject];
    this.setState({isLoadervisible:false,marketArray:tempMarketArray},()=>{});
}

renderMarketPicker=()=>{
    let pickerObject=this.state.marketArray.map((marketObj,i) => {
      let key=i+""+new Date().toLocaleString()+"_marketPickerList"
      return( 
        <Picker.Item label={marketObj.name} value={marketObj.id}  key={key}/>        
      );
    });
  return (pickerObject);
}


getMainId=(lat,lng)=>{  
  console.log("Main 3")
  let paramObject = {
      
      path: Constant.Api.GetSites,
      showErrorMsg: true ,
      params: {
                // "siteId":this.state.siteId,
                "latitude":lat,
                "longitude": lng}, 
  };
  console.log("Main 4", paramObject)

   //display loader first
   this.setState({isLoadervisible:true},()=>{
      PostRequest(paramObject).then((responseApisuccess) => {
        console.log("Main 5")
              let response=responseApisuccess.data; 
              console.log(response)
              // unable to fetch accounts
              if(!response || !response.status || response.status==0 || !response.data){ 
                  //if due to ary reason failed to fetch account retry again in case of error
                  if(this.apiRetryAttempt<3){
                      this.apiRetryAttempt++;
                      // this.getAccount();
                      return;
                  }
                  this.setState({isLoadervisible:false},()=>{ });
                  return;
              }
                
              if(response.data.length==0){
                this.setState({isLoadervisible:false},()=>{ 
                    this.displayAlertMsg(
                        I18n.t('SIGNUPPAGE.NO_ACCOUNT_TITLE'),
                        I18n.t('SIGNUPPAGE.NO_ACCOUNT_MSG'),null
                    ); 
                })
                return;
              }

              //got accounts 
              this.apiRetryAttempt=0; 
              this.state.mainIdArray = response.data
              console.log(this.state.mainIdArray)
              for(let i = 0; i<this.state.mainIdArray.length; i++){
                this.state.mainIdArray[i].site_name = this.state.mainIdArray[i].site_id;
              }
              // this.setState({isMainIDData: true})
              this.setState({isLoadervisible: false})
              // let tempAccountArray= [...this.state.accountArray, ...response.data]; 
              // this.setState({site:tempAccountArray},()=>{
              //   // this.getPlannedSOW();
              // });
                
           
      }, (error) => {
        //alert(JSON.stringify(error));
          this.setState({isLoadervisible:false},()=>{});
      }); 
  });
}


  getWorkOrderNumber=(siteId)=>{ 
    this.state.WONArray = []
    this.state.selectedMainId = siteId
    console.log("WON 1 ",siteId)
    if(siteId==-1){
        let tempMarketArray= [this.tempMarketObject];
        this.setState({selectedAcount: siteId,marketArray:tempMarketArray},()=>{}); 
        return;
    } 
     
    let paramObject = {
        path: Constant.Api.GetWonPor,
        showErrorMsg: true,
        params: {"siteId":siteId}, 
    };
    console.log("WON 2 ")
     //display loader first
     this.setState({isLoadervisible:true},()=>{
        PostRequest(paramObject).then((responseApisuccess) => { 
          console.log("WON 3 ")
            this.setState({isLoadervisible:false},()=>{
              console.log("WON 4 ")
                let response=responseApisuccess.data; 
                console.log("WON 5 ",response)
                // unable to fetch market
                if(!response || !response.status || response.status==0 || !response.data){
                    //if due to ary reason failed to fetch market retry again in case of error
                    if(this.apiRetryAttempt<3){
                        this.apiRetryAttempt++;
                        this.getWorkOrderNumber(siteId);
                        return;
                    } 
                    this.makeEmptyMarketArray(); 
                    return;
                }

                if(response.data.length==0){ 
                    this.makeEmptyMarketArray();  
                    this.displayAlertMsg(
                        I18n.t('SIGNUPPAGE.NO_MARKET_TITLE'),
                        I18n.t('SIGNUPPAGE.NO_MARKET_MSG'),null
                    );
                    return;
                }
                    
                //got accounts
                console.log("WON 6 ")
                this.state.PORData = []
                if(response.data && response.data.length>0){ 
                  console.log("WON 7 ")
                    this.apiRetryAttempt=0; 
                    // response.data.unshift(this.tempWONObject); 
                    let tempWONArray= response.data;
                    this.setState({WONArray:tempWONArray,isWONData: true},()=>{});
                    console.log("WON 8 ",response.data)
                    
                    response.data.forEach(resp=>{
					  console.log(resp.por)
					  this.state.PORData.push({"id":resp.won,"name":resp.won,"children":this.getPORData(resp)})

                      
                      // console.log(arr2)
                      // this.state.PORData = arr2
                      console.log(this.state.PORData)
                      // this.setState({isPORData: true})
                    //   if(resp.por)
                    //   this.tempPORObject.push
                    })
                } 
            });      
        }, (error) => {  
            this.makeEmptyMarketArray(); 
        }); 
    });
    this.state.mainIdArray.forEach((marketObj,index) => {
      
      if(marketObj.site_id == siteId){
        this.state.selectedAcount = marketObj.accountId
        this.state.selectedMarket = marketObj.marketId
        console.log("asdfsdf",this.state.selectedAcount)
        this.setState({isAccountData: true})
        this.getMarket(this.state.selectedAcount)
      }
    })
    
}


getPORData=(resp)=>{
	this.state.PORDatas= []
	let arr = resp.por.split(",")
                      let arr2 = [];
                      console.log(arr)
                      for(let i=0; i<arr.length; i++){
					   console.log("arrrray ",arr[i])
					//    return ({"id":resp.won+'/'+arr[i],"name":arr[i]})
                      this.state.PORDatas.push({"id":resp.won+'/'+arr[i],"name":arr[i]})
                        
					  }
					  return this.state.PORDatas
}

dayMOPVALUEChange=(id)=>{
  console.log(id)
  if(id == 1){
    this.setState({isDayMOP: true})
    this.state.selectedDTO = id
  }
}

renderMainIdPicker=()=>{
  console.log(this.state.mainIdArray)
  let pickerObject=this.state.mainIdArray.map((marketObj,i) => {
    // console.log(marketObj)
    let key=i+""+new Date().toLocaleString()+"_marketPickerList"
    return( 
      <Picker.Item label={marketObj.site_name} value={marketObj.site_id}  key={key}/>        
    );
  });
return (pickerObject);
}

renderWorkOrderNUmberPicker=()=>{
  let pickerObject=this.state.WONArray.map((marketObj,i) => {
    console.log("WON 11" ,marketObj)
    let key=i+""+new Date().toLocaleString()+"_marketPickerList"
    return( 
      <Picker.Item label={marketObj.won} value={marketObj.won}  key={key}/>        
    );
  });
return (pickerObject);
}

renderPORPicker=()=>{
  let pickerObject=this.state.marketArray.map((marketObj,i) => {
    let key=i+""+new Date().toLocaleString()+"_marketPickerList"
    return( 
      <Picker.Item label={marketObj.name} value={marketObj.id}  key={key}/>        
    );
  });
return (pickerObject);
}

renderSOWPicker=()=>{
  let pickerObject=this.state.sowArray.map((sowObject,i) => {
    let key=i+""+new Date().toLocaleString()+"_sowPickerList"
    return( 
      <Picker.Item label={sowObject.name} value={sowObject.id}  key={key}/>        
    );
  });
return (pickerObject);
}

renderSOWDetailsPicker=()=>{
  let pickerObject=this.state.sowArrayDetails.map((sowObject,i) => {
    let key=i+""+new Date().toLocaleString()+"_sowPickerList"
    return( 
      <Picker.Item label={sowObject.name} value={sowObject.id}  key={key}/>        
    );
  });
return (pickerObject);
}


renderTMOPicker=()=>{
  let pickerObject=this.state.tmoArray.map((accountObj,i) => {
    console.log("asdfsdf",accountObj)
    let key=i+""+new Date().toLocaleString()+"_accountPickerList"
    return( 
      <Picker.Item label={accountObj.name} value={accountObj.Id}  key={key}/>        
    );
  });
  return (pickerObject);
}


renderDateComponent=(dateStateTitle)=>{
  console.log(dateStateTitle)
		
  return(<DatePicker
    style={StyleDashboard.datePickerContainerStyle}
    date={this.state[dateStateTitle]}
    mode="date"
    showIcon={true}
    placeholder="select date"
    format="MM-DD-YYYY"
    minDate="01-01-2016"
    maxDate={this.currentDate}
    confirmBtnText="Confirm"
    cancelBtnText="Cancel"
    customStyles={{
      dateIcon: StyleDashboard.datepickerIcon,
      dateInput:StyleDashboard.datepickerInput  
    }}
    onDateChange={(selectedDate) => { 
      let dateObj={};
      dateObj[dateStateTitle]=selectedDate; 
      this.setState(dateObj); 
    }}
  /> )
}

// getSelectedItemsExt(value){
//   console.log(value)
// }

renderAccountPicker=()=>{
  let pickerObject=this.state.accountArray.map((accountObj,i) => {
    console.log("asdfsdf",accountObj)
    let key=i+""+new Date().toLocaleString()+"_accountPickerList"
    return( 
      <Picker.Item label={accountObj.name} value={accountObj.id}  key={key}/>        
    );
  });
  return (pickerObject);
}


  filterAutoComplete(query) {  
    if (query === '') {
      this.autoCompleteQueryStr="";
      this.setState({autoCompleteFilteredArray:[]}); 
      return;
    }
    this.autoCompleteQueryStr=query;
    const regex = new RegExp(`${query.trim()}`, 'i');
    let filteredArray= this.autoCompleteAarrayFilter.filter(filteredObj => filteredObj[this.autoCompleteArrayKey].search(regex) >= 0);
    this.setState({autoCompleteFilteredArray: filteredArray}) 
  }


  showHideAutoCompletePopup=(bool,keyTofilter,arrayName,placeholderText)=>{
    if(bool){
      this.autoCompletePlaceholder=placeholderText;
      this.autoCompleteArrayKey=keyTofilter;
      this.autoCompleteAarrayFilter=this[arrayName]; 
    }else{
        this.resetAutoCompleteList();
    }
    this.autoCompleteQueryStr="";
    this.setState({isAutoCompletePopupVisible:bool,autoCompleteFilteredArray:[]});
  }

  resetAutoCompleteList=()=>{
    this.autoCompleteQueryStr="";
    this.autoCompleteArrayKey="";
    autoCompleteAarrayFilter=[];  
  }


  renderPlaceHolder(){
    let array=[];
		for(var i=0;i<3;i++){ 
			array.push( 
        
        <Placeholder animation="fade" key={i}> 
          <View style={StyleGCSiteLogin.AC_DropdownText}>
            <Line style={StyleGCSiteLogin.lineDateTimeButton} />
          </View> 
        </Placeholder>
      )
    } 
    return array;
  }
  
  setAutoCompleteValue(selectedObject){
      //set site id values
      if(this.autoCompleteArrayKey=="site_id"){ 
          this.site_id=selectedObject.id;
          let acountId="-1";
         
          for(var i=0; i<this.seatIdArray.length; i++){
             if(this.seatIdArray[i].id==selectedObject.id){
              acountId=this.seatIdArray[i].m_account_id;
              this.selectedMarketId=this.seatIdArray[i].m_market_id;
              break;
             }
          }
          
          this.setState({site_id:selectedObject.site_id,selectedAcount:acountId,selectedMarket:this.selectedMarketId},()=>{
            this.showHideAutoCompletePopup(false);
            // this.getMarket(acountId);
          }); 
      } 
  }
  
  renderFlatListItem=({item},index)=>{ 
    let indexItem=index+""+new Date().toLocaleString()+"_itemflatlist"
		return( 
			<View  key={indexItem}> 
          <TouchableOpacity onPress={() => this.setAutoCompleteValue(item)}>
				    <Text style={StyleGCSiteLogin.AC_DropdownText}>{item[this.autoCompleteArrayKey]}</Text>
         </TouchableOpacity>
			</View> 
		); 
  }
  

  getCurrentDate=()=>{
		var now = new Date(); 
		let currentDateTime = new Date()
		let month = currentDateTime.getMonth() + 1
		if (month < 10) {
			month = '0' + month; 
		}
		
		let day = currentDateTime.getDate()
		if (day < 10) {
			day = '0' + day; 
		}
		
		let year = currentDateTime.getFullYear()
		let fullDate=month + "/" + day + "/" + year;
		return fullDate;
  }
  
  selectedSnapShot = () =>{
    if(this.state.snapDataBase64 !== "")
    {
      return(
        <Text style={StyleGCSiteLogin.snapshotFileText}
              numberOfLines= {1}>
              {"NFSD SnapShot Captured"}
        </Text>
      )
    }
    else
    {
      return(
        <View />
      )
    }
  }

  selectparmater=(parm)=>{
    console.log(parm)

  }
  getUtcDate=(start)=>{
		const start_date = start;
					   
	  
	 
	   const start_dt = start_date.getUTCDate() >= 10 ? start_date.getUTCDate() : "0" + start_date.getUTCDate();
	   const start_month = start_date.getUTCMonth() + 1 >= 10 ? start_date.getUTCMonth() + 1 : "0" + (start_date.getUTCMonth() + 1); //Months are zero based
	   const start_year = start_date.getUTCFullYear();
	   const start_hr = start_date.getUTCHours() >= 10 ? start_date.getUTCHours() : "0" + start_date.getUTCHours(); 
	   const start_min = start_date.getUTCMinutes() >= 10 ? start_date.getUTCMinutes() : "0" + start_date.getUTCMinutes();
	   const start_sec = start_date.getUTCSeconds() >= 10 ? start_date.getUTCSeconds() : "0" + start_date.getUTCSeconds();
	   
		const start_timestamp = start_year + "/" + start_month + "/" + start_dt ;
	 
		return start_timestamp 
	   }
  submitGcLogin= async ()=>{ 
    let Details=""
     if(Array.isArray(  this.state.SelectedSOWDetails))
    this.state.SelectedSOWDetails.map((itm,i)=>{
      if(i==0){
        Details=Details+itm
      } else{
        Details=Details+","+itm
      }
    })
 let par={
  "sowDetails":Details ,
  "sowComment":this.state.SOWComment,
  "nestingTime":this.state.nestedTime,
  "id": this.state.itemId,
  "crewId": this.state.cressIdds
 }
      let paramObject = {
        path: Constant.Api.PreCheckReq,
        showErrorMsg: true,
        params: par 
    };
    console.log("ttttt",paramObject)

    // alert(Constant.Api.LCLOGIN.GCSITELOGIN)

     //display loader first
     this.setState({isLoadervisible:false},()=>{
        PostRequest(paramObject).then((gciLoginSuccess) => { 

          // alert(JSON.stringify(gciLoginSuccess))
            this.setState({isLoadervisible:false},async()=>{ 
                 console.log("response", gciLoginSuccess )
                 if(gciLoginSuccess.data.status==1)
                 {
                  let alertTitle= I18n.t('GCSITELOGINPAGE.GCLOGINSUCCESS');
                  let alertMessage= I18n.t('GCSITELOGINPAGE.GCLOGINSUCCESS_MSG');
                  let buttonStr= I18n.t('OK_BUTTON');

                  Alert.alert(
                    alertTitle,
                    gciLoginSuccess.data.message,
                    [
                      
                      {text: buttonStr, onPress: () => {this.props.navigation.goBack();}},
                    ],
                    {cancelable: false},
                  );  

                 // Toast.show(gciLoginSuccess.data.message, Toast.LONG);
                  await AsyncStorage.setItem("snapDataBase64", "");
                  this.goToPreviousPage();


                 }else{
                   
                  this.displayAlertMsg(
                    "GC Login",
                    gciLoginSuccess.data.message,null
                ); 
                 }
            });      
        }, (error) => {  
            console.log(error)
        }); 
    }); 
}




openModals=()=>{
  this.setState({selectFromModal: true})
}

getChecked = (value) => {
  // value = our checked value
  console.log(value)
  this.state.loginType = value;
  }

  render() {  
    const { selectedItems } = this.state;
         let details= this.state.details;
          if(details){
              details=details[0]
          }
  let psow=""
  if(details && details.Planned_SoW ){
   let ar=  details.Planned_SoW.split(',')
    if(Array.isArray(ar)){
      
      ar.map((itt,j)=>{
         if(itt.includes('1')){
            if(psow.length>1)
            psow=psow+","+"CX"
            else
            psow=psow+" "+"CX"
         }
         if(itt.includes('2')){
          if(psow.length>1)
          psow=psow+","+"IX"
          else
          psow=psow+" "+"IX"
       }
       if(itt.includes('3')){
        if(psow.length>1)
        psow=psow+","+"Troubleshooting"
        else
        psow=psow+" "+ "Troubleshooting"
     }
     if(itt.includes('4')){
      if(psow.length>1)
      psow=psow+","+"E911-Call Test"
      else
      psow=psow+" "+ "E911-Call Test"  
   }
      })

    }
      
  }
          
         console.log("gccc d",this.state.details)
    return (
      <KeyboardAvoidingView style={StyleGCSiteLogin.container}>
      <StatusBarComponent isHidden={false} />
      <OfflineBarComponent />
      {
					this.state.isLoadervisible &&
					<Loader msg={I18n.t('LOADING')} />
				}
      { DeviceInfo.isAndroid() && 
        <ToolbarComponent 
          title= "GC SIte "
          navIcon={Constant.ICONS.BACK_ICON}
          navClick={() => this.props.navigation.goBack(null)}
        /> 
      }

      <Modal  
        animationType="fade" 
        visible={this.state.isAutoCompletePopupVisible}
        onRequestClose={() => {}}> 
        <View style={StyleGCSiteLogin.popupContainerParent}>         
          <View style={StyleGCSiteLogin.searchBarContainerer}>
            <View style={StyleGCSiteLogin.backIconContainer}>
              <Icon name={Constant.ICONS.BACK_ICON} 
              size={30} 
              color={Colors.white} 
              onPress={()=>this.showHideAutoCompletePopup(false)}/>
            </View>
            <View  style={StyleGCSiteLogin.searchBoxContainer}>  
              <TextInput 
                value={this.autoCompleteQueryStr}
                placeholder={this.autoCompletePlaceholder}
                keyboardType='default' 
                style={StyleGCSiteLogin.autoCompleteInput} 
                onChangeText={(keyPress) => this.filterAutoComplete(keyPress,true)} 
              /> 
            </View>
            <View style={StyleGCSiteLogin.crossIconContainer}> 
              <Icon name={Constant.ICONS.CROSS_ICON} 
              size={30} 
              color={Colors.white} 
              onPress={()=>this.filterAutoComplete("")}/> 
            </View>
          </View>
          <View style={StyleGCSiteLogin.flextListContainer}> 
            <FlatList
              ListEmptyComponent={this.renderPlaceHolder}  
              data={this.state.autoCompleteFilteredArray}
              extraData={this.state} i
              nitialNumToRender={20} 
              keyExtractor={(item, index) => index+""+new Date().toLocaleString()}
              renderItem={this.renderFlatListItem} 
            />  
          </View>     
        </View>  
      </Modal> 
      
      {!this.state.isModalVisible &&
      <View style={StyleGCSiteLogin.formContainer}>
        <ScrollView keyboardShouldPersistTaps='always'> 
          <View style={StyleGCSiteLogin.formContainerInner}> 
              {/* MainId */}
            
              
              <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                {I18n.t('GCSITELOGINPAGE.MainId')}
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                {details?details.site_id:'- -'}
              </Text>
              </View>
            </View>
            
              
  
              
              {/* Work Order Number */}
















            
                <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
                  <Text style={StyleGCSiteLogin.inputTitle}>
                    {/* {I18n.t('GCSITELOGINPAGE.MARKET')} */}
                    {/* Work Order Number */}
                    Project Code
                  </Text>
                  </View>
                  <View style={StyleGCSiteLogin.dropdownContainer}>
              <Text style={StyleGCSiteLogin.inputTitle}>
              {details?details.Work_Order_No:'- -'}
              </Text>
              </View>
                  
                
                
               
              
              
              {/* POR */}
           
              <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                {/* {I18n.t('GCSITELOGINPAGE.MARKET')} */}
                {/* Select Por */}
                Select Technology
              </Text>
              
              <View style={StyleGCSiteLogin.dropdownContainer}>
              <Text style={StyleGCSiteLogin.inputTitle}>
              {details?details.POR:'- -'}
              </Text>
              </View>
           
            </View>
            
              
              
              {/* T-Mobile TTID Otherwise NA */}
              <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
                <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {/* {I18n.t('GCSITELOGINPAGE.PRE_WORK_ID')} */}
                      T-Mobile TTID
                    </Text>
                    <View style={StyleGCSiteLogin.dropdownContainer}>
                <Text style={StyleGCSiteLogin.inputTitle}>
                    {details?details.T_Mobile_TicketID:'- -'}
                </Text>
              </View>
                </View>
                <View style={StyleGCSiteLogin.gridColumn}>
                <Text style={StyleGCSiteLogin.inputTitle}>
                  {I18n.t('GCSITELOGINPAGE.ACCOUNT')}
                </Text>
                <Text></Text>
                <View style={StyleGCSiteLogin.dropdownContainer}>
              <Text style={StyleGCSiteLogin.inputTitle}>
              {details?details.accountName:'- -'}
              </Text>
              </View>
                </View>
              </View>
              
              
              {/* Market */}
              <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
                <Text style={StyleGCSiteLogin.inputTitle}>
                  {I18n.t('GCSITELOGINPAGE.MARKET')}
                </Text>
                <View style={StyleGCSiteLogin.dropdownContainer}>
              <Text style={StyleGCSiteLogin.inputTitle}>
              {details?details.marketName:'- -'}
              </Text>
              </View>
              </View>
            
              {/* Planned SOW */}
              <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
                <Text style={StyleGCSiteLogin.inputTitle}>
                  {I18n.t('GCSITELOGINPAGE.PLANSOW')}
                </Text>
              
                <View style={StyleGCSiteLogin.dropdownContainer}>
              <Text style={StyleGCSiteLogin.inputTitle}>
              {psow!=""?psow:'- -'}
              </Text>
              </View>

              </View>

              {/* TMO Deployment Manager */}
              <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
                <Text style={StyleGCSiteLogin.inputTitle}>
                  {/* {I18n.t('GCSITELOGINPAGE.PLANSOW')} */}
                  TMO Deployment Manager
                </Text>
                 <View style={StyleGCSiteLogin.dropdownContainer}>
              <Text style={StyleGCSiteLogin.inputTitle}>
              {details?details.tmoEngineerName:'- -'} 
              </Text>
              </View>
              </View>

              {/* Service Effecting */}
              


    



              <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
                <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {I18n.t('GCSITELOGINPAGE.SERVICE_AFFECTING')}
                    </Text>
                    <View style={StyleGCSiteLogin.dropdownContainer}>
              <Text style={StyleGCSiteLogin.inputTitle}>
              {details?details.serviceAffectingName:'- -'} 
              </Text>
              </View>
                </View>
                {/* <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {I18n.t('GCSITELOGINPAGE.DTO')}
                    </Text>
                    <View style={StyleGCSiteLogin.dropdownContainer}> 
                      <Picker
                          enabled = {true} 
                          selectedValue={this.state.selectedDTO}
                          style={StyleGCSiteLogin.dropdown}
                          onValueChange={(selectedDTO, itemIndex) =>
                          this.setState({selectedDTO: selectedDTO})
                        }>
                        <Picker.Item label="Yes" value="1" />
                        <Picker.Item label="No" value="0" />
                      </Picker>
                    </View>
                </View> */}
              </View>




              <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
                <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {/* {I18n.t('GCSITELOGINPAGE.SERVICE_AFFECTING')} */}
                      RF Approved MW (Start Time)
                    </Text>
                    
                </View>
                <View style={StyleGCSiteLogin.dropdownContainer}>
              <Text style={StyleGCSiteLogin.inputTitle}>
              {details?details.startTime:'- -'} 
              </Text>
              </View>



                
              </View>
              



              <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
                <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {/* {I18n.t('GCSITELOGINPAGE.SERVICE_AFFECTING')} */}
                      How Much it will take 
                    </Text>
                    
                </View>
                <View style={StyleGCSiteLogin.dropdownContainer}>
              <Text style={StyleGCSiteLogin.inputTitle}>
              {details?details.endTime:'- -'} 
              </Text>
              </View>
			  </View>
              


              <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
              {/* <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}> */}
                      {/* {I18n.t('GCSITELOGINPAGE.SERVICE_AFFECTING')} 
                      {/* RF Approved  */}
                    {/* </Text>
                    <View style={StyleGCSiteLogin.dropdownContainer}> */}
              {/* <Text style={StyleGCSiteLogin.inputTitle}>
              {details?details.rfApproval:'- -'} 
              </Text> */}
              {/* </View>      
                </View>  */}
             
                   
                <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {/* {I18n.t('GCSITELOGINPAGE.DTO')} */}
                      Day MOP or Night Mop
                    </Text>
                    <View style={StyleGCSiteLogin.dropdownContainer}>
              <Text style={StyleGCSiteLogin.inputTitle}>
              {details?details.mopName:'- -'} 
              </Text>
              </View>
                </View>
              </View>

              {this.state.isDayMOP &&
                <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
                  <Text style={StyleGCSiteLogin.inputTitle}>
                    Day MOP
                  </Text>
                  <View style={StyleGCSiteLogin.dropdownContainer}>
                  <TextInput style={StyleGCSiteLogin.inputField}  
                      keyboardType='default'
                      returnKeyType="next"
                      maxLength = {100}
                      value={this.state.dayMOPComment} 
                      onChangeText={(prework) => this.setState({dayMOPComment:prework})}
                      />
                </View>


                  {/* <TextInput style={StyleGCSiteLogin.inputField}  
                   keyboardType='default'
                   returnKeyType="next" 
                   maxLength = {200}
                   onChangeText={(sowDetail) => this.setState({SowDetail:sowDetail})}
                   value={this.state.SowDetail}  /> */}
              </View>
              }


              
              
              <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
                  <Text style={StyleGCSiteLogin.inputTitle}>
                    {I18n.t('GCSITELOGINPAGE.SOW_DETAILS')}
                  </Text>
                  <View style={StyleGCSiteLogin.dropdownContainer}>
                  {/* <Picker
                    enabled = {true} 
                    selectedValue={this.state.SelectedSOW}
                    style={StyleGCSiteLogin.dropdown}
                    onValueChange={(selectedSow, itemIndex) =>
                    this.setState({SelectedSOW: selectedSow})
                    }>  
                      {this.renderSOWPicker()}
                  </Picker> */}
                  <MultiSelect
                    hideTags
                    items={this.state.sowArrayDetails}
                    uniqueKey="id"
                    ref={(component) => { this.multiSelect1 = component }}
                    onSelectedItemsChange={this.onSelectedItemsChangeSOWDetail}
                    selectedItems={this.state.SelectedSOWDetails}
                    selectText="SOW Details"
                    searchInputPlaceholderText="Search SOW Details..."
                    onChangeInput={ (text)=> console.log(text)}
                    altFontFamily="ProximaNova-Light"
                    tagRemoveIconColor="#CCC"
                    tagBorderColor="#CCC"
                    tagTextColor="#CCC" 
                    selectedItemTextColor="#CCC"
                    selectedItemIconColor="#CCC"
                    itemTextColor="#000"
                    displayKey="name"
                    searchInputStyle={{ color: '#CCC' }}
                    submitButtonColor="#CCC"
                    submitButtonText="Submit"
                  />
                  <View>
                    {this.multiSelect && this.multiSelect1.getSelectedItemsExt(selectedItems)}
                  </View>
                </View>
                  {/* <View style={StyleGCSiteLogin.dropdownContainer}>
                  <Picker
                    enabled = {true} 
                    selectedValue={this.state.SelectedSOWDetails}
                    style={StyleGCSiteLogin.dropdown}
                    onValueChange={(selectedSowDetails, itemIndex) =>
                    this.setState({SelectedSOWDetails: selectedSowDetails})
                    }>  
                      {this.renderSOWDetailsPicker()}
                  </Picker>
                </View> */}


                  {/* <TextInput style={StyleGCSiteLogin.inputField}  
                   keyboardType='default'
                   returnKeyType="next" 
                   maxLength = {200}
                   onChangeText={(sowDetail) => this.setState({SowDetail:sowDetail})}
                   value={this.state.SowDetail}  /> */}
              </View>
              <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
                <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {/* {I18n.t('GCSITELOGINPAGE.PRE_WORK_ID')} */}
                      SOW Comment
                    </Text>
                    <View style={StyleGCSiteLogin.dropdownContainer}> 
                        <TextInput style={StyleGCSiteLogin.inputField}  
                        keyboardType='default'
                        returnKeyType="next"
                        maxLength = {100}
                        value={this.state.SOWComment} 
                        onChangeText={(prework) => this.setState({SOWComment:prework})}
                        /> 
                    </View>
                </View>
                <View style={StyleGCSiteLogin.gridColumn}>
                <Text style={StyleGCSiteLogin.inputTitle}>
                  Nesting Time
                </Text>
                <View style={StyleGCSiteLogin.dropdownContainer}> 
                        <TextInput style={StyleGCSiteLogin.inputField}  
                        autoCapitalize="none"
                        keyboardType='numeric'
                        returnKeyType="next" 
                        maxLength = {2}
                        value={this.state.nestedTime} 
                        onChangeText={(prework) => this.setState({nestedTime:prework})}
                        /> 
                    </View>
                </View>
              </View>
              
            
            
            
            
            
{/*             
              <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
                  <Text style={StyleGCSiteLogin.inputTitle}>
                    {I18n.t('GCSITELOGINPAGE.ESTIMATED_TIME')}
                  </Text>
                  <TextInput style={StyleGCSiteLogin.inputField}  
                    keyboardType='numeric'
                    returnKeyType="next"
                    maxLength = {3}
                    onChangeText={(estTime) => this.setState({estimatedTime:estTime})}
                  />
              </View>
              <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
                <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {I18n.t('GCSITELOGINPAGE.PRE_WORK_ID')}
                    </Text>
                    <View style={StyleGCSiteLogin.dropdownContainer}> 
                        <TextInput style={StyleGCSiteLogin.inputField}  
                        keyboardType='default'
                        returnKeyType="next"
                        maxLength = {100}
                        value={this.state.preWorkID} 
                        onChangeText={(prework) => this.setState({preWorkID:prework})}
                        /> 
                    </View>
                </View>
                <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {I18n.t('GCSITELOGINPAGE.EMI_LOGIN')}
                    </Text>
                    <View style={StyleGCSiteLogin.dropdownContainer}> 
                        <TextInput style={StyleGCSiteLogin.inputField}  
                          keyboardType='default'
                          returnKeyType="next"
                          maxLength = {100}
                          onChangeText={(emiLogin) => this.setState({emiLoginStr:emiLogin})}
                          value={this.state.emiLoginStr}
                        /> 
                    </View>
                </View>
              </View>


              

              <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop, {alignItems: "center"}]}>
                <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {I18n.t('GCSITELOGINPAGE.GC_NFSD_SNAPSHOT')}
                    </Text>
                </View>
                <View style={StyleGCSiteLogin.gridColumn}>
                    <TouchableOpacity style={[StyleGCSiteLogin.uploadButtonContainer, {backgroundColor:this.state.buttonBackground}]}  
                      onPress={()=>this.setSelectFromModalVisibility(true)}>
                      <Text style={StyleGCSiteLogin.uploadButtonText}>
                          {I18n.t('GCSITELOGINPAGE.UPLOAD_SNAP')}
                      </Text>
                    </TouchableOpacity>
                </View>
              </View>

              <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
                  <Text style={StyleGCSiteLogin.inputTitle}>
                    Additional Email ID
                  </Text>
                  <TextInput style={StyleGCSiteLogin.inputField}  
                    maxLength = {50}
                    keyboardType='email-address'
                    returnKeyType="next"  
                    onChangeText={(addemailid) => this.setState({AdditionalEmailId:addemailid})}
                    value={this.state.AdditionalEmailId} 
                  />
              </View>
              {this.state.selectedNFSDLogin=="No" &&
                <View style={[StyleGCSiteLogin.gridColumn,StyleGCSiteLogin.additionMarginTop]}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {I18n.t('GCSITELOGINPAGE.NFSD_REASON')}
                    </Text>
                    <View style={StyleGCSiteLogin.dropdownContainer}> 
                        <TextInput style={StyleGCSiteLogin.inputField}  
                          keyboardType='default'
                          returnKeyType="next"
                          maxLength = {100}
                          
                        /> 
                    </View>
                </View> 
              } */}


              
              <View style={StyleGCSiteLogin.bottomContainer}>   
                   <TouchableOpacity style={StyleGCSiteLogin.buttonContainer} onPress={
                  ()=>    this.displayAlertMsg1("Confirm","Are you sure to request Precheck?",()=> this.submitGcLogin())
                   }>
                      <Text style={StyleGCSiteLogin.buttonText}>
                          {I18n.t('GCSITELOGINPAGE.LOGIN')}
                      </Text>
                  </TouchableOpacity>
              </View> 
          </View>
        </ScrollView>
      </View>

            }
      {this.state.isModalVisible && 
      <View style={StyleGCSiteLogin.formContainer}>
          <ScrollView keyboardShouldPersistTaps='always'> 
            <View style={StyleGCSiteLogin.formContainerInner}> 
        
                <Text>Are you!</Text>
          <RadioGroup getChecked={this.getChecked}>
            <Radio iconName={"lens"} label={"CX"} value={"1"}/>
            <Radio iconName={"lens"} label={"IX"} value={"2"}/>
            <Radio iconName={"lens"} label={"Both"} value={"3"}/>
          </RadioGroup>
              
              
          <View style={StyleGCSiteLogin.bottomContainer}>   
                      <TouchableOpacity style={StyleGCSiteLogin.buttonContainer} onPress={() => this.submitGcLogin()}>
                          <Text style={StyleGCSiteLogin.buttonText}>
                              {I18n.t('GCSITELOGINPAGE.LOGIN')}
                          </Text>
                      </TouchableOpacity>
                  </View>
              
              
                {/* <Button title="Submit" onPress={() => this.()}/> */}
              
            
            </View> 
            </ScrollView>
        </View>
	  
	  	}

<Modal
        animationType="none"
        transparent={true}
        closeOnClick={true}
        visible={this.state.selectFromModal}
        onRequestClose={() => {
          this.setSelectFromModalVisibility(false)
        }}>
        <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'rgba(0,0,0,0.6)'}}>

          <View style={{width: '90%',backgroundColor: '#FFFFFF', elevation:10, borderTopLeftRadius:8,borderTopEndRadius:8, margin: 5}}>

            <View style={{ flexDirection: 'row',height:55, backgroundColor: Colors.lightOrange,alignItems:'center'}}>
              <Text style={{flex: 1, paddingLeft: 7, fontWeight:'bold', fontSize: 17, fontFamily: 'Roboto', color: '#FFFFFF', textAlign: 'left'}}>
                Are You
              </Text>
              <View style={{width:80,height:30,justifyContent: 'center',alignSelf:'center',backgroundColor:'#fff',borderRadius:15, marginRight: 10}}>
                <TouchableOpacity
                  onPress={()=>this.setSelectFromModalVisibility(false)}>
                    <Text style={{ fontWeight:'bold', fontSize: 12, fontFamily: 'Roboto', color: Colors.lightOrange, textAlign: 'center'}}>
                       close
                    </Text>
                </TouchableOpacity>
              </View>
              
            </View>

            <View style = {{flexDirection: 'row', height:200,alignSelf: 'center', alignItems:'center', justifyContent: 'center',}}>
			<View style={StyleGCSiteLogin.formContainerInner}> 
        
		
  <RadioGroup getChecked={this.getChecked}>
	<Radio iconName={"lens"} label={"CX"} value={"1"}/>
	<Radio iconName={"lens"} label={"IX"} value={"2"}/>
	<Radio iconName={"lens"} label={"Both"} value={"3"}/>
  </RadioGroup>
	  
	  
  <View style={StyleGCSiteLogin.bottomContainer}>   
			  <TouchableOpacity style={StyleGCSiteLogin.buttonContainer} onPress={() => this.submitGcLogin()}>
				  <Text style={StyleGCSiteLogin.buttonText}>
					  {I18n.t('GCSITELOGINPAGE.LOGIN')}
				  </Text>
			  </TouchableOpacity>
		  </View>
	  
	  
		{/* <Button title="Submit" onPress={() => this.()}/> */}
	  
	
	</View>   
                </View>

            </View>

          </View>
       
       
        </Modal>
      

      <Modal
        animationType="none"
        transparent={true}
        closeOnClick={true}
        visible={this.state.selectFromModal1}
        onRequestClose={() => {
          this.setSelectFromModalVisibility(false)
        }}>
        <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'rgba(0,0,0,0.6)'}}>

          <View style={{width: '90%',backgroundColor: '#FFFFFF', elevation:10, borderTopLeftRadius:8,borderTopEndRadius:8, margin: 5}}>

            <View style={{ flexDirection: 'row',height:55, backgroundColor: Colors.lightOrange,alignItems:'center'}}>
              <Text style={{flex: 1, paddingLeft: 7, fontWeight:'bold', fontSize: 17, fontFamily: 'Roboto', color: '#FFFFFF', textAlign: 'left'}}>
                Select From
              </Text>
              <View style={{width:80,height:30,justifyContent: 'center',alignSelf:'center',backgroundColor:'#fff',borderRadius:15, marginRight: 10}}>
                <TouchableOpacity
                  onPress={()=>this.setSelectFromModalVisibility(false)}>
                    <Text style={{ fontWeight:'bold', fontSize: 12, fontFamily: 'Roboto', color: Colors.lightOrange, textAlign: 'center'}}>
                       close
                    </Text>
                </TouchableOpacity>
              </View>
              
            </View>

            <View style = {{flexDirection: 'row', height:150,alignSelf: 'center', alignItems:'center', justifyContent: 'center',}}>
                <View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>
                    <TouchableOpacity  style={{width:70,height:70,backgroundColor:'white',alignItems:"center",justifyContent:"center",borderRadius:60, }}
                      onPress={ this.openCamera.bind(this)}>
                      <Image 
                        style={{width:60,height:60, tintColor: Colors.lightOrange}}
                        resizeMode= "center"
                        source={require('../../Images/Camera/camera.png')} />
                    </TouchableOpacity>   
                    <Text style={{alignSelf:'stretch',padding: 5, fontSize: 15, fontFamily: 'Roboto', fontWeight: 'bold', color: Colors.lightOrange,textAlign:'center'}}>{'Camera'}</Text>     
                  </View>
                  
                  <View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>
                    <TouchableOpacity  style={{width:70,height:70,backgroundColor:'white',alignItems:"center",justifyContent:"center",borderRadius:60}}
                      onPress={this.openGallery.bind(this)}>
                      <Image 
                        style={{width:60,height:60, tintColor: Colors.lightOrange}}
                        resizeMode= "center"
                        source={require('../../Images/Camera/choose_gallery.png')} />
                    </TouchableOpacity>   
                    <Text style={{alignSelf:'stretch',padding: 5, fontSize: 15, fontFamily: 'Roboto', fontWeight: 'bold', color: Colors.lightOrange,textAlign:'center'}}>{'Gallery'}</Text>      
                  </View>     
                </View>

            </View>

          </View>
        </Modal>
      
	  </KeyboardAvoidingView>
    );
  }
}