
import React, { PureComponent } from 'react';
import { View, ScrollView,Image, TouchableOpacity, Text, TextInput,TouchableWithoutFeedback, KeyboardAvoidingView,ImageBackground,Picker } from 'react-native';
import StatusBarComponent from '../../Components/StatusBar'
import { StyleSignup } from '../../Styles/PageStyle';
import AppImages from '../../Images';
import { CommonMethods,GlobalVariable } from '../../Utils/Utils'
import I18n from '../../Utils/i18n';
import OfflineBarComponent from '../../Components/OfflineBar'
import {PostRequest} from '../../Utils/ServiceCalls';
import Constant from '../../Utils/Constant';
import Loader from '../../Components/Loader';
import { Dialog} from '../../Utils/Utils';
import AsyncStorage from '@react-native-community/async-storage';
 import Autocomplete from 'react-native-autocomplete-input';

export default class SignupScreen extends PureComponent {
    apiRetryAttempt=0;
    tempMarketObject={"id":"-1",name:I18n.t('SIGNUPPAGE.SELECT_MARKET')};
    constructor(props) {
        super(props); 
        
        this.state = {
            accountArray:[{"id":"-1",name:I18n.t('SIGNUPPAGE.SELECT_ACCOUNT')}],
            vendorArray:[{"Id":"-1",vendor:"Select Vendor"}],
            companyArray:[{"Id":"-1",gc_company_name:"Select Company"}],
            marketArray:[this.tempMarketObject], 
            selectedAcount:"-1", 
            SelectedMarket:"-1", 
            selectedVendor:"-1",
            SelectedCrewCompany:"-1",
            isLoadervisible:false,
            mobileNumber:"",  
            companyName:"",
            contactName:"",
            EmailId:"",
            password:"",
            companies: [],
            query: ''
            // marketArrayD:[]
        };  
    }

    componentDidMount() {
        //first get accounts
        this.getAccount();
        this.getVendor();
        this.getCompany();
        
    }
    
    getCompany=()=>{  
        let paramObject = {
            path: Constant.Api.SIGNUP.GetCrewCompany,
            showErrorMsg: true 
        };

         //display loader first
         this.setState({isLoadervisible:true},()=>{
            PostRequest(paramObject).then((responseApisuccess) => {
                this.setState({isLoadervisible:false},()=>{ 
                    let response=responseApisuccess.data; 
                    // unable to fetch accounts
                    if(!response || !response.status || response.status==0 || !response.data){ 
                        //if due to ary reason failed to fetch account retry again in case of error
                        if(this.apiRetryAttempt<3){
                            this.apiRetryAttempt++;
                            this.getCompany();
                            return;
                        }
                        return;
                    }
                      
                    if(response.data.length==0){ 
                        this.displayAlertMsg(
                            I18n.t('SIGNUPPAGE.NO_ACCOUNT_TITLE'),
                            I18n.t('SIGNUPPAGE.NO_ACCOUNT_MSG'),null
                        ); 
                        return;
                    }

                    //got accounts
                    if(response.data && response.data.length>0){
                        console.log(response.data)
                       this.apiRetryAttempt=0; 
                       let tempAccountArray= [...this.state.companyArray, ...response.data]; 
                       this.setState({companyArray:tempAccountArray,companies:[...response.data]},()=>{});
                       console.log(this.state.companyArray)
                    } 
                }); 
            }, (error) => {
                this.setState({isLoadervisible:false},()=>{});
            }); 
        });
    }

    getVendor=()=>{  
        let paramObject = {
            path: Constant.Api.SIGNUP.GETVENDOR,
            showErrorMsg: true 
        };

         //display loader first
         this.setState({isLoadervisible:true},()=>{
            PostRequest(paramObject).then((responseApisuccess) => {
                this.setState({isLoadervisible:false},()=>{ 
                    let response=responseApisuccess.data; 
                    // unable to fetch accounts
                    if(!response || !response.status || response.status==0 || !response.data){ 
                        //if due to ary reason failed to fetch account retry again in case of error
                        if(this.apiRetryAttempt<3){
                            this.apiRetryAttempt++;
                            this.getAccount();
                            return;
                        }
                        return;
                    }
                      
                    if(response.data.length==0){ 
                        this.displayAlertMsg(
                            I18n.t('SIGNUPPAGE.NO_ACCOUNT_TITLE'),
                            I18n.t('SIGNUPPAGE.NO_ACCOUNT_MSG'),null
                        ); 
                        return;
                    }

                    //got accounts
                    if(response.data && response.data.length>0){
                        console.log(response.data)
                       this.apiRetryAttempt=0; 
                       let tempAccountArray= [...this.state.vendorArray, ...response.data]; 
                       this.setState({vendorArray:tempAccountArray},()=>{});
                       console.log(this.state.vendorArray)
                    } 
                }); 
            }, (error) => {
                this.setState({isLoadervisible:false},()=>{});
            }); 
        });
    }

    getAccount=()=>{  
        let paramObject = {
            path: Constant.Api.SIGNUP.GETACCOUNT,
            showErrorMsg: true 
        };

         //display loader first
         this.setState({isLoadervisible:true},()=>{
            PostRequest(paramObject).then((responseApisuccess) => {
                this.setState({isLoadervisible:false},()=>{ 
                    let response=responseApisuccess.data; 
                    // unable to fetch accounts
                    if(!response || !response.status || response.status==0 || !response.data){ 
                        //if due to ary reason failed to fetch account retry again in case of error
                        if(this.apiRetryAttempt<3){
                            this.apiRetryAttempt++;
                            this.getAccount();
                            return;
                        }
                        return;
                    }
                      
                    if(response.data.length==0){ 
                        this.displayAlertMsg(
                            I18n.t('SIGNUPPAGE.NO_ACCOUNT_TITLE'),
                            I18n.t('SIGNUPPAGE.NO_ACCOUNT_MSG'),null
                        ); 
                        return;
                    }

                    //got accounts
                    if(response.data && response.data.length>0){
                       this.apiRetryAttempt=0; 
                       let tempAccountArray= [...this.state.accountArray, ...response.data]; 
                       this.setState({accountArray:tempAccountArray},()=>{});
                    } 
                }); 
            }, (error) => {
                this.setState({isLoadervisible:false},()=>{});
            }); 
        });
    }


    getMarket=(accountID)=>{ 
        if(accountID==-1){
            let tempMarketArray= [this.tempMarketObject];
            this.setState({selectedAcount: accountID,marketArray:tempMarketArray},()=>{}); 
            return;
        } 
         
        let paramObject = {
            path: Constant.Api.SIGNUP.GETMARKET,
            showErrorMsg: true,
            params: {"account_id":accountID}, 
        };

         //display loader first
         this.setState({isLoadervisible:true,selectedAcount:accountID},()=>{
            PostRequest(paramObject).then((responseApisuccess) => { 
                this.setState({isLoadervisible:false},()=>{
                    let response=responseApisuccess.data; 
                    // unable to fetch market
                    if(!response || !response.status || response.status==0 || !response.data){
                        //if due to ary reason failed to fetch market retry again in case of error
                        if(this.apiRetryAttempt<3){
                            this.apiRetryAttempt++;
                            this.getMarket(accountID);
                            return;
                        } 
                        this.makeEmptyMarketArray(); 
                        return;
                    }

                    if(response.data.length==0){ 
                        this.makeEmptyMarketArray();  
                        this.displayAlertMsg(
                            I18n.t('SIGNUPPAGE.NO_MARKET_TITLE'),
                            I18n.t('SIGNUPPAGE.NO_MARKET_MSG'),null
                        );
                        return;
                    }
                        
                    //got accounts
                    if(response.data && response.data.length>0){ 
                        this.apiRetryAttempt=0; 
                        response.data.unshift(this.tempMarketObject); 
                        let tempMarketArray= response.data;
                        this.setState({marketArray:tempMarketArray},()=>{});

                        // this.setState({marketArrayD:response.data});

                    } 

                });      
            }, (error) => {  
                this.makeEmptyMarketArray(); 
            }); 
        });
    }
    makeEmptyMarketArray=()=>{
        let tempMarketArray= [this.tempMarketObject];
        this.setState({isLoadervisible:false,marketArray:tempMarketArray},()=>{});
    }
    renderMarketPicker=()=>{
        let pickerObject=this.state.marketArray.map((marketObj,i) => {
            let key=i+""+new Date().toLocaleString()+"_marketPickerList"
			return( 
                <Picker.Item label={marketObj.name} value={marketObj.id}  key={key}/>        
            );
		});
		return (pickerObject);
    }


    renderAccountPicker=()=>{
        let pickerObject=this.state.accountArray.map((accountObj,i) => {
            let key=i+""+new Date().toLocaleString()+"_accountPickerList"
			return( 
                <Picker.Item label={accountObj.name} value={accountObj.id}  key={key}/>        
            );
		});
		return (pickerObject);
    }


    renderVendorPicker=()=>{
        let pickerObject=this.state.vendorArray.map((accountObj,i) => {
            let key=i+""+new Date().toLocaleString()+"_accountPickerList"
			return( 
                <Picker.Item label={accountObj.vendor} value={accountObj.Id}  key={key}/>        
            );
		});
		return (pickerObject);
    }

    renderCrewCompanyPicker=()=>{
        let pickerObject=this.state.companyArray.map((accountObj,i) => {
            let key=i+""+new Date().toLocaleString()+"_accountPickerList"
			return( 
                <Picker.Item label={accountObj.gc_company_name} value={accountObj.Id}  key={key}/>        
            );
		});
		return (pickerObject);
    }


    validation=()=>{

        // alert(this.state.SelectedMarket);
        //  return;
        console.log(this.state.password)

        let contactName=this.state.contactName.trim();
        let companyName=this.state.companyName.trim();
        let mobileNumber=this.state.mobileNumber.trim();
        if(this.state.selectedAcount==-1){
            this.displayAlertMsg(
                I18n.t('SIGNUPPAGE.SELECT_ACCOUNT_TITLE'),
                I18n.t('SIGNUPPAGE.SELECT_ACCOUNT_MSG'),null
            ); 
            return;
        }else if(this.state.SelectedMarket==-1){ 
            this.displayAlertMsg(
                I18n.t('SIGNUPPAGE.SELECT_MARKET_TITLE'),
                I18n.t('SIGNUPPAGE.SELECT_MARKET_MSG'),null
            ); 
            return;
        }else if(contactName==""){
            this.displayAlertMsg(
                I18n.t('SIGNUPPAGE.ENTER_CONTACT_NAME_TITLE'),
                I18n.t('SIGNUPPAGE.ENTER_CONTACT_NAME_MSG'),null
            ); 
            return;
        }else if(companyName==-1){
            this.displayAlertMsg(
                I18n.t('SIGNUPPAGE.ENTER_COMPANY_TITLE'),
                I18n.t('SIGNUPPAGE.ENTER_COMPANY_MSG'),null
            ); 
            return;
        }
        else if(mobileNumber.length<10)
        {
            this.displayAlertMsg(
                I18n.t('SIGNUPPAGE.ENTER_CONTACT_NUMBER_TITLE'),
                I18n.t('SIGNUPPAGE.ENTER_CONTACT_NUMBER_MSG'),null
            ); 
            return;
        }
        else if(this.state.EmailId=="")
        {
            this.displayAlertMsg("Email Id !","Please enter Email Id."); 
            return;
        }
        else if(!this.state.EmailId.includes("@"))
        {
            this.displayAlertMsg("Invalid Email Id !","Please enter valid Email Id."); 
            return;
        }else if(this.state.password == ""){
            
            this.displayAlertMsg("Password !","Please enter Password."); 
            return;
        }


        let dt=   this.state.companyArray.filter((itm)=>{
            return itm.Id== this.state.SelectedCrewCompany
      })

       let cn="" 
        if(this.state.query.length>0){
            cn=this.state.query
        }else{
           
            this.displayAlertMsg(
                I18n.t('SIGNUPPAGE.ENTER_COMPANY_TITLE'),
                I18n.t('SIGNUPPAGE.ENTER_COMPANY_MSG'),null
            ); 
            return;
        }
        // validation is successfully done.
        this.callRegisterUserApi(contactName,cn,mobileNumber)
    }

    callRegisterUserApi=(contactName,companyName,mobileNumber)=>{

   
        let paramObj = 
        {
            "vendor": this.state.selectedVendor ,
            "account":this.state.selectedAcount,
            "market":this.state.SelectedMarket,
            "name":contactName,
            "contactNo":mobileNumber,
            "crewEmail":this.state.EmailId,
            "crewCompany":this.state.query,
            "password":this.state.password
           } 
        
        

           
        let paramObject = {
            path: Constant.Api.SIGNUP.SIGNUP_API,
            params: paramObj,
            showErrorMsg: true 
        };

          console.log("signupp", paramObject)
         //display loader first
         console.log(paramObject)
         this.setState({isLoadervisible:true},()=>{
            PostRequest(paramObject).then((responseApisuccess) => {
                console.log("signupp",responseApisuccess)
                this.setState({isLoadervisible:false},()=>{
                    let response=responseApisuccess.data; 
                    console.log(response)    
                    // check if any error during registration
                    if(!response || !response.status ){
                        console.log(response)
                        let title=I18n.t('SIGNUPPAGE.REGISTRATION_ERROR_TITLE');
                        let message=response.message;
                        this.displayAlertMsg(title,message,null);
                        // this.disAlertDialogFailure();    
                    }
                    // user is already registered case
                    if(response.status && response.status==0 &&response.message){
                        console.log(response)
                        let title=I18n.t('SIGNUPPAGE.REGISTRATION_ERROR_TITLE');
                        let message=response.message;   
                        this.displayAlertMsg(title,message,null);
                    }
                        
                    //successfully registered
                    if(response.status && response.status==1 && response.data){
                        console.log(response)
                        try{
                            console.log(response)
                            let dataObject=response.data;
                            GlobalVariable.USERID=dataObject.id;
                            let stringifiedResponse=JSON.stringify(dataObject); 
                            AsyncStorage.setItem(Constant.LOCAL_STORAGE.LOGINRESPONSE, stringifiedResponse).then(()=>{
                                console.log(response)
                                let title=I18n.t('SIGNUPPAGE.REGISTRATION_SUCCESS_TITLE');
                                let message=I18n.t('SIGNUPPAGE.REGISTRATION_SUCCESS_MSG');
                                message=  response.message
                                this.displayAlertMsg(title,message,()=>{
                                    this.props.navigation.navigate("LoginScreen");  
                                });
                            });  
                        }catch(error){
                            console.log(error)
                            this.disAlertDialogFailure();   
                        }  
                    }  
                     // user is already registered case
                     if(response.status && response.status==1 &&response.message){
                        console.log(response)
                        let title=I18n.t('SIGNUPPAGE.REGISTRATION_ERROR_TITLE');
                        let message=response.message;
                        this.displayAlertMsg(title,message,null);
                    }
                });
            }, (error) => {
                this.setState({isLoadervisible:false},()=>{});
                console.log(error)
            }); 
        });
    }
    /**	   
	* @desc display alert dialog box
	* @param :title: string title of alert dialog
    * @param :message: string message of alert dialog
    * @param :callback: callback function on ok button click
	* @returns none
	*/
    displayAlertMsg=(title,message,callback)=>{
        Dialog.alertDialog(title,message,I18n.t('OK_BUTTON'),false,null).then(()=>{
            if(callback){
                callback();
            }
        })
    }
    

    disAlertDialogFailure=()=>{
        this.displayAlertMsg(
            I18n.t('SIGNUPPAGE.REGISTRATION_ERROR_TITLE'),
            I18n.t('SIGNUPPAGE.REGISTRATION_ERROR_MSG'),null
        ); 
    }

    showHideAutoComplete=()=>{
        alert("s");
    }
   
    findCompanies(query) {
        if (query === '') {
          return [];
        }
    
        const { companies } = this.state;
        const regex = new RegExp(`${query}`, 'i');
        return companies.filter(cmp => cmp.gc_company_name.search(regex) >= 0);
      }
    render() {
        const { query } = this.state;
        const companies = this.findCompanies(query);
        const comp = (a, b) => a.toLowerCase() === b.toLowerCase();       
        return (
            <KeyboardAvoidingView   keyboardVerticalOffset={-500} behavior="padding"  style={StyleSignup.container}>
                <StatusBarComponent isHidden={true} />
                <OfflineBarComponent />
                {
					this.state.isLoadervisible &&
					<Loader msg={I18n.t('LOADING')} />
				}
                <ImageBackground fadeDuration={100} source={AppImages.SplashBackground} style={StyleSignup.bgImgContainer} resizeMode={'cover'}>
				</ImageBackground>
                <View style={StyleSignup.formContainer}> 
                    <View style={StyleSignup.formContainerInner}>
                        <View style={StyleSignup.logoContainer}>
                            <Image
                                resizeMode="contain"
                                style={StyleSignup.logoImageStyle}
                                source={AppImages.Logo_Login_Image}
                            />
                            {/* <Text style={StyleSignup.logoText}>
                                {I18n.t('SPLASHPAGE.COMPANYNAME')}
                            </Text>
                            <Text style={[StyleSignup.logoText,StyleSignup.addMarginBottom]}>
                                {I18n.t('SPLASHPAGE.COMPANYNAME1')}
                            </Text>   */}
                        </View>
                        <View style={StyleSignup.middleContainer}>
                            <ScrollView style={StyleSignup.scrollView} showsHorizontalScrollIndicator={false}> 
                                <View style={StyleSignup.scrollViewInnerContainer}>
                                    <View style={StyleSignup.scrollViewContentContainer}>
                                        
                                    <Text style={StyleSignup.inputHeadingTitle}>
                                            {/* {I18n.t('SIGNUPPAGE.ACCOUNT')} */}
                                            Vendor
                                        </Text>
                                        <Picker  
                                            selectedValue={this.state.selectedVendor}
                                            style={StyleSignup.dropdown}
                                            onValueChange={
                                                (accountId, itemIndex) =>{
                                                    this.setState({selectedVendor: accountId});  
                                                }
                                            }> 
                                           {this.renderVendorPicker()}
                                        </Picker>
                                        <View style={StyleSignup.dividerLine}></View>
                                        
                                        
                                        <Text style={StyleSignup.inputHeadingTitle}>
                                            {I18n.t('SIGNUPPAGE.ACCOUNT')}
                                        </Text>
                                        <Picker  
                                            selectedValue={this.state.selectedAcount}
                                            style={StyleSignup.dropdown}
                                            onValueChange={
                                                (accountId, itemIndex) =>{
                                                    this.getMarket(accountId);  
                                                }
                                            }> 
                                           {this.renderAccountPicker()}
                                        </Picker>
                                        <View style={StyleSignup.dividerLine}></View>
                                        <Text style={StyleSignup.inputHeadingTitle}>
                                            {I18n.t('SIGNUPPAGE.MARKET')}
                                        </Text>
                                        <Picker
                                            enabled = {true} 
                                            selectedValue={this.state.SelectedMarket}
                                            style={StyleSignup.dropdown}
                                            onValueChange={(marketId, itemIndex) =>
                                            this.setState({SelectedMarket:marketId})
                                        }>
                                             {this.renderMarketPicker()}
                                        </Picker>
                                        <View style={StyleSignup.dividerLine}></View>  
                                        <Text style={StyleSignup.inputHeadingTitle}>
                                            {I18n.t('SIGNUPPAGE.CONTACT_NAME')}
                                        </Text>
                                        <TextInput style={StyleSignup.inputField}
                                            onChangeText={(contactNM) => this.setState({contactName:contactNM})} 
                                            maxLength = {50}
                                            keyboardType='default'
                                            returnKeyType="next"
                                        /> 
                                        <Text style={StyleSignup.inputHeadingTitle}>
                                            {I18n.t('SIGNUPPAGE.COMPANY_NAME')}
                                        </Text> 
                                        
        <Autocomplete
          autoCapitalize="none"
          autoCorrect={false}
          style={{
            backgroundColor: 'transparent',  
            borderRadius: 0,
            borderColor: 'transparent',  
            color:'#ffffff',
            height: 36,   
            fontSize: 17,
            padding: 7,
            borderWidth:0,
            marginTop:4, 
           
border:'none' 
          }}
          containerStyle={{  borderWidth:0,
            color:'#ffffff',
            marginTop:4,  
border:'none' }}
          data={companies.length === 1 && comp(query, companies[0].gc_company_name) ? [] : companies}
          defaultValue={query}
          onChangeText={text => this.setState({ query: text })}
          placeholder=""   
                 
          renderItem={(itm) => {
               console.log("comppp",itm)  
               let cmp= itm.item.gc_company_name
          return(   <TouchableOpacity onPress={() => this.setState({ query: cmp})}>
              <Text>
                 {cmp}
                {/* {gc_company_name} */}
              </Text>
            </TouchableOpacity> ) 
    }}
        />
        {/* <View>  
          {companies.length > 0 ? (
            AutocompleteExample.renderFilm(companies[0])
          ) : (
            <Text>      
              Enter Company Name   
            </Text>
          )}
        </View> */}

                                        {/* <Picker
                                            enabled = {true} 
                                            selectedValue={this.state.SelectedCrewCompany}
                                            style={StyleSignup.dropdown}
                                            onValueChange={(marketId, itemIndex) =>
                                            this.setState({SelectedCrewCompany:marketId})
                                        }>
                                             {this.renderCrewCompanyPicker()}
                                        </Picker>   */}
                                        <View style={StyleSignup.dividerLine}></View>   
                                        <Text style={StyleSignup.inputHeadingTitle}>
                                            {I18n.t('SIGNUPPAGE.CONTACT_NUMBER')}
                                        </Text>
                                        <TextInput style={[StyleSignup.inputField,StyleSignup.addMarginBottom]}
                                            maxLength = {10}
                                            onChangeText={(phNumber) => this.setState({mobileNumber:phNumber})}
                                            keyboardType='numeric' 
                                        /> 
                                         <Text style={StyleSignup.inputHeadingTitle}>
                                            Email Id
                                        </Text>
                                        <TextInput style={[StyleSignup.inputField,StyleSignup.addMarginBottom]}
                                            maxLength = {40}
                                            onChangeText={(emailId) => this.setState({EmailId:emailId})}
                                            keyboardType='email-address' 
                                        />
                                        <Text style={StyleSignup.inputHeadingTitle}>
                                            Password
                                        </Text>
                                        <TextInput style={[StyleSignup.inputField,StyleSignup.addMarginBottom]}
                                            maxLength = {40}
                                            onChangeText={(emailId) => this.setState({password:emailId})}
                                            keyboardType='default' 
                                        /> 
                                    </View>
                                </View>    
                            </ScrollView> 
                        </View>
                        <View style={StyleSignup.bottomContainer}>   
                            <TouchableOpacity style={StyleSignup.buttonContainer} onPress={() => this.validation()}>
                                <Text style={StyleSignup.buttonText}>
                                        {I18n.t('SPLASHPAGE.SIGNUP')}
                                </Text>
                            </TouchableOpacity>
                        </View> 
                    </View> 
                </View>
            </KeyboardAvoidingView>
        );
    }
}