import React, { Component } from 'react';
import ToolbarComponent from "../../Components/Toolbar";
import I18n from '../../Utils/i18n';
import { CommonMethods,DeviceInfo,Dialog,GlobalVariable } from '../../Utils/Utils'
import {Text,TouchableOpacity,View,KeyboardAvoidingView,TextInput,Picker,ScrollView, Alert} from 'react-native';
import { StyleGCSiteLogin } from '../../Styles/PageStyle';
import StatusBarComponent from '../../Components/StatusBar';
import OfflineBarComponent from '../../Components/OfflineBar';
import Constant from '../../Utils/Constant';
import {Colors} from '../../Styles/Themes'; 
import AsyncStorage from '@react-native-community/async-storage'; 
import Loader from '../../Components/Loader'; 

import axios from 'axios'; 
import GetLocation from 'react-native-get-location';
import moment from 'moment';


export default class EOS_Form extends Component {
  apiRetryAttempt=0;

  constructor(props) {
    super(props);
    this.state = { 
      isLoadervisible:false,
      isLoading :false,
      contactName:"",
      contactEmail:"",
      contactNumber:"", 
      GCCompanyName:"",
      
      id: 0,
      schId: "",
      week: "",
      date: "",
      siteId: "",
      tech: "",
      technology: "",
      sow: "",
      powerUpgrade: 0,
      GSM_Reconfiguration: 0,
      UMTS_Reconfiguration: 0,
      L600_Call_Test:0,
      L700_Call_Test: 0,
      L1900_Call_Test: 0,
      L1900_1st_Carrier: 0,
      L2100_Call_Test: 0,
      AAS_Call_Test: 0,
      AWS_Call_Test: 0,
      L2_IX_Baseband_integrated: 0,
      Nokia_IXR_Router: 0,
      N2_IX: 0,
      BB6630_Installation: 0,
      BB5216_Installation: 0,
      Baseband_Installed: 0,
      XM_Installation: 0,
      SOW_Completed: 0,
      sowPendingReason: "",
      hoursSpent: "",
      milesTravelled: "",
      driveHour: "",
      totalHour: "",
      delayDueToTC: "",
      delayDueToNIC: "",
      delayDueToSwitch: "",
      timeLine: "",
      comments: "",
      planSowData: [{id: '4', name: 'E911-Call Test'}, {id: '1', name: 'CX'}, {id: '2', name: 'IX'}, {id: '3', name: 'Troubleshooting'}]      
    };

  };


  async componentDidMount()
  {  
    let data = this.props.navigation.getParam('item', null);
    if(data)
    {
      let sowStr = "";
      let porStr = "";
      let schId = data.Id;
      let siteId = data.site_id;
      let date   = data.Scheduled_Date;
      let sow    = data.sow;
      let por    = data.work_por;
      var week   = moment(date, "MM/DD/YYYY").week();
      let sowArray = sow.split(",");
      let porArray = por.split(",");
      for (let index = 0; index < sowArray.length; index++) {
        let i = this.state.planSowData.findIndex(obj => obj.id == sowArray[index])
        if(i >= 0)
        {
          if(index == 0)
          {
            sowStr = sowStr + this.state.planSowData[i].name;
          }
          else
          {
            sowStr = sowStr + ", " + this.state.planSowData[i].name;
          }
        }
         
      }

      for (let index = 0; index < porArray.length; index++) {
      if(index == 0)
        {
          porStr = porStr + porArray[index];
        }
        else
        {
          porStr = porStr + ", " + porArray[index];
        }
      }
      this.setState({schId: schId, siteId: siteId, date: date, week: week.toString(), sow: sowStr, technology: porStr}, ()=> {
        this.getUsersLocalDetails();
        this.getData();
      });
    }
  }
      
 
  goToPreviousPage = () =>{
    this.props.navigation.state.params.onNavigateBack();
    this.props.navigation.goBack()
    return true;
  }
  

  getUsersLocalDetails=async()=>{
    console.log("Main 1")
    const userDetails = await AsyncStorage.getItem(Constant.LOCAL_STORAGE.LOGINRESPONSE);
    try
    {
      console.log(JSON.parse(userDetails))
      let userDetailsObject=JSON.parse(userDetails);
      let contactName=userDetailsObject.contact_name;
      let contactEmail=userDetailsObject.crew_lead_email_id;
      let contactNumber=userDetailsObject.contact_number;
      let companyName = userDetailsObject.m_company_id;
      let tech = userDetailsObject.crew_lead_name;
      this.state.emailIdUser = contactEmail
      console.log(this.state.emailIdUser, contactEmail)
      this.setState({GCCompanyName:companyName});
      this.setState({contactName:contactName,contactEmail:contactEmail,contactNumber:contactNumber, tech: tech},()=>{
      })
    }
    catch(err)
    {
       
    }
  }




  getData= () => {
    let jsn= {
      schId: this.state.schId
      }
      this.setState({isLoading :true});
    axios( {
      method: "POST",
      url:Constant.Api.ServerDomain+Constant.Api.EOSFORM.GETDATA,         
      data:  jsn 
    }).then(response => {
      this.setState({isLoading :false});
      alert(JSON.stringify(response));
      if(response.data.status == 1 )
      {
        if(response.data.data != null){
          let data = response.data.data;
          let id                          = data.id;
          let schId                       = data.schId;
          let week                        = data.week;
          let date                        = data.date;
          let siteId                      = data.siteId;
          let tech                        = data.tech;
          let technology                  = data.technology;
          let sow                         = data.sow;
          let powerUpgrade                = data.powerUpgrade;
          let GSM_Reconfiguration         = data.GSM_Reconfiguration;
          let UMTS_Reconfiguration        = data.UMTS_Reconfiguration;
          let L600_Call_Test              = data.L600_Call_Test;
          let L700_Call_Test              = data.L700_Call_Test;
          let L1900_Call_Test             = data.L1900_Call_Test;
          let L1900_1st_Carrier           = data.L1900_1st_Carrier;
          let L2100_Call_Test             = data.L2100_Call_Test;
          let AAS_Call_Test               = Number(data.AAS_Call_Test);
          let AWS_Call_Test               = Number(data.AWS_Call_Test);
          let L2_IX_Baseband_integrated   = Number(data.L2_IX_Baseband_integrated);
          let Nokia_IXR_Router            = Number(data.Nokia_IXR_Router);
          let N2_IX                       = data.N2_IX;
          let BB6630_Installation         = data.BB6630_Installation;
          let BB5216_Installation         = data.BB5216_Installation;
          let Baseband_Installed          = data.Baseband_Installed;
          let XM_Installation             = data.XM_Installation;
          let SOW_Completed               = data.SOW_Completed;
          let sowPendingReason            = data.sowPendingReason;
          let hoursSpent                  = data.hoursSpent;
          let milesTravelled              = data.milesTravelled;
          let driveHour                   = data.driveHour;
          let totalHour                   = data.totalHour;
          let delayDueToTC                = data.delayDueToTC;
          let delayDueToNIC               = data.delayDueToNIC;
          let delayDueToSwitch            = data.delayDueToSwitch;
          let timeLine                    = data.timeLine;
          let comments                    = data.comments;

          this.setState({
            id                          : id,
            schId                       : schId,
            week                        : week,
            date                        : date,
            siteId                      : siteId,
            tech                        : tech,
            technology                  : technology,
            sow                         : sow ,
            powerUpgrade                : powerUpgrade,
            GSM_Reconfiguration         : GSM_Reconfiguration,
            UMTS_Reconfiguration        : UMTS_Reconfiguration,
            L600_Call_Test              : L600_Call_Test,
            L700_Call_Test              : L700_Call_Test,
            L1900_Call_Test             : L1900_Call_Test,
            L1900_1st_Carrier           : L1900_1st_Carrier,
            L2100_Call_Test             : L2100_Call_Test,
            AAS_Call_Test               : AAS_Call_Test,
            AWS_Call_Test               : AWS_Call_Test,
            L2_IX_Baseband_integrated   : L2_IX_Baseband_integrated,
            Nokia_IXR_Router            : Nokia_IXR_Router,
            N2_IX                       : N2_IX,
            BB6630_Installation         : BB6630_Installation,
            BB5216_Installation         : BB5216_Installation,
            Baseband_Installed          : Baseband_Installed,
            XM_Installation             : XM_Installation,
            SOW_Completed               : SOW_Completed,
            sowPendingReason            : sowPendingReason,
            hoursSpent                  : hoursSpent,
            milesTravelled              : milesTravelled,
            driveHour                   : driveHour,
            totalHour                   : totalHour,
            delayDueToTC                : delayDueToTC,
            delayDueToNIC               : delayDueToNIC,
            delayDueToSwitch            : delayDueToSwitch,
            timeLine                    : timeLine,
            comments                    : comments                    
          })
        } 
      }
      else
      {
        Alert.alert(
          "Failed",
          "Something went wrong. \nPlease try again.",
          [
            { text: "OK", onPress: () => {} }
          ]);
      }
      }).catch(error => { 
        alert("Failed !")
        this.setState({isLoading :false});
    }); 
  };   





  submitForm= () => {
    let jsn= {
      id:                         this.state.id,
      schId:                     this.state.schId,
      week:                      this.state.week,
      date:                      this.state.date,
      siteId:                    this.state.siteId,
      tech:                      this.state.tech,
      technology:                this.state.technology,
      sow:                       this.state.sow,
      powerUpgrade:              this.state.powerUpgrade,
      GSM_Reconfiguration:       this.state.GSM_Reconfiguration,
      UMTS_Reconfiguration:      this.state.UMTS_Reconfiguration,
      L600_Call_Test:            this.state.L600_Call_Test,
      L700_Call_Test:            this.state.L700_Call_Test,
      L1900_Call_Test:           this.state.L1900_Call_Test,
      L1900_1st_Carrier:         this.state.L1900_1st_Carrier,
      L2100_Call_Test:           this.state.L2100_Call_Test,
      AAS_Call_Test:             this.state.AAS_Call_Test.toString(),
      AWS_Call_Test:             this.state.AWS_Call_Test.toString(),
      L2_IX_Baseband_integrated: this.state.L2_IX_Baseband_integrated.toString(),
      Nokia_IXR_Router:          this.state.Nokia_IXR_Router.toString(),
      N2_IX:                     this.state.N2_IX,
      BB6630_Installation:       this.state.BB6630_Installation,
      BB5216_Installation:       this.state.BB5216_Installation,
      Baseband_Installed:        this.state.Baseband_Installed,
      XM_Installation:           this.state.XM_Installation,
      SOW_Completed:             this.state.SOW_Completed,
      sowPendingReason:          this.state.sowPendingReason,
      hoursSpent:                this.state.hoursSpent,
      milesTravelled:            this.state.milesTravelled,
      driveHour:                 this.state.driveHour,
      totalHour:                 this.state.totalHour,
      delayDueToTC:              this.state.delayDueToTC,
      delayDueToNIC:             this.state.delayDueToNIC,
      delayDueToSwitch:          this.state.delayDueToSwitch,
      comments:                  this.state.comments,
    }
    alert(JSON.stringify(jsn));
    this.setState({isLoading :true});
    axios( {
      method: "POST",
      url:Constant.Api.ServerDomain+Constant.Api.EOSFORM.SUBMIT,         
      data:  jsn 
    }).then(response => {
      this.setState({isLoading :false});
        if(response != null && response != undefined){
          // Alert.alert(
          //   "Successful",
          //   "Your EOS data saved successfully.",
          //   [
          //     { text: "OK", onPress: () => this.props.navigation.goBack(null) }
          //   ]);
        } 
        else
        {
          Alert.alert(
            "Failed",
            "Something went wrong. \nPlease try again.",
            [
              { text: "OK", onPress: () => {} }
            ]);
        }
      }).catch(error => { 
        alert("Failed !")
        this.setState({isLoading :false});
      }); 
  };   

  

  render() {  
    return (
      <KeyboardAvoidingView style={StyleGCSiteLogin.container}>
        <StatusBarComponent isHidden={false} />
        <OfflineBarComponent />
        {
					this.state.isLoadervisible &&
					<Loader msg={I18n.t('LOADING')} />
				}

        { DeviceInfo.isAndroid() && 
          <ToolbarComponent 
            title= "EOS Form"
            navIcon={Constant.ICONS.BACK_ICON}
            navClick={() => this.props.navigation.goBack(null)}
          /> 
        }
         
        <ScrollView contentContainerStyle = {{paddingBottom: 25}}>
          
          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                Week
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}> 
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='default'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.week} 
                  editable = {false} /> 
              </View>
            </View>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                DATE
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}>
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='default'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.date} 
                  editable = {false} />                  
              </View>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                Site ID
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}> 
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='default'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.siteId} 
                  editable = {false} /> 
              </View>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                TECH
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}> 
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='default'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.tech} 
                  editable = {false} /> 
              </View>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                Technology
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}> 
                <Text style={[StyleGCSiteLogin.inputField1]} > {this.state.technology} </Text> 
              </View>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                SOW
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}> 
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='default'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.sow} 
                  editable = {false} /> 
              </View>
            </View>
          </View>


          {/* <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                Site ID
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}> 
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='default'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.siteId} 
                  editable = {false} /> 
              </View>
            </View>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                TECH
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}>
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='default'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.tech} 
                  editable = {false} />                  
              </View>
            </View>
          </View> */}


          {/* <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                Technology
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}> 
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='default'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.technology} 
                  editable = {false} /> 
              </View>
            </View>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                SOW
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}>
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='default'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.sow} 
                  editable = {false} />                  
              </View>
            </View>
          </View> */}

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn1}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                Power upgrade
              </Text>
            </View>
            <View style={StyleGCSiteLogin.gridColumn2}>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: this.state.powerUpgrade == 1? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({powerUpgrade: 1})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"Yes"}</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 1, borderRightWidth: 1, borderBottomWidth: 1,  backgroundColor: this.state.powerUpgrade == 0? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({powerUpgrade: 0})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"No"}</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn1}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                GSM Reconfiguration
              </Text>
            </View>
            <View style={StyleGCSiteLogin.gridColumn2}>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: this.state.GSM_Reconfiguration == 1? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({GSM_Reconfiguration: 1})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"Yes"}</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 1, borderRightWidth: 1, borderBottomWidth: 1,  backgroundColor: this.state.GSM_Reconfiguration == 0? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({GSM_Reconfiguration: 0})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"No"}</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn1}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                UMTS Reconfiguration
              </Text>
            </View>
            <View style={StyleGCSiteLogin.gridColumn2}>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: this.state.UMTS_Reconfiguration == 1? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({UMTS_Reconfiguration: 1})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"Yes"}</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 1, borderRightWidth: 1, borderBottomWidth: 1,  backgroundColor: this.state.UMTS_Reconfiguration == 0? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({UMTS_Reconfiguration: 0})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"No"}</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn1}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                L600 Call Test
              </Text>
            </View>
            <View style={StyleGCSiteLogin.gridColumn2}>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: this.state.L600_Call_Test == 1? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({L600_Call_Test: 1})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"Yes"}</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 1, borderRightWidth: 1, borderBottomWidth: 1,  backgroundColor: this.state.L600_Call_Test == 0? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({L600_Call_Test: 0})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"No"}</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn1}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                L700 Call Test
              </Text>
            </View>
            <View style={StyleGCSiteLogin.gridColumn2}>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: this.state.L700_Call_Test == 1? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({L700_Call_Test: 1})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"Yes"}</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 1, borderRightWidth: 1, borderBottomWidth: 1,  backgroundColor: this.state.L700_Call_Test == 0? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({L700_Call_Test: 0})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"No"}</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn1}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                L1900 Call Test
              </Text>
            </View>
            <View style={StyleGCSiteLogin.gridColumn2}>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: this.state.L1900_Call_Test == 1? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({L1900_Call_Test: 1})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"Yes"}</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 1, borderRightWidth: 1, borderBottomWidth: 1,  backgroundColor: this.state.L1900_Call_Test == 0? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({L1900_Call_Test: 0})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"No"}</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn1}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                L1900 Call Test 1st Carrier
              </Text>
            </View>
            <View style={StyleGCSiteLogin.gridColumn2}>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: this.state.L1900_1st_Carrier == 1? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({L1900_1st_Carrier: 1})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"Yes"}</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 1, borderRightWidth: 1, borderBottomWidth: 1,  backgroundColor: this.state.L1900_1st_Carrier == 0? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({L1900_1st_Carrier: 0})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"No"}</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn1}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                L2100 Call Test
              </Text>
            </View>
            <View style={StyleGCSiteLogin.gridColumn2}>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: this.state.L2100_Call_Test == 1? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({L2100_Call_Test: 1})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"Yes"}</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 1, borderRightWidth: 1, borderBottomWidth: 1,  backgroundColor: this.state.L2100_Call_Test == 0? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({L2100_Call_Test: 0})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"No"}</Text>
              </TouchableOpacity>
            </View>
          </View>

          {/* <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn1}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                AAS Call Test
              </Text>
            </View>
            <View style={StyleGCSiteLogin.gridColumn2}>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: this.state.AAS_Call_Test == 1? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({AAS_Call_Test: 1})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"Yes"}</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 1, borderRightWidth: 1, borderBottomWidth: 1,  backgroundColor: this.state.AAS_Call_Test == 0? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({AAS_Call_Test: 0})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"No"}</Text>
              </TouchableOpacity>
            </View>
          </View> */}

          {/* <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                AAS Call Test
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}> 
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='default'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.AAS_Call_Test} 
                  onChangeText = {(AAS_Call_Test) => this.setState({AAS_Call_Test})}
                  editable = {true} /> 
              </View>
            </View>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                AWS 3 L2100 Call Test
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}>
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='default'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.AWS_Call_Test} 
                  onChangeText = {(AWS_Call_Test) => this.setState({AWS_Call_Test})}
                  editable = {true} />                  
              </View>
            </View>
          </View> */}

          {/* <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                L2.5 IX ( Yes/NO)/No. of Baseband integrated
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}> 
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='default'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.L2_IX_Baseband_integrated} 
                  onChangeText = {(L2_IX_Baseband_integrated) => this.setState({L2_IX_Baseband_integrated})}
                  editable = {true} /> 
              </View>
            </View>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                Nokia IXR-E Router IX
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}>
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='default'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.Nokia_IXR_Router} 
                  onChangeText = {(Nokia_IXR_Router) => this.setState({Nokia_IXR_Router})}
                  editable = {true} />                  
              </View>
            </View>
          </View> */}

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn1}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                AAS Call Test
              </Text>
            </View>
            <View style={StyleGCSiteLogin.gridColumn2}>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: this.state.AAS_Call_Test == 1? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({AAS_Call_Test: 1})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"Yes"}</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 1, borderRightWidth: 1, borderBottomWidth: 1,  backgroundColor: this.state.AAS_Call_Test == 0? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({AAS_Call_Test: 0})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"No"}</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn1}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                AWS 3 L2100 Call Test
              </Text>
            </View>
            <View style={StyleGCSiteLogin.gridColumn2}>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: this.state.AWS_Call_Test == 1? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({AWS_Call_Test: 1})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"Yes"}</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 1, borderRightWidth: 1, borderBottomWidth: 1,  backgroundColor: this.state.AWS_Call_Test == 0? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({AWS_Call_Test: 0})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"No"}</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn1}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                L2.5 IX ( Yes/NO)/No. of Baseband integrated
              </Text>
            </View>
            <View style={StyleGCSiteLogin.gridColumn2}>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: this.state.L2_IX_Baseband_integrated == 1? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({L2_IX_Baseband_integrated: 1})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"Yes"}</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 1, borderRightWidth: 1, borderBottomWidth: 1,  backgroundColor: this.state.L2_IX_Baseband_integrated == 0? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({L2_IX_Baseband_integrated: 0})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"No"}</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn1}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                Nokia IXR-E Router IX
              </Text>
            </View>
            <View style={StyleGCSiteLogin.gridColumn2}>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: this.state.Nokia_IXR_Router == 1? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({Nokia_IXR_Router: 1})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"Yes"}</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 1, borderRightWidth: 1, borderBottomWidth: 1,  backgroundColor: this.state.Nokia_IXR_Router == 0? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({Nokia_IXR_Router: 0})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"No"}</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn1}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                N 2.5 IX
              </Text>
            </View>
            <View style={StyleGCSiteLogin.gridColumn2}>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: this.state.N2_IX == 1? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({N2_IX: 1})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"Yes"}</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 1, borderRightWidth: 1, borderBottomWidth: 1,  backgroundColor: this.state.N2_IX == 0? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({N2_IX: 0})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"No"}</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn1}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                BB6630 Installation & Migration 
              </Text>
            </View>
            <View style={StyleGCSiteLogin.gridColumn2}>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: this.state.BB6630_Installation == 1? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({BB6630_Installation: 1})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"Yes"}</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 1, borderRightWidth: 1, borderBottomWidth: 1,  backgroundColor: this.state.BB6630_Installation == 0? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({BB6630_Installation: 0})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"No"}</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn1}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                BB5216 Installation & Migration
              </Text>
            </View>
            <View style={StyleGCSiteLogin.gridColumn2}>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: this.state.BB5216_Installation == 1? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({BB5216_Installation: 1})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"Yes"}</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 1, borderRightWidth: 1, borderBottomWidth: 1,  backgroundColor: this.state.BB5216_Installation == 0? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({BB5216_Installation: 0})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"No"}</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn1}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                Baseband Installed Count
              </Text>
            </View>
            <View style={StyleGCSiteLogin.gridColumn2}>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: this.state.Baseband_Installed == 1? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({Baseband_Installed: 1})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"Yes"}</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 1, borderRightWidth: 1, borderBottomWidth: 1,  backgroundColor: this.state.Baseband_Installed == 0? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({Baseband_Installed: 0})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"No"}</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn1}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                XMU Installation & Migration
              </Text>
            </View>
            <View style={StyleGCSiteLogin.gridColumn2}>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: this.state.XM_Installation == 1? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({XM_Installation: 1})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"Yes"}</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 1, borderRightWidth: 1, borderBottomWidth: 1,  backgroundColor: this.state.XM_Installation == 0? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({XM_Installation: 0})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"No"}</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn1}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                SOW Completed
              </Text>
            </View>
            <View style={StyleGCSiteLogin.gridColumn2}>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopLeftRadius: 5, borderBottomLeftRadius: 5, borderWidth: 1, backgroundColor: this.state.SOW_Completed == 1? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({SOW_Completed: 1})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"Yes"}</Text>
              </TouchableOpacity>
              <TouchableOpacity style = {{width: 50, height: 30, borderTopRightRadius: 5, borderBottomRightRadius: 5, borderTopWidth: 1, borderRightWidth: 1, borderBottomWidth: 1,  backgroundColor: this.state.SOW_Completed == 0? Colors.lightGreen: Colors.white, justifyContent: "center", alignItems: "center"}}
                onPress = {()=> this.setState({SOW_Completed: 0})}>
                <Text style = {{fontWeight: "bold", fontSize: 15, color: Colors.black}}>{"No"}</Text>
              </TouchableOpacity>
            </View>
          </View>

          
          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                SOW Pending Reason
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}> 
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='default'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.sowPendingReason} 
                  onChangeText = {(sowPendingReason) => this.setState({sowPendingReason})}
                  editable = {true} /> 
              </View>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                Hours Spent at Site
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}> 
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='numeric'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.hoursSpent} 
                  onChangeText = {(hoursSpent) => this.setState({hoursSpent})}
                  editable = {true} /> 
              </View>
            </View>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                Miles Travelled round trip
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}>
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='default'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.milesTravelled} 
                  onChangeText = {(milesTravelled) => this.setState({milesTravelled})}
                  editable = {true} />                  
              </View>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                Drive Hour
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}> 
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='numeric'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.driveHour} 
                  onChangeText = {(driveHour) => this.setState({driveHour})}
                  editable = {true} /> 
              </View>
            </View>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                Total Hour
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}>
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='numeric'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.totalHour} 
                  onChangeText = {(totalHour) => this.setState({totalHour})}
                  editable = {true} />                  
              </View>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                Delays Due to TC
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}> 
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='default'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.delayDueToTC} 
                  onChangeText = {(delayDueToTC) => this.setState({delayDueToTC})}
                  editable = {true} /> 
              </View>
            </View>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                Delays Due to NIC
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}>
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='default'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.delayDueToNIC} 
                  onChangeText = {(delayDueToNIC) => this.setState({delayDueToNIC})}
                  editable = {true} />                  
              </View>
            </View>
          </View>

          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                Delays Due to PAG/TMO Switch
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}> 
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='default'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.delayDueToSwitch} 
                  onChangeText = {(delayDueToSwitch) => this.setState({delayDueToSwitch})}
                  editable = {true} /> 
              </View>
            </View>
          </View>


          <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
            <View style={StyleGCSiteLogin.gridColumn}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                Comments
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}> 
                <TextInput style={StyleGCSiteLogin.inputField}  
                  keyboardType='default'
                  returnKeyType="next"
                  maxLength = {100}
                  value={this.state.comments} 
                  onChangeText = {(comments) => this.setState({comments})}
                  editable = {true} /> 
              </View>
            </View>
          </View>

          <View style={StyleGCSiteLogin.bottomContainer}>   
            <TouchableOpacity style={StyleGCSiteLogin.buttonContainer} onPress={() => this.submitForm()}>
              <Text style={StyleGCSiteLogin.buttonText}>
                  {"Submit"}
              </Text>
            </TouchableOpacity>
          </View> 
        </ScrollView>

        {this.state.isLoading   &&  
					<Loader msg={'Uploading...'} />
				} 
              

     </KeyboardAvoidingView>
    );
  }
}