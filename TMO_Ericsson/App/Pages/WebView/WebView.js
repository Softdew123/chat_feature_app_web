import React, { PureComponent,Component } from 'react';
import ToolbarComponent from "../../Components/Toolbar";
import I18n from '../../Utils/i18n';
import { CommonMethods, DeviceInfo, Dialog } from '../../Utils/Utils'
import { WebView,Image,Button,Dimensions, FlatList, RefreshControl, Text, TouchableOpacity, View, KeyboardAvoidingView,Modal, TextInput, Picker, ScrollView, TouchableWithoutFeedback} from 'react-native';
import { StylePreCheck } from '../../Styles/PageStyle';

const width = Dimensions.get('window').width;
const height =  Dimensions.get('window').height ;
 
export default class WebViews  extends PureComponent {
    
  precheckImage="";
  constructor(props) {
	super(props);
	
      this.state={data:''}

  }


  componentDidMount(){
	let params= this.props.navigation.state.params;
	   console.log("wwww",params.item)
	    this.setState({data: params.item  })
  } 
	
	  
 
     
     render() {
       return(<View style={{flex: 1, flexDirection:'column'}}  >
		   
		    <WebView    injectedJavaScript={`const meta = document.createElement('meta'); meta.setAttribute('content', 'width=width, initial-scale=0.8, maximum-scale=2.0, user-scalable=2.0'); meta.setAttribute('name', 'viewport'); document.getElementsByTagName('head')[0].appendChild(meta); `}
	   scalesPageToFit={true }  javaScriptEnabled={true}
		   domStorageEnabled={true}
		   scrollEnabled={true}
							  
		   startInLoadingState={false}  source={{uri :this.state.data }} style={{ height:7000,marginTop:50}} />
		   
		     {/* <WebView
        source={{
          uri: this.state.data 
        }}
        style={{ marginTop: 20 }}
      />  */}


		   </View>)
    
    }   
 }
