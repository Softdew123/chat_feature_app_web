import React , { Component } from 'react';
import { GiftedChat, Bubble, Send, SystemMessage } from 'react-native-gifted-chat';
import { ActivityIndicator, View, StyleSheet, Image, TouchableOpacity, Modal, Text, Platform, Alert, PermissionsAndroid } from 'react-native';
import firebase from 'react-native-firebase';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';	  
import {Colors} from '../../Styles/Themes'; 
import I18n from '../../Utils/i18n';
const RNFS = require("react-native-fs");
import FileViewer from "react-native-file-viewer";
import { launchImageLibrary } from "react-native-image-picker";
var RNFetchBlob = require('react-native-fetch-blob').default;
import AsyncStorage from '@react-native-community/async-storage';	
import MessageImage from '../MessageImageViewer/MessageImage';
//import Video from 'react-native-video';
import StatusBarComponent from '../../Components/StatusBar';
import ToolbarComponent from "../../Components/Toolbar";
import { DeviceInfo } from '../../Utils/Utils';
import Constant from '../../Utils/Constant';
// import { Icon } from 'react-native-vector-icons';

export default class ChatWindow extends Component{

    constructor(props) {
        super(props);
    }
    state = {
        messages: [],
        thread: this.props.navigation.getParam('item',null),
        selectFromModal: false, 
        _filename: "",
        imageURL: ""
    }

    handleSend = async(messages) => {
        const text = messages[0].text;
        console.log("text", text);
        firebase.firestore()
          .collection('THREADS')
          .doc(this.state.thread._id)//this.state.thread._id   //this.props.navigation.getParam('paramKey',null)
          .collection('MESSAGES')
          .add({
            text,
            createdAt: new Date().getTime(),
            user: {
              _id: firebase.auth().currentUser.uid,
              email: firebase.auth().currentUser.email
            }
          });
    
        await firebase.firestore()
          .collection('THREADS')
          .doc(this.state.thread._id)//this.state.thread._id
          .set(
            {
              latestMessage: {
                text,
                createdAt: new Date().getTime()
              }
            },
            { merge: true }
          );
      }

      handleDocumentSend = async(messages, filetype, filename) => {
        const text = messages;
        var senddoc = "";
        console.log("filetype ", filetype);
        if(filetype.includes('image')){
        senddoc = {
          img: text,
          createdAt: new Date().getTime(),
          user: {
            _id: firebase.auth().currentUser.uid,
            email: firebase.auth().currentUser.email
          }
        };
        }else if(filetype.includes('application') || filetype.includes('doc') || filetype.includes('docx')){
          senddoc = {
            application: text,
            filename: filename,
            createdAt: new Date().getTime(),
            user: {
              _id: firebase.auth().currentUser.uid,
              email: firebase.auth().currentUser.email
            }
        }

        // else if(filetype.includes('video')){
        //   senddoc = {
        //     video: text,
        //     createdAt: new Date().getTime(),
        //     user: {
        //       _id: firebase.auth().currentUser.uid,
        //       email: firebase.auth().currentUser.email
        //     }
        //   };
        // }
      }
        
        console.log("senddoc ", senddoc);
        console.log("text", text);
        firebase.firestore()
          .collection('THREADS')
          .doc(this.state.thread._id)//this.state.thread._id   //this.props.navigation.getParam('paramKey',null)
          .collection('MESSAGES')
          .add(senddoc);
    
        await firebase.firestore()
          .collection('THREADS')
          .doc(this.state.thread._id)//this.state.thread._id
          .set(
            {
              latestMessage: {
                text,
                createdAt: new Date().getTime()
              }
            },
            { merge: true }
          );
      }

    componentDidMount() {
      this.requestStoragePermission();
        console.log("params", this.state.thread._id);//this.state.thread._id
        const messagesListener = firebase.firestore()
        .collection("THREADS")
        .doc(this.state.thread._id)//this.state.thread._id
        .collection('MESSAGES')
        .orderBy('createdAt', 'desc')
        .onSnapshot(querySnapshot => {
            const messages = querySnapshot.docs.map(doc => {
              const firebaseData = doc.data();
    
              const data = {
                _id: doc.id,
                text: '',
                img: '',
                createdAt: new Date().getTime(),
                ...firebaseData
              };
    
              if (!firebaseData.system) {
                data.user = {
                  ...firebaseData.user,
                  name: firebaseData.user.email
                };
              }
              return data;
            });
            this.setState({messages});
          });
          return () => messagesListener();
    }

    renderBubble = (props) => {
        return (
          <View>
            {this.renderImage(props)}
            {this.renderCustomMessage(props)}
          <Bubble
            {...props}
            wrapperStyle={{
              left: {
                backgroundColor: '#6646ee'
              }
            }}
            textStyle={{
              left: {
                color: '#fff'
              }
            }}
          />
          </View>
        );
      }

      renderImage = (props) => {
        if(props.currentMessage.img){
          console.log("image -> ",props.currentMessage.img);
        return(
          <MessageImage message={props.currentMessage.img} />
          // <View style={{backgroundColor: '#6646ee', padding:15}}>
          // <TouchableOpacity>
          //   <Image source = {{uri: props.currentMessage.img}} style={{height:100, width: 125 , backgroundColor: '#6646ee'}}/>
          // </TouchableOpacity>
          // </View>
        );
      }
      }

      // renderMessageVideo = (props) => {
      //   if(props.currentMessage.video){
      //   return (
      //     <View style={{ padding: 20 }}>
      //        {/* <Video
      //         resizeMode="contain"
      //         useNativeControls
      //         shouldPlay={false}
      //         source={{ uri: props.currentMessage.video }}
      //         style={{height:100, width: 125 , backgroundColor: '#6646ee'}}
      //       /> */}
      //       <Video source={{uri: props.currentMessage.video}}   // Can be a URL or a local file.
      //         ref={(ref) => {
      //             this.player = ref
      //             }}                                      // Store reference
      //         onBuffer={this.onBuffer}                // Callback when remote video is buffering
      //         onError={this.videoError}               // Callback when video cannot be loaded
      //         style={{height:100, width: 125 , backgroundColor: '#6646ee'}} />
      //       </View>
      //     );
      //   }
      // };

      renderCustomMessage = (props) => {
        if(props.currentMessage.filename){
          console.log("application ", props.currentMessage.application);
            return(
              <View style={styles.documentMessage}>
                    <Text style={{textAlign: 'center', width: 200 , backgroundColor: '#0084ff', color: 'white', marginTop: 8, marginBottom: 8}}>
                    {props.currentMessage.filename}
                    </Text>
                    <TouchableOpacity onPress={()=>this.downloadfile(props.currentMessage.application)}>
                      <Image source={require('../../Images/ChatIcons/downloadbtn.png')}/>
                    </TouchableOpacity>
              </View>
            );
        }
      }

    renderLoading = () =>{
        return (
          <View style={styles.loadingContainer}>
            <ActivityIndicator size='large' color='#6646ee' />
          </View>
        );
      }
    
      renderSend = (props) => {
        return (
          <Send {...props}>
            {/* <View> */}
            {/* style={styles.sendingContainer} */}
            <View>
                <Image source={require("../../Images/ChatIcons/sendcircle.png")}/>
            </View>
            {/* </View> */}
          </Send>
        );
      }
    
      scrollToBottomComponent = () => {
        return (
          <View style={styles.bottomComponentContainer}>
              <TouchableOpacity>
                <Image source={require("../../Images/ChatIcons/chevron_double_down.png")}/>
            </TouchableOpacity>
            {/* <IconButton icon='chevron_double_down' size={36} color='#6646ee' /> */}
          </View>
        );
      }
    
      renderSystemMessage = (props) => {
        return (
          <SystemMessage
            {...props}
            wrapperStyle={styles.systemMessageWrapper}
            textStyle={styles.systemMessageText}
          />
        );
      }

      setSelectFromModalVisibility(visibility)	
      {	
        this.setState({selectFromModal: visibility});  	
      }	

    pickDocument = () => {
      console.log("res 0");
        DocumentPicker.show({
            filetype: [DocumentPickerUtil.allFiles()],
          },(error,res) => {
            console.log("res 1 ",res);
            // Android
            if(res.uri != null){
            console.log(
               res.uri,
               res.type,
               res.fileName,
               res.fileSize,
            );
            this.setState({path: res.uri});
            this.setState({name: res.fileName});
            }
            console.log("res 2 ",res);
            RNFetchBlob.fs
            .stat(res.uri)
            .then(stats => {
              console.log("stats.path 3 ",stats.path);
              this.useCallback(stats.path, res.fileName, res.type);
            }).catch(
              (error) => console.log(error)
            )
            console.log("res 3 ",res);
        });
    }

    handleOnNavigateBack = async (refreshPage) => {	
      let returnFrom  = await AsyncStorage.getItem("okCamera");	
      if(returnFrom !== "ok")	
        return;	
        
      let picturePath = await AsyncStorage.getItem("picturePath");
      console.log("picturePath =>",picturePath);
      var filename = picturePath.substring(picturePath.lastIndexOf("/") + 1, picturePath.length);
      console.log("filename => ",filename);
      this.useCallback(picturePath, filename, "image");
      //await AsyncStorage.removeItem("picturePath");	
      //this.resize1(picturePath);	
    }	
  
      openCamera= async()=>{	
        this.setSelectFromModalVisibility(false);	
        // AsyncStorage.setItem("fromCOP", 1);	
        this.props.navigation.navigate('CaptureNFSDSnapShot',{onNavigateBack: this.handleOnNavigateBack});	
      }
    
    getFileLocalPath = (response) => {
      const { path, uri } = response;
      console.log("response path-> "+response);
      return Platform.OS === 'android' ? response : uri; // path
    };
    
    createStorageReferenceToFile = (filename) => {
      const id = new Date().getTime();
      return firebase.storage().ref().child(`uploads/${id}-${filename}`);// fileName
    };
    
    uploadFileToFireBase = (response, filename) => {
      const fileSource = this.getFileLocalPath(response);
      const storageRef = this.createStorageReferenceToFile(filename);
      return storageRef.putFile(fileSource);
    };
    
    useCallback = (response, filename, filetype) => {
      console.log("upload line 1")
      const uploadTask = this.uploadFileToFireBase(response, filename);
      console.log("upload line 2")
      const unsubscribe = uploadTask.on(
        firebase.storage.TaskEvent.STATE_CHANGED,
        async snapshot => {
          console.log("upload line 3")
          //setProgress(
            console.log(snapshot.bytesTransferred / snapshot.totalBytes);
          //);
          console.log("upload line 4")
          // if (snapshot.state === firebase.storage.TaskState.SUCCESS) {
          //   console.log("upload line 5")
          //   var downloadUrl = await snapshot.ref.getDownloadURL();
          //   console.log("upload line 6")
          //   console.log("Download Url ",downloadUrl);
          //   console.log("upload line 7")
          //   this.handleDocumentSend(downloadUrl);
          //   console.log("upload line 8")
          // }
        },
        error => {
          console.log("error ->> ",error);
        },
        complete => {
          console.log("Completed ->> ",complete);
          console.log("download url --> ", complete.downloadURL);
          const downloadurl = complete.downloadURL;
          this.handleDocumentSend(downloadurl, filetype, filename);
          unsubscribe();
        }
      );
    };
    
     pickAttachmenet = async () => {
      if (Platform.OS === "ios") {
        const options = ["Image", "Document", "Cancel"];
        ActionSheetIOS.showActionSheetWithOptions(
          { options, cancelButtonIndex: 2, title: "Pick a data type" },
          async (buttonIndex) => {
            if (buttonIndex === 0) {
              // Open Image Picker
              launchImageLibrary({ mediaType: "photo" }, (res) => {
                if (!res.didCancel) {
                  return { path: res.uri, name: res.fileName };
                }
              });
            } else if (buttonIndex === 1) {
              // Open Document Picker
              return this.pickDocument();
            } else {
              // exit
            }
          }
        );
      } else {
        // For Android we can just use the normal DocumentPicker, as it can also access images
        return this.pickDocument();
      }
    };

    requestStoragePermission = async () => {

      if (Platform.OS !== "android") return true
  
      const pm1 = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE);
      const pm2 = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
  
      if (pm1 && pm2) return true
  
      const userResponse = await PermissionsAndroid.requestMultiple([
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
      ]);
  
      if (userResponse['android.permission.READ_EXTERNAL_STORAGE'] === 'granted' &&
          userResponse['android.permission.WRITE_EXTERNAL_STORAGE'] === 'granted') {
          return true
      } else {
          return false
      }
    }

      micBtn = (sendProps) => {
        return (
          <View flexDirection='row' onTouchStart={() => this.micBtnPressed()}>
          <TouchableOpacity style={styles.myStyle} activeOpacity={0.5}>
          <Image
            source={require('../../Images/ChatIcons/attachment.png')}
            style={styles.MicIconStyle}
          />
        </TouchableOpacity>
        </View>
        );
      }

      micBtnPressed = () => { // messages=[]
        this.setState({
          selectFromModal: true
        })
      }

      downloadfile = (fileurl) => {
        console.log("fileurl 1 ", fileurl);
        const { config, fs } = RNFetchBlob;
        const date = new Date();

        const { DownloadDir } = fs.dirs; // You can check the available directories in the wiki.
        const options = {
            fileCache: true,
            addAndroidDownloads: {
            useDownloadManager: true, // true will use native manager and be shown on notification bar.
            notification: true,
            path: `${DownloadDir}/me_${Math.floor(date.getTime() + date.getSeconds() / 2)}.pdf`,
            description: 'Downloading.',
          },
        };
        console.log("fileurl 2 ", fileurl);
        config(options).fetch('GET', fileurl).then((res) => {
            console.log('do some magic in here' + res);
        });
      }

      uploadFile = async () => {
        console.log("upload file found");
        const uri = this.state._filename;
        console.log("uri "+ uri);
        const filename = uri.substring(uri.lastIndexOf('/') + 1);
        console.log("filename "+filename);
        const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
        console.log("uploading");
        const task = firebase.storage().ref(filename)
          .putFile(decodeURI(uploadUri.uri));
          
        task.on('state_changed', snapshot => {
          console.log("bytes transfered",snapshot.bytesTransferred);
        },
        error => {
          console.log("error occured here "+error);
        },
        () => {
          firebase.storage()
            .getDownloadURL()
            .then(url => {
              this.setState({
                ...this.state, imageURL: url
              })
              console.log("download url "+this.state.imageURL)
              //this.props.setmsgImgUrl(url)
            })
        });
        console.log("uploaded");
        try {
          await task;
        } catch (e) {
          console.error(e);
        }
        
        this.setState({
          _filename: ""
        })
        return task;
      };

      render(){
        return (
          <View style={styles.container}>
            <StatusBarComponent isHidden={false} />
            {DeviceInfo.isAndroid() &&               
            <ToolbarComponent   
              title= {this.state.thread._id}
              navIcon={Constant.ICONS.BACK_ICON}         
              navClick={() => this.props.navigation.goBack()}
            />
          }
            
               <GiftedChat
                    messages={this.state.messages}
                    onSend={newmessage => this.handleSend(newmessage)}
                    user={{ _id: firebase.auth().currentUser.uid }}
                    placeholder='Type your message here...'
                    alwaysShowSend
                    showUserAvatar
                    scrollToBottom
                    renderBubble={(props) => this.renderBubble(props)}
                    renderLoading={()=>this.renderLoading}
                    renderSend={(props) => this.renderSend(props)}
                    scrollToBottomComponent={()=>this.scrollToBottomComponent}
                    renderSystemMessage={(props) => this.renderSystemMessage(props)}
                    renderActions={messages => this.micBtn(messages)}
                    renderCustomMessage={(props) => this.renderCustomMessage(props)}
                />

{/* renderMessageVideo={(props) => this.renderMessageVideo(props)} */}

                <Modal	
            animationType="none"	
            transparent={true}	
            closeOnClick={true}	
            visible={this.state.selectFromModal}	
            onRequestClose={() => {	
            this.setSelectFromModalVisibility(false)	
            }}>	
            <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'rgba(0,0,0,0.6)'}}>	
            <View style={{width: '90%',backgroundColor: '#FFFFFF', elevation:10, borderTopLeftRadius:8,borderTopEndRadius:8, margin: 5}}>	
                <View style={{ flexDirection: 'row',height:55, backgroundColor: Colors.lightOrange,alignItems:'center'}}>	
                <Text style={{flex: 1, paddingLeft: 7, fontWeight:'bold', fontSize: 17, fontFamily: 'Roboto', color: '#FFFFFF', textAlign: 'left'}}>	
                    {I18n.t('COPPREPOST.SELECT_FROM')}	
                </Text>	
                <View style={{width:80,height:30,justifyContent: 'center',alignSelf:'center',backgroundColor:'#fff',borderRadius:15, marginRight: 10}}>	
                    <TouchableOpacity	
                    onPress={()=>this.setSelectFromModalVisibility(false)}>	
                        <Text style={{ fontWeight:'bold', fontSize: 12, fontFamily: 'Roboto', color: Colors.lightOrange, textAlign: 'center'}}>	
                            {I18n.t('COPPREPOST.DISMISS')}	
                        </Text>	
                    </TouchableOpacity>	
                </View>	
                	
                </View>	
                <View style = {{flexDirection: 'row', height:150,alignSelf: 'center', alignItems:'center', justifyContent: 'center',}}>	
                    <View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>	
                        <TouchableOpacity  style={{width:70,height:70,backgroundColor:'white',alignItems:"center",justifyContent:"center",borderRadius:60}}	
                        onPress={ this.openCamera.bind(this)}>	
                        <Image 	
                            style={{width:60,height:60, tintColor: Colors.lightOrange}}	
                            resizeMode= "center"	
                            source={require('../../Images/Camera/camera.png')} />	
                        </TouchableOpacity>   	
                        <Text style={{alignSelf:'stretch',padding: 5, fontSize: 15, fontFamily: 'Roboto', fontWeight: 'bold', color: Colors.lightOrange,textAlign:'center'}}>{'Camera'}</Text>     	
                    </View>	
                    	
                    <View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>	
                        <TouchableOpacity  style={{width:70,height:70,backgroundColor:'white',alignItems:"center",justifyContent:"center",borderRadius:60}}	
                        onPress={this.pickAttachmenet.bind(this)}>	
                        <Image 	
                            style={{width:60,height:60, tintColor: Colors.lightOrange}}	
                            resizeMode= "center"	
                            source={require('../../Images/Camera/choose_gallery.png')} />	
                        </TouchableOpacity>   	
                        <Text style={{alignSelf:'stretch',padding: 5, fontSize: 15, fontFamily: 'Roboto', fontWeight: 'bold', color: Colors.lightOrange,textAlign:'center'}}>{'Gallery'}</Text>      	
                    </View>     	
                    </View>	
                </View>	
            </View>	
          </Modal>

          </View>
        );
      }
      
}

const styles = StyleSheet.create({
    loadingContainer: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    sendingContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#6646ee'
    },
    bottomComponentContainer: {
      justifyContent: 'center',
      alignItems: 'center'
    },
    systemMessageWrapper: {
      backgroundColor: '#6646ee',
      borderRadius: 4,
      padding: 5
    },
    systemMessageText: {
      fontSize: 14,
      color: '#fff',
      fontWeight: 'bold'
    },
    myStyle: {
      flexDirection: 'row',
      alignItems: 'center',
      //backgroundColor: '#485a96',
      borderWidth: 0.5,
      borderColor: '#fff',
      height: 40,
      width: 35,
      borderRadius: 5,
      margin: 5,
    },
    MicIconStyle: {
      padding: 5,
      marginRight: 10,
      height: 35,
      width: 35,
      resizeMode: 'contain',
    },
    container: {
      flex: 1
    },
    documentMessage: {
      flex:1,
      justifyContent: "center",
      alignItems: "center", 
      backgroundColor: '#0084ff', 
      flexDirection: 'column',
      borderBottomLeftRadius: 15,
      borderTopLeftRadius: 15,
      borderTopRightRadius: 15,
      borderBottomRightRadius: 15,
      padding: 5,
      borderColor: '#ddd'
    }
  });