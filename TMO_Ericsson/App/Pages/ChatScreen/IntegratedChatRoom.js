import React , { Component } from 'react';
import { GiftedChat, Bubble, Send, SystemMessage, Time } from 'react-native-gifted-chat';
import { ActivityIndicator, View, StyleSheet, Image, TouchableOpacity, Modal, Text, Platform, Alert, PermissionsAndroid } from 'react-native';
import firebase from 'react-native-firebase';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';	  
import {Colors} from '../../Styles/Themes'; 
import I18n from '../../Utils/i18n';
const RNFS = require("react-native-fs");
import FileViewer from "react-native-file-viewer";
import { launchImageLibrary } from "react-native-image-picker";
var RNFetchBlob = require('react-native-fetch-blob').default;
import AsyncStorage from '@react-native-community/async-storage';	
import MessageImage from '../MessageImageViewer/MessageImage';
//import Video from 'react-native-video';   // This dependency has been removed, add updated version or other library. App on start is crashing due to lower version of okhttp library used in this dependency.
import StatusBarComponent from '../../Components/StatusBar';
import ToolbarComponent from "../../Components/Toolbar";
import { DeviceInfo } from '../../Utils/Utils';
import Constant from '../../Utils/Constant';
import FormData from 'form-data';

export default class IntegratedChatWindow extends Component{

  deviceId_set = new Set();
  serverKey = 'AAAABHositI:APA91bEuweKrYFSl6Gbx8viHnNCTweYUhWmwUsyQ_5Tbcfmqc-IFdBPE2i0QLI76F3Z5wOj8A8xckaFuFhnIFn3JGxMPDwdxFeuT8add96Xjw-q7lBZ_zFoxM-LGgzS8YoP4M8o18ef4';

    constructor(props) {
        super(props);
    }

  state = {
      messages: [],
      thread: this.props.navigation.getParam('paramKey',null),
      selectFromModal: false, 
      _filename: "",
      imageURL: "",
      userId:"",  // this.state.userId
      userName:"", // this.state.userName
      userEmail:""  // this.state.userEmail
  }

  handleSend = async(messages) => {
    const text = messages[0].text;
    let fcm_id = await AsyncStorage.getItem("fcmToken");
    console.log("deviceId_set", Array.from(this.deviceId_set));
    firebase.firestore()
      .collection('SITES')
      .doc(this.props.navigation.state.params.siteId)// unable to get the state so directly using navigation props
      .collection('MESSAGES')
      .add({
        text,
        createdAt: new Date().getTime(),
        user: {
          _id: this.state.userId,
          email: this.state.userEmail,
          name: this.state.userName,
          deviceId: fcm_id
        }
      });

    await firebase.firestore()
      .collection('SITES')
      .doc(this.props.navigation.state.params.siteId)//this.state.thread._id
      .set(
        {
          latestMessage: {
            text,
            createdAt: new Date().getTime()
          }
        },
        { merge: true }
      );
      this.sendNotifications(text);
      let fcm_ids = await AsyncStorage.getItem("fcmToken");
      console.log("fcm_id -> ",fcm_ids);
  }

  sendNotifications(text) {
    console.log("Array set -> ", Array.from(this.deviceId_set));
    var notification = "";
    if(text!=null)
    notification = {
      'title': this.props.navigation.state.params.siteId,
      'body': text,
      'icon': '../../Images/ChatIcons/login_logo.png'
    };
    var body_data = {
      'notification': notification,
      'registration_ids': Array.from(this.deviceId_set)
    };
    
    //console.log("body_data => ", body_data);
fetch('https://fcm.googleapis.com/fcm/send', {
    method: 'POST',
    headers: {
        Authorization: 'key='+this.serverKey,
        'Content-Type': 'application/json'
    },
    body: JSON.stringify(body_data)
})
.then(function (response) {
    console.log("response -> ",response);
}).catch(function (error) {
    console.error("error -> ",error);
})
}

  handleDocumentSend = async(messages, filetype, filename) => {
    var doctype = "New Document received";
    const text = messages;
    let fcm_id = await AsyncStorage.getItem("fcmToken");
    var senddoc = "";
    console.log("filetype ", filetype);
    if(filetype.includes('image')){
      doctype = "New Image Received";
    senddoc = {
      img: text,
      createdAt: new Date().getTime(),
      user: {
        _id: this.state.userId,
        email: this.state.userEmail,
        name: this.state.userName,
        deviceId: fcm_id
      }
    };
    }else{ // Any kind of file is included
      doctype = "New Document Received";
      // if(filename.includes('application') || filename.includes('doc') || filename.includes('docx') || filename.includes('pdf')
      // || filename.includes('txt') || filetype.includes('text'))
      senddoc = {
        application: text,
        filename: filename,
        createdAt: new Date().getTime(),
        user: {
          _id: this.state.userId,
          email: this.state.userEmail,
          name: this.state.userName,
          deviceId: fcm_id
        }
    }
    // else if(filetype.includes('video')){  // This is used to render video in chat
    //   doctype = "New Video Received";
    //   senddoc = {
    //     video: text,
    //     createdAt: new Date().getTime(),
    //     user: {
    //       _id: this.state.userId,
    //       email: this.state.userEmail,
    //       name: this.state.userName,
    //       deviceId: fcm_id
    //     }
    //   };
    // }
  }
    
    console.log("senddoc ", senddoc);
    console.log("text", text);
    firebase.firestore()
      .collection('SITES')
      .doc(this.props.navigation.state.params.siteId)//this.state.thread._id   //this.props.navigation.getParam('paramKey',null)
      .collection('MESSAGES')
      .add(senddoc);

    await firebase.firestore()
      .collection('SITES')
      .doc(this.props.navigation.state.params.siteId)//this.state.thread._id
      .set(
        {
          latestMessage: {
            text,
            createdAt: new Date().getTime()
          }
        },
        { merge: true }
      );

      this.sendNotifications(doctype);
  }

componentDidMount() {
  this.requestStoragePermission();
  this.getUsersLocalDetails();
    console.log("params", this.props.navigation.state.params.siteId);//this.state.thread._id

    // First check if the site id exists or not 
    var usersRef = firebase.firestore().collection('SITES').doc(this.props.navigation.state.params.siteId);

          usersRef.get()
                  .then((docSnapshot) => {
                          if (docSnapshot.exists) {
                          usersRef.onSnapshot((doc) => {
                          // do stuff with the data
                        });
                    } else {
                        usersRef.set({
                          name: this.props.navigation.state.params.siteId,
                          latestMessage: {
                            text: "You have joined the room "+ this.props.navigation.state.params.siteId,
                            createdAt: new Date().getTime()
                          }
                        }) // create the document

                        firebase.firestore().collection("SITES").doc(this.props.navigation.state.params.siteId).collection("MESSAGES").add({
                          text: "You have joined the room "+ this.props.navigation.state.params.siteId,
                          createdAt: new Date().getTime(),
                          system: true
                        });
                      }
                    });


    const messagesListener = firebase.firestore()
    .collection("SITES")
    .doc(this.props.navigation.state.params.siteId)//this.state.thread._id
    .collection('MESSAGES')
    .orderBy('createdAt', 'desc')
    .onSnapshot(querySnapshot => {
        const messages = querySnapshot.docs.map(doc => {
          const firebaseData = doc.data();

          const data = {
            _id: doc.id,
            text: '',
            img: '',
            createdAt: new Date().getTime(),
            ...firebaseData
          };

          if (!firebaseData.system) {
            data.user = {
              ...firebaseData.user,
              name: firebaseData.user.name
            };
            if(this.state.userId != data.user._id){
                this.deviceId_set.add(data.user.deviceId);
            }
          }
          
          return data;
        });
        this.setState({messages});
      });
      return () => messagesListener();
}

renderName = (props) => {
    //const { user: self } = this.props; // where your user data is stored;
    //console.log("props.currentMessage => ", props.currentMessage.user.name);
    const { user = {} } = props.currentMessage;
    const { user: pUser = {} } = props.previousMessage;
    const isSameUser = pUser._id === user._id;
    const isSelf = user._id === this.state.userId;
    const shouldNotRenderName = isSameUser;
    let firstName = user.name.split(" ")[0];
    let lastName = user.name.split(" ")[1]; // user.name.split(" ")[1][0] // To print only last name first word
    return shouldNotRenderName ? (
        <View />
    ) : (
            <View>
            {isSelf ? 
                <Text style={{ fontWeight: 'bold', color: "blue", padding: 2, alignSelf: "flex-end" }}>
                    {`${firstName} ${lastName}`}
                </Text> 
                : 
                <Text style={{ fontWeight: 'bold', color: "black", padding: 2, alignSelf: "flex-start" }}>
                    {`${firstName} ${lastName}`}
                </Text>}
                
            </View>
        );
  }

  renderTime = (props) => {
    return (
      <Time
      {...props}
        timeTextStyle={{
          left: {
            color: 'white',
          },
          right: {
            color: 'white',
          },
        }}
      />
    );
  };

renderBubble = (props) => {
    return (
      <View>
        {this.renderName(props)}
        {this.renderImage(props)}
        {this.renderCustomMessage(props)}
      <Bubble
        {...props}
        textStyle={{
          right: {
              color: '#000',
          },
          left: {
              color: '#fff',
          },
        }}
        wrapperStyle={{
          left: {
              backgroundColor: "#ff704d",
          },
          right: {
              backgroundColor: '#ff6666'
          }
        }}
      />
      </View>
    );
  }

  renderImage = (props) => {
    if(props.currentMessage.img){
      console.log("image -> ",props.currentMessage.img);
    return(
      <MessageImage message={props.currentMessage.img} />
    );
  }
  }

  // renderMessageVideo = (props) => { // This function is used to show video in chat. Drawback is it has Okhttp library that would crash for the version and when changed the version of Okhttp then it doesn't create release build
  //   if(props.currentMessage.video){ // It only renders video and play withing chat without opening a new video window
  //   return (
  //     <View style={{ padding: 1 }}>
  //        {/* <Video
  //         resizeMode="contain"
  //         useNativeControls
  //         shouldPlay={false}
  //         source={{ uri: props.currentMessage.video }}
  //         style={{height:100, width: 125 , backgroundColor: '#6646ee'}}
  //       /> */}
  //       <Video source={{uri: props.currentMessage.video}}   // Can be a URL or a local file.
  //         ref={(ref) => {
  //             this.player = ref
  //             }}                                      // Store reference
  //         onBuffer={this.onBuffer}                // Callback when remote video is buffering
  //         onError={this.videoError}               // Callback when video cannot be loaded
  //         style={{height:100, width: 150 , backgroundColor: '#6646ee'}} />
  //       </View>
  //     );
  //   }
  // };

  renderCustomMessage = (props) => {
    if(props.currentMessage.filename){
    //   console.log("application ", props.currentMessage.application);
    //   const { user = {} } = props.currentMessage;
    // const { user: pUser = {} } = props.previousMessage;
    // const isSameUser = pUser._id === user._id;
    // const isSelf = user._id === this.state.userId;
    // const shouldNotRenderName = isSameUser;
    // return shouldNotRenderName ? (
    //     <View />
    // ) : (
    //         <View>
    //         {isSelf ? 
    //             <View style={styles.documentMessage}>
    //             <Text style={{textAlign: 'center', width: 200 , backgroundColor: '#ff6666', color: 'white', marginTop: 8, marginBottom: 8}}>
    //             {props.currentMessage.filename}
    //             </Text>
    //             <TouchableOpacity onPress={()=>this.downloadfile(props.currentMessage.application, props.currentMessage.filename)}>
    //               <Image source={require('../../Images/ChatIcons/downloadbtn.png')}/>
    //             </TouchableOpacity>
    //             </View>
    //             : 
    //             <View style={styles.documentMessageleft}>
    //             <Text style={{textAlign: 'center', width: 200 , backgroundColor: '#ff704d', color: 'white', marginTop: 8, marginBottom: 8}}>
    //             {props.currentMessage.filename}
    //             </Text>
    //             <TouchableOpacity onPress={()=>this.downloadfile(props.currentMessage.application, props.currentMessage.filename)}>
    //               <Image source={require('../../Images/ChatIcons/downloadbtn.png')}/>
    //             </TouchableOpacity>
    //             </View>}   
    //         </View>
    //     );




        return(
          <View style={styles.documentMessage}>
                <Text style={{textAlign: 'center', width: 200 , backgroundColor: '#ff6666', color: 'white', marginTop: 8, marginBottom: 8}}>
                {props.currentMessage.filename}
                </Text>
                <TouchableOpacity onPress={()=>this.downloadfile(props.currentMessage.application, props.currentMessage.filename)}>
                  <Image source={require('../../Images/ChatIcons/downloadbtn.png')}/>
                </TouchableOpacity>
          </View>
        );
    }
  }

renderLoading = () =>{
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size='large' color='#6646ee' />
      </View>
    );
  }

  renderSend = (props) => {
    return (
      <Send {...props}>
        <View>
            <Image source={require("../../Images/ChatIcons/sendcircle.png")}/>
        </View>
      </Send>
    );
  }

  scrollToBottomComponent = () => {
    return (
      <View style={styles.bottomComponentContainer}>
          <TouchableOpacity>
            <Image source={require("../../Images/ChatIcons/chevron_double_down.png")}/>
        </TouchableOpacity>
      </View>
    );
  }

  renderSystemMessage = (props) => {
    return (
      <SystemMessage
        {...props}
        wrapperStyle={styles.systemMessageWrapper}
        textStyle={styles.systemMessageText}
      />
    );
  }

  setSelectFromModalVisibility(visibility)	
  {	
    this.setState({selectFromModal: visibility});  	
  }	

  showAlert = (title, body) => {
		Alert.alert(
		  title, body,
		  [
			  { text: 'OK'},
		  ],
		  { cancelable: true },
		);
	  }

pickDocument = () => { // Required to write code for ios
  console.log("res 0");
    DocumentPicker.show({
        filetype: [DocumentPickerUtil.allFiles()],
      },(error,res) => {
        console.log("res 1 ",res);
        // Android
        if(res != null){
        this.setState({path: res.uri});
        this.setState({name: res.fileName});
        
        console.log("res 2 ",res);
        RNFetchBlob.fs
        .stat(res.uri)
        .then(stats => {
          console.log("stats.path 3 ",stats.path + " -- "+ res.type);
          this.uploadFileToServer(stats.path, res.fileName, res.type, "Gallery");
        }).catch(
          (error) => console.log(error)
        )
        console.log("res 3 ",res);
        }
    });
}

handleOnNavigateBack = async (refreshPage) => {	
  let returnFrom  = await AsyncStorage.getItem("okCamera");	
  if(returnFrom !== "ok")	
    return;	
    
  let picturePath = await AsyncStorage.getItem("picturePath");
  console.log("sunil picturePath =>",picturePath);
  var filename = picturePath.substring(picturePath.lastIndexOf("/") + 1, picturePath.length);
  console.log("sunil filename => ",filename);
  this.uploadFileToServer(picturePath, filename, "image/jpeg", "Camera");
  await AsyncStorage.removeItem("picturePath");	
  //this.resize1(picturePath);	
}	

  openCamera= async()=>{	
    this.setSelectFromModalVisibility(false);	
    // AsyncStorage.setItem("fromCOP", 1);	
    this.props.navigation.navigate('CaptureNFSDSnapShot',{onNavigateBack: this.handleOnNavigateBack});	
  }

getFileLocalPath = (response) => {
  const { path, uri } = response;
  console.log("response path-> "+response);
  return Platform.OS === 'android' ? response : uri; // path
};

createStorageReferenceToFile = (filename) => {
  const id = new Date().getTime();
  return firebase.storage().ref().child(`uploads/${id}-${filename}`);// fileName
};

uploadFileToFireBase = (response, filename) => {
  const fileSource = this.getFileLocalPath(response);
  const storageRef = this.createStorageReferenceToFile(filename);
  return storageRef.putFile(fileSource);
};

uploadFileToServer = async (response, filename, filetype, fromwhere) => {// This method is used to send files to AWS server only this is used in this project.
  var filepath = this.getFileLocalPath(response);
  console.log("filepath => ",filepath);

  if(fromwhere === "Gallery"){ // This is required because it is required to have file:// concatenated in from of file path for "Camera" - it handles automatically
    filepath = 'file://' + filepath;
  }

    var formdata = new FormData();
    formdata.append('file', {
      name: filename,
      type: filetype,
      uri: filepath,
    });
    
    const res = await fetch(
      Constant.Api.ChatFileUpload+Constant.Api.CHAT.FILEUPLOAD,    // 'https://entmomcps.mpulsenet.com/api/chat/UploadFile',
      {
        method: 'POST',
        body: formdata
      }
    );
    
    let responseJson = await res.json();
    console.log("file responseJson => ",responseJson);
    this.handleDocumentSend(responseJson.data, filetype, responseJson.message);
}

useCallback = (response, filename, filetype) => {  // This method is used to send files to Firebase firestore only and can be removed later has no use currently
  console.log("upload line 1")
  const uploadTask = this.uploadFileToFireBase(response, filename);
  console.log("upload line 2")
  const unsubscribe = uploadTask.on(
    firebase.storage.TaskEvent.STATE_CHANGED,
    async snapshot => {
    },
    error => {
      this.showAlert("error = ",error);
      //console.log("error ->> ",error);
    },
    complete => {
      console.log("Completed ->> ",complete);
      console.log("download url --> ", complete.downloadURL);
      const downloadurl = complete.downloadURL;
      this.handleDocumentSend(downloadurl, filetype, filename);
      unsubscribe();
    }
  );
};

 pickAttachmenet = async () => {
  this.setSelectFromModalVisibility(false)
  if (Platform.OS === "ios") {
    const options = ["Image", "Document", "Cancel"];
    ActionSheetIOS.showActionSheetWithOptions(
      { options, cancelButtonIndex: 2, title: "Pick a data type" },
      async (buttonIndex) => {
        if (buttonIndex === 0) {
          // Open Image Picker
          launchImageLibrary({ mediaType: "photo" }, (res) => {
            if (!res.didCancel) {
              return { path: res.uri, name: res.fileName };
            }
          });
        } else if (buttonIndex === 1) {
          // Open Document Picker
          return this.pickDocument();
        } else {
          // exit
        }
      }
    );
  } else {
    // For Android we can just use the normal DocumentPicker, as it can also access images
    return this.pickDocument();
  }
};

requestStoragePermission = async () => {

  if (Platform.OS !== "android") return true

  const pm1 = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE);
  const pm2 = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);

  if (pm1 && pm2) return true

  const userResponse = await PermissionsAndroid.requestMultiple([
      PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
  ]);

  if (userResponse['android.permission.READ_EXTERNAL_STORAGE'] === 'granted' &&
      userResponse['android.permission.WRITE_EXTERNAL_STORAGE'] === 'granted') {
      return true
  } else {
      return false
  }
}

  attachBtn = (sendProps) => {
    return (
      <View flexDirection='row' onTouchStart={() => this.attachBtnPressed()}>
      <TouchableOpacity style={styles.myStyle} activeOpacity={0.5}>
      <Image
        source={require('../../Images/ChatIcons/attachment.png')}
        style={styles.MicIconStyle}
      />
    </TouchableOpacity>
    </View>
    );
  }

  attachBtnPressed = () => { // messages=[]
    this.setState({
      selectFromModal: true
    })
  }

  downloadfile = (fileurl, filename) => {
    console.log("fileurl 1 ", fileurl);
    const { config, fs } = RNFetchBlob;
    const date = new Date();

    const { DownloadDir } = fs.dirs; // You can check the available directories in the wiki.
    const options = {
        fileCache: true,
        addAndroidDownloads: {
        useDownloadManager: true, // true will use native manager and be shown on notification bar.
        notification: true,
        path: `${DownloadDir}/${filename}`,
        description: 'Downloading.',
      },
    };
    console.log("fileurl 2 ", fileurl);
    config(options).fetch('GET', fileurl).then((res) => {
        console.log('do some magic in here' + res);
    });
  }

  uploadFile = async () => {
    console.log("upload file found");
    const uri = this.state._filename;
    console.log("uri "+ uri);
    const filename = uri.substring(uri.lastIndexOf('/') + 1);
    console.log("filename "+filename);
    const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
    console.log("uploading");
    const task = firebase.storage().ref(filename)
      .putFile(decodeURI(uploadUri.uri));
      
    task.on('state_changed', snapshot => {
      console.log("bytes transfered",snapshot.bytesTransferred);
    },
    error => {
      console.log("error occured here "+error);
    },
    () => {
      firebase.storage()
        .getDownloadURL()
        .then(url => {
          this.setState({
            ...this.state, imageURL: url
          })
          console.log("download url "+this.state.imageURL)
          //this.props.setmsgImgUrl(url)
        })
    });
    console.log("uploaded");
    try {
      await task;
    } catch (e) {
      console.error(e);
    }
    
    this.setState({
      _filename: ""
    })
    return task;
  };

  getUsersLocalDetails=async()=>{
		const userDetails = await AsyncStorage.getItem(Constant.LOCAL_STORAGE.LOGINRESPONSE);
	
		try
		{
		  console.log("user_detillllll",JSON.parse(userDetails))
		  let userDetailsObject=JSON.parse(userDetails);
		   
		  let contactName=userDetailsObject.crew_lead_name;
      this.setState({userName: contactName});  	
		  let contactEmail=userDetailsObject.crew_lead_email_id;
      this.setState({userEmail: contactEmail});  	
		  let userid= userDetailsObject.id
      this.setState({userId: userid});  	
		}
		catch(err)
		{
		   
		}
	  }

  render(){
    return (
      <View style={styles.container}>
        <StatusBarComponent isHidden={false} />
        {DeviceInfo.isAndroid() &&               
        <ToolbarComponent   
          title= {this.props.navigation.state.params.siteId}
          navIcon={Constant.ICONS.BACK_ICON}         
          navClick={() => this.props.navigation.goBack()}
        />
      }
                        {/* renderUsernameOnMessage={true} // will show user name within the chat message but only for other user */}
                        {/* showUserAvatar // To show current user's avatar*/}
           <GiftedChat
                messages={this.state.messages}
                onSend={newmessage => this.handleSend(newmessage)}
                user={{_id: this.state.userId }}
                placeholder='Type your message here...'
                alwaysShowSend
                scrollToBottom
                showUserAvatar
                renderBubble={(props) => this.renderBubble(props)}
                renderLoading={()=>this.renderLoading}
                renderSend={(props) => this.renderSend(props)}
                scrollToBottomComponent={()=>this.scrollToBottomComponent}
                renderSystemMessage={(props) => this.renderSystemMessage(props)}
                renderActions={messages => this.attachBtn(messages)}
                renderCustomMessage={(props) => this.renderCustomMessage(props)}
                renderTime={(props)=>this.renderTime(props)}
            />
            {/* renderMessageVideo={(props) => this.renderMessageVideo(props)} */}

            <Modal	
        animationType="none"	
        transparent={true}	
        closeOnClick={true}	
        visible={this.state.selectFromModal}	
        onRequestClose={() => {	
        this.setSelectFromModalVisibility(false)	
        }}>	
        <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'rgba(0,0,0,0.6)'}}>	
        <View style={{width: '90%',backgroundColor: '#FFFFFF', elevation:10, borderTopLeftRadius:8,borderTopEndRadius:8, margin: 5}}>	
            <View style={{ flexDirection: 'row',height:55, backgroundColor: Colors.lightOrange,alignItems:'center'}}>	
            <Text style={{flex: 1, paddingLeft: 7, fontWeight:'bold', fontSize: 17, fontFamily: 'Roboto', color: '#FFFFFF', textAlign: 'left'}}>	
                {I18n.t('COPPREPOST.SELECT_FROM')}	
            </Text>	
            <View style={{width:80,height:30,justifyContent: 'center',alignSelf:'center',backgroundColor:'#fff',borderRadius:15, marginRight: 10}}>	
                <TouchableOpacity	
                onPress={()=>this.setSelectFromModalVisibility(false)}>	
                    <Text style={{ fontWeight:'bold', fontSize: 12, fontFamily: 'Roboto', color: Colors.lightOrange, textAlign: 'center'}}>	
                        {I18n.t('COPPREPOST.DISMISS')}	
                    </Text>	
                </TouchableOpacity>	
            </View>	
              
            </View>	
            <View style = {{flexDirection: 'row', height:150,alignSelf: 'center', alignItems:'center', justifyContent: 'center',}}>	
                <View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>	
                    <TouchableOpacity  style={{width:70,height:70,backgroundColor:'white',alignItems:"center",justifyContent:"center",borderRadius:60}}	
                    onPress={ this.openCamera.bind(this)}>	
                    <Image 	
                        style={{width:60,height:60, tintColor: Colors.lightOrange}}	
                        resizeMode= "center"	
                        source={require('../../Images/Camera/camera.png')} />	
                    </TouchableOpacity>   	
                    <Text style={{alignSelf:'stretch',padding: 5, fontSize: 15, fontFamily: 'Roboto', fontWeight: 'bold', color: Colors.lightOrange,textAlign:'center'}}>{'Camera'}</Text>     	
                </View>	
                  
                <View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>	
                    <TouchableOpacity  style={{width:70,height:70,backgroundColor:'white',alignItems:"center",justifyContent:"center",borderRadius:60}}	
                    onPress={this.pickAttachmenet.bind(this)}>	
                    <Image 	
                        style={{width:60,height:60, tintColor: Colors.lightOrange}}	
                        resizeMode= "center"	
                        source={require('../../Images/Camera/choose_gallery.png')} />	
                    </TouchableOpacity>   	
                    <Text style={{alignSelf:'stretch',padding: 5, fontSize: 15, fontFamily: 'Roboto', fontWeight: 'bold', color: Colors.lightOrange,textAlign:'center'}}>{'Gallery'}</Text>      	
                </View>     	
                </View>	
            </View>	
        </View>	
      </Modal>

      </View>
    );
  }
  
}

const styles = StyleSheet.create({
loadingContainer: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center'
},
sendingContainer: {
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: '#6646ee'
},
bottomComponentContainer: {
  justifyContent: 'center',
  alignItems: 'center'
},
systemMessageWrapper: {
  backgroundColor: '#ff6666',
  borderRadius: 4,
  padding: 5
},
systemMessageText: {
  fontSize: 14,
  color: '#fff',
  fontWeight: 'bold'
},
myStyle: {
  flexDirection: 'row',
  alignItems: 'center',
  //backgroundColor: '#485a96',
  borderWidth: 0.5,
  borderColor: '#fff',
  height: 40,
  width: 35,
  borderRadius: 5,
  margin: 5,
},
MicIconStyle: {
  padding: 5,
  marginRight: 10,
  height: 35,
  width: 35,
  resizeMode: 'contain',
},
container: {
  flex: 1
},
documentMessage: {
  flex:1,
  justifyContent: "center",
  alignItems: "center", 
  backgroundColor: '#ff6666', 
  flexDirection: 'column',
  borderBottomLeftRadius: 6,
  borderTopLeftRadius: 6,
  borderTopRightRadius: 6,
  padding: 5,
  borderColor: '#ddd'
},
documentMessageleft: {
  flex:1,
  justifyContent: "center",
  alignItems: "center", 
  backgroundColor: '#ff704d', 
  flexDirection: 'column',
  borderTopLeftRadius: 6,
  borderTopRightRadius: 6,
  borderBottomRightRadius: 6,
  padding: 5,
  borderColor: '#ddd'
}
});