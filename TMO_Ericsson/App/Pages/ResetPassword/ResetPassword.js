import React, { Component } from 'react';
import ToolbarComponent from "../../Components/Toolbar";
import I18n from '../../Utils/i18n';
import { DeviceInfo,Dialog,GlobalVariable } from '../../Utils/Utils'
import {Image,Text,TouchableOpacity,View,KeyboardAvoidingView,TextInput} from 'react-native';
import { ResetPasswordStyles } from '../../Styles/PageStyle';
import StatusBarComponent from '../../Components/StatusBar';
import OfflineBarComponent from '../../Components/OfflineBar';
import Constant from '../../Utils/Constant';
import Icon from 'react-native-vector-icons/Ionicons';
import {Colors} from '../../Styles/Themes'; 
import Placeholder,{Line} from 'rn-placeholder';
import {PostRequest} from '../../Utils/ServiceCalls'; 
import Loader from '../../Components/Loader'; 
import Toast from 'react-native-simple-toast';
import AppImages from '../../Images';
import AsyncStorage from '@react-native-community/async-storage';



export default class ResetPasswordScreen extends Component {
  constructor(props) {
    super(props); 
    this.state = { 
      isLoadervisible:false,
      currentPassword:"",
      enterPassword:"",
    };
  }


  goToPreviousPage = () =>{
    this.props.navigation.state.params.onNavigateBack();
    this.props.navigation.goBack()
    return true;
  }
  

 /**	   
	* @desc display alert dialog box
	* @param :title: string title of alert dialog
    * @param :message: string message of alert dialog
    * @param :callback: callback function on ok button click
	* @returns none
	*/
  displayAlertMsg=(title,message,callback)=>{
    Dialog.alertDialog(title,message,I18n.t('OK_BUTTON'),false,null).then(()=>{
        if(callback){
            callback();
        }
    })
} 

/**	   
	* @desc validate mobile number and password and display alert if wrong
	* @param :mobile: number mobile number of user
    * @param :password: string password of user 
	* @returns bool if valid credentials true else false
	*/ 
    validattion=(enterPassword)=>{ 
        if(enterPassword === "" || enterPassword === null)
        {
            Toast.showWithGravity("Please enter new password !", Toast.SHORT, Toast.BOTTOM);
            return false;
        }
        
        else
        {
            return true;
        } 
    }

    validattionUser= async (currentPassword)=>{ 

        if(currentPassword === "" || currentPassword === null)
        {
            Toast.showWithGravity("Please enter your current password !", Toast.SHORT, Toast.BOTTOM);
            return false;
        }
        let storedPassword = await AsyncStorage.getItem("Password");
        if(currentPassword === storedPassword){ 
            return true;
        }else {
            this.displayAlertMsg(I18n.t('RESETPASSWORD.INVALIDPASSWORD'),I18n.t('RESETPASSWORD.PASSWORDINCORRECTMESSAGE'));
            return false; 
        }
    }


    /**	   
	* @desc call API end point and authenticate user's credentials
	* @param :none
	* @returns none
	*/ 
    resetPassword=async ()=>{ 
        let currentPassword     = this.state.currentPassword.trim();
        let isValidated1 = await this.validattionUser(currentPassword);
        if(!isValidated1){
            return;
        }
        else
        {
            let enterPassword         = this.state.enterPassword.trim();
            let isValidated2 = await this.validattion(enterPassword); 
            //stop code execution due to invalid credentials
            if(!isValidated2){
                return;
            }
            else
            {
                let tbl_gc_id = GlobalVariable.USERID;
                let paramObj = {"crewId":tbl_gc_id,"password":enterPassword};
                let paramObject = {
                    path: Constant.Api.PASSWORD_RESET.RESET,
                    params: paramObj,
                    showErrorMsg: true 
                };
        
        
                //display loader first
                this.setState({isLoadervisible:true},()=>{
                    PostRequest(paramObject).then((responseApisuccess) => {
                        this.setState({isLoadervisible:false}, async()=>{
                            let response=responseApisuccess.data; 
                            //  alert(JSON.stringify(responseApisuccess.data))
                            //user does not exist
                            if(!response || !response.status || response.status==0){
                                this.displayAlertMsg(I18n.t('RESETPASSWORD.ERROR'),I18n.t(response.message),()=>{
                                    // this.setState({mobileNumber:"",password:""});
                                });     
                            }
                                
                            else
                            {
                                await AsyncStorage.setItem("Password", enterPassword);
                                this.props.navigation.goBack(null);
                                Toast.show("Your password updated", Toast.SHORT);
                            }
                        });
                    }, (error) => {
                        this.setState({isLoadervisible:false},()=>{});
                    }); 
                });
            }
        }
    }
   

  render() {  

    return (
        <View style={ResetPasswordStyles.container} >
            <StatusBarComponent isHidden={false} />
            <OfflineBarComponent />
            {
                    this.state.isLoadervisible &&
                    <Loader msg={I18n.t('LOADING')} />
            }
            { DeviceInfo.isAndroid() && 
                <ToolbarComponent 
                title={I18n.t('RESETPASSWORD.PAGETITLE')}
                navIcon={Constant.ICONS.BACK_ICON}
                navClick={() => this.props.navigation.goBack(null)}
                /> 
            }
        
            <View style={ResetPasswordStyles.formContainer}>
            {/* <Image
                resizeMode="contain"
                style={[ResetPasswordStyles.logoImageStyle,ResetPasswordStyles.addMarginBottom]}
                source={AppImages.Logo_Login_Image}
            /> */}

                        <Text style={ResetPasswordStyles.inputHeadingTitle}>
                            {I18n.t('RESETPASSWORD.CURRENTPASSWORD')}
                        </Text>
                        <TextInput style={ResetPasswordStyles.inputField}
                            autoCapitalize="none"
                            keyboardType='default' 
                            maxLength = {20}
                            autoFocus = {true} 
                            onChangeText={(currentPassword) => this.setState({currentPassword:currentPassword})}
                            secureTextEntry />

                        <Text style={ResetPasswordStyles.inputHeadingTitle}>
                        {I18n.t('RESETPASSWORD.NEWPASSWORD')}
                        </Text>
                        <TextInput style={[ResetPasswordStyles.inputField,ResetPasswordStyles.addMarginBottom]}
                            autoCapitalize="none"
                            keyboardType='default'
                            maxLength = {20}
                            onChangeText={(enterPassword) => this.setState({enterPassword:enterPassword})}
                            secureTextEntry />
                            {/* <Text style={ResetPasswordStyles.inputHeadingTitle}>
                        {I18n.t('RESETPASSWORD.REENTERPASSWORD')}
                        </Text>
                        <TextInput style={[ResetPasswordStyles.inputField,ResetPasswordStyles.addMarginBottom]}
                            autoCapitalize="none"
                            keyboardType='default'
                            maxLength = {20}
                            onChangeText={(reEnterPassword) => this.setState({reEnterPassword:reEnterPassword})}
                            secureTextEntry /> */}
                        <TouchableOpacity style={ResetPasswordStyles.buttonContainer} onPress={() => this.resetPassword()}>
                            <Text style={ResetPasswordStyles.buttonText}>
                                {I18n.t('RESETPASSWORD.RESET')}
                            </Text>
                        </TouchableOpacity>
            </View> 
        </View>
    );
  }
}