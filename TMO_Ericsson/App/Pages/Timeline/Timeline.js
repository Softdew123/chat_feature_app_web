import React, { PureComponent,Component } from 'react';
import ToolbarComponent from "../../Components/Toolbar";
import I18n from '../../Utils/i18n';
import { CommonMethods, DeviceInfo, Dialog } from '../../Utils/Utils'
import {  View, Text, Dimensions, TouchableOpacity, TextInput, Image, StyleSheet ,Dimension, ScrollView} from 'react-native';
import { StyleDashboard } from '../../Styles/PageStyle';
import { TimelineStyle } from '../../Styles/PageStyle';  
import StatusBarComponent from '../../Components/StatusBar';                
import DatePicker from 'react-native-datepicker'    
import Constant from '../../Utils/Constant';                                 
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';  
import AddNew from './../../Images/Cope/addNew.png'
import Loader from '../../Components/Loader';  
import Removeicon from './../../Images/Cope/delete.png'   
import { array } from 'prop-types';
const width = Dimensions.get('window').width;
const height =  Dimensions.get('window').height ;
 
export default class GCSiteLoingScreen extends PureComponent {
    

  constructor(props) {
  super(props);
    this.currenttime=this.getCurrentTime();
	this.currentDate=this.getCurrentDate(); 
  this.state = {
    email: '',
    password: '',  
    date:new Date(),
    data:[{text: "", time:this.currenttime}] ,    
    sdata:[],   
    item:{},crewId:""
 } }    
  componentDidMount(){
    let params= this.props.navigation.state.params;
    this.setState({item:params.item,crewId:params.crewId},async ()=>{
      this.getData()    
      let key =this.state.item.site_id+3
     
      try{
     let data= await  AsyncStorage.getItem(key);
     console.log("dataaatttt",data)
     data= JSON.parse(data)
     let blankData = [];
      if(Array.isArray(data)){
        for (let index = 0; index < data.length; index++) {
          if(index == data.length - 1)
          {
            if(data[data.length - 1].text == "")
            {
              data.splice(data.length - 1, 1, {text: "", time: this.getCurrentTime()});
            }
          }
          else
          {
            if(data[index].text == "")
            {
              blankData.push(index);
            }
          }
        }
        let reverseData = blankData.reverse();
        for (let i = 0; i < reverseData.length; i++) {
          data.splice(reverseData[i], 1);
        }
        this.setState({data:data})

      }
     }      
     catch(err){  

     }
    })  
  }
  componentWillUnmount(){
    let key =this.state.item.site_id+3
    let data =JSON.stringify(this.state.data)
    
    AsyncStorage.setItem(key,data)
  }
  onRemove=(i)=>{
    console.log("oioioipp",i)
       let data=[...this.state.data];  
        data.splice(i,1)
       this.setState({data:data})
  }

     
 submit = () => {
 let flag=true;
 this.state.data.forEach((itm,i)=>{
   if(itm.time=="" || itm.text==""){
     flag=false;
     
   }
 })
 if(flag){
let jsn={
  "schId":this.state.item.Id,
  "siteId": this.state.item.site_id,
   "timeLine":this.state.data,
   "crewId":this.state.crewId   

}

             

console.log("submit_img",jsn) ; 
  
     this.setState({isLoading :true})
  axios( {
    method: "POST",
    url:Constant.Api.ServerDomain+Constant.Api.Time.SUBMITTIMELINE,         
    data:  jsn 
  
  })

    .then(response => {
    console.log("submit_img", response) ; 
    console.log("upload succes", response);    
    this.setState({isLoading :false})
    if(response.data && response.data.status=="1"){
      let key =this.state.item.site_id+3
     
      
      AsyncStorage.removeItem(key)
      alert("Data Uploaded!")
      this.setState({
        date:new Date(),
    data:[{text: "", time:this.currenttime}] ,
     sdata:[]    
   
       },()=>{
        this.getData() 
       } )
    }else{
      alert("Failed!")    
    }
   
   
    })
    .catch(error => {
     
    console.log("submit_img", error); 
    console.log("upload error", error);  
    this.setState({isLoading :false})
    alert("Failed!") 
    });
  }else{alert("Please fill all fields!")}
    console.log("imagessssss","end"); 
  };   
 onAdd=()=>{
  let data=[...this.state.data]
  // data.push({text: "", time:this.currenttime} )
  data.push({text: "", time:this.getCurrentTime()} )

  this.setState({ data:data})  
 }
 handleText = (text,i) => {
    let data=[...this.state.data]
    let itm=data[i]
    itm.text=text
    this.setState({ data:data})
 }
 handleTime = (tine,i) => {
  console.log("timmmmm",tine) 
  let data=[...this.state.data]
  let itm=data[i]
   itm.time=tine+":00"
  this.setState({ data:data})
}
 handlePassword = (text) => {
    this.setState({ password: text })
 }
 login = (email, pass) => {
    alert('email: ' + email + ' password: ' + pass)
 }

  getCurrentDate=()=>{
	var now = new Date(); 
	let currentDateTime = new Date()              
	let month = currentDateTime.getMonth() + 1
	if (month < 10) {
		month = '0' + month; 
	}
	
	let day = currentDateTime.getDate()
	if (day < 10) {
		day = '0' + day; 
	}
	
	let year = currentDateTime.getFullYear()
	let fullDate=month + "/" + day + "/" + year;
	return fullDate;
}
 

getCurrentTime=()=>{
	var now = new Date(); 
	let currentDateTime = new Date()              
	let month = currentDateTime.getHours() 
	if (month < 10) {
		month = '0' + month; 
	}
	
	let day = currentDateTime.getMinutes()
	if (day < 10) {
		day = '0' + day; 
	}
	
	let year = currentDateTime.getFullYear()
  let fullDate=month + ":" + day + ":00" ;
  console.log("currrentdate",fullDate)     
	return fullDate;
}
 
   displayAlertMsg=(title,message,callback)=>{
   Dialog.alertDialog(title,message,I18n.t('OK_BUTTON'),false,null).then(()=>{
     if(callback){
       callback();
     }
   })
 } 
 
    
	  
	renderDateComponent=(dateStateTitle)=>{
		
		return(<DatePicker
			style={StyleDashboard.datePickerContainerStyle}
			date={this.state.date}
			mode="time"             
			showIcon={true}
			placeholder="select date"
			format="MM-DD-YYYY"
			minDate="01-01-2016"
			maxDate={this.currentDate}
			confirmBtnText="Confirm"                                  
			cancelBtnText="Cancel"      
			customStyles={{
				dateIcon: StyleDashboard.datepickerIcon,
				dateInput:StyleDashboard.datepickerInput  
			}}
			onDateChange={(selectedDate) => { 
			
				this.setState( {  date:  selectedDate} ); 
			}}
		/> )
  }
  
  


  
  getData= () => {


    let jsn={
   
   "schId":this.state.item.Id,
    "siteId":this.state.item.site_id ,
    "crewId":this.state.crewId      
  }  
    
                 
    
    console.log("submit_img",Constant.Api.ServerDomain+Constant.Api.COPE) ; 
      
         this.setState({isLoading :false})
      axios( {
        method: "POST",
        url:Constant.Api.ServerDomain+"GetTimeLine",         
        data:  jsn 
      
      })        
       
        .then(response => {
        console.log("submit_img", response) ; 
        console.log("upload succes", response);   
        if(response.data && Array.isArray(response.data.data)){
          let data= response.data.data 
             if(data.length>0){ 
         let tmp=      data.map((item,j)=>{
                 return({text: item.text  ,time:item.time
                 
                     })
               })
               this.setState({sdata:tmp})
             }
         

        } 
        this.setState({isLoading :false,})   
      
        })
        .catch(error => {
        console.log("submit_img", error); 
        console.log("upload error", error);  
        this.setState({isLoading :false})
      
        });
       
        console.log("imagessssss","end"); 
      };   
 
     render() {
    
      let data=this.state.data;
       console.log("data...",data)
       console.log("data... s",this.state.sdata)
      return(
          <View style={TimelineStyle.container}>
            <StatusBarComponent isHidden={false} />
            {DeviceInfo.isAndroid() &&
            <ToolbarComponent   
              title= "TimeLine"
              navIcon={Constant.ICONS.BACK_ICON}
              navClick={() => this.props.navigation.goBack()}
            />
          }
          <ScrollView >    
          {this.state.sdata.map((itm,j)=>{
       return(
        < View   style = {TimelineStyle.hv2}    >

         
      <TextInput style = {TimelineStyle.input1}    
        multiline={true}
        value={itm.text}
        editable={false} selectTextOnFocus={false}  
         underlineColorAndroid = "transparent"
         placeholder = "Comment"
         placeholderTextColor = "#9a73ef"
         autoCapitalize = "none"
         onChangeText = {(txt)=>this.handleText(txt,j)}/>  
      
      
      <DatePicker      
      disabled={true}
style={TimelineStyle.input}
date={itm.time}          
mode="time"                
showIcon={true}
placeholder="select Time"
maxDate={this.currentDate}              
confirmBtnText="Confirm"                                  
cancelBtnText="Cancel"      
customStyles={{     
  dateIcon: StyleDashboard.datepickerIcon,
  dateInput:StyleDashboard.datepickerInput  
}}
onDateChange={(selectedDate) => { 

  this.handleTime(selectedDate,j)
}}
/>    


</View>     
       )
     })}
     {data.map((itm,j)=>{
       return(
        < View   style = {TimelineStyle.hv2}    >

         
      <TextInput style = {TimelineStyle.input1}    
        multiline={true}
        value={itm.text}
        
         underlineColorAndroid = "transparent"
         placeholder = "Comment"
         placeholderTextColor = "#9a73ef"
         autoCapitalize = "none"
         onChangeText = {(txt)=>this.handleText(txt,j)}/>  
      
      
      <DatePicker      
style={TimelineStyle.input}
date={itm.time}          
mode="time"                
showIcon={true}
placeholder="select Time"
maxDate={this.currentDate}              
confirmBtnText="Confirm"                                  
cancelBtnText="Cancel"      
customStyles={{     
  dateIcon: StyleDashboard.datepickerIcon,
  dateInput:StyleDashboard.datepickerInput
}}
onDateChange={(selectedDate) => { 

  this.handleTime(selectedDate,j)
}}
/>    
{  (data.length!=1) &&      
           <TouchableOpacity  onPress={()=>this.onRemove(j)} >
                  <Image style={TimelineStyle.img_add} source={Removeicon} ></Image>
           </TouchableOpacity>    } 
{(data.length==(j+1)) &&  

           <TouchableOpacity  onPress={()=>this.onAdd()} >
                  <Image style={TimelineStyle.img_add} source={AddNew} ></Image>
</TouchableOpacity>    }

</View>     
       )
     })}
           

            <TouchableOpacity
               style = {TimelineStyle.submitButton}   
               onPress = {
                  () => this.submit()
               }>
               <Text style = {TimelineStyle.submitButtonText}> Submit </Text>
            </TouchableOpacity>
            <View
               style = {TimelineStyle.submitButton1}   
                 >
               
           </View>    
         
	 


		  

            </ScrollView>
            {this.state.isLoading   &&  
					<Loader msg={'Uploading...'} />
				}  
      
                
      
      </View>     
      );
  
    
    }
 }



