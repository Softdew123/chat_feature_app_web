import React, { Component } from 'react';

import {TouchableOpacity,View, BackHandler, Image, Text, AsyncStorage, FlatList } from 'react-native';
import { RNCamera } from 'react-native-camera';
const RNFS = require("react-native-fs");

// import ScanbotSDK from 'react-native-scanbot-sdk';

class MultipleCapture extends Component
{
    constructor(props)
    {
        super(props);
    }
  
    state =
    {
      isCameraReady:false,
      refreshPage: true,
      picturePath: [],
      picturePreview: false,
    }

    async handleBackButtonClick() 
    {
      await AsyncStorage.setItem('okCamera', "cancel");
      for(let i = 0; i < this.state.picturePath.length; i++)
      {
        await RNFS.unlink(this.state.picturePath[i]);
      }
      this.props.navigation.state.params.onNavigateBack(!this.state.refreshPage);
      this.props.navigation.goBack(null);
      return true;
    }

    async handleOkButtonClick() 
    {
        await AsyncStorage.setItem('picturePath', JSON.stringify(this.state.picturePath));
        await AsyncStorage.setItem('okCamera', "ok");
        this.props.navigation.state.params.onNavigateBack(!this.state.refreshPage);
        this.props.navigation.goBack(null);
        return true;
    }

    takePicture = async function() 
    //async takePicture ()
    {
        if (this.camera) 
        {
            const options = { quality:0.3, base64:true, fixOrientation:true };
            const data    = await this.camera.takePictureAsync(options);
            // this.setState({snapDataBase64: data.base64});
            let tempData = this.state.picturePath;
            tempData.push(data.uri);
            this.setState({picturePreview: true, picturePath: tempData});
            
        }
    };

    pushImage = () => {
        tempData.push(data.uri);
        this.setState({picturePreview: true, picturePath: tempData});
    }

    render() 
    {    
        return (
            <View style = {{flex: 1}}>
                <RNCamera
                ref={ref => { this.camera = ref; }}
                style = {{flex: 1}}
                flashMode={this.state.isCameraReady ? RNCamera.Constants.FlashMode.torch : RNCamera.Constants.FlashMode.off}
                type={RNCamera.Constants.Type.back}
                permissionDialogTitle={'Permission to use camera'}
                permissionDialogMessage={'We need your permission to use your camera phone'}
                onGoogleVisionBarcodesDetected={({ barcodes }) => { console.log(barcodes)}}  
                />

                <View style={{position: 'absolute',top: 0,left: 0,width: '100%',height: '100%', alignItems: 'center',justifyContent: 'space-around',}}>
                    <TouchableOpacity 
                        style = {{width:100,height:100,borderRadius:50,borderWidth:2,borderColor:'#fff',justifyContent:'center',alignItems:"center"}}
                        onPress = {this.takePicture.bind(this)} >
                        <Image
                        style={{width: 50, height: 50}}
                        source={require('../../Images/Camera/camera.png')} />
                    </TouchableOpacity>
                </View>

                {this.state.picturePreview &&
                    <View style={{width: "100%", height: 120, position: 'absolute', bottom: 0}}>
                        <FlatList 
                            contentContainerStyle = {{paddingBottom: 75}}
                            horizontal={true}
                            data={this.state.picturePath}
                            extraData={this.state}
                            keyExtractor={(item, index) => index+""+new Date().toLocaleString()}
                            renderItem={({item, index}) => this.renderItem(item, index)}/>

                        <View style={{width: '100%', height: 60, flexDirection: 'row', position: 'absolute', bottom: 0, justifyContent: 'center', backgroundColor: '#e54f3b'}}>
                            
                            <View style={{flex: 1, justifyContent: 'center', marginLeft: 10}}>
                                <TouchableOpacity style={{ width:100,height:40, justifyContent: 'center', marginLeft: 10,backgroundColor:'#FFFFFF',borderRadius:20, marginLeft: 10}}
                                    onPress={()=>this.handleBackButtonClick()}>
                                    <Text style={{fontFamily:'Roboto', fontSize: 15, fontWeight: 'bold', color: '#e54f3b', textAlign: 'center'}}>
                                        CANCEL
                                    </Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{justifyContent: 'center',marginRight: 10}}>
                                <TouchableOpacity style={{ width:100,height:40, justifyContent: 'center', marginLeft: 10,backgroundColor:'#FFFFFF',borderRadius:20, marginRight: 10}}
                                    onPress={()=>this.handleOkButtonClick()}>
                                    <Text style={{fontFamily:'Roboto', fontSize: 15, fontWeight: 'bold', color: '#e54f3b', textAlign: 'center'}}>
                                        OK
                                    </Text>
                                </TouchableOpacity>
                            </View>

                        </View>
                    </View>
                }
            </View>
        )
    }

    renderItem=(item,index)=>{
        return(
            <View style = {{width: 60, height: 60, padding: 5}}>
                <Image style={{width: 50, height: 50}} resizeMode={'cover'} source={{uri: item}} />
            </View>
        )
    }
}


export default MultipleCapture;