import React, { Component } from 'react';
import ToolbarComponent from "../../Components/Toolbar";
import I18n from '../../Utils/i18n';
import Dialogs, { DialogContent } from 'react-native-popup-dialog';
// import MultiSelect from 'react-native-multiple-select';
// import { MultiSelect } from '@progress/kendo-react-dropdowns';
import MultiSelect from 'react-native-multiple-select';
import { CommonMethods,DeviceInfo,Dialog,GlobalVariable } from '../../Utils/Utils'
import {Alert,FlatList,Button,RefreshControl,Modal,Text,TouchableOpacity,View,KeyboardAvoidingView,TextInput,Picker,ScrollView,TouchableWithoutFeedback, Image} from 'react-native';
import { StyleGCSiteLogin } from '../../Styles/PageStyle';
import { StyleDashboard } from '../../Styles/PageStyle';
import StatusBarComponent from '../../Components/StatusBar';
import OfflineBarComponent from '../../Components/OfflineBar';
import Constant from '../../Utils/Constant';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon1 from 'react-native-vector-icons/MaterialIcons';
import {Colors} from '../../Styles/Themes'; 
import Placeholder,{Paragraph,Line, Media } from 'rn-placeholder';
import AsyncStorage from '@react-native-community/async-storage'; 
import {PostRequest} from '../../Utils/ServiceCalls'; 
import Loader from '../../Components/Loader'; 
import Toast from 'react-native-simple-toast';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import { PermissionsAndroid } from 'react-native';
import DatePicker from 'react-native-datepicker';
import RadioGroup,{Radio} from "react-native-radio-input";
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import GetLocation from 'react-native-get-location'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
// import Geolocation from '@react-native-community/geolocation';
// import ImagePicker from 'react-native-image-picker';
const RNFS = require("react-native-fs");
var radio_props = [
  {label: 'CX', value: 1 },
  {label: 'IX', value: 2 },
  {label: 'Both', value: 3 }
];


export default class GCSiteLoingScreen extends Component {
  apiRetryAttempt=0;
  autoCompleteArrayKey="";
  autoCompleteAarrayFilter=[]; 
  seatIdArray=[];
  // states = { value: [] };
  site_id="";
  autoCompleteQueryStr="";
  autoCompletePlaceholder="";
  tempMarketObject={"id":"-1",name:I18n.t('SIGNUPPAGE.SELECT_MARKET')};
  tempWONObject={};
  tempPORObject={};
  selectedMarketId="-1"
  
  

  constructor(props) {
    super(props);
    this.currentDate=this.getCurrentDate(); 
		this.currentDateMMDDYYYY = new Date(CommonMethods.chaangeDateFormat(this.currentDate));
    this.state = { 
      isModalVisible: false,
      selectedItems: [],
      selectedPlannedSOW: [],
      selectedItemsPOR: [],
      fromDate:this.currentDate,
      toDate:this.currentDate,
      isLoadervisible:false,
      isAutoCompletePopupVisible:false,
      autoCompleteFilteredArray:[], 
      contactName:"",
      contactEmail:"",
      contactNumber:"", 
      GCCompanyName:"",
      site_id:I18n.t('GCSITELOGINPAGE.ENTER_SITEID'),
      accountArray:[{"id":"-1",name:I18n.t('SIGNUPPAGE.SELECT_ACCOUNT')}],
      tmoArray:[{"id":"-1",name:"Select TMO Deployment Manger"}],
      sowArray:[{"id":"-1",name:I18n.t('GCSITELOGINPAGE.SELECT_SOW')}],
      sowArray:[],
      ttIDMobile:"",
      sowArrayDetails:[],
      marketArray:[this.tempMarketObject], 
      mainIdArray: [{"site_id":"-1","site_name":"Select Main Id","accountId":"-1","marketId":"-1"}],
      WONArray: [],
    PORData: [],
    TempPORData: [],
    selectedItemsPors:[],
    isSerachMainId: false,
	  PORDatas: [],
      selectedMainId:"-1",
      selectedWON: "-1",
      selectedAcount:"-1", 
      selectedMarket:"-1", 
      SelectedSOW:"-1",
      selectedTMO:"-1",
      SelectedSOWDetails:"",
      selectedRFApproval:"-1",
      selectedServiceAffected:"-1",
      selectedDTO:"-1",
      selectedNFSDLogin:"1",
      estimatedTime:"",
      preWorkID:"",
      emiLoginStr:"",
      SowDetail:"",
      loginType: 1,
      AdditionalEmailId:"",
      picturePath: "",
      buttonBackground: "#ed462d",
      mainIdArraySearch:[],
	  selectFromModal: false,
	  selectFromModal1: false,
      isMainIDData: false,
      isWONData: false,
      isPORData: false,
      isAccountData: false,
      isMarketData: false,
      isDayMOP: false,
      lat: "",
      lan: "",
      nestedTime:"",
      SOWComment: "",
      dayMOP:"",
      dayMOPComment:"",
      RFApproved:"",
      mainIdValuess:"",
	  emailIdUser:"",
	  startHH:"00",
	  startMM:"00",
	  endHH:"00",
	  endMM:"00",

    isParam:false,params:"",
	  
    planSowData: [

      {
        id: '4',
        name: 'E911-Call Test',
      },
      {
        id: '1',
        name: 'CX',
      },
      {
        id: '2',
        name: 'IX',
      },
      {
        id: '3',
        name: 'Troubleshooting',
      },

        ],
      showDetailNew: [

          
    
            ]
      
    };

    
  }

  onSelectedItemsChange = selectedItems => {

    this.setState({ selectedItems },()=>{
      let TempPORData  = this.state.PORData.filter((item)=>{
                  for(let it of selectedItems){
                     if(item.name==it){
                        return true
                        }
                  }
                   return false
          })
          console.log( "tttt", this.state.TempPORData )
          this.setState({isPORData: true,TempPORData:TempPORData,selectedItemsPORs:[]})

    });
     

   
    console.log("tttt",selectedItems)
  };



  onSelectedItemsChangeSOWDetail  = SelectedSOWDetails => {
console.log("SelectedSOWDetails",SelectedSOWDetails)
console.log("Planed sow data",this.state.planSowData)
console.log("details sow data",this.state.sowArrayDetails)
    this.setState({ SelectedSOWDetails });
    console.log(SelectedSOWDetails)  
  };
  onSelectedItemsChangePLannedSow  =selectedPlannedSOW=> {
    console.log("SelectedSOWDetails",selectedPlannedSOW)
        this.setState({ selectedPlannedSOW });
        console.log(selectedPlannedSOW)  
      };
  onSelectedItemsChangePOR = selectedItemsPORs => {
    console.log(selectedItemsPORs)
        this.setState({ selectedItemsPORs });
        console.log(selectedItemsPORs)
      };


  // async requestLocationPermission(){
  //  console.log("step 1")
  //   try {
  //     console.log("step 1 1")
  //     const granted = await PermissionsAndroid.request(
        
  //       PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  //       {
  //         'title': 'Example App',
  //         'message': 'Example App access to your location '
  //       }
  //     )
  //     console.log("step 1 2")
  //     if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //       console.log("You can use the location",navigator.geolocation.watchPosition)
  //       navigator.geolocation.watchPosition((position)=>{
  //     console.log(position.coords.latitude)
  //     this.state.lat = position.coords.latitude
  //     this.state.lan = position.coords.longitude
  //     console.log(this.state.lan, this.state.lat)
  //     console.log("step 1 3")
  //     this.getMainId();
    
      
  //   });
  //     } else {
  //       console.log("location permission denied")
  //       alert("Location permission denied");
  //     }
  //   } catch (err) {
  //     console.warn(err)
  //   }
  //   console.log("step 1 222222222222222222222")
  // }
  
  //  async componentDidMount() {
  //    await requestLocationPermission()
  //   }




  async componentDidMount()
  {  
     
   
   this.requestForLoc()
    console.log("step 2")
    await AsyncStorage.setItem("picturePath", "");
     this.getUsersLocalDetails();
     this.getAccount()
     this.getTMO()
  }
      
 async requestForLoc(){
    const granted = await PermissionsAndroid.check( PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION );
    
    if (granted) {
      GetLocation.getCurrentPosition({
        enableHighAccuracy: true,
        timeout: 15000,
    })
    .then(location => {
        console.log("location raj",location);
        this.getUsersDetails(location.latitude,location.longitude)
         //this.getMainId(location.latitude,location.longitude)
    })
    .catch(error => {
        const { code, message } = error;
  
        console.warn("location raj catch ", message);
        GetLocation.openGpsSettings();
    })
    } 
    else {
      this.requestLocationPermission();
    }
  }
  
 async requestLocationPermission() 
  {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': 'TMOEricsson  App',
          'message': 'TMOEricsson App access to your location '
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
         this.requestForLoc()
      } else {
       

      }
    } catch (err) {
      console.warn(err)
    }
  }
  goToPreviousPage = () =>{
    this.props.navigation.state.params.onNavigateBack();
    this.props.navigation.goBack()
    return true;
  }
  

  getUsersLocalDetails=async()=>{
    console.log("Main 1")
    const userDetails = await AsyncStorage.getItem(Constant.LOCAL_STORAGE.LOGINRESPONSE);

    try
    {
      console.log(JSON.parse(userDetails))
      let userDetailsObject=JSON.parse(userDetails);
      let contactName=userDetailsObject.contact_name;
      let contactEmail=userDetailsObject.crew_lead_email_id;
      let contactNumber=userDetailsObject.contact_number;
      let companyName = userDetailsObject.m_company_id;
      this.state.emailIdUser = contactEmail
      console.log(this.state.emailIdUser, contactEmail)
      this.setState({GCCompanyName:companyName});
      this.setState({contactName:contactName,contactEmail:contactEmail,contactNumber:contactNumber},()=>{
          this.callGetSiteApi(); 
      })
    }
    catch(err)
    {
       
    }
  }

  getUsersDetails=async(lat,lng)=>{
    console.log("Main 4.1")
    const userDetails = await AsyncStorage.getItem(Constant.LOCAL_STORAGE.LOGINRESPONSE);

    try
    {
      console.log(JSON.parse(userDetails))
      let userDetailsObject=JSON.parse(userDetails);
      let marketid=userDetailsObject.market_id;
      console.log("marketid --->",marketid)
      this.getMainId(lat,lng, marketid)
    }
    catch(err)
    {
       
    }
  }

  handleOnNavigateBack = async (refreshPage) => {
    let returnFrom  = await AsyncStorage.getItem("okCamera");
    if(returnFrom !== "ok")
      return;
    let picturePath = await AsyncStorage.getItem("picturePath");
    await this.setState({picturePath: picturePath});

    if(picturePath !== null && picturePath !== "")
    {
      this.setState({buttonBackground: "#006600"});
    }
    else
    {
      this.setState({buttonBackground: "#ed462d"});
    }
  }

  setSelectFromModalVisibility(visibility)
  {
    this.setState({selectFromModal: visibility});
  }
  
  openCamera= async()=>{
    this.setSelectFromModalVisibility(false);
    // await AsyncStorage.setItem("snapDataBase64", "");
    this.props.navigation.navigate('CaptureNFSDSnapShot',{onNavigateBack: this.handleOnNavigateBack});
  }

  async openGallery()
  {
    this.setSelectFromModalVisibility(false);
    try
		{
		  DocumentPicker.show( {filetype: [DocumentPickerUtil.images()]},async (error,res) => 
			{
				if(res != null)
				{
          // let imageData = "";
          // await RNFS.readFile(res.uri, "base64").then(async(data) => {
          //   // binary data
          //      imageData = data;
          //   });
          await this.setState({picturePath:res.uri});
          this.setState({buttonBackground: "#006600"});
        }
        // else
        // {
        //   this.setState({buttonBackground: "#ed462d"});
        // }
			});
    }
    catch(error1)
    {
      Toast.show(error1, Toast.LONG);
    }
    
  }


  

  callGetSiteApi=()=>{
    console.log("Main 2")
      // let paramObject = {
      //     path: Constant.Api.LCLOGIN.GETSITES,
      //     showErrorMsg: true 
      // };

      //display loader first
      // this.setState({isLoadervisible:true},()=>{
      //     PostRequest(paramObject).then((responseApisuccess) => {
             
      //             let response=responseApisuccess.data; 
      //             // unable to fetch GC SITES ID
      //             if(!response || !response.status || response.status==0 || !response.data){ 
      //                 //if due to ary reason failed to fetch seatid retry again in case of error
      //                 if(this.apiRetryAttempt<3){
      //                     this.apiRetryAttempt++;
      //                     this.callGetSiteApi();
      //                     return;
      //                 }else{
      //                   this.setState({isLoadervisible:false},()=>{ 
      //                     let alertTitle= I18n.t('GCSITELOGINPAGE.ERROR');
      //                     let alertMessage= I18n.t('GCSITELOGINPAGE.GET_SITE_ERROR');
      //                     let buttonStr= I18n.t('OK_BUTTON');
      //                     Dialog.alertDialog(alertTitle, alertMessage, buttonStr, true, null); 
      //                   }); 

      //                   return;
      //                 } 
      //             }
                   
      //             // no seat id available
      //             if(response.data.length==0){ 
      //               let alertTitle= I18n.t('GCSITELOGINPAGE.NO_SITES_TITLE');
      //               let alertMessage= I18n.t('GCSITELOGINPAGE.NO_SITE_MSG');
      //               let buttonStr= I18n.t('OK_BUTTON');
      //               this.setState({isLoadervisible:false},()=>{ 
      //                 Dialog.alertDialog(alertTitle, alertMessage, buttonStr, true, null); 
      //               }) 
      //               return;
      //             }

      //             //got seat id
                 
      //               this.apiRetryAttempt=0; 
      //               this.seatIdArray= response.data;  
      //               // this.getMainId();
      //               // this.getAccount();
      //               // this.getPlannedSOWDetails();
                   
      //     }, (error) => { 
      //         this.setState({isLoadervisible:false},()=>{});
      //     }); 
      // });


      
                    this.getAccount();
                    this.getPlannedSOWDetails();
  }


  getAccount=()=>{  
    let paramObject = {
        path: Constant.Api.SIGNUP.GETACCOUNT,
        showErrorMsg: true 
    };

     //display loader first
     this.setState({isLoadervisible:true},()=>{
        PostRequest(paramObject).then((responseApisuccess) => {
           console.log("Account data")
                let response=responseApisuccess.data; 
                // unable to fetch accounts
                if(!response || !response.status || response.status==0 || !response.data){ 
                    //if due to ary reason failed to fetch account retry again in case of error
                    if(this.apiRetryAttempt<3){
                        this.apiRetryAttempt++;
                        this.getAccount();
                        return;
                    }
                    this.setState({isLoadervisible:false},()=>{ });
                    return;
                }
                  
                if(response.data.length==0){
                  this.setState({isLoadervisible:false},()=>{ 
                      this.displayAlertMsg(
                          I18n.t('SIGNUPPAGE.NO_ACCOUNT_TITLE'),
                          I18n.t('SIGNUPPAGE.NO_ACCOUNT_MSG'),null
                      ); 
                  })
                  return;
                }

                //got accounts 
                this.apiRetryAttempt=0; 
                let tempAccountArray= [...this.state.accountArray, ...response.data]; 
                this.setState({accountArray:tempAccountArray},()=>{
                  this.getPlannedSOW();
                });
                  
             
        }, (error) => {
          //alert(JSON.stringify(error));
            this.setState({isLoadervisible:false},()=>{});
        }); 
    });
}


getTMO=()=>{  
  let paramObject = {
      path: Constant.Api.GetTmoEngineer,
      showErrorMsg: true 
  };

   //display loader first
   this.setState({isLoadervisible:true},()=>{
      PostRequest(paramObject).then((responseApisuccess) => {
         console.log("Account data")
              let response=responseApisuccess.data; 
              // unable to fetch accounts
              if(!response || !response.status || response.status==0 || !response.data){ 
                  //if due to ary reason failed to fetch account retry again in case of error
                  if(this.apiRetryAttempt<3){
                      this.apiRetryAttempt++;
                      this.getTMO();
                      return;
                  }
                  this.setState({isLoadervisible:false},()=>{ });
                  return;
              }
                
              if(response.data.length==0){
                this.setState({isLoadervisible:false},()=>{ 
                    this.displayAlertMsg(
                        I18n.t('SIGNUPPAGE.NO_ACCOUNT_TITLE'),
                        I18n.t('SIGNUPPAGE.NO_ACCOUNT_MSG'),null
                    ); 
                })
                return;
              }

              //got accounts 
              this.apiRetryAttempt=0; 
              let tempAccountArray= [...this.state.tmoArray, ...response.data]; 
              this.setState({tmoArray:tempAccountArray},()=>{
                // this.getPlannedSOW();
              });
                
           
      }, (error) => {
        //alert(JSON.stringify(error));
          this.setState({isLoadervisible:false},()=>{});
      }); 
  });
}


 /**	   
	* @desc display alert dialog box
	* @param :title: string title of alert dialog
    * @param :message: string message of alert dialog
    * @param :callback: callback function on ok button click
	* @returns none
	*/
  displayAlertMsg=(title,message,callback)=>{
    Dialog.alertDialog(title,message,I18n.t('OK_BUTTON'),false,null).then(()=>{
        if(callback){
            callback();
        }
    })
} 
getPlannedSOW=()=>{  
    let paramObject = {
        path: Constant.Api.LCLOGIN.GETSOW,
        showErrorMsg: true 
    };
 
   //display loader first
   this.setState({isLoadervisible:true},()=>{
      PostRequest(paramObject).then((responseApisuccess) => {
          this.setState({isLoadervisible:false},()=>{ 
              let response=responseApisuccess.data; 
              // unable to fetch SOW
              if(!response || !response.status || response.status==0 || !response.data){ 
                  //if due to ary reason failed to fetch Planned Sow retry again in case of error
                  if(this.apiRetryAttempt<3){
                      this.apiRetryAttempt++;
                      this.getPlannedSOW();
                      return;
                  }else{
                    this.displayAlertMsg(
                      I18n.t('GCSITELOGINPAGE.ERROR'),
                      I18n.t('GCSITELOGINPAGE.GET_SOW_ERROR'),null
                    ); 
                    return;
                  } 
              }
                
              if(response.data.length==0){ 
                  this.displayAlertMsg(
                      I18n.t('GCSITELOGINPAGE.NO_SOW_TITLE'),
                      I18n.t('GCSITELOGINPAGE.NO_SOW_MSG'),null
                  ); 
                  return;
              }

              //got accounts
              if(response.data && response.data.length>0){
                 this.apiRetryAttempt=0; 
                //  let tempSOWArray= [...this.state.sowArray, ...response.data]; 
                //  this.setState({sowArray:tempSOWArray},()=>{});label={sowObject.name} value={sowObject.id}
  let ps=[]
  console.log("sowdetails", response.data)
  if(Array.isArray(response.data)){
     
   ps= response.data.map(( sowObject,i)=>{
       return {id:sowObject.id,name: ps}

     
    })
  }
                 this.setState({sowArray:response.data});
              } 
          }); 
      }, (error) => {
          this.setState({isLoadervisible:false},()=>{});
      }); 
  });
}

getPlannedSOWDetails=()=>{  
  let paramObject = {
      path: Constant.Api.LCLOGIN.GETSOWDetail,
      showErrorMsg: true 
  };

 //display loader first
 this.setState({isLoadervisible:true},()=>{
    PostRequest(paramObject).then((responseApisuccess) => {
        this.setState({isLoadervisible:false},()=>{ 
            let response=responseApisuccess.data; 
            // unable to fetch SOW
            if(!response || !response.status || response.status==0 || !response.data){ 
                //if due to ary reason failed to fetch Planned Sow retry again in case of error
                if(this.apiRetryAttempt<3){
                    this.apiRetryAttempt++;
                    this.getPlannedSOWDetails();
                    return;
                }else{
                  this.displayAlertMsg(
                    I18n.t('GCSITELOGINPAGE.ERROR'),
                    I18n.t('GCSITELOGINPAGE.GET_SOW_ERROR'),null
                  ); 
                  return;
                } 
            }
              
            if(response.data.length==0){ 
                this.displayAlertMsg(
                    I18n.t('GCSITELOGINPAGE.NO_SOW_TITLE'),
                    I18n.t('GCSITELOGINPAGE.NO_SOW_MSG'),null
                ); 
                return;
            }

            //got accounts
            if(response.data && response.data.length>0){
               this.apiRetryAttempt=0; 
              //  let tempSOWArray= [...this.state.sowArray, ...response.data]; 
              //  this.setState({sowArray:tempSOWArray},()=>{});
                    
              let tmp=[]
              console.log(response.data,"site data sowDetails")
            if(Array.isArray(response.data)){
               tmp= response.data.map((item,i)=>{
                  return {id:item.Id,name:item.name}
               })
            }            
               this.setState({sowArrayDetails:tmp});
            } 
        }); 
    }, (error) => {
        this.setState({isLoadervisible:false},()=>{});
    }); 
});
}


getMarket=(accountID)=>{ 
  console.log("market 1 : ",accountID)
    if(accountID==-1){
        let tempMarketArray= [this.tempMarketObject];
        this.setState({selectedAcount: accountID,marketArray:tempMarketArray},()=>{}); 
        return;
    } 
     
    let paramObject = {
        path: Constant.Api.SIGNUP.GETMARKET,
        showErrorMsg: true,
        params: {"account_id":accountID}, 
    };

     //display loader first
     this.setState({isLoadervisible:true,selectedAcount:accountID},()=>{
        PostRequest(paramObject).then((responseApisuccess) => { 
            this.setState({isLoadervisible:false},()=>{
                let response=responseApisuccess.data; 
                console.log("market 2",response)
                // unable to fetch market
                if(!response || !response.status || response.status==0 || !response.data){
                    //if due to ary reason failed to fetch market retry again in case of error
                    if(this.apiRetryAttempt<3){
                        this.apiRetryAttempt++;
                        this.getMarket(accountID);
                        return;
                    } 
                    this.makeEmptyMarketArray(); 
                    return;
                }

                if(response.data.length==0){ 
                    this.makeEmptyMarketArray();  
                    this.displayAlertMsg(
                        I18n.t('SIGNUPPAGE.NO_MARKET_TITLE'),
                        I18n.t('SIGNUPPAGE.NO_MARKET_MSG'),null
                    );
                    return;
                }
                    
                //got accounts
                if(response.data && response.data.length>0){ 
                    this.apiRetryAttempt=0; 
                    response.data.unshift(this.tempMarketObject); 
                    let tempMarketArray= response.data;
                    this.setState({marketArray:tempMarketArray},()=>{});
                } 
            });      
        }, (error) => {  
            this.makeEmptyMarketArray(); 
        }); 
    });
}

makeEmptyMarketArray=()=>{
    let tempMarketArray= [this.tempMarketObject];
    this.setState({isLoadervisible:false,marketArray:tempMarketArray},()=>{});
}

renderMarketPicker=()=>{
    let pickerObject=this.state.marketArray.map((marketObj,i) => {
      let key=i+""+new Date().toLocaleString()+"_marketPickerList"
      return( 
        <Picker.Item label={marketObj.name} value={marketObj.id}  key={key}/>        
      );
    });
  return (pickerObject);
}


getMainId=(lat,lng,mktid)=>{  
  console.log("Main 3")
  console.log(GlobalVariable.USERID)
  console.log("Market IDDDDD "+mktid)
  let paramObject = {
      path: Constant.Api.GetSitesNew,
      showErrorMsg: true ,
      params: {
                // "siteId":this.state.siteId,
                "latitude":lat,
                "longitude": lng,
                "userid":GlobalVariable.USERID,
                "marketId":mktid
              }, 
  };
  console.log("Main 4", paramObject)

   //display loader first
   this.setState({isLoadervisible:true},()=>{
      PostRequest(paramObject).then((responseApisuccess) => {
        console.log("Main 5")
              let response=responseApisuccess.data; 
              console.log(response)
              // unable to fetch accounts
              if(!response || !response.status || response.status==0 || !response.data){ 
                  //if due to ary reason failed to fetch account retry again in case of error
                  if(this.apiRetryAttempt<3){
                      this.apiRetryAttempt++;
                      // this.getAccount();
                      return;
                  }
                  this.setState({isLoadervisible:false},()=>{ });
                  return;
              }
                
              if(response.data.length==0){
                this.setState({isLoadervisible:false},()=>{ 
                    this.displayAlertMsg(
                        I18n.t('SIGNUPPAGE.NO_ACCOUNT_TITLE'),
                        I18n.t('SIGNUPPAGE.NO_ACCOUNT_MSG'),null
                    ); 
                })
                return;
              }

              //got accounts 
              this.apiRetryAttempt=0; 
              this.state.mainIdArray = response.data
              console.log(this.state.mainIdArray)
              for(let i = 0; i<this.state.mainIdArray.length; i++){
                this.state.mainIdArray[i].site_name = this.state.mainIdArray[i].site_id;
              }
              // this.setState({isMainIDData: true})
              this.setState({isLoadervisible: false})
              // let tempAccountArray= [...this.state.accountArray, ...response.data]; 
              // this.setState({site:tempAccountArray},()=>{
              //   // this.getPlannedSOW();
              // });
                
           
      }, (error) => {
        //alert(JSON.stringify(error));
          this.setState({isLoadervisible:false},()=>{});
      }); 
  });
}


  getWorkOrderNumber=(siteId)=>{ 
    this.state.WONArray = []
    this.state.selectedMainId = siteId
    console.log("WON 1 ",siteId)
    if(siteId==-1){
        let tempMarketArray= [this.tempMarketObject];
        this.setState({selectedAcount: siteId,marketArray:tempMarketArray},()=>{}); 
        return;
    } 
     
    let paramObject = {
        path: Constant.Api.GetWonPor,
        showErrorMsg: true,
        params: {"siteId":siteId}, 
    };
    console.log("WON 2 ")
     //display loader first
     this.setState({isLoadervisible:true},()=>{
        PostRequest(paramObject).then((responseApisuccess) => { 
          console.log("WON 3 ")
            this.setState({isLoadervisible:false},()=>{
              console.log("WON 4 ")
                let response=responseApisuccess.data; 
                console.log("WON 5 ",response)
                // unable to fetch market
                if(!response || !response.status || response.status==0 || !response.data){
                    //if due to ary reason failed to fetch market retry again in case of error
                    if(this.apiRetryAttempt<3){
                        this.apiRetryAttempt++;
                        this.getWorkOrderNumber(siteId);
                        return;
                    } 
                    this.makeEmptyMarketArray(); 
                    return;
                }

                if(response.data.length==0){ 
                    this.makeEmptyMarketArray();  
                    this.displayAlertMsg(
                        I18n.t('SIGNUPPAGE.NO_MARKET_TITLE'),
                        I18n.t('SIGNUPPAGE.NO_MARKET_MSG'),null
                    );
                    return;
                }
                    
                //got accounts
                console.log("WON 6 ")
                this.state.PORData = []
                if(response.data && response.data.length>0){ 
                  console.log("WON 7 ")
                    this.apiRetryAttempt=0; 
                    // response.data.unshift(this.tempWONObject); 
                    let tempWONArray= response.data;
                    this.setState({WONArray:tempWONArray,isWONData: true},()=>{});
                    console.log("WON 8 ",response.data)
                    
                    response.data.forEach(resp=>{
					  console.log(resp.por)
					  this.state.PORData.push({"id":resp.won,"name":resp.won,"children":this.getPORData(resp)})

                      
                      // console.log(arr2)
                      // this.state.PORData = arr2
                      console.log(this.state.PORData)
                      // this.setState({isPORData: true})
                    //   if(resp.por)
                    //   this.tempPORObject.push
                    })
                } 
            });      
        }, (error) => {  
            this.makeEmptyMarketArray(); 
        }); 
    });
    this.state.mainIdArray.forEach((marketObj,index) => {
      
      if(marketObj.site_id == siteId){
        this.state.selectedAcount = marketObj.accountId
        this.state.selectedMarket = marketObj.marketId
        console.log("asdfsdf",this.state.selectedAcount)
        this.setState({isAccountData: true})
        this.getMarket(this.state.selectedAcount)
      }
    })
    
}


getPORData=(resp)=>{
	this.state.PORDatas= []
	let arr = resp.por.split(",")
                      let arr2 = [];
                      console.log(arr)
                      for(let i=0; i<arr.length; i++){
					   console.log("arrrray ",arr[i])
					//    return ({"id":resp.won+'/'+arr[i],"name":arr[i]})
                      this.state.PORDatas.push({"id":resp.won+'/'+arr[i],"name":arr[i]})
                        
					  }
					  return this.state.PORDatas
}

dayMOPVALUEChange=(id)=>{
  console.log(id)
  if(id == 1){
    this.setState({isDayMOP: true})
    this.state.selectedDTO = id
  }
}

renderMainIdPicker=(serachKey)=>{
  console.log(serachKey)
  console.log(this.state.mainIdArray)
  let pickerObject=this.state.mainIdArray.map((marketObj,i) => {
    // console.log(marketObj)
    let key=i+""+new Date().toLocaleString()+"_marketPickerList"
    return( 
      <Text>vijay</Text>
      // label={marketObj.site_name} value={marketObj.site_id}  key={key}/>        
    );
  });
return <Text>pal</Text>;
}

renderWorkOrderNUmberPicker=()=>{
  let pickerObject=this.state.WONArray.map((marketObj,i) => {
    console.log("WON 11" ,marketObj)
    let key=i+""+new Date().toLocaleString()+"_marketPickerList"
    return( 
      <Picker.Item label={marketObj.won} value={marketObj.won}  key={key}/>        
    );
  });
return (pickerObject);
}

renderPORPicker=()=>{
  let pickerObject=this.state.marketArray.map((marketObj,i) => {
    let key=i+""+new Date().toLocaleString()+"_marketPickerList"
    return( 
      <Picker.Item label={marketObj.name} value={marketObj.id}  key={key}/>        
    );
  });
return (pickerObject);
}

renderSOWPicker=()=>{
  let pickerObject=this.state.sowArray.map((sowObject,i) => {
    let key=i+""+new Date().toLocaleString()+"_sowPickerList"
    return( 
      <Picker.Item label={sowObject.name} value={sowObject.id}  key={key}/>        
    );
  });
return (pickerObject);
}

renderSOWDetailsPicker=()=>{
  let pickerObject=this.state.sowArrayDetails.map((sowObject,i) => {
    let key=i+""+new Date().toLocaleString()+"_sowPickerList"
    return( 
      <Picker.Item label={sowObject.name} value={sowObject.id}  key={key}/>        
    );
  });
return (pickerObject);
}


renderTMOPicker=()=>{
  let pickerObject=this.state.tmoArray.map((accountObj,i) => {
    console.log("asdfsdf",accountObj)
    let key=i+""+new Date().toLocaleString()+"_accountPickerList"
    return( 
      <Picker.Item label={accountObj.name} value={accountObj.Id}  key={key}/>        
    );
  });
  return (pickerObject);
}


renderDateComponent=(dateStateTitle)=>{
  console.log(dateStateTitle)
		
  return(<DatePicker
    style={StyleDashboard.datePickerContainerStyle}
    date={this.state[dateStateTitle]}
    mode="date"
    showIcon={true}
    placeholder="select date"
    format="MM-DD-YYYY"
    minDate="01-01-2016"
    maxDate={this.currentDate}
    confirmBtnText="Confirm"
    cancelBtnText="Cancel"
    customStyles={{
      dateIcon: StyleDashboard.datepickerIcon,
      dateInput:StyleDashboard.datepickerInput  
    }}
    onDateChange={(selectedDate) => { 
      let dateObj={};
      dateObj[dateStateTitle]=selectedDate; 
      this.setState(dateObj); 
    }}
  /> )
}

// getSelectedItemsExt(value){
//   console.log(value)
// }

renderAccountPicker=()=>{
  let pickerObject=this.state.accountArray.map((accountObj,i) => {
    console.log("asdfsdf",accountObj)
    let key=i+""+new Date().toLocaleString()+"_accountPickerList"
    return( 
      <Picker.Item label={accountObj.name} value={accountObj.id}  key={key}/>        
    );
  });
  return (pickerObject);
}


  filterAutoComplete(query) {  
    if (query === '') {
      this.autoCompleteQueryStr="";
      this.setState({autoCompleteFilteredArray:[]}); 
      return;
    }
    this.autoCompleteQueryStr=query;
    const regex = new RegExp(`${query.trim()}`, 'i');
    let filteredArray= this.autoCompleteAarrayFilter.filter(filteredObj => filteredObj[this.autoCompleteArrayKey].search(regex) >= 0);
    this.setState({autoCompleteFilteredArray: filteredArray}) 
  }


  showHideAutoCompletePopup=(bool,keyTofilter,arrayName,placeholderText)=>{
    if(bool){
      this.autoCompletePlaceholder=placeholderText;
      this.autoCompleteArrayKey=keyTofilter;
      this.autoCompleteAarrayFilter=this[arrayName]; 
    }else{
        this.resetAutoCompleteList();
    }
    this.autoCompleteQueryStr="";
    this.setState({isAutoCompletePopupVisible:bool,autoCompleteFilteredArray:[]});
  }

  resetAutoCompleteList=()=>{
    this.autoCompleteQueryStr="";
    this.autoCompleteArrayKey="";
    autoCompleteAarrayFilter=[];  
  }


  renderPlaceHolder(){
    let array=[];
		for(var i=0;i<3;i++){ 
			array.push( 
        
        <Placeholder animation="fade" key={i}> 
          <View style={StyleGCSiteLogin.AC_DropdownText}>
            <Line style={StyleGCSiteLogin.lineDateTimeButton} />
          </View> 
        </Placeholder>
      )
    } 
    return array;
  }
  
  setAutoCompleteValue(selectedObject){
      //set site id values
      if(this.autoCompleteArrayKey=="site_id"){ 
          this.site_id=selectedObject.id;
          let acountId="-1";
         
          for(var i=0; i<this.seatIdArray.length; i++){
             if(this.seatIdArray[i].id==selectedObject.id){
              acountId=this.seatIdArray[i].m_account_id;
              this.selectedMarketId=this.seatIdArray[i].m_market_id;
              break;
             }
          }
          
          this.setState({site_id:selectedObject.site_id,selectedAcount:acountId,selectedMarket:this.selectedMarketId},()=>{
            this.showHideAutoCompletePopup(false);
            // this.getMarket(acountId);
          }); 
      } 
  }
  
  renderFlatListItem=({item},index)=>{ 
    let indexItem=index+""+new Date().toLocaleString()+"_itemflatlist"
		return( 
			<View  key={indexItem}> 
          <TouchableOpacity onPress={() => this.setAutoCompleteValue(item)}>
				    <Text style={StyleGCSiteLogin.AC_DropdownText}>{item[this.autoCompleteArrayKey]}</Text>
         </TouchableOpacity>
			</View> 
		); 
  }
  

  getCurrentDate=()=>{
		var now = new Date(); 
		let currentDateTime = new Date()
		let month = currentDateTime.getMonth() + 1
		if (month < 10) {
			month = '0' + month; 
		}
		
		let day = currentDateTime.getDate()
		if (day < 10) {
			day = '0' + day; 
		}
		
		let year = currentDateTime.getFullYear()
		let fullDate=month + "/" + day + "/" + year;
		return fullDate;
  }
  
  selectedSnapShot = () =>{
    if(this.state.snapDataBase64 !== "")
    {
      return(
        <Text style={StyleGCSiteLogin.snapshotFileText}
              numberOfLines= {1}>
              {"NFSD SnapShot Captured"}
        </Text>
      )
    }
    else
    {
      return(
        <View />
      )
    }
  }

  selectparmater=(parm)=>{
    console.log(parm)

  }
  getradiobtn(){
      return(<RadioGroup getChecked={this.getChecked}>
        <Radio iconName={"lens"} label={"CX"} value={"1"}/>
        <Radio iconName={"lens"} label={"IX"} value={"2"}/>
        <Radio iconName={"lens"} label={"Both"} value={"3"}/>
        </RadioGroup> )    
   }
  getUtcDate=(start)=>{
		const start_date = start;
					   
	  
	 
	   const start_dt = start_date.getUTCDate() >= 10 ? start_date.getUTCDate() : "0" + start_date.getUTCDate();
	   const start_month = start_date.getUTCMonth() + 1 >= 10 ? start_date.getUTCMonth() + 1 : "0" + (start_date.getUTCMonth() + 1); //Months are zero based
	   const start_year = start_date.getUTCFullYear();
	   const start_hr = start_date.getUTCHours() >= 10 ? start_date.getUTCHours() : "0" + start_date.getUTCHours(); 
	   const start_min = start_date.getUTCMinutes() >= 10 ? start_date.getUTCMinutes() : "0" + start_date.getUTCMinutes();
	   const start_sec = start_date.getUTCSeconds() >= 10 ? start_date.getUTCSeconds() : "0" + start_date.getUTCSeconds();
	   
		const start_timestamp = start_year + "/" + start_month + "/" + start_dt ;
	 
		return start_timestamp 
	   }
  submitGcLogin= async ()=>{ 
    console.log("teps 0")
  console.log("teps : ")
  let now=  this.getUtcDate(new Date())
	let frm = now + ' '+ this.state.startHH +':'+this.state.startMM+':00'
	let end = now + ' '+ this.state.endHH +':'+this.state.endMM+':00'
	console.log(frm)
	console.log(end)

	  
console.log("teps 1")
 
  if(this.state.selectedItemsPORs==null ||  this.state.selectedItemsPORs=="" ||  (Array.isArray( this.state.selectedItemsPORs) &&  this.state.selectedItemsPORs.length==0 ))
    {
      alert("Please Select Technology");   //"Please Select Por"  
      return;
    }
    console.log("teps 2")
  //  console.log(par) 
    if(this.state.selectedMainId==null || this.state.selectedMainId=="" || this.state.selectedMainId == "Select Site")
    {
      alert("Please Select Site Id");
      return;
    }
    console.log("teps 3")
    if(this.state.ttIDMobile==null || this.state.ttIDMobile=="" || this.state.ttIDMobile == "Select Site")
    {
      alert("Please Select T Mobile");
      return;
    }
    console.log("teps 4")

    if(this.state.selectedServiceAffected==null || this.state.selectedServiceAffected=="" || this.state.selectedServiceAffected == "Select Site")
    {
      alert("Please Select Service Affecting");
      return;
    }
    if(this.state.selectedPlannedSOW==null || this.state.selectedPlannedSOW=="" || this.state.selectedPlannedSOW == "Select Site")
    {
      alert("Please Select Planned SOW");
      return;
    }
    
    if(this.state.selectedDTO==null || this.state.selectedDTO=="" || this.state.selectedDTO == "Select Site")
    {
      alert("Please Select Day MOP");
      return;
    }
    console.log("teps 5")

    if(this.state.selectedDTO==null || this.state.selectedDTO=="" || this.state.selectedDTO == "Select Site")
    {
      alert("Please Select Day MOP");
      return;
    }
    if(this.state.SelectedSOWDetails==null || this.state.SelectedSOWDetails=="" || this.state.SelectedSOWDetails == "Select Site")
    {
      alert("Please Select SOW Details");
      return;
    }
    
  //   let siteidd = 0;
  //   for(var i=0; i<this.seatIdArray.length; i++)
  //   {
  //     if(this.seatIdArray[i].site_id==this.state.site_id)
  //     {
  //       siteidd =this.seatIdArray[i].id;
  //       break;
  //     }
  //  }

  //  let imageData = await this.state.picturePath

  //  if(this.state.picturePath !== "" && this.state.picturePath !== null)
  //  {
  //    await RNFS.readFile(this.state.picturePath, "base64").then(async(data) => {
  //    // binary data
  //       imageData = data;
  //    });
  //  }
  console.log("teps 6")
      let currentTime= new Date().getHours()+":"+new Date().getMinutes();
      let params=
      {


        "site_id":this.state.selectedMainId,
"Work_Order_No":this.state.selectedWON,
"POR":this.state.selectedPOR,
"T_Mobile_TicketID":"NA",
"Service_Affecting":this.state.selectedServiceAffected,
"Day_MOP_Night_MOP":this.state.selectedDTO,
"Reason_for_Day_MOP":this.state.dayMOP,
"RF_Approval":this.state.selectedRFApproval,
"RF_Approved_MW_Time":"2020-03-21 15:00:00",
"RF_Approved_End_Time":"2020-03-21 05:00:00",
"TMO_Deployment_Manager":this.state.selectedTMO,
"Planned_SoW":this.state.selectedPlannedSOW,
"LoginType":"1",

        // "tbl_site_id":siteidd,
        // "tbl_gc_id": GlobalVariable.USERID,
        // "tbl_gc_login_id":"",
        // "m_account_id":this.state.selectedAcount,
        // "m_market_id":this.state.selectedMarket,
        // "service_affecting":this.state.selectedServiceAffected,
        // "dto":this.state.selectedDTO,
        // "planned_sow":this.state.SelectedSOW,
        // "planned_sow_details":this.state.SowDetail,
        // "estimated_time_on_site":this.state.estimatedTime,
        // "emiLoginStr":this.state.emiLoginStr,
        // "nfsd":this.state.preWorkID,
        // "eim":this.state.emiLoginStr,
        // "pre_nfsd_snap":"2",
        // "nfsd_login_snap_file": imageData,
        //  "date": this.currentDate,
        //  "pre_request_received":currentTime,
        //  "additional_email_id":this.state.AdditionalEmailId

      };
//
// { tbl_site_id: 1, tbl_gc_id: 33, m_account_id: 1, m_market_id: 2, service_affecting: 0,
//    dto: 1,planned_sow:6,planned_sow_details:'none',estimated_time_on_site:6 }
let par= [];
let sows=""
let Details=""

  this.state.SelectedSOWDetails.map((itm,i)=>{
    if(i==0){
      Details=Details+itm
    } else{
      Details=Details+","+itm
    }
  })

this.state.selectedPlannedSOW.map((itm,i)=>{
   if(i==0){
      sows=sows+itm
   } else{
     sows=sows+","+itm
   }
})
this.state.selectedItemsPORs.forEach(resp=>{
  console.log(resp)
  let respp = resp.split("/")
  console.log(respp[0])
  console.log(respp[1])
  // this.selectparmater(resp)
  console.log("ttttt",Details)  
  console.log("ttttt",sows)
  par.push({
    "CX_Crew_Ld_E_mail_ID":this.state.emailIdUser,
    "site_id":this.state.selectedMainId,
    "Work_Order_No":respp[0],
    "POR": respp[1],
    "T_Mobile_TicketID": this.state.ttIDMobile,
    "Account": this.state.selectedAcount,
    "Market": this.state.selectedMarket,
    "Service_Affecting": this.state.selectedServiceAffected,
    "Day_MOP_Night_MOP": this.state.selectedDTO,
    "Reason_for_Day_MOP": this.state.dayMOPComment,
    "RF_Approval": this.state.selectedRFApproval,
    "RF_Approved_MW_Time": frm,
    "RF_Approved_End_Time": end,
    "TMO_Deployment_Manager": this.state.selectedTMO,
    "Planned_SoW": sows,
    "LoginType": this.state.loginType,
    "sowDetails":Details,
    "sowComment":this.state.SOWComment,
    "nestingTime":this.state.nestedTime,
  })
  
})
      let paramObject = {
        path: Constant.Api.PreCheckReqNew,
        showErrorMsg: true,
        params: par 
    };
    console.log("ttttt",paramObject)

    // alert(Constant.Api.LCLOGIN.GCSITELOGIN)
    console.log("teps 7")
     //display loader first
     this.setState({isLoadervisible:false},()=>{
        PostRequest(paramObject).then((gciLoginSuccess) => { 
          console.log("teps 8")
          // alert(JSON.stringify(gciLoginSuccess))
            this.setState({isLoadervisible:false},async()=>{ 
              console.log("teps 9")
              console.log("teps 10", gciLoginSuccess)
                 console.log("response", gciLoginSuccess )
                 if(gciLoginSuccess.data && gciLoginSuccess.data.status==1)
                 {
                  let alertTitle= I18n.t('GCSITELOGINPAGE.GCLOGINSUCCESS');
                  let alertMessage= I18n.t('GCSITELOGINPAGE.GCLOGINSUCCESS_MSG');
                  let buttonStr= I18n.t('OK_BUTTON');

                  Alert.alert(
                    alertTitle,
                    gciLoginSuccess.data.message,
                    [
                      
                      {text: buttonStr, onPress: () => { this.goToPreviousPage();}},
                    ],
                    {cancelable: false},
                  );  

               //   Toast.show(gciLoginSuccess.data.message, Toast.LONG);
                  await AsyncStorage.setItem("snapDataBase64", "");
                  // this.goToPreviousPage();


                 }else{
                   
                  this.displayAlertMsg(
                    "GC Login",
                    gciLoginSuccess.data.message,null
                ); 
                 }
            });      
        }, (error) => {  
            console.log(error)
        }); 
    }); 
}


filterList(e){
  let updateList = this.state.mainIdArray;
  updateList = updateList.filter(item => {
    return item.toLowerCase().search(
      e.target.value.toLowerCase()
      ) !== -1;
  });

  this.setState({
    serachMainIdData: updateList
  });
}

openModals=()=>{
  this.setState({selectFromModal: true})
}

getChecked = (value) => {
  // value = our checked value

  console.log("file",value)  
  this.state.loginType = value;
  }

  render() {  
    const { selectedItems } = this.state;

    return (
      <KeyboardAvoidingView style={StyleGCSiteLogin.container}>
      <StatusBarComponent isHidden={false} />
      <OfflineBarComponent />
      {
					this.state.isLoadervisible &&
					<Loader msg={I18n.t('LOADING')} />
				}
      { DeviceInfo.isAndroid() && 
        <ToolbarComponent 
          title= "GC Site "
          navIcon={Constant.ICONS.BACK_ICON}
          navClick={() => this.props.navigation.goBack(null)}
        /> 
      }

      <Modal  
        animationType="fade" 
        visible={this.state.isAutoCompletePopupVisible}
        onRequestClose={() => {}}> 
        <View style={StyleGCSiteLogin.popupContainerParent}>         
          <View style={StyleGCSiteLogin.searchBarContainerer}>
            <View style={StyleGCSiteLogin.backIconContainer}>
              <Icon name={Constant.ICONS.BACK_ICON} 
              size={30} 
              color={Colors.white} 
              onPress={()=>this.showHideAutoCompletePopup(false)}/>
            </View>
            <View  style={StyleGCSiteLogin.searchBoxContainer}>  
              <TextInput 
                value={this.autoCompleteQueryStr}
                placeholder={this.autoCompletePlaceholder}
                keyboardType='default' 
                style={StyleGCSiteLogin.autoCompleteInput} 
                onChangeText={(keyPress) => this.filterAutoComplete(keyPress,true)} 
              /> 
            </View>
            <View style={StyleGCSiteLogin.crossIconContainer}> 
              <Icon name={Constant.ICONS.CROSS_ICON} 
              size={30} 
              color={Colors.white} 
              onPress={()=>this.filterAutoComplete("")}/> 
            </View>
          </View>
          <View style={StyleGCSiteLogin.flextListContainer}> 
            <FlatList
              ListEmptyComponent={this.renderPlaceHolder}  
              data={this.state.autoCompleteFilteredArray}
              extraData={this.state} i
              nitialNumToRender={20} 
              keyExtractor={(item, index) => index+""+new Date().toLocaleString()}
              renderItem={this.renderFlatListItem} 
            />  
          </View>     
        </View>  
      </Modal> 
      
      {!this.state.isModalVisible &&
      <View style={StyleGCSiteLogin.formContainer}>
        <ScrollView keyboardShouldPersistTaps='always'> 
          <View style={StyleGCSiteLogin.formContainerInner}> 
              {/* MainId */}
              {!this.state.isMainIDData && 
              
              <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                {I18n.t('GCSITELOGINPAGE.MainId')}
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}>
                {/* <Picker 
                    selectedValue={this.state.selectedMainId}
                    style={StyleGCSiteLogin.dropdown}
                    enabled = {true}
                    onValueChange={(marketId, itemIndex) =>
                      
                    this.getWorkOrderNumber(marketId)
                  }
                  >
                  {this.renderMainIdPicker()}
                </Picker> */}
                <TextInput style={StyleGCSiteLogin.inputField}  
                    autoCapitalize="none"
                    keyboardType='default'
                    returnKeyType="next"
                    value={this.state.mainIdValuess} 
                    onChangeText={(prework) => this.setState(()=>{
                      // console.log(r)
                      this.state.mainIdValuess = prework
                if(prework.length >= 1){
                  // this.renderMainIdPicker(prework)
                  // console.log(this.state.isSerachMainId,'seratch')
                  this.setState({isSerachMainId:true})
                  // this.serachMainDataKey = prework;
                  this.state.mainIdArraySearch= []
                  this.state.mainIdArray.forEach(resp=>{
                    // console.log(resp,",,,,,goggggle")
                    console.log(resp.site_name.substring(0,prework.length))
                    if (resp.site_id.substring(0, prework.length) ==  (prework.toUpperCase())) {
                      console.log("Object with index number  contains " + resp.site_id);
                       this.state.mainIdArraySearch.push(resp)
                    }
                    console.log('checkkkdsddf',this.state.mainIdArraySearch)
                  })
                  console.log(this.state.isSerachMainId,'seratch')
								}else{
                  
									this.setState({isSerachMainId:false})
                  console.log(this.state.isSerachMainId,'seratch')
								}
							})}
							/>
              {this.state.isSerachMainId == true &&
                // this.renderMainIdPicker()
                // <Text>AJAY</Text>
                // console.log(this.state.mainIdArray)
                 this.state.mainIdArraySearch.map((resp,i) => {
                  console.log(resp.site_id)
                  
                  //  if(resp.site_id == )
                  // let key=i+""+new Date().toLocaleString()+"_marketPickerList"
                  return(
                    <Text style={{paddingLeft: 10,paddingTop: 5 ,paddingBottom:5, fontSize: 16}} onPress={( itemIndex)=>{
                      // console.log(marketId,"markekesttt")
                      this.state.selectedMainId=resp.site_id
                     this.setState({ mainIdValuess: resp.site_id})
                      this.getWorkOrderNumber(resp.site_id)
                      this.setState({isSerachMainId: false })
                      this.mainIdArraySearch = []
                    }}>{resp.site_id}</Text>
                  )
                    // label={marketObj.site_name} value={marketObj.site_id}  key={key}/>        
                  
                })
              }


              </View>
            </View>
            
              
              }
              
              {/* Work Order Number */}
















              {this.state.isWONData &&
                <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
                  <Text style={StyleGCSiteLogin.inputTitle}>
                    {/* {I18n.t('GCSITELOGINPAGE.MARKET')} */}
                    {/* Work Order Number */}
                    Project Code
                  </Text>
                  <View style={StyleGCSiteLogin.dropdownContainer}>
                    {/* <Picker 
                        selectedValue={this.state.selectedWON}
                        style={StyleGCSiteLogin.dropdown}
                        enabled = {true}
                        onValueChange={(marketId, itemIndex) =>
                       this.getPOR()
                      }>

{this.renderWorkOrderNUmberPicker()}
                    </Picker> */}
                        <MultiSelect
                      hideTags
                      items={this.state.WONArray}
                      uniqueKey="won"
                      ref={(component) => { this.multiSelect = component }}
                      onSelectedItemsChange={this.onSelectedItemsChange}
                      selectedItems={selectedItems}
                      // selectText="Work Order Number"
                      selectText="Project Code"
                      searchInputPlaceholderText="Search Project Code Number..."
                      onChangeInput={ (text)=> console.log(text)}
                      altFontFamily="ProximaNova-Light"
                      tagRemoveIconColor="#CCC"
                      tagBorderColor="#CCC"
                      tagTextColor="#CCC"
                      selectedItemTextColor="#CCC"
                      selectedItemIconColor="#CCC"
                      itemTextColor="#000"
                      displayKey="won"
                      searchInputStyle={{ color: '#CCC' }}
                      submitButtonColor="#CCC"
                      submitButtonText="Submit"
                    />
                      
                  </View>
                </View>
               }
              
              
              {/* POR */}
              {this.state.isPORData && 
              <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
              <Text style={StyleGCSiteLogin.inputTitle}>
                {/* {I18n.t('GCSITELOGINPAGE.MARKET')} */}
                Select Technology
              </Text>
              <View style={StyleGCSiteLogin.dropdownContainer}>
                {/* <Picker 
                    selectedValue={this.state.selectedPOR}
                    style={StyleGCSiteLogin.dropdown}
                    enabled = {false}
                    onValueChange={(marketId, itemIndex) =>
                    this.setState({selectedMarket:marketId})
                  }>
                  {this.renderPORPicker()}
                </Picker> */}
                  {/* <MultiSelect
                    hideTags
                    items={this.state.PORData}
                    uniqueKey="id"
                    ref={(component) => { this.multiSelect = component }}
                    onSelectedItemsChange={this.onSelectedItemsChangePOR}
                    selectedItems={this.state.selectedItemsPOR}
                    selectText="POR"
                    searchInputPlaceholderText="Search POR..."
                    onChangeInput={ (text)=> console.log(text)}
                    altFontFamily="ProximaNova-Light"
                    tagRemoveIconColor="#CCC"
                    tagBorderColor="#CCC"
                    tagTextColor="#CCC"
                    selectedItemTextColor="#CCC"
                    selectedItemIconColor="#CCC"
                    itemTextColor="#000"
                    displayKey="name"
                    searchInputStyle={{ color: '#CCC' }}
                    submitButtonColor="#CCC"
                    submitButtonText="Submit"
                  /> */}



		<SectionedMultiSelect
				items={this.state.TempPORData}
				uniqueKey="id"
				subKey="children"
				// selectText="Select Por"
        selectText="Select Technology"
				showDropDowns={true}
				readOnlyHeadings={true}
				onSelectedItemsChange={this.onSelectedItemsChangePOR}
        selectedItems={this.state.selectedItemsPORs}
        IconRenderer = {Icon1}
        />
              </View>
            </View>
            
              }
              
              {/* T-Mobile TTID Otherwise NA */}
              <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
                <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {/* {I18n.t('GCSITELOGINPAGE.PRE_WORK_ID')} */}
                      T-Mobile TTID
                    </Text>
                    <View style={StyleGCSiteLogin.dropdownContainer}> 
                        <TextInput style={StyleGCSiteLogin.inputField}  
                        keyboardType='default'
                        returnKeyType="next"
                        maxLength = {100}
                        value={this.state.ttIDMobile} 
                        onChangeText={(prework) => this.setState({ttIDMobile:prework})}
                        /> 
                    </View>
                </View>
                <View style={StyleGCSiteLogin.gridColumn}>
                <Text style={StyleGCSiteLogin.inputTitle}>
                  {I18n.t('GCSITELOGINPAGE.ACCOUNT')}
                </Text>
                <View style={StyleGCSiteLogin.dropdownContainer}>
                  {/* {this.state.isAccountData &&  */}
            
            {console.log("asdfsdf",this.state.selectedAcount)}
                    <Picker
                      style={StyleGCSiteLogin.dropdown} 
                      selectedValue={this.state.selectedAcount} 
                      enabled = {false}
                      onValueChange={
                      (accountId, itemIndex) =>{
                      this.getMarket(accountId);  
                      }
                      }> 
                      {this.renderAccountPicker()}
                  </Picker>
                  {/* } */}
                  
                </View>
                </View>
              </View>
              
              
              {/* Market */}
              <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
                <Text style={StyleGCSiteLogin.inputTitle}>
                  {I18n.t('GCSITELOGINPAGE.MARKET')}
                </Text>
                <View style={StyleGCSiteLogin.dropdownContainer}>
                
                  <Picker 
                      selectedValue={this.state.selectedMarket}
                      style={StyleGCSiteLogin.dropdown}
                      enabled = {false}
                      onValueChange={(marketId, itemIndex) =>
                      this.setState({selectedMarket:marketId})
                    }>
                    {this.renderMarketPicker()}
                  </Picker>
                </View>
              </View>
            
              {/* Planned SOW */}
              <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
                <Text style={StyleGCSiteLogin.inputTitle}>
                  {I18n.t('GCSITELOGINPAGE.PLANSOW')}
                </Text>
                <View style={StyleGCSiteLogin.dropdownContainer}>
                  {/* <Picker
                    enabled = {true} 
                    selectedValue={this.state.SelectedSOW}
                    style={StyleGCSiteLogin.dropdown}
                    onValueChange={(selectedSow, itemIndex) =>
                    this.setState({SelectedSOW: selectedSow})
                    }>  
                      {this.renderSOWPicker()}
                  </Picker> */}
                  <MultiSelect
                    hideTags
                    items={this.state.planSowData}
                    uniqueKey="id"
                    ref={(component) => { this.multiSelect = component }}
                    onSelectedItemsChange={this.onSelectedItemsChangePLannedSow}
                    selectedItems={this.state.selectedPlannedSOW}
                    selectText="Plan SOW"
                    // searchInputPlaceholderText="Search POR..."
                    searchInputPlaceholderText="Search Technology..."
                    onChangeInput={ (text)=> console.log(text)}
                    altFontFamily="ProximaNova-Light"
                    tagRemoveIconColor="#CCC"
                    tagBorderColor="#CCC"
                    tagTextColor="#CCC"
                    selectedItemTextColor="#CCC"
                    selectedItemIconColor="#CCC"
                    itemTextColor="#000"
                    displayKey="name"
                    searchInputStyle={{ color: '#CCC' }}
                    submitButtonColor="#CCC"
                    submitButtonText="Submit"
                  />
                  <View>
                    {this.multiSelect && this.multiSelect.getSelectedItemsExt(selectedItems)}
                  </View>
                </View>
              
              </View>

              {/* TMO Deployment Manager */}
              <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
                <Text style={StyleGCSiteLogin.inputTitle}>
                  {/* {I18n.t('GCSITELOGINPAGE.PLANSOW')} */}
                  TMO Deployment Manager
                </Text>
                <View style={StyleGCSiteLogin.dropdownContainer}>
                  <Picker
                    enabled = {true} 
                    selectedValue={this.state.selectedTMO}
                    style={StyleGCSiteLogin.dropdown}
                    onValueChange={(selectedSow, itemIndex) =>
                      
                    this.setState({selectedTMO: selectedSow})
                    }>  
                      {this.renderTMOPicker()}
                  </Picker>
                </View>
              </View>

              {/* Service Effecting */}
              


    



              <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
                <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {I18n.t('GCSITELOGINPAGE.SERVICE_AFFECTING')}
                    </Text>
                    <View style={StyleGCSiteLogin.dropdownContainer}> 
                      <Picker
                          enabled = {true} 
                          selectedValue={this.state.selectedServiceAffected}
                          style={StyleGCSiteLogin.dropdown}
                          onValueChange={(serviceAffected, itemIndex) =>
                          this.setState({selectedServiceAffected: serviceAffected})
                        }>
                          <Picker.Item label="Select Service Affect" value="-1" />
                        <Picker.Item label="Yes" value="1" />
                        <Picker.Item label="No" value="0" />
                      </Picker>
                  </View>
                </View>
                {/* <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {I18n.t('GCSITELOGINPAGE.DTO')}
                    </Text>
                    <View style={StyleGCSiteLogin.dropdownContainer}> 
                      <Picker
                          enabled = {true} 
                          selectedValue={this.state.selectedDTO}
                          style={StyleGCSiteLogin.dropdown}
                          onValueChange={(selectedDTO, itemIndex) =>
                          this.setState({selectedDTO: selectedDTO})
                        }>
                        <Picker.Item label="Yes" value="1" />
                        <Picker.Item label="No" value="0" />
                      </Picker>
                    </View>
                </View> */}
              </View>




              <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
                <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {/* {I18n.t('GCSITELOGINPAGE.SERVICE_AFFECTING')} */}
                      RF Approved MW (Start Time)
                    </Text>
                    
                </View>

				<View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle,{ marginLeft: 22, marginBottom: 5}}>
                      {/* {I18n.t('GCSITELOGINPAGE.SERVICE_AFFECTING')} */}
                      HH                     MM
                    </Text>
					<View style = {{flexDirection: 'row',width:140 ,height:40,alignSelf: 'center', alignItems:'center', justifyContent: 'center',}}>
					<View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>
					<TextInput style={StyleGCSiteLogin.inputField}  
							autoCapitalize="none"
							keyboardType='numeric'
							returnKeyType="next" 
							maxLength = {2}
							value={this.state.startHH} 
							onChangeText={(prework) => this.setState(()=>{
								if(prework > 23){
									console.log("value greater than : ",prework)
									return false
								}else{
									this.setState({startHH:prework})
									console.log("value smaller than : ",prework)
								}
							})}
							/>
						{/* <Text style={{alignSelf:'stretch',padding: 5, fontSize: 15, fontFamily: 'Roboto', fontWeight: 'bold', color: Colors.lightOrange,textAlign:'center'}}>{'Camera'}</Text>      */}
					</View>
					<View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>
						<Text style={StyleGCSiteLogin.inputTitle}>:</Text>
					</View>
					<View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>
					<TextInput style={StyleGCSiteLogin.inputField}  
							autoCapitalize="none"
							keyboardType='numeric'
							returnKeyType="next" 
							maxLength = {2}
							value={this.state.startMM} 
							onChangeText={(prework) => this.setState(()=>{
								if(prework > 59){
									console.log("value greater than : ",prework)
									return false
								}else{
									this.setState({startMM:prework})
									console.log("value smaller than : ",prework)
								}
							})}
							/>
						{/* <Text style={{alignSelf:'stretch',padding: 5, fontSize: 15, fontFamily: 'Roboto', fontWeight: 'bold', color: Colors.lightOrange,textAlign:'center'}}>{'Gallery'}</Text>       */}
					</View>     
                </View>
    
                </View>

				




                
              </View>
              



              <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
                <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {/* {I18n.t('GCSITELOGINPAGE.SERVICE_AFFECTING')} */}
                      How Much it will take 
                    </Text>
                    
                </View>
				<View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle,{ marginLeft: 22, marginBottom: 5}}>
                      {/* {I18n.t('GCSITELOGINPAGE.SERVICE_AFFECTING')} */}
                      HH                     MM
                    </Text>
					<View style = {{flexDirection: 'row',width:140 ,height:40,alignSelf: 'center', alignItems:'center', justifyContent: 'center',}}>
					<View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>
					<TextInput style={StyleGCSiteLogin.inputField}  
							autoCapitalize="none"
							keyboardType='numeric'
							returnKeyType="next" 
							maxLength = {2}
							value={this.state.endHH} 
							onChangeText={(prework) => this.setState(()=>{
								if(prework > 23){
									console.log("value greater than : ",prework)
									return false
								}else{
									this.setState({endHH:prework})
									console.log("value smaller than : ",prework)
								}
							})}
							/>
						{/* <Text style={{alignSelf:'stretch',padding: 5, fontSize: 15, fontFamily: 'Roboto', fontWeight: 'bold', color: Colors.lightOrange,textAlign:'center'}}>{'Camera'}</Text>      */}
					</View>
					<View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>
						<Text style={StyleGCSiteLogin.inputTitle}>:</Text>
					</View>
					<View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>
					<TextInput style={StyleGCSiteLogin.inputField}  
							autoCapitalize="none"
							keyboardType='numeric'
							returnKeyType="next" 
							maxLength = {2}
							value={this.state.endMM} 
							onChangeText={(prework) => this.setState(()=>{
								if(prework > 59){
									console.log("value greater than : ",prework)
									return false
								}else{
									this.setState({endMM:prework})
									console.log("value smaller than : ",prework)
								}
							})}
							/>
						{/* <Text style={{alignSelf:'stretch',padding: 5, fontSize: 15, fontFamily: 'Roboto', fontWeight: 'bold', color: Colors.lightOrange,textAlign:'center'}}>{'Gallery'}</Text>       */}
					</View>     
                </View>
    
                </View>

			  </View>
              


              <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
                {/* <View style={StyleGCSiteLogin.gridColumn}> */}
                    {/* <Text style={StyleGCSiteLogin.inputTitle}> */}
                      {/* {I18n.t('GCSITELOGINPAGE.SERVICE_AFFECTING')}
                      {/* RF Approved 
                    </Text>
                    <View style={StyleGCSiteLogin.dropdownContainer}> 
                      <Picker
                          enabled = {true} 
                          selectedValue={this.state.selectedRFApproval}
                          style={StyleGCSiteLogin.dropdown}
                          onValueChange={(serviceAffected, itemIndex) =>
                          this.setState({selectedRFApproval: serviceAffected})
                        }>
                          <Picker.Item label="Select RF Approval" value="-1" />
                         <Picker.Item label="Yes" value="1" />
                        <Picker.Item label="No" value="0" />
                      </Picker>
                  </View>
                </View> */}    
                <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {/* {I18n.t('GCSITELOGINPAGE.DTO')} */}
                      Day MOP or Night Mop
                    </Text>
                    <View style={StyleGCSiteLogin.dropdownContainer}> 
                      <Picker
                          enabled = {true} 
                          selectedValue={this.state.selectedDTO}
                          style={StyleGCSiteLogin.dropdown}
                          onValueChange={(selectedDTO, itemIndex) =>
                            this.setState({selectedDTO: selectedDTO},()=>{
                              if(selectedDTO == 1){
                                this.setState({isDayMOP: true})
                              }else{
                                this.setState({isDayMOP: false})
                              }
                            })
                          // this.dayMOPVALUEChange(selectedDTO)
                        }>
                          <Picker.Item label="Select MOP" value="-1" />
                        <Picker.Item label="Day MOP" value="1" />
                        <Picker.Item label="Night MOP" value="2" />
                      </Picker>
                    </View>
                </View>
              </View>

              {this.state.isDayMOP &&
                <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
                  <Text style={StyleGCSiteLogin.inputTitle}>
                  Reason For  Day MOP
                  </Text>
                  <View style={StyleGCSiteLogin.dropdownContainer}>
                  <TextInput style={StyleGCSiteLogin.inputField}  
                      keyboardType='default'
                      returnKeyType="next"
                      maxLength = {100}
                      value={this.state.dayMOPComment} 
                      onChangeText={(prework) => this.setState({dayMOPComment:prework})}
                      />
                </View>


                  {/* <TextInput style={StyleGCSiteLogin.inputField}  
                   keyboardType='default'
                   returnKeyType="next" 
                   maxLength = {200}
                   onChangeText={(sowDetail) => this.setState({SowDetail:sowDetail})}
                   value={this.state.SowDetail}  /> */}
              </View>
              }


              
              
              <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
                  <Text style={StyleGCSiteLogin.inputTitle}>
                    {I18n.t('GCSITELOGINPAGE.SOW_DETAILS')}
                  </Text>
                  <View style={StyleGCSiteLogin.dropdownContainer}>
                  {/* <Picker
                    enabled = {true} 
                    selectedValue={this.state.SelectedSOW}
                    style={StyleGCSiteLogin.dropdown}
                    onValueChange={(selectedSow, itemIndex) =>
                    this.setState({SelectedSOW: selectedSow})
                    }>  
                    planSowData
                    sowArrayDetails
                      {this.renderSOWPicker()}
                  </Picker> */}
                  
                  <MultiSelect
                    hideTags
                    items={this.state.sowArrayDetails}
                    uniqueKey="id"
                    ref={(component) => { this.multiSelect1 = component }}
                    onSelectedItemsChange={this.onSelectedItemsChangeSOWDetail}
                    selectedItems={this.state.SelectedSOWDetails}
                    selectText="SOW Details"
                    searchInputPlaceholderText="Search SOW Details..."
                    onChangeInput={ (text)=> console.log(text)}
                    altFontFamily="ProximaNova-Light"
                    tagRemoveIconColor="#CCC"
                    tagBorderColor="#CCC"
                    tagTextColor="#CCC" 
                    selectedItemTextColor="#CCC"
                    selectedItemIconColor="#CCC"
                    itemTextColor="#000"
                    displayKey="name"
                    searchInputStyle={{ color: '#CCC' }}
                    submitButtonColor="#CCC"
                    submitButtonText="Submit"
                  />
                  <View>
                    {this.multiSelect && this.multiSelect1.getSelectedItemsExt(selectedItems)}
                  </View>
                </View>
              
                  {/* <View style={StyleGCSiteLogin.dropdownContainer}>
                  <Picker
                    enabled = {true} 
                    selectedValue={this.state.SelectedSOWDetails}
                    style={StyleGCSiteLogin.dropdown}
                    onValueChange={(selectedSowDetails, itemIndex) =>
                    this.setState({SelectedSOWDetails: selectedSowDetails})
                    }>  
                      {this.renderSOWDetailsPicker()}
                  </Picker>
                </View> */}


                  {/* <TextInput style={StyleGCSiteLogin.inputField}  
                   keyboardType='default'
                   returnKeyType="next" 
                   maxLength = {200}
                   onChangeText={(sowDetail) => this.setState({SowDetail:sowDetail})}
                   value={this.state.SowDetail}  /> */}
              </View>
              <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
                <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {/* {I18n.t('GCSITELOGINPAGE.PRE_WORK_ID')} */}
                      SOW Comment
                    </Text>
                    <View style={StyleGCSiteLogin.dropdownContainer}> 
                        <TextInput style={StyleGCSiteLogin.inputField}  
                        keyboardType='default'
                        returnKeyType="next"
                        maxLength = {100}
                        value={this.state.SOWComment} 
                        onChangeText={(prework) => this.setState({SOWComment:prework})}
                        /> 
                    </View>
                </View>
                <View style={StyleGCSiteLogin.gridColumn}>
                <Text style={StyleGCSiteLogin.inputTitle}>
                  Nesting Time
                </Text>
                <View style={StyleGCSiteLogin.dropdownContainer}> 
                        <TextInput style={StyleGCSiteLogin.inputField}  
                        autoCapitalize="none"
                        keyboardType='numeric'
                        returnKeyType="next" 
                        maxLength = {2}
                        value={this.state.nestedTime} 
                        onChangeText={(prework) => this.setState({nestedTime:prework})}
                        /> 
                    </View>
                </View>
              </View>
              
            
            
            
            
            
{/*             
              <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
                  <Text style={StyleGCSiteLogin.inputTitle}>
                    {I18n.t('GCSITELOGINPAGE.ESTIMATED_TIME')}
                  </Text>
                  <TextInput style={StyleGCSiteLogin.inputField}  
                    keyboardType='numeric'
                    returnKeyType="next"
                    maxLength = {3}
                    onChangeText={(estTime) => this.setState({estimatedTime:estTime})}
                  />
              </View>
              <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop]}>
                <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {I18n.t('GCSITELOGINPAGE.PRE_WORK_ID')}
                    </Text>
                    <View style={StyleGCSiteLogin.dropdownContainer}> 
                        <TextInput style={StyleGCSiteLogin.inputField}  
                        keyboardType='default'
                        returnKeyType="next"
                        maxLength = {100}
                        value={this.state.preWorkID} 
                        onChangeText={(prework) => this.setState({preWorkID:prework})}
                        /> 
                    </View>
                </View>
                <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {I18n.t('GCSITELOGINPAGE.EMI_LOGIN')}
                    </Text>
                    <View style={StyleGCSiteLogin.dropdownContainer}> 
                        <TextInput style={StyleGCSiteLogin.inputField}  
                          keyboardType='default'
                          returnKeyType="next"
                          maxLength = {100}
                          onChangeText={(emiLogin) => this.setState({emiLoginStr:emiLogin})}
                          value={this.state.emiLoginStr}
                        /> 
                    </View>
                </View>
              </View>


              

              <View style={[StyleGCSiteLogin.gridContainer,StyleGCSiteLogin.additionMarginTop, {alignItems: "center"}]}>
                <View style={StyleGCSiteLogin.gridColumn}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {I18n.t('GCSITELOGINPAGE.GC_NFSD_SNAPSHOT')}
                    </Text>
                </View>
                <View style={StyleGCSiteLogin.gridColumn}>
                    <TouchableOpacity style={[StyleGCSiteLogin.uploadButtonContainer, {backgroundColor:this.state.buttonBackground}]}  
                      onPress={()=>this.setSelectFromModalVisibility(true)}>
                      <Text style={StyleGCSiteLogin.uploadButtonText}>
                          {I18n.t('GCSITELOGINPAGE.UPLOAD_SNAP')}
                      </Text>
                    </TouchableOpacity>
                </View>
              </View>

              <View style={[StyleGCSiteLogin.dropdownContainerParent,StyleGCSiteLogin.additionMarginTop]}>
                  <Text style={StyleGCSiteLogin.inputTitle}>
                    Additional Email ID
                  </Text>
                  <TextInput style={StyleGCSiteLogin.inputField}  
                    maxLength = {50}
                    keyboardType='email-address'
                    returnKeyType="next"  
                    onChangeText={(addemailid) => this.setState({AdditionalEmailId:addemailid})}
                    value={this.state.AdditionalEmailId} 
                  />
              </View>
              {this.state.selectedNFSDLogin=="No" &&
                <View style={[StyleGCSiteLogin.gridColumn,StyleGCSiteLogin.additionMarginTop]}>
                    <Text style={StyleGCSiteLogin.inputTitle}>
                      {I18n.t('GCSITELOGINPAGE.NFSD_REASON')}
                    </Text>
                    <View style={StyleGCSiteLogin.dropdownContainer}> 
                        <TextInput style={StyleGCSiteLogin.inputField}  
                          keyboardType='default'
                          returnKeyType="next"
                          maxLength = {100}
                          
                        /> 
                    </View>
                </View> 
              } */}


              
              <View style={StyleGCSiteLogin.bottomContainer}>   
                  <TouchableOpacity style={StyleGCSiteLogin.buttonContainer} onPress={() => this.openModals()}>
                      <Text style={StyleGCSiteLogin.buttonText}>
                          {I18n.t('GCSITELOGINPAGE.LOGIN')}
                      </Text>
                  </TouchableOpacity>
              </View> 
          </View>
        </ScrollView>
      </View>

            }
   

    
<Modal
        animationType="none"
        transparent={true}
        closeOnClick={true}
        visible={this.state.selectFromModal}
        onRequestClose={() => {
          this.setSelectFromModalVisibility(false)
        }}>
        <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'rgba(0,0,0,0.6)'}}>

          <View style={{width: '90%',backgroundColor: '#FFFFFF', elevation:10, borderTopLeftRadius:8,borderTopEndRadius:8, margin: 5}}>

            <View style={{ flexDirection: 'row',height:55, backgroundColor: Colors.lightOrange,alignItems:'center'}}>
              <Text style={{flex: 1, paddingLeft: 7, fontWeight:'bold', fontSize: 17, fontFamily: 'Roboto', color: '#FFFFFF', textAlign: 'left'}}>
                Are You
              </Text>
              <View style={{width:80,height:30,justifyContent: 'center',alignSelf:'center',backgroundColor:'#fff',borderRadius:15, marginRight: 10}}>
                <TouchableOpacity
                  onPress={()=>this.setSelectFromModalVisibility(false)}>
                    <Text style={{ fontWeight:'bold', fontSize: 12, fontFamily: 'Roboto', color: Colors.lightOrange, textAlign: 'center'}}>
                       close
                    </Text>
                </TouchableOpacity>
              </View>
              
            </View>

            <View style = {{flexDirection: 'row', height:200,alignSelf: 'center', alignItems:'center', justifyContent: 'center',}}>
			<View style={StyleGCSiteLogin.formContainerInner}> 
        
      <RadioForm
          radio_props={radio_props}
          initial={0}
          buttonColor={'#e54f3b'}
          onPress={(value) => {this.getChecked(value)}}
        />  
       
	  
	  
  <View style={StyleGCSiteLogin.bottomContainer}>   
 
			  <TouchableOpacity style={StyleGCSiteLogin.buttonContainer} onPress={() => this.submitGcLogin()}>
				  <Text style={StyleGCSiteLogin.buttonText}>
					  {I18n.t('GCSITELOGINPAGE.LOGIN')}
				  </Text>
			  </TouchableOpacity>
		  </View>
	  
	  
		{/* <Button title="Submit" onPress={() => this.()}/> */}
	  
	
	</View>   
                </View>

            </View>

          </View>
       
       
        </Modal>
      

   
    </KeyboardAvoidingView>
    );
  }
}