import React, { Component, useEffect, useState } from "react";
//import storage, { FirebaseStorageTypes } from "@react-native-firebase/storage";
import firebase from 'react-native-firebase';
import AntDesignIcon from "react-native-vector-icons/AntDesign";
import { Text, View, TouchableOpacity, Alert, Platform, ActionSheetIOS, StyleSheet, PermissionsAndroid } from "react-native";
import RNFS from "react-native-fs";
import FileViewer from "react-native-file-viewer";
import { launchImageLibrary } from "react-native-image-picker";
import { DocumentPicker, DocumentPickerUtil } from "react-native-document-picker";
var RNFetchBlob = require('react-native-fetch-blob').default;

export default class FileManager extends Component{

    constructor(props) {
        super(props);
    }

    state = {
        uploads: [],
        path: "",
        name: ""
    }

 pickDocument = () => {
    DocumentPicker.show({
        filetype: [DocumentPickerUtil.allFiles()],
      },(error,res) => {
        // Android
        console.log("res file ", res.uri);
        if(res){
        console.log(
           res.uri,
           res.type,
           res.fileName,
           res.fileSize,
        );
        this.setState({path: res.uri});
        this.setState({name: res.fileName});
        }
        RNFetchBlob.fs
        .stat(res.uri)
        .then(
          stats => 
          { console.log("stats path ", stats.path);
            this.useCallback(stats.path, res.fileName);
          })
        .catch(
          (err) => console.log(err)
        )
    });
}

getFileLocalPath = (response) => {
  const { path, uri } = response;
  console.log("response path-> "+response);
  return Platform.OS === 'android' ? response : uri; // path - uri is used for ios and need to be worked
};

createStorageReferenceToFile = (fileName) => {
  const id = new Date().getTime();
  return firebase.storage().ref(`uploads/${id}_${fileName}`);// fileName
};

uploadFileToFireBase = (path, fileName) => {
  console.log("localpath response ",path);
  const fileSource = this.getFileLocalPath(path);
  console.log("storage response ", fileSource);
  const storageRef = this.createStorageReferenceToFile(fileName);
  console.log("storage ref ", storageRef);
  return storageRef.putFile(fileSource);
};

useCallback = async (path, fileName) => {
  console.log("use callback response ", path);
  const uploadTask = this.uploadFileToFireBase(path, fileName);
  console.log("use callback uploadtask  ", uploadTask);
  uploadTask.on(
    firebase.storage.TaskEvent.STATE_CHANGED,
    async snapshot => {
      //setProgress(
        console.log(snapshot.bytesTransferred / snapshot.totalBytes);
      //);
      if (snapshot.state === firebase.storage.TaskState.SUCCESS) {
        console.log(await snapshot.ref.getDownloadURL());
      }
    },
    error => {
      console.log("error ->> ",error);
    }
  );
};

 pickAttachmenet = async () => {
  if (Platform.OS === "ios") {
    const options = ["Image", "Document", "Cancel"];
    ActionSheetIOS.showActionSheetWithOptions(
      { options, cancelButtonIndex: 2, title: "Pick a data type" },
      async (buttonIndex) => {
        if (buttonIndex === 0) {
          // Open Image Picker
          launchImageLibrary({ mediaType: "photo" }, (res) => {
            if (!res.didCancel) {
              return { path: res.uri, name: res.fileName };
            }
          });
        } else if (buttonIndex === 1) {
          // Open Document Picker
          return this.pickDocument();
        } else {
          // exit
        }
      }
    );
  } else {
    return this.pickDocument();
  }
};

  componentDidMount = async () => {
    this.requestStoragePermission();
    const listRef = firebase.storage().ref().child("uploads");
    console.log("listref ", listRef)
    const res = (await listRef).items;
    this.setState({uploads: res});
  }

  requestStoragePermission = async () => {

    if (Platform.OS !== "android") return true

    const pm1 = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE);
    const pm2 = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);

    if (pm1 && pm2) return true

    const userResponse = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
    ]);

    if (userResponse['android.permission.READ_EXTERNAL_STORAGE'] === 'granted' &&
        userResponse['android.permission.WRITE_EXTERNAL_STORAGE'] === 'granted') {
        return true
    } else {
        return false
    }
}

render(){
  return (
    <View style={styles.loadingContainer}>
      { this.state.uploads &&
      this.state.uploads.map((upload) => {
        return (
          <View key={upload.name}>
            <TouchableOpacity
              onPress={async () => {
                const localPath = `${RNFS.DocumentDirectoryPath}/${upload.name}`;

                await RNFS.downloadFile({
                  fromUrl: await upload.getDownloadURL(),
                  toFile: localPath,
                }).promise;

                FileViewer.open(localPath, {
                  displayName: upload.name,
                });
              }}
            >
              <Text>{upload.name}</Text>
            </TouchableOpacity>
            <AntDesignIcon
              name="delete"
              onPress={() => {
                Alert.alert(`delete ${upload.name}?`, undefined, [
                  { text: "No" },
                  {
                    text: "Yes",
                    onPress: async () => {
                      const fileRef = firebase.storage().ref().child("uploads").child(upload.name);
                      await fileRef.delete();
                      this.setState({uploads: uploads.filter((u) => u.name === upload.name)});
                    },
                  },
                ]);
              }}
            />
          </View>
        );
      })
    }

      <TouchableOpacity
        children={<Text>Upload File</Text>}
        onPress={() => this.pickAttachmenet()}
      ></TouchableOpacity>
    </View>
  );
}
}

const styles = StyleSheet.create({
    loadingContainer: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    }
})
