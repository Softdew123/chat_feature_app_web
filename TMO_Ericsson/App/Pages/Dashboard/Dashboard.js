
import React, { PureComponent } from 'react';
import {Platform,AppState, Alert, View,FlatList,RefreshControl,TouchableOpacity,Text, Modal, TextInput,ScrollView, Image } from 'react-native';
import StatusBarComponent from '../../Components/StatusBar';
import ToolbarComponent from "../../Components/Toolbar";
import { StyleDashboard } from '../../Styles/PageStyle';
import Loader from '../../Components/Loader';
import I18n from '../../Utils/i18n';
import { CommonMethods,DeviceInfo,Dialog,GlobalVariable } from '../../Utils/Utils'
import Placeholder,{Paragraph,Line, Media } from 'rn-placeholder';
import {Colors } from '../../Styles/Themes'
import Constant from '../../Utils/Constant';
import Icon from 'react-native-vector-icons/Ionicons';
import { StyleGCSiteLogin } from '../../Styles/PageStyle';
import axios from 'axios'; 
import RadioGroup,{Radio} from "react-native-radio-input";
// import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {PostRequest} from '../../Utils/ServiceCalls';
import DatePicker from 'react-native-datepicker'
import AsyncStorage from '@react-native-community/async-storage'; 
import {NavigationActions,StackActions } from 'react-navigation';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import firebase from 'react-native-firebase';
import ImagePicker from 'react-native-image-crop-picker';  
const RNFS = require("react-native-fs");

let isLoaderShow = true;

export default class DashboardScreen extends PureComponent {
	
	apiRetryAttempt=0;
 
	constructor(props) {
		super(props); 
		this.currentDate=this.getCurrentDate(); 
		this.currentDateMMDDYYYY = new Date(CommonMethods.chaangeDateFormat(this.currentDate));
		this.state = {
			appDashData: [],

			isLoadervisible: false,
			refreshing:false,
			siteList: [],
			fromDate:this.currentDate,
			toDate:this.currentDate,
			isSitesAvailable:false ,
			isAppDataAvailable: false,
			listRefresh:false,

			requestConfirmationModal: false,
			isModalVisible: false,
			logoutTicketNumber: "",
			selectFromModal: false,
			selectFromModal11: false,
			selectFromModal12: false,
			picturePath: "",
			cameraIconColor: "#ed462d",
			galleryIconColor: "#ed462d",
			sitePrecheckID: "",
			index: 0,
			appState: AppState.currentState,
			cressIdds: "",
			crewType: "",
			isLoading:false,
			threads: []
		} 	 
	}

	handleOnNavigateBack = () => {
			// this.getGCSiteList(this.state.fromDate,this.state.toDate); 
		}

	handleOnNavigateBack1 = async (refreshPage) => {
		this.setRequestConfirmationVisibility(true, 0, 0,0);
		let picturePath = await AsyncStorage.getItem("picturePath");
		let ok = await AsyncStorage.getItem("okCamera");
		this.setState({picturePath: picturePath});
		if(picturePath !== null && picturePath !== "" && ok === "ok")
		{
		  this.setState({cameraIconColor: "#006600"});
		  this.setState({galleryIconColor: "#ed462d"});
		}
	  }
	  getUsersLocalDetails=async()=>{
		console.log("Main 1")
		const userDetails = await AsyncStorage.getItem(Constant.LOCAL_STORAGE.LOGINRESPONSE);
	
		try
		{
		  console.log("user_detillllll",JSON.parse(userDetails))
		  let userDetailsObject=JSON.parse(userDetails);
		   
		  let contactName=userDetailsObject.contact_name;
		  let contactEmail=userDetailsObject.crew_lead_email_id;
		  let contactNumber=userDetailsObject.contact_number;
		  let companyName = userDetailsObject.m_company_id;
		  let idss= userDetailsObject.id
		  let usertype= userDetailsObject.userType
		  this.state.emailIdUser = contactEmail
		  console.log(this.state.emailIdUser, contactEmail)
		//   this.cressIdds
		  this.setState({GCCompanyName:companyName,cressIdds: idss});
		  console.log(this.state.cressIdds,"before data post")
		  this.setState({contactid:idss,usertype:usertype,contactName:contactName,contactEmail:contactEmail,contactNumber:contactNumber},()=>{
			  
		  })
		}
		catch(err)
		{
		   
		}
	  }

    componentWillMount() 
	{   
		// alert("step 1") 
		this.getUsersLocalDetails( ).then(()=>{
			// alert("step 2") 
		   this.searchGCSites();
		//    alert("step 3") 
		//this.getAppDashboard(this.state.contactEmail);
	});
		 
	this.createNotificationListeners(); //add this line
		
	}

	getAppDashboard= async(email,start,end)=>{
		// alert('22')
		console.log( "contactEmail",email ) 
		console.log( "UTC asfdate 1",start) 
		console.log( "UTCasdf date 1",end ) 
		let paramObj = {   
			"crewEmail": email,
			  "startDate":start,
			  "endDate":end ,
			  "crewId":this.state.contactid, 
"userType":this.state.usertype
			   
				   }
				   console.log( "paramobjjjjjjjj",paramObj)      
				//    alert("start : "+paramObj.startDate)
			
		let paramObject = {
				path: Constant.Api.DASHBAORD.AppDashboard,
				showErrorMsg: true,
				params: paramObj 
			};
			console.log("API= ",Constant.Api.DASHBAORD.AppDashboard);
			// this.clearData();
			// alert('23')
			PostRequest(paramObject).then((responseApisuccess) => 
			{           
				 
				console.log("dashboard 55",responseApisuccess)
				// alert(JSON.stringify(responseApisuccess));
                this.setState({isLoadervisible:false},()=>{ 
					let response=responseApisuccess.data; 
					console.log(response,"mexico")
					if(response==null || response=="")
					{ 
						let alertTitle= I18n.t('DASHBOARDPAGE.DASHBOARD_SITE_ERROR_TITLE');
						let alertMessage= I18n.t('DASHBOARDPAGE.DASHBOARD_SITE_ERROR_MESSAGE');
						let buttonStr= I18n.t('OK_BUTTON');
						Dialog.alertDialog(alertTitle, alertMessage, buttonStr, true, null); 
                        return;
                    }

					if(response!=null && response!=""){
						
						if(response.data && response.data.Table){
							// alert('24')
							let dashData = response.data.Table
							// this.state.appDashData = dashData    
							
							this.setState({appDashData:[]},()=>{
							
									console.log("mexico ")    
									this.setState({isAppDataAvailable:true,appDashData:dashData})
							
								    
							})
							
							// this.state.isAppDataAvailable = true;
							console.log(this.state.appDashData)
  
							
						} else{
							// alert('25')
							this.state.appDashData=[]
							this.setState({isAppDataAvailable:false})
 
						}

					}
				})
			}	)	
			
		 
	}

	componentWillUnmount()
	{
		// this.notificationListener();
		// this.notificationOpenedListener();
		// this.notificationDisplayedListener();
		// this.messageListener();
		// alert("step 4")
		this.searchGCSites()
		// alert("step 5")
	}

	componentDidMount() {
		// this.getGCSiteList(this.state.fromDate,this.state.toDate);
		// this.props.navigation.addListener('willFocus', this.getGCSiteList()); 
		// alert("step 6")
		const {navigation} = this.props;
		// alert("step 7")
		navigation.addListener ('willFocus', () =>{
			// alert("step 8")
			this.getUsersLocalDetails( ).then(()=>{
				// alert("step 9")
				this.searchGCSites();  
				// alert("step 10")
				 //this.getAppDashboard(this.state.contactEmail);
			 }); 
			// this.getGCSiteList(this.state.fromDate,this.state.toDate);
			AppState.addEventListener('change', this._handleAppStateChange);
	});
	this.getUsersLocalDetails( ).then(()=>{
		// alert("step 11")
		this.searchGCSites();
		// alert("step 12")
		 //this.getAppDashboard(this.state.contactEmail);
	 }); 
	 
	 
	 const subscriber = firebase.firestore()
    .collection("THREADS")
    .orderBy("latestMessage.createdAt", "desc")
    .onSnapshot(querySnapshot => {
        const threads = querySnapshot.docs.map(documentSnapshot => {
          console.log("docsnapshot id", documentSnapshot.id);
            return{
              _id: documentSnapshot.id,
              name: '',
              latestMessage: {
                text:''
              },
              ...documentSnapshot.data()
            };
        });
        this.setState({threads})
    });
    return () => subscriber();

	}

	_handleAppStateChange = (nextAppState) => {
		if ( this.state.appState.match(/inactive|background/) && nextAppState === 'active')
		{
			console.log("AppState");
			this.getUsersLocalDetails( ).then(()=>{
				this.searchGCSites();
				 //this.getAppDashboard(this.state.contactEmail);
			 });  
			// this.getGCSiteList(this.state.fromDate,this.state.toDate);
		}
		this.setState({appState: nextAppState});
	  };



	gotoGCLogin=()=>{
		this.props.navigation.navigate("GC_SITE_LOGIN_Screen", {onNavigateBack: this.handleOnNavigateBack}); 
		//this.props.navigation.navigate("GC_SITE_LOGIN_Screen", {onNavigateBack: this.handleOnNavigateBack}); 
	}

	resetPassword=()=>{
		this.props.navigation.navigate("ResetPasswordScreen"); 
	}


	setSelectFromModalVisibility(visibility)
	{
	  this.setState({selectFromModal11: visibility});
	  this.setState({selectFromModal12: visibility});
	}

	openCamera= async()=>{
		this.setRequestConfirmationVisibility(false, 0, 0,0);
		this.props.navigation.navigate('CaptureNFSDSnapShot',{onNavigateBack: this.handleOnNavigateBack1});
	  }
	
	  async openGallery()
	  {
		try
			{
			  	DocumentPicker.show( {filetype: [DocumentPickerUtil.images()]},(error,res) => 
				{
					if(res != null)
					{
						this.setState({picturePath:res.uri});
						this.setState({galleryIconColor: "#006600"});
						this.setState({cameraIconColor: "#ed462d"});
					}
				});
		}
		catch(error1)
		{
		  Toast.show(error1, Toast.LONG);
		}
		
	  }


	getCurrentDate=()=>{
		var now = new Date(); 
		let currentDateTime = new Date()
		let month = currentDateTime.getMonth() + 1
		if (month < 10) {
			month = '0' + month; 
		}
		
		let day = currentDateTime.getDate()
		if (day < 10) {
			day = '0' + day; 
		}
		
		let year = currentDateTime.getFullYear()
		let fullDate= month + "-" + day + "-" + year;
		return fullDate;
	}

	loginDashApi=()=>{
		console.log(this.state.idss,this.state.cressIdds,this.state.crewType,"before post data")
		let paramObj = 
        {
			"Id":this.state.idss,
			"crewId":this.state.cressIdds,
			"crewType":this.state.crewType
           
		   } 
		   
		   console.log(paramObj,"before post data")
        
        

           
        let paramObject = {
            path: Constant.Api.SiteLoginReq,
            params: paramObj,
            showErrorMsg: true 
        };

           
		 //display loader first
		 this.setSelectFromModalVisibility(false)
         this.setState({isLoadervisible:true},()=>{
            PostRequest(paramObject).then((responseApisuccess) => {
                console.log("Approve" ,responseApisuccess)
                this.setState({isLoadervisible:false},()=>{
                    let response=responseApisuccess.data; 
                    // check if any error during registration
                    if(!response || !response.status ){
                        this.disAlertDialogFailure();    
                    }
                    // user is already registered case
                    if(response.status && response.status==1 &&response.message){
                        let title=I18n.t('SIGNUPPAGE.REGISTRATION_ERROR_TITLE');
                        let message=response.message;
						this.displayAlertMsg("Request",response.message,null);
						this.searchGCSites()
                    }
                        
                    //successfully registered
                    if(response.status && response.status==1 && response.data){
                        try{
							console.log(response.data,"before post data")
						   let   message=  response.message
						   
                        }catch(error){
                            this.disAlertDialogFailure();   
                        }  
                    }  
                });
            }, (error) => {
                this.setState({isLoadervisible:false},()=>{});
            }); 
        });
	}





	logoutDashApi=()=>{
		console.log(this.state.idss,this.state.cressIdds,this.state.crewType,"before post data")
		let paramObj = 
        {
			"Id":this.state.idss,
			"crewId":this.state.cressIdds,
			"crewType":this.state.crewType
           
		   } 
		   
		   console.log(paramObj,"before post data")
        
        

           
        let paramObject = {
            path: Constant.Api.SiteLogoutReq,
            params: paramObj,
            showErrorMsg: true 
        };

           
		 //display loader first
		 this.setSelectFromModalVisibility(false)
         this.setState({isLoadervisible:true},()=>{
            PostRequest(paramObject).then((responseApisuccess) => {
                console.log("Approve" ,responseApisuccess)
                this.setState({isLoadervisible:false},()=>{
                    let response=responseApisuccess.data; 
                    // check if any error during registration
                    if(!response || !response.status ){
                        this.disAlertDialogFailure();    
                    }
                    // user is already registered case
                    if(response.status && response.status==1 &&response.message){
                        let title=I18n.t('SIGNUPPAGE.REGISTRATION_ERROR_TITLE');
                        let message=response.message;
						this.displayAlertMsg("Request",response.message,null);
						this.searchGCSites()
                    }
                        
                    //successfully registered
                    if(response.status && response.status==1 && response.data){
                        try{
							console.log(response.data,"before post data")
						   let   message=  response.message
						   
                        }catch(error){
                            this.disAlertDialogFailure();   
                        }  
                    }  
                });
            }, (error) => {
                this.setState({isLoadervisible:false},()=>{});
            }); 
        });
	}




	getGCSiteList= async(fromDate,toDate)=>{
		let splittedFromDateArray=fromDate.split("-");
		let splittedToDateArray=toDate.split("-");
		
		let fcm_id = await AsyncStorage.getItem("fcmToken");
		let newFromDate=splittedFromDateArray[0]+"/"+splittedFromDateArray[1]+"/"+splittedFromDateArray[2];
		let newToDate=splittedToDateArray[0]+"/"+splittedToDateArray[1]+"/"+splittedToDateArray[2];
		// let paramObj={"tbl_gc_id":GlobalVariable.USERID,"from_date":newFromDate,"to_date":newToDate} ;
		let paramObj={"tbl_gc_id":GlobalVariable.USERID,"from_date":newFromDate,"to_date":newToDate, "fcm_id": fcm_id} ;
	
        let paramObject = {
            path: Constant.Api.DASHBAORD.GCSITELIST,
			showErrorMsg: true,
			params: paramObj 
		};

         //display loader first
         this.setState({isLoadervisible:isLoaderShow},()=>{
			isLoaderShow = false;
            // PostRequest(paramObject).then((responseApisuccess) => {
            //     this.setState({isLoadervisible:isLoaderShow,isSitesAvailable:false},()=>{ 
			// 		let response=responseApisuccess.data; 

			fetch("https://mpulsenet.com/mPulseAPI/"+Constant.Api.DASHBAORD.GCSITELIST, {
				method: 'POST',
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(paramObj),
				})
				.then((response1) => response1.json())
    			.then((response) => {
					this.setState({isLoadervisible:isLoaderShow,isSitesAvailable:false});
                    // unable to fetch gc site
                    if(!response || !response.status || response.status==0 || !response.data){ 
                        //if due to ary reason failed to fetch gc sites retry again in case of error
                        if(this.apiRetryAttempt<3){
                            this.apiRetryAttempt++;
                            this.getGCSiteList(this.state.fromDate,this.state.toDate);
                            return;
                        }
                        return;
                    }

                    if(response.data.length==0){ 
						let alertTitle= I18n.t('DASHBOARDPAGE.DASHBOARD_SITE_ERROR_TITLE');
						let alertMessage= I18n.t('DASHBOARDPAGE.DASHBOARD_SITE_ERROR_MESSAGE');
						let buttonStr= I18n.t('OK_BUTTON');
						// Dialog.alertDialog(alertTitle, alertMessage, buttonStr, true, null); 
                        return;
                    }

                    //got sites
                    if(response.data && response.data.length>0){
					   this.apiRetryAttempt=0; 
					   let tempGCSitesArray = response.data;  
                       this.setState({isSitesAvailable:true,siteList:tempGCSitesArray},()=>{});
                    } 
                }); 
            }, (error) => { 
				console.log(error);
				// alert("Error - "+JSON.stringify(error));
				if(this.state.siteList.length==0 || this.state.siteList==null || this.state.siteList=='')
				{
					this.setState({isSitesAvailable:false,isLoadervisible:false},()=>{
					
					});
				}
				else
				{
					this.setState({isSitesAvailable:true,isLoadervisible:false},()=>{
					
					});
				}
            }); 
        // });
    }



	/**	   
	* @desc display empty cards animation
	* @param :none
	* @returns array of empty cards
	*/
	placeholderList=()=>{ 
		let array=[];
		for(var i=0;i<3;i++){ 
			array.push(  
				<View style={StyleDashboard.card} key={i}>
					<Placeholder animation="fade">
						<View style={[StyleDashboard.pl_gridContainer,StyleDashboard.additionMargin]}> 
							<View style={StyleDashboard.pl_gridColumn}>
								<Line style={StyleDashboard.lineDateTime} />
							</View>
							<View style={StyleDashboard.pl_gridColumn}>
								<Line style={StyleDashboard.lineDateTime} />
							</View> 
						</View> 
						<View style={StyleDashboard.pl_gridColumnSingle}>
							<Line style={[StyleDashboard.lineDateTime]} />
						</View>
						<View style={StyleDashboard.pl_gridColumnSingle}>
							<Line style={[StyleDashboard.lineDateTime]} />
						</View>
						<View style={[StyleDashboard.pl_gridContainer,StyleDashboard.additionMargin]}> 
							<View style={StyleDashboard.pl_gridColumn}>
								<Line style={StyleDashboard.lineDateTimeButton} />
							</View>
							<View style={StyleDashboard.pl_gridColumn}>
								<Line style={StyleDashboard.lineDateTimeButton} />
							</View> 
						</View>
					</Placeholder> 
				</View> 
			);
		}
		return (array);
	}


	appDataList=()=>{ 
		// setTimeout(()=>{
			let array=[];
			let preCheckColor=Colors.red;
		if(item.ob_gc_login.is_prechecked){
			preCheckColor=Colors.greenColor;	
		}
		// console.log(this.state.appDashData.length)
		for(var i=0;i<this.state.appDashData.length;i++){ 
			array.push(  
				<View style={StyleDashboard.card}> 
				<View style={[StyleDashboard.pl_gridContainer,StyleDashboard.additionMargin]}> 
					<View style={StyleDashboard.pl_gridColumnDate}>  
						<Text  style={StyleDashboard.dateOther}></Text>
						<Text style={StyleDashboard.dateOther}></Text> 
					</View>
					<View style={StyleDashboard.pl_gridColumn}>
						{/* {this.state.isAppDataAvai  */}
							<Text  style={StyleDashboard.cardTitle}>{this.state.appDashData[i].site_id}</Text>
						{/* } */}
						{/* <Text style={StyleDashboard.time}>
							<Icon  name={Constant.ICONS.DATE} size={17} color={Colors.orange}  />
							{"  "+CommonMethods.chaangeDateFormat(this.state.appDashData[i].Scheduled_Date)+ "     "}
							<Icon  name={Constant.ICONS.TIME} size={17} color={Colors.orange}  /> 
							{"  "+time} */}
							{/* {"  "+CommonMethods.changeTimetoAmPMFormat(time)} */}
						{/* </Text>  */}
						{/* {(item.ob_planned && item.ob_planned.name)&& 
						<Text  style={[StyleDashboard.time]}>
							{item.ob_planned.name}
						</Text>
						}   */}

					  <Text>
					     Estimated Time on Site(hrs) - {this.state.appDashData[i].work_por}
					  </Text>

					</View> 
				</View>
				<View style={[StyleDashboard.pl_gridContainer,StyleDashboard.pl_BorderTop]}>
					<View style={StyleDashboard.pl_gridColumn}>
						{/* <TouchableOpacity style={StyleDashboard.buttonContainer}  onPress={() => this.gotoPrecheck(preToolUrl)} >
							<Text style={[StyleDashboard.buttonText,{color:preCheckColor}]}>
							    {I18n.t('DASHBOARDPAGE.PRECHECK')}
							</Text>
						 </TouchableOpacity> */}
					</View>
					{/* <View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
					
					
                       {this.returnPostCheckButton(item,index)}
					
					</View> */}
					
				</View> 
			</View> 
			);
		}
		return (array);

		// },1000)
		
	}
		 
	/**	   
	* @desc render data of list item and fit them in card design
	* @param :item: object of each item to be displayed in list
	* @param :index: index of list
	* @returns designed card box
	*/
	renderFlatListItem=(item,index)=>{ 

		let gcLoginDate="";
		let onlyYear="";
		let onlyDate="";
		let onlyMonth="";
		let completeDate="";
		let isSiteAvaliAble=false;
		let imageUrl="";

		let preToolUrl = "";
		let postToolUrl = "";

		if(item.ob_site){
			isSiteAvaliAble=true;  
		}
			
		if(item.ob_precheck && item.ob_precheck.precheck_image){
			imageUrl=item.ob_precheck.precheck_image;
		}

		preToolUrl = item.pre_tool_url ;
		postToolUrl = item.post_tool_url ;

		// Dialog.alertDialog("Pre Url - ", JSON.stringify(item.pre_tool_url), "buttonStr", true, null);
		// Dialog.alertDialog("Post Url - ", JSON.stringify(item), "buttonStr", true, null);



		if(item.ob_gc_login && item.ob_gc_login.login_date){
			gcLoginDate=item.ob_gc_login.login_date;
			if(gcLoginDate.includes("T")){
				let splittedArray=item.LoginDate.split(" ");
				completeDate=splittedArray[0];
				time=splittedArray[1];
				// let reg = new RegExp("PST")
				// time = temp_time.replace(reg,":00");
				let splitDate=completeDate.split("-")
				onlyDate=splitDate[2];
				onlyYear=splitDate[0];
				onlyMonth=splitDate[1];
			}
		}
		 
		let preCheckColor=Colors.red;
		if(item.ob_gc_login.is_prechecked){
			preCheckColor=Colors.greenColor;	
		}
		

		let indexItem=index+""+new Date().toLocaleString()+"_itemflatlist"
		return( 
			<View style={StyleDashboard.card} key={indexItem}> 
				<View style={[StyleDashboard.pl_gridContainer,StyleDashboard.additionMargin]}> 
					<View style={StyleDashboard.pl_gridColumnDate}>  
						<Text  style={StyleDashboard.dateOther}></Text>
						<Text style={StyleDashboard.dateOther}></Text> 
					</View>
					<View style={StyleDashboard.pl_gridColumn}>
						{(isSiteAvaliAble && item.ob_site.site_id)&& 
							<Text  style={StyleDashboard.cardTitle}>{item.ob_site.site_id}</Text>
						}
						<Text style={StyleDashboard.time}>
							<Icon  name={Constant.ICONS.DATE} size={17} color={Colors.orange}  />
							{"  "+CommonMethods.chaangeDateFormat(gcLoginDate)+ "     "}
							<Icon  name={Constant.ICONS.TIME} size={17} color={Colors.orange}  /> 
							{"  "+time}
							{/* {"  "+CommonMethods.changeTimetoAmPMFormat(time)} */}
						</Text> 
						{(item.ob_planned && item.ob_planned.name)&& 
						<Text  style={[StyleDashboard.time]}>
							{item.ob_planned.name}
						</Text>
						}  

					  <Text  style={StyleDashboard.time}>
					     Estimated Time on Site(hrs) - {item.ob_gc_login.estimated_time_on_site}
					  </Text>

					</View> 
				</View>
				<View style={[StyleDashboard.pl_gridContainer,StyleDashboard.pl_BorderTop]}>
					{/* <View style={StyleDashboard.pl_gridColumn}>
						<TouchableOpacity style={StyleDashboard.buttonContainer}  onPress={() => this.gotoPrecheck(item)} >
							<Text style={[StyleDashboard.buttonText,{color:preCheckColor}]}>
							    {I18n.t('DASHBOARDPAGE.PRECHECK')}
							</Text>
						 </TouchableOpacity>
					</View> */}
					<View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
					
						{/* <View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
							<TouchableOpacity style={StyleDashboard.buttonContainer} onPress={() => this.gotoPostcheck(postToolUrl)}>
								<Text style={[StyleDashboard.buttonText]}>
								    {I18n.t('DASHBOARDPAGE.POSTCHECK')}
								</Text>
							</TouchableOpacity>
						</View> */}
                       {this.returnPostCheckButton(item,index)}
					
					</View>
					
				</View> 
			</View> 
		); 
	}
	activeAc=(item)=>{

   
        let paramObj =     
        {
			"Id":item.Id,
			"crewId":this.state.cressIdds,            
			"crewType":this.state.crewType
           } 
        
        
      console.log("PreChekReq",paramObj)
           
        let paramObject = {
            path: Constant.Api.PostCheckReq,
            params: paramObj,
            showErrorMsg: true 
        };

           
         //display loader first
         this.setState({isLoadervisible:true},()=>{
            PostRequest(paramObject).then((responseApisuccess) => {
                console.log("Approve" ,responseApisuccess)
                this.setState({isLoadervisible:false},()=>{
                    let response=responseApisuccess.data; 
                    // check if any error during registration
                    if(!response || !response.status ){
                        this.disAlertDialogFailure();    
                    }
                    // user is already registered case
                    if(response.status && response.status==1 &&response.message){
                        let title=I18n.t('SIGNUPPAGE.REGISTRATION_ERROR_TITLE');
                        let message=response.message;
						this.displayAlertMsg("Request",response.message,null);
						this.searchGCSites()
                    }
                        
                    //successfully registered
                    if(response.status && response.status==1 && response.data){
                        try{
						   let   message=  response.message
						   this.searchGCSites()  
                            this.displayAlertMsg("Request",message,()=>{
                               // this.props.navigation.navigate("LoginScreen");  
                            });
                        }catch(error){
                            this.disAlertDialogFailure();   
                        }  
                    }  
                });
            }, (error) => {
                this.setState({isLoadervisible:false},()=>{});
            }); 
        });
	}

      

	jewelStyle(item){
		console.log(item,"jewelStyle")
		// if(item.)
	}
	 parseSow=(sow)=>{
		let psow=""
		if(sow){
		 let ar=  sow.split(',')
		  if(Array.isArray(ar)){
			
			ar.map((itt,j)=>{
			   if(itt.includes('1')){
				  if(psow.length>1)
				  psow=psow+","+"CX"
				  else
				  psow=psow+" "+"CX"
			   }
			   if(itt.includes('2')){
				if(psow.length>1)
				psow=psow+","+"IX"
				else
				psow=psow+" "+"IX"
			 }
			 if(itt.includes('3')){
			  if(psow.length>1)
			  psow=psow+","+"Troubleshooting"
			  else
			  psow=psow+" "+ "Troubleshooting"
		   }
		   if(itt.includes('4')){
			if(psow.length>1)
			psow=psow+","+"E911-Call Test"
			else
			psow=psow+" "+ "E911-Call Test" 
		 }
			})
	  
		  }
			
		}
		 return psow;
	 }

	gotoDashLogin=(id,crewType)=>{
		this.state.idss=id;
		this.state.crewType= crewType;
console.log(id,crewType,"before post data")

		this.setState({selectFromModal11: true})
		console.log(this.state.idss,this.state.crewType,"before post data ")

	}
	gotoEOS_Form=(itm)=>{
		console.log("timelineeee","go to TimeLine")
		let  params={item:itm}
		this.props.navigation.navigate("EOS_Form",params); 
	}
	gotoTimeline=(itm)=>{
		console.log("timelineeee","go to TimeLine")
		let  params={item:itm,crewId:this.state.contactid}
		this.props.navigation.navigate("TimelineScreen",params); 
	}
	gotoUpload=(itm)=>{
		console.log("timelineeee","go to TimeLine")
		let  params={item:itm,crewId:this.state.contactid}
		 this.props.navigation.navigate("UploadScreen",params); 

		

	}
	gotoCopPre=(itm)=>{
		console.log("timelineeee","go to TimeLine")
		let  params={item:itm,crewId:this.state.contactid}
		 this.props.navigation.navigate("CopPreScreen",params); 

		

	}

	  gotoCopPost  =(itm)=>{
		// console.log("timelineeee","go to TimeLine")
		let  params={item:itm,crewId:this.state.contactid }
		this.props.navigation.navigate("CopPostScreen",params); 
	}

	gotoChat = (itm) => {
		console.log("siteId ", itm);//Id
		//let params = {siteId: "IE04438A", Id: "7310"}
		let params = {siteId: itm.site_id, Id: itm.Id}
		this.props.navigation.navigate('IntegratedChatWindow', params)

		// if(itm.site_id == "LAVILA-EXAMPLE"){
		// 	this.props.navigation.navigate('ChatWindow', {paramKey: "LAVILA-EXAMPLE"})
		// }else if(itm.site_id == "TEST-OFFICE"){
		// 	this.props.navigation.navigate('ChatWindow', {paramKey: "TEST-OFFICE"})
		// }
	}

	gotoDashLogout=(id,crewType)=>{
		this.state.idss=id;
		this.state.crewType= crewType;
console.log(id,crewType,"before post data")

		this.setState({selectFromModal12: true})
		console.log(this.state.idss,this.state.crewType,"before post data ")

	}


	renderAppDataListItem=(item,index)=>{ 
console.log("iiiiiii",item)

		let gcLoginDate="";
		let onlyYear="";
		let onlyDate="";
		let onlyMonth="";
		let completeDate="";
		let isSiteAvaliAble=false;
		let imageUrl="";

		let preToolUrl = "";
		let postToolUrl = "";

		if(item.site_id){
			isSiteAvaliAble=true;  
		}
			
		// if(item.ob_precheck && item.ob_precheck.precheck_image){
		// 	imageUrl=item.ob_precheck.precheck_image;
		// }

		// preToolUrl = item.pre_tool_url ;
		// postToolUrl = item.post_tool_url ;

		// Dialog.alertDialog("Pre Url - ", JSON.stringify(item.pre_tool_url), "buttonStr", true, null);
		// Dialog.alertDialog("Post Url - ", JSON.stringify(item), "buttonStr", true, null);



		// if(item.Scheduled_Date){
		// 	gcLoginDate=item.Scheduled_Date;
		// 	if(gcLoginDate.includes("T")){
		// 		let splittedArray=item.LoginDate.split(" ");
		// 		completeDate=splittedArray[0];
		// 		time=splittedArray[1];
		// 		// let reg = new RegExp("PST")
		// 		// time = temp_time.replace(reg,":00");
		// 		let splitDate=completeDate.split("-")
		// 		onlyDate=splitDate[2];
		// 		onlyYear=splitDate[0];
		// 		onlyMonth=splitDate[1];
		// 	}    
		// }
		 
		let preCheckColor=Colors.red;

		if(item.Scheduled_Date.is_prechecked){
			preCheckColor=Colors.greenColor;	     
		}
		


		let indexItem=index+""+new Date().toLocaleString()+"_itemflatlist"
	//    let loginRespnse= AsyncStorage.getItem(Constant.LOCAL_STORAGE.LOGINRESPONSE,{})
	//     console.log("response..........",loginRespnse )

		return( 
			<View style={StyleDashboard.card} key={indexItem}> 
				<View style={[StyleDashboard.pl_gridContainer,StyleDashboard.additionMargin]}> 
					<View style={StyleDashboard.pl_gridColumnDate}>  
						<Text  style={StyleDashboard.dateOther}></Text>
						<Text style={StyleDashboard.dateOther}></Text> 
					</View>
					<View style={StyleDashboard.pl_gridColumn}>
						{(isSiteAvaliAble && item.site_id)&& 
							<Text  style={StyleDashboard.cardTitle}>{item.site_id}</Text>
						}
						{/* <Text style={StyleDashboard.time}>
							<Icon  name={Constant.ICONS.DATE} size={17} color={Colors.orange}  />
							{"  "+CommonMethods.chaangeDateFormat(gcLoginDate)+ "     "}
							<Icon  name={Constant.ICONS.TIME} size={17} color={Colors.orange}  /> 
							{"  "+time} */}
							{/* {"  "+CommonMethods.changeTimetoAmPMFormat(time)} */}
						{/* </Text> 
						{(item.ob_planned && item.ob_planned.name)&& 
						<Text  style={[StyleDashboard.time]}>
							{item.ob_planned.name}
						</Text>
						}   */}
						<Text  style={StyleDashboard.time}>
							Scheduled Date - {item.Scheduled_Date}
						</Text>
						<Text  style={StyleDashboard.time}>
							Start Time - {item.RF_Approved_MW_Time}   
						</Text>
						<Text  style={StyleDashboard.time}>    
			  				{/* Work Order Number - {item.work_por} */}
							  Project Code - {item.work_por}
						</Text>
						<Text  style={StyleDashboard.time}>
							SOW - {this.parseSow(item.sow)}
						</Text>
						{(this.state.usertype==1)  && (item.status!=2) && (item.status!=3) && 	<TouchableOpacity onPress={()=>this.gotoUpload(item)} style={[StyleDashboard.upload_button]} >
						<Text style={[StyleDashboard.upload_text]}>
							Upload Images     
						</Text>
						
					</TouchableOpacity>   }  
						{item.status == 2 &&
							<Text style={StyleDashboard.reasonText}>Site Status - Site is Pending at TMO</Text>
							
						}
						{item.status == 3 &&
							<Text style={StyleDashboard.reasonText}>Site Status - Site is cancelled</Text>
							
						}
						{/* {(item.status == 3 || item.status == 2) && item.cancelReason &&
						// <Text>Cancel Reason</Text>
						<Text style={StyleDashboard.reasonText}>Cancel Reason</Text>

						} */}
						{(item.status == 3 || item.status == 2) && item.cancelReason &&
						// <Text>Cancel Reason</Text>
						<Text style={StyleDashboard.reasonText}>Cancel Reason - {item.cancelReason}</Text>

						}
					</View> 
				</View>
				
						 {item.status != 3 && item.status != 2 &&
							<View style={[StyleDashboard.pl_gridContainer,StyleDashboard.pl_BorderTop]}>
								<View style={StyleDashboard.pl_gridColumn}>
									{/* <Text>Login</Text> */}
									{item.loginType == 0 &&
									<TouchableOpacity style={StyleDashboard.buttonContainer}  onPress={() => this.gotoDashLogin(item.Id,item.crewType)} >
										<Text style={[StyleDashboard.buttonText,{color:preCheckColor}]}>
											Login
										</Text>
									</TouchableOpacity> 
									}
									{item.loginType != 0 &&
									<TouchableOpacity style={StyleDashboard.buttonContainer} disabled={true}  >
										<Text style={[StyleDashboard.buttonTexts]}>
											Login
										</Text>
									</TouchableOpacity> 
									} 
									
									{/* {this.returnPreCheckButton(item,index)} */}
									
								</View>
								<View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
									{this.returnPostCheckButton(item,index)}

								</View>
								
							</View> 
				
								}		
				{item.status != 3 && item.status != 2 &&
				<View style={[StyleDashboard.pl_gridContainer,StyleDashboard.pl_BorderTop]}>
					<View style={StyleDashboard.pl_gridColumn}>
						{/* <TouchableOpacity style={StyleDashboard.buttonContainer}  onPress={() => this.gotoPrecheck(preToolUrl)} >
							<Text style={[StyleDashboard.buttonText,{color:preCheckColor}]}>
							    {I18n.t('DASHBOARDPAGE.PRECHECK')}
							</Text>
						 </TouchableOpacity> */}
						 
						 {this.returnPreCheckButton(item,index)}
						 
					</View>
					<View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
					
						{/* <View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
							<TouchableOpacity style={StyleDashboard.buttonContainer} onPress={() => this.gotoPostcheck(postToolUrl)}>
								<Text style={[StyleDashboard.buttonText]}>
								    {I18n.t('DASHBOARDPAGE.POSTCHECK')}
								</Text>
							</TouchableOpacity>
						</View> */}
                       {/* <Text>out : {item.logoutType}, in :  {item.loginType}, pre : {item.preStatus}, post : {item.postStatus}</Text> */}
					   {item.logoutType == 0 && (item.loginType == 1 && item.preStatus == 1 && item.postStatus == 1) &&
				<TouchableOpacity style={StyleDashboard.buttonContainer}  onPress={() => this.gotoDashLogout(item.Id,item.crewType)} >
						<Text style={[StyleDashboard.buttonText]}>
							Logout
						</Text>
					 </TouchableOpacity> 
				}
					{
						console.log((item.logoutType == 0 && !(item.loginType == 1 && item.preStatus == 1 && item.postStatus == 1)),"condition check")
					}
				{(item.logoutType == 0 && !(item.loginType == 1 && item.preStatus == 1 && item.postStatus == 1))&&
				<TouchableOpacity style={StyleDashboard.buttonContainer}   disabled={true} >
						<Text style={[StyleDashboard.buttonText]}>
							Logout
						</Text>
					 </TouchableOpacity> 
				}


						
				{/* {item.logoutType == 0 && item.loginType != 1 && item.preStatus == 1 && item.postStatus != 1 &&
				<TouchableOpacity style={StyleDashboard.buttonContainer}  disabled={true} >
						<Text style={[StyleDashboard.buttonText]}>
							Logout
						</Text>
					 </TouchableOpacity> 
				}
				{item.logoutType == 0 && item.loginType != 1 && item.preStatus != 1 && item.postStatus != 1 &&
				<TouchableOpacity style={StyleDashboard.buttonContainer}  disabled={true} >
						<Text style={[StyleDashboard.buttonText]}>
							Logout
						</Text>
					 </TouchableOpacity> 
				} */}



				{item.logoutType == 1  &&
				<TouchableOpacity style={StyleDashboard.buttonContainer}  >
						<Text style={[StyleDashboard.buttonTexts]}>
							Logout
						</Text>
					 </TouchableOpacity> 
				}
				{/* {(item.logoutType == 1 && !(item.loginType == 1 && item.preStatus == 1 && item.postStatus == 1)) &&
				<TouchableOpacity style={StyleDashboard.buttonContainer}  >
						<Text style={[StyleDashboard.buttonTexts]}>
							Logout4
						</Text>
					 </TouchableOpacity> 
				}	 */}
				{/* {item.logoutType == 1 && item.loginType == 1 && item.preStatus != 1 && item.postStatus != 1 &&
				<TouchableOpacity style={StyleDashboard.buttonContainer}  disabled={true} >
						<Text style={[StyleDashboard.buttonTexts]}>
							Logout
						</Text>
					 </TouchableOpacity> 
				}
				{item.logoutType == 1 && item.loginType != 1 && item.preStatus == 1 && item.postStatus != 1 &&
				<TouchableOpacity style={StyleDashboard.buttonContainer}  disabled={true} >
						<Text style={[StyleDashboard.buttonTexts]}>
							Logout
						</Text>
					 </TouchableOpacity> 
				}
				{item.logoutType == 1 && item.loginType != 1 && item.preStatus != 1 && item.postStatus != 1 &&
				<TouchableOpacity style={StyleDashboard.buttonContainer}  disabled={true} >
						<Text style={[StyleDashboard.buttonTexts]}>
							Logout
						</Text>
					 </TouchableOpacity> 
				} */}
 
				
					</View>
					 

				</View>     
					}
					 
					 {(this.state.usertype==2)  && (item.status!=2) && (item.status!=3) &&
<View style={[StyleDashboard.pl_gridContainer,StyleDashboard.pl_BorderTop]}>
								<View style={StyleDashboard.pl_gridColumn}>
									{/* <Text>Login</Text> */}
									{true &&
									<TouchableOpacity style={StyleDashboard.buttonContainer}  onPress={() => this. gotoCopPre(item)} >
										<Text style={[StyleDashboard.buttonText,{color:preCheckColor}]}>
										EOS Pre   
										{/* COP Pre */}
										</Text>
									</TouchableOpacity> 
									}
									{false &&
									<TouchableOpacity style={StyleDashboard.buttonContainer} disabled={true}  >
										<Text style={[StyleDashboard.buttonTexts]}>
										EOS Pre
										{/* COP Pre */}
										</Text>
									</TouchableOpacity> 
									} 
									
									{/* {this.returnPreCheckButton(item,index)} */}
									
								</View>
								<View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
								{true &&
									<TouchableOpacity style={StyleDashboard.buttonContainer}  onPress={() => this. gotoCopPost(item)} >
										<Text style={[StyleDashboard.buttonText,{color:preCheckColor}]}>
										{/* COP Post */}
										EOS Post
										</Text>
									</TouchableOpacity> 
									}
									{false &&
									<TouchableOpacity style={StyleDashboard.buttonContainer} disabled={true}  >
										<Text style={[StyleDashboard.buttonTexts]}>
										{/* COP Post */}
										EOS Post
										</Text>
									</TouchableOpacity> 
									} 
									


								</View>
								   
								</View>  }

								{/* New Button Added By Vijay */}
								<View style={[StyleDashboard.pl_gridContainer,StyleDashboard.pl_BorderTop]}>
								<View style={StyleDashboard.pl_gridColumn}>
									{/* <Text>Login</Text> */}
									<TouchableOpacity style={StyleDashboard.buttonContainer}  onPress={() => this.gotoEOS_Form(item)} >
										<Text style={[StyleDashboard.buttonText,{color:preCheckColor}]}>
											Fill EOS Table
										</Text>
									</TouchableOpacity> 
									
									{/* {this.returnPreCheckButton(item,index)} */}
									
								</View>   
								<View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
									<TouchableOpacity style={StyleDashboard.buttonContainer}  onPress={() => this. gotoChat(item)} >
										<Text style={[StyleDashboard.buttonText,{color:preCheckColor}]}>
										{/* COP Post */}
										Chat
										</Text>
									</TouchableOpacity> 
								</View> 
								  
								</View>
	 
						{(this.state.usertype==2) && (item.status!=2) && (item.status!=3) &&	
						<View style={[StyleDashboard.pl_gridContainer,StyleDashboard.pl_BorderTop]}>
								<View style={StyleDashboard.pl_gridColumn}>
									{/* <Text>Login</Text> */}
									{true &&
									<TouchableOpacity style={StyleDashboard.buttonContainer}  onPress={() => this.gotoTimeline(item)} >
										<Text style={[StyleDashboard.buttonText,{color:preCheckColor}]}>
											Timeline
										</Text>
									</TouchableOpacity> 
									}
									{false &&
									<TouchableOpacity style={StyleDashboard.buttonContainer} disabled={true}  >
							   			<Text style={[StyleDashboard.buttonTexts]}>
										       Timeline
										</Text>
									</TouchableOpacity> 
									} 
									
									{/* {this.returnPreCheckButton(item,index)} */}
									
								</View> 
												
								  
								</View>   }  


					
				  

				 
				  
			</View> 
		); 
	}
  
    displayAlertMsg=(title,message,callback)=>{
		Dialog.alertDialog(title,message,I18n.t('OK_BUTTON'),true,null).then(()=>{
			if(callback){
				callback();
			}
		})
	} 
	returnPostCheckButton=(item,index)=>
	{	
		console.log("dashboardddddd ",item)	
		// alert(JSON.stringify(item))
		let postCheckRequest  = "";
		postCheckRequest = item.postStatus ;
		// if(item.postStatus!=null && item.preStatus!=null && item.preStatus==1)
		// {     console.log("postcheck ","chala hai")
		//   postCheckRequest = item.postStatus ;
		// }
console.log("postcheck ",postCheckRequest)
		// console.log(item.loginType,item.logoutType ,item.preStatus,"vijay checkss")
		if(item.loginType == 1 && item.preStatus == 1){
			if( item.preStatus==1 && postCheckRequest==0)
			{
				return(
					<View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
						<TouchableOpacity style={StyleDashboard.buttonContainer} 
										onPress={() => 
											this.displayAlertMsg("Confirm","Are you sure to request PostCheck?",()=>this.activeAc(item))}>
										{/* onPress={() => this.requestPostcheck(item.ob_gc_login.id,index)}> */}
							<Text style={[StyleDashboard.buttonText]}>
							{/*    {I18n.t('DASHBOARDPAGE.REQUEST_POSTCHECK')} */}
							 Post-Check  Request 
							</Text>
						</TouchableOpacity>
					</View>
				);     
			}      
			else if( item.preStatus==1 &&  postCheckRequest==1 )
			{
				return(
					<View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
						<TouchableOpacity style={StyleDashboard.buttonContainer} onPress={() => this.gotoPostcheck(item)}>
							<Text style={[StyleDashboard.buttonText]}>
							{/*     {I18n.t('DASHBOARDPAGE.POSTCHECK')} */}
							View Post-Check
							{/* Request Post Check */}
							</Text>
						</TouchableOpacity>
					</View>
				);
			}else 
			{          
				return(
					<View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
						<TouchableOpacity   disabled={true}   style={StyleDashboard.buttonContainer} >
							<Text style={[StyleDashboard.buttonText]}>
							{/*     {I18n.t('DASHBOARDPAGE.POSTCHECK')} */}
							 Post Check  Request 
							</Text>
						</TouchableOpacity>
					</View>
				);
			}
		}else{
			if( item.preStatus==1 && postCheckRequest==0)
		{
			return(
				<View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
					<TouchableOpacity style={StyleDashboard.buttonContainer} 
									>
									{/* onPress={() => this.requestPostcheck(item.ob_gc_login.id,index)}> */}
						<Text style={[StyleDashboard.buttonText]}>
						{/*    {I18n.t('DASHBOARDPAGE.REQUEST_POSTCHECK')} */}
						 Post-Check  Request
						</Text>
					</TouchableOpacity>
				</View>
			);
			}
			else if( item.preStatus==1 &&  postCheckRequest==1 )
			{
				return(
					<View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
						<TouchableOpacity style={StyleDashboard.buttonContainer} >
							<Text style={[StyleDashboard.buttonText]}>
							    {I18n.t('DASHBOARDPAGE.POSTCHECK')}
							
							{/* Request Post Check */}
							</Text>
						</TouchableOpacity>
					</View>
				);
			}else 
			{          
				return(
					<View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
						<TouchableOpacity   disabled={true}   style={StyleDashboard.buttonContainer} >
							<Text style={[StyleDashboard.buttonText]}>
							{/*     {I18n.t('DASHBOARDPAGE.POSTCHECK')} */}
							Post Check  Request
							</Text>
						</TouchableOpacity>
					</View>
				);
			}
		}
		
		
	}


	returnPreCheckButton=(item,index)=>
	{		
		// alert(JSON.stringify(item))
		console.log("pre ",item)
		let postCheckRequest  = "";
		if(item.preStatus!=null)
		{
		  postCheckRequest = item.preStatus ;
		}
		if(item.loginType == 1){
			if(postCheckRequest==null || postCheckRequest=="" ||postCheckRequest==0)
			{
				return(
					<View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
						<TouchableOpacity style={StyleDashboard.buttonContainer} 
										onPress={() => 
											this.gotoGC_SITE_LOGIN_Screen(item) 
										}>
										{/* onPress={() => this.requestPostcheck(item.ob_gc_login.id,index)}> */}
							<Text style={[StyleDashboard.buttonText]}>
							{/*    {I18n.t('DASHBOARDPAGE.REQUEST_POSTCHECK')} */}
							 Pre-Check Request
							</Text>
						</TouchableOpacity>
					</View>
				);
			}
			else if(postCheckRequest==null || postCheckRequest=="" ||postCheckRequest==1)
			{
				return(
					<View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
						<TouchableOpacity style={StyleDashboard.buttonContainer} onPress={() =>
							   this.gotoPrecheck(item)  
							}>
							<Text style={[StyleDashboard.buttonText]}>
							{/*     {I18n.t('DASHBOARDPAGE.PRECHECK')} */}
							View Pre-Check
							</Text>
						</TouchableOpacity>
					</View>
				);
			}else if(postCheckRequest==null || postCheckRequest=="" ||postCheckRequest==2)
			{
				return(
					<View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
						<TouchableOpacity style={StyleDashboard.buttonContainer}   disabled={true}   >
							<Text style={[StyleDashboard.buttonText]}>
							{/*     {I18n.t('DASHBOARDPAGE.PRECHECK')} */}
							Pending Pre-Check
							</Text>
						</TouchableOpacity>
					</View>
				);
			}
		}else{
			if(postCheckRequest==null || postCheckRequest=="" ||postCheckRequest==0)
		{
			return(
				<View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
					<TouchableOpacity style={StyleDashboard.buttonContainer} 
									>
									{/* onPress={() => this.requestPostcheck(item.ob_gc_login.id,index)}> */}
						<Text style={[StyleDashboard.buttonText]}>
						{/*    {I18n.t('DASHBOARDPAGE.REQUEST_POSTCHECK')} */}
						 Pre-Check Request
						</Text>
					</TouchableOpacity>
				</View>
			);
		}
		else if(postCheckRequest==null || postCheckRequest=="" ||postCheckRequest==1)
		{
			return(
				<View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
					<TouchableOpacity style={StyleDashboard.buttonContainer}>
						<Text style={[StyleDashboard.buttonText]}>
						{/*     {I18n.t('DASHBOARDPAGE.PRECHECK')} */}
						View Pre-Check
						</Text>
					</TouchableOpacity>
				</View>
			);
		}else if(postCheckRequest==null || postCheckRequest=="" ||postCheckRequest==2)
		{
			return(
				<View style={[StyleDashboard.pl_gridColumn,StyleDashboard.pl_BorderLeft]}>
					<TouchableOpacity style={StyleDashboard.buttonContainer}   disabled={true}   >
						<Text style={[StyleDashboard.buttonText]}>
						{/*     {I18n.t('DASHBOARDPAGE.PRECHECK')} */}
						Pending Pre-Check
						</Text>
					</TouchableOpacity>
				</View>
			);
		}
		}
		
		
	}






	renderDateComponent=(dateStateTitle)=>{
		
		return(<DatePicker
			style={StyleDashboard.datePickerContainerStyle}
			date={this.state[dateStateTitle]}
			mode="date"
			showIcon={true}
			placeholder="select date"
			format="MM-DD-YYYY"
			minDate="01-01-2016"
			maxDate={this.currentDate}
			confirmBtnText="Confirm"
			cancelBtnText="Cancel"
			customStyles={{
				dateIcon: StyleDashboard.datepickerIcon,
				dateInput:StyleDashboard.datepickerInput  
			}}
			onDateChange={(selectedDate) => { 
				let dateObj={};
				dateObj[dateStateTitle]=selectedDate; 
				this.setState(dateObj); 
			}}
		/> )
	}

	/**	   
	* @desc calls this function when user drag to refrsh screen
	* @param :none 
	*/
	onDragRefresh=()=>{
		this.searchGCSites() 
		 //this.getGCSiteList(this.state.fromDate,this.state.toDate);
	}

	navigationActions=(position)=>{ 
		if(position==0){
			this.gotoGCLogin();
		}else if(position==1){
			this.resetPassword(); 

			// firebase.auth().onAuthStateChanged((user) => {
			// 	if (user) {
			// 		this.props.navigation.navigate("HomeScreenWindow");// FileManager // HomeScreenWindow
			// 	}else{
			// 		this.props.navigation.navigate("ChatRegForm");
			// 	}
			//  });

			// let params = {siteId: "3ALA010A", Id: "7315"}
			// this.props.navigation.navigate('IntegratedChatWindow', params)
		} else if(position==2){
			this.logout(); 
		} 
	}

	/**	   
	  log out user from application
	 * @param: none
	*/
	logout=()=>{ 
		AsyncStorage.removeItem(Constant.LOCAL_STORAGE.LOGINRESPONSE,() => { 
			const actionToDispatch = StackActions.reset({
				index: 0,  
				actions: [NavigationActions.navigate({ routeName: 'SplashScreen' })],
				key: "AuthProcess"
			});
			this.props.navigation.dispatch(actionToDispatch);  
		}); 
            	}

	gotoPrecheck=(imageUrl)=>{ 
console.log("preCheck 1 ",imageUrl)
		if(imageUrl==null || imageUrl=="")
		{
		 
			
			let alertTitle= "No Image";
			let alertMessage= "Pre Check going on.";
			let buttonStr= I18n.t('OK_BUTTON');
			Dialog.alertDialog(alertTitle, alertMessage, buttonStr, true, null);
			return;
		}
		console.log("preCheck 2 ",imageUrl)
			let params = { preCheckImage:imageUrl.Id,view: imageUrl.preStatus}; 
			this.props.navigation.navigate("PreCheck_Screen",params); 
		
	}
   gotoGC_SITE_LOGIN_Screen=(data)=>{
	     console.log("gccc",data)
	let params = { item:data}; 
	  this.props.navigation.navigate("GC_SITE_Req",params); 
	
	 
   }
	gotoPostcheck=(imageUrl)=>
	{ 
		console.log("Dashboard 55",imageUrl)
		if(imageUrl==null || imageUrl=="")
		{
			let alertTitle= "No Image";
			let alertMessage= "Post Check going on.";
			let buttonStr= I18n.t('OK_BUTTON');
			Dialog.alertDialog(alertTitle, alertMessage, buttonStr, true, null);
			return;
		}
		 
			let params = {siteId:imageUrl}; 

			this.props.navigation.navigate("PostCheckFormScreen",params); 
		
		// if(sitePrecheckID)
		// {
		// 	this.props.navigation.navigate("PostCheckFormScreen",params); 
		// }

		// let siteListArr = this.state.siteList;
		// siteListArr[index].ob_gc_login.postcheck_request = "2" ;
		// this.setState({siteList:siteListArr});
	}


	setRequestConfirmationVisibility = async (visibility, fromList, ob_gc_login_id,index)=>
	{
		console.log(fromList)
		if(fromList === 1)
		{
			this.setState({sitePrecheckID: ob_gc_login_id, index: index});
		}
		else if(fromList === 2)
		{
			this.clearData();
		}
	    this.setState({requestConfirmationModal: visibility});
	}


	// requestPostcheck=(sitePrecheckID,index)=>

	submitRequestPostCheckConfirmation = () =>
	{
		this.setRequestConfirmationVisibility(false, 0, 0, 0);
		Alert.alert(
        	"Submit Confirmation",
			"Are you sure to submit Request Post-Check",
			[
				{
					text: 'Cancel',
					onPress: () => this.clearData(),
					style: 'cancel'
				},
				{
					text: "Ok", 
					onPress: () => this.requestPostcheck()
				},
			],
			{cancelable: false},
			// console.log('Cancel Pressed')
		);
	}

	clearData =async () =>
	{
		this.setState({logoutTicketNumber: ""});
		this.setState({picturePath: ""});
		await AsyncStorage.setItem("picturePath", "");
		this.setState({cameraIconColor: "#ed462d"});
		this.setState({galleryIconColor: "#ed462d"});
	}

	requestPostcheck=()=>
	{		

			// alert(this.state.picturePath);

         //display loader first
         this.setState({isLoadervisible:true},async()=>{


			let snap_data = "";
			if(this.state.picturePath !== "" && this.state.picturePath !== null)
			{
				await RNFS.readFile(this.state.picturePath, "base64").then(async(data) => {
					snap_data = data;
				});
			}
			let index = this.state.index;
			let sitePrecheckID = this.state.sitePrecheckID;
			let paramObj={"tbl_gc_login_id":sitePrecheckID,"nfsd_logout_snap_file":snap_data, "logout_ticket_number":this.state.logoutTicketNumber};
			let paramObject = {
				path: Constant.Api.POSTCHECKREQUEST.POSTCHECKREQUESTAPI,
				showErrorMsg: true,
				params: paramObj 
			};

			this.clearData();

			PostRequest(paramObject).then((responseApisuccess) => 
			{
                this.setState({isLoadervisible:false},()=>{ 
					let response=responseApisuccess.data; 
					if(response==null || response=="")
					{ 
						let alertTitle= I18n.t('DASHBOARDPAGE.DASHBOARD_SITE_ERROR_TITLE');
						let alertMessage= I18n.t('DASHBOARDPAGE.DASHBOARD_SITE_ERROR_MESSAGE');
						let buttonStr= I18n.t('OK_BUTTON');
						Dialog.alertDialog(alertTitle, alertMessage, buttonStr, true, null); 
                        return;
                    }

					if(response!=null && response!="")
					{
						
					   let buttonStr= I18n.t('OK_BUTTON');
					   if(response.status==1)
					   {
						   let siteListArr = this.state.siteList;
						   siteListArr[index].ob_gc_login.postcheck_request = 2 ;

						   this.setState({siteList:siteListArr});
						   this.setState({listRefresh:!this.state.listRefresh});

						   Dialog.alertDialog("Post Check Request", response.message, buttonStr, true, null); 
					   }
					   else
					   {
						  Dialog.alertDialog("Post Check Request", response.message, buttonStr, true, null); 
					   }
                    } 
                }); 
            }, (error) => { 
                this.setState({isLoadervisible:false},()=>{});
            }); 
        });
	}


	searchGCSites=()=>{
		// alert("step 20")
		let fromDate=this.state.fromDate;
		let toDate=this.state.toDate;	 
		
		console.log("zzzz : ",new Date(fromDate))
		
		let  splitFromDate = fromDate.split("-");
		let  splitToDate = toDate.split("-");
		
		// let splittedFromDate =new Date(splitFromDate[2]+"-"+splitFromDate[1]+"-"+splitFromDate[0]);
		let splittedFromDate =new Date(splitFromDate[2]+"-"+splitFromDate[0]+"-"+splitFromDate[1]);
		// console.log("zzzz : ",splitFromDate)
		// let splittedToDate =new Date(splitToDate[2]+"-"+splitToDate[1]+"-"+splitToDate[0]);
		let splittedToDate =new Date(splitToDate[2]+"-"+splitToDate[0]+"-"+splitToDate[1]);
		 
		if(splittedFromDate.getTime()>splittedToDate.getTime()){
			let alertTitle= I18n.t('DASHBOARDPAGE.INVALID_DATE_Title');
			let alertMessage= I18n.t('DASHBOARDPAGE.INVALID_DATE_MSG');
			let buttonStr= I18n.t('OK_BUTTON');
			Dialog.alertDialog(alertTitle, alertMessage, buttonStr, true, null);
			return;
		}
		// console.log("UTC date 2") 
		// setTimeout(()=>{
		// 	alert("UTC date 1 "+(new Date(splitFromDate[2]+"-"+splitFromDate[0]+"-"+splitFromDate[1])),
		// 	alert("UTC date "+(new Date(splitFromDate[2]+"-"+splitFromDate[0]+"-"+splitFromDate[1])).getMonth()))
		// },2000)
		// alert("UTC date "+new Date(this.state.fromDate).getMonth())
		// console.log("UTC date 4")
		let  checkdate =new Date(splitFromDate[2]+"-"+splitFromDate[0]+"-"+splitFromDate[1]).toUTCString()
		// alert("after check "+checkdate)
		// setTimeout(()=>{
			// alert("UTC date 1 "+new Date(checkdate).getDate())
			// alert("UTC date "+(new Date(splitFromDate[2]+"-"+splitFromDate[0]+"-"+splitFromDate[1])).toUTCString())
		// },2000)
		// alert("77 ",checkdate)
		console.log("new 1")
let datez
console.log("new 2")
datez = new Date(checkdate);
	//Local time converted to UTC
	console.log("new 3",datez.getUTCFullYear())
    console.log("new 4 ",datez.getUTCMonth());
	// var localOffset = datez.getTimezoneOffset() * 60000;
	console.log("new 5 ",datez.getUTCDate())
	// var localTime = datez.getTime();
	// console.log("new 6 ")
    // if (toUTC) {
    //     date = localTime + localOffset;
    // } else {
    //     date = localTime - localOffset;
    // }
	// datez = new Date(datez);

	// console.log("new date ",datez)

		// let  splitFromDate1 = checkdate.split("-");
		// let  splitToDate1 = toDate.split("-");
		// alert("check 55 "+new Date(splitFromDate1[2]+"-"+splitFromDate1[0]+"-"+splitFromDate1[1]).getDate() )
		let getDateMon = ((datez.getUTCMonth()+1) > 9) ? (datez.getUTCMonth()+1) : '0'+(datez.getUTCMonth()+1)
		// alert("hello "+new Date(checkdate).getTimezoneOffset())
		let getDateYear = (datez.getUTCDate() > 9 ? datez.getUTCDate() : '0'+datez.getUTCDate())
		console.log("get dateYear : ")
		// alert("1 : "+getDateYear)
		let ffromDate =  getDateMon+'-'+getDateYear+'-'+datez.getUTCFullYear()
		// alert("")





		let  checkdate1 =new Date(splitToDate[2]+"-"+splitToDate[0]+"-"+splitToDate[1]).toUTCString()
	
	

		let datez1
		console.log("new 2")
		datez1 = new Date(checkdate1);
		console.log("zzzz : ",datez1)
			//Local time converted to UTC
			console.log("new 3",datez1.getUTCFullYear())
			console.log("new 4 ",datez1.getUTCMonth());
			// var localOffset = datez.getTimezoneOffset() * 60000;
			console.log("new 5 ",datez1.getUTCDate())
	
		let getDateMon1 = ((datez1.getUTCMonth()+1) > 9) ? (datez1.getUTCMonth()+1) : '0'+(datez1.getUTCMonth()+1)
		let getDateYear1 = (datez1.getUTCDate() > 9 ? datez1.getUTCDate() : '0'+datez1.getUTCDate())
		let eendDate =  getDateMon1+'-'+getDateYear1+'-'+datez1.getUTCFullYear()

	isLoaderShow = true;
	// alert("getDash : "+ffromDate)
	// alert("step 21")
	// alert("4 check"+ffromDate)
	// alert ("5 check"+eendDate)
	 this.getAppDashboard( this.state.contactEmail,ffromDate,eendDate)
		// this.getGCSiteList(this.state.fromDate,this.state.toDate);
	}

	render() { 
	
		const toolbarAction=[
			{title:"add",show:"always",iconName:Constant.ICONS.PLUSICON},
			{title:"changePassword",show:"always",iconName:Constant.ICONS.CHANGEPASSWORD},
			{title:"Logout",show:"always",iconName:Constant.ICONS.LOGOUT}
		];
		
		return (
			<View style={StyleDashboard.container}>
				<StatusBarComponent isHidden={false} />
				{
					this.state.isLoadervisible &&
					<Loader msg={I18n.t('LOADING')} />
				}
				{   DeviceInfo.isAndroid() && 
					<ToolbarComponent 
						title={I18n.t('DASHBOARDPAGE.DASHBOARD')} 
						actionOptions={toolbarAction}
						onActionSelected={this.navigationActions}
					/> 
                }
				<View style={StyleDashboard.scrollContainer}> 
					<View style={StyleDashboard.dateConainerParent}>
						<View style={[StyleDashboard.dateConainerChild,StyleDashboard.dateMarginLeft]}>
							<Text style={[StyleDashboard.dateTitle,StyleDashboard.dateMarginLeft]}>
								{I18n.t('DASHBOARDPAGE.FROM_DATE')}
							</Text>
							{this.renderDateComponent("fromDate")}
						</View>
						<View style={StyleDashboard.dateConainerChild}>
							<Text style={StyleDashboard.dateTitle}>
							  	{I18n.t('DASHBOARDPAGE.TO_DATE')}
							</Text>
							{this.renderDateComponent("toDate")}
						</View>
						<View style={StyleDashboard.searchButtonContainer}>
							<TouchableOpacity style={StyleDashboard.searchButton} onPress={() => this.searchGCSites()}>
								<Text style={StyleDashboard.searchButtonText}>
								    {I18n.t('DASHBOARDPAGE.Search')}
								</Text>
							</TouchableOpacity>
						</View>
					</View>
					<View style={StyleDashboard.flatListContainer}> 
						



					{this.state.isAppDataAvailable &&
						<FlatList 
	                        refreshControl={
	                            <RefreshControl
	                                refreshing={this.state.refreshing}
	                                onRefresh={this.onDragRefresh}
	                                colors={[Colors.orange]}
	                                progressViewOffset={20}
	                                enabled={true}   

	                            />
	                        } 
	//                         ListEmptyComponent={this.appDataList} 
	                        data={this.state.appDashData}
	                        extraData={this.state.listRefresh}
	                       // initialNumToRender={20}
	                        keyExtractor={(item, index) => index+""+new Date().toLocaleString()}
	                        renderItem={({item, index}) => this.renderAppDataListItem(item, index)}
	                    /> 
						}

{
				this.state.isLoading    &&        
					<Loader msg={'Uploading...'} />
				} 

  

						{/* {this.state.isSitesAvailable &&
						<FlatList 
	                        refreshControl={
	                            <RefreshControl
	                                refreshing={this.state.refreshing}
	                                onRefresh={this.onDragRefresh}
	                                colors={[Colors.orange]}
	                                progressViewOffset={20}
	                                enabled={true}
	                            />
	                        } 
	                        ListEmptyComponent={this.placeholderList} 
	                        data={this.state.siteList}
	                        extraData={this.state.listRefresh}
	                        initialNumToRender={20}
	                        keyExtractor={(item, index) => index+""+new Date().toLocaleString()}
	                        renderItem={({item, index}) => this.renderFlatListItem(item, index)}
	                    /> 
						}
						{!this.state.isSitesAvailable &&
							<View style={StyleDashboard.noGCSiteContainer}>		
								<Text style={StyleDashboard.noGCSiteText}>{I18n.t('DASHBOARDPAGE.NO_GC_SITEs')} </Text>
									<TouchableOpacity style={StyleDashboard.noSiteButton} onPress={() => this.gotoGCLogin()}>
										<Text style={StyleDashboard.noSiteBUttonText}>
											{I18n.t('DASHBOARDPAGE.GC_LOGIN')}
										</Text>
									</TouchableOpacity>
							</View>	
						} */}
					</View> 	
				</View>


				{/* <Modal
					animationType="none"
					transparent={true}
					closeOnClick={true}
					visible={this.state.requestConfirmationModal}
					onRequestClose={() => {
					this.setRequestConfirmationVisibility(false, 0, 0,0)
					}}>
					<View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'rgba(0,0,0,0.6)'}}>

						<View style={{width: '90%',backgroundColor: '#FFFFFF', elevation:10, borderTopLeftRadius:8,borderTopEndRadius:8, margin: 5}}>

							<View style={{ flexDirection: 'row',height:55, backgroundColor: Colors.lightOrange,alignItems:'center'}}>
								<Text style={{flex: 1, paddingLeft: 7, fontWeight:'bold', fontSize: 17, fontFamily: 'Roboto', color: '#FFFFFF', textAlign: 'left'}}>
									Submit Request
								</Text>
								<View style={{width:80,height:30,justifyContent: 'center',alignSelf:'center',backgroundColor:'#fff',borderRadius:15, marginRight: 10}}>
									<TouchableOpacity
									onPress={()=>this.setRequestConfirmationVisibility(false, 2, 0,0)}>
										<Text style={{ fontWeight:'bold', fontSize: 12, fontFamily: 'Roboto', color: Colors.lightOrange, textAlign: 'center'}}>
											Cancel
										</Text>
									</TouchableOpacity>
								</View>
							
							</View>

							<View style={{}}>
								<Text style={{paddingLeft: 10, fontWeight:'bold', fontSize: 15, fontFamily: 'Roboto', color: "#000000", marginTop:10, marginBottom: 6}}>
									Logout Ticket Number
								</Text>
								<TextInput style={{height: 40,color: "#000000",borderWidth:0.5,paddingLeft:10,borderColor: "#000000",fontSize: 15,backgroundColor:Colors.white, marginLeft: 10, marginRight: 10}}  
									keyboardType='default'
									returnKeyType="next" 
									maxLength = {200}
									onChangeText={(logoutTicketNumber) => this.setState({logoutTicketNumber:logoutTicketNumber})}
									value={this.state.logoutTicketNumber}  />
							</View>

							
							 <Text style={{paddingLeft: 10, paddingRight: 10, fontSize: 15, fontWeight: "bold", fontFamily: 'Roboto', color: "#000000", marginTop:25}} numberOfLines={2}>
 								NFSD Snap Logout Upload
 							</Text>
							<View style = {{flexDirection: 'row', height:120,alignSelf: 'center', alignItems:'center', justifyContent: 'center',}}>
								<View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>
									<TouchableOpacity  style={{width:70,height:70,backgroundColor:'white',alignItems:"center",justifyContent:"center",borderRadius:60, }}
									onPress={ this.openCamera.bind(this)}>
									<Image 
										style={{width:60,height:60, tintColor: this.state.cameraIconColor}}
										resizeMode= "center"
										source={require('../../Images/Camera/camera.png')} />
									</TouchableOpacity>   
									<Text style={{alignSelf:'stretch',padding: 5, fontSize: 14, fontFamily: 'Roboto', fontWeight: 'bold', color: "#000000",textAlign:'center'}}>{'Camera'}</Text>     
								</View>
								
								<View style={{flex: 1, margin:5, justifyContent: 'center', alignItems: 'center'}}>
									<TouchableOpacity  style={{width:70,height:70,backgroundColor:'white',alignItems:"center",justifyContent:"center",borderRadius:60}}
									onPress={this.openGallery.bind(this)}>
									<Image 
										style={{width:60,height:60, tintColor: this.state.galleryIconColor}}
										resizeMode= "center"
										source={require('../../Images/Camera/choose_gallery.png')} />
									</TouchableOpacity>   
									<Text style={{alignSelf:'stretch',padding: 5, fontSize: 14, fontFamily: 'Roboto', fontWeight: 'bold', color: "#000000",textAlign:'center'}}>{'Gallery'}</Text>      
								</View>     
								</View>


				

							<View style = {{flexDirection: 'row', height:50, marginTop: 10}}>
								<View style={{flex: 1}}/>
								<View style={{flex: 1, flexDirection: "row",justifyContent: 'center', alignItems: 'center'}}>   
									<TouchableOpacity style = {{flex: 1, alignItems: "flex-end", paddingRight: 5}}
													onPress={()=>this.setRequestConfirmationVisibility(false, 2, 0,0)}>
										<Text style={{ fontSize: 15, fontFamily: 'Roboto', fontWeight: 'bold', color: Colors.lightOrange}}>
											{'Cancel'}
										</Text> 
									</TouchableOpacity>
									<TouchableOpacity style = {{flex: 1, alignItems: "flex-end", paddingRight: 15}}
										onPress={()=>this.submitRequestPostCheckConfirmation()}>
										<Text style={{fontSize: 15, fontFamily: 'Roboto', fontWeight: 'bold', color: Colors.green}}>
											{'Submit'}
										</Text> 
									</TouchableOpacity>
								</View>     
							

							</View>
						</View>
					</View>
				</Modal> */}







<Modal
        animationType="none"
        transparent={true}
        closeOnClick={true}
        visible={this.state.selectFromModal11}
        onRequestClose={() => {
          this.setSelectFromModalVisibility(false)
        }}>
        <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'rgba(0,0,0,0.6)'}}>

          <View style={{width: '90%',backgroundColor: '#FFFFFF', elevation:10, borderTopLeftRadius:8,borderTopEndRadius:8, margin: 5}}>

            <View style={{ flexDirection: 'row',height:55, backgroundColor: Colors.lightOrange,alignItems:'center'}}>
              <Text style={{flex: 1, paddingLeft: 7, fontWeight:'bold', fontSize: 17, fontFamily: 'Roboto', color: '#FFFFFF', textAlign: 'left'}}>
                
              </Text>
              <View style={{width:80,height:30,justifyContent: 'center',alignSelf:'center',backgroundColor:'#fff',borderRadius:15, marginRight: 10}}>
                <TouchableOpacity
                  onPress={()=>this.setSelectFromModalVisibility(false)}>
                    <Text style={{ fontWeight:'bold', fontSize: 12, fontFamily: 'Roboto', color: Colors.lightOrange, textAlign: 'center'}}>
                       close
                    </Text>
                </TouchableOpacity>
              </View>
              
            </View>

            <View style = {{flexDirection: 'row', height:200,alignSelf: 'center', alignItems:'center', justifyContent: 'center',}}>
			<View style={StyleGCSiteLogin.formContainerInner}> 
        
		
  {/* <RadioGroup getChecked={this.getChecked}>
	<Radio iconName={"lens"} label={"CX"} value={"1"}/>
	<Radio iconName={"lens"} label={"IX"} value={"2"}/>
	<Radio iconName={"lens"} label={"Both"} value={"3"}/>
  </RadioGroup> */}
  <Text style={{fontSize:18,alignSelf: 'center', alignItems:'center', justifyContent: 'center'}}>Are You on the Site</Text>
	  
  <View style={[StyleDashboard.pl_gridContainer]}>
  <View style={StyleDashboard.pl_gridColumn}>   
  			  
				<TouchableOpacity style={StyleGCSiteLogin.buttonContainers} onPress={() => this.setSelectFromModalVisibility(false)}>
								<Text style={StyleGCSiteLogin.buttonText}>
									No
								</Text>
							</TouchableOpacity>
							  
							  
						</View>
						<View style={StyleDashboard.pl_gridColumn}>   
						<TouchableOpacity style={StyleGCSiteLogin.buttonContainers} onPress={() => this.loginDashApi()}>
								<Text style={StyleGCSiteLogin.buttonText}>
									Yes
								</Text>
							</TouchableOpacity>
							</View>
	</View>
  		  {/* <View style={StyleGCSiteLogin.bottomContainer}>   
  			  <TouchableOpacity style={StyleGCSiteLogin.buttonContainer} onPress={() => this.loginDashApi()}>
				  <Text style={StyleGCSiteLogin.buttonText}>
					  no
				  </Text>
			  </TouchableOpacity>
		  </View>   */}
	  
		{/* <Button title="Submit" onPress={() => this.()}/> */}
	  
	
	</View>   
                </View>

            </View>

          </View>
       
       
        </Modal>

		<Modal
        animationType="none"
        transparent={true}
        closeOnClick={true}
        visible={this.state.selectFromModal12}
        onRequestClose={() => {
          this.setSelectFromModalVisibility(false)
        }}>
        <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'rgba(0,0,0,0.6)'}}>

          <View style={{width: '90%',backgroundColor: '#FFFFFF', elevation:10, borderTopLeftRadius:8,borderTopEndRadius:8, margin: 5}}>

            <View style={{ flexDirection: 'row',height:55, backgroundColor: Colors.lightOrange,alignItems:'center'}}>
              <Text style={{flex: 1, paddingLeft: 7, fontWeight:'bold', fontSize: 17, fontFamily: 'Roboto', color: '#FFFFFF', textAlign: 'left'}}>
                
              </Text>
              <View style={{width:80,height:30,justifyContent: 'center',alignSelf:'center',backgroundColor:'#fff',borderRadius:15, marginRight: 10}}>
                <TouchableOpacity
                  onPress={()=>this.setSelectFromModalVisibility(false)}>
                    <Text style={{ fontWeight:'bold', fontSize: 12, fontFamily: 'Roboto', color: Colors.lightOrange, textAlign: 'center'}}>
                       closed
                    </Text>
                </TouchableOpacity>
              </View>
              
            </View>

            <View style = {{flexDirection: 'row', height:200,alignSelf: 'center', alignItems:'center', justifyContent: 'center',}}>
			<View style={StyleGCSiteLogin.formContainerInner}> 
        
		
  {/* <RadioGroup getChecked={this.getChecked}>
	<Radio iconName={"lens"} label={"CX"} value={"1"}/>
	<Radio iconName={"lens"} label={"IX"} value={"2"}/>
	<Radio iconName={"lens"} label={"Both"} value={"3"}/>
  </RadioGroup> */}
  <Text style={{fontSize:18,alignSelf: 'center', alignItems:'center', justifyContent: 'center'}}>You want to logout this site ?</Text>


	 <View style={[StyleDashboard.pl_gridContainer]}>
  <View style={StyleDashboard.pl_gridColumn}>   
  			  
				<TouchableOpacity style={StyleGCSiteLogin.buttonContainers} onPress={() => this.setSelectFromModalVisibility(false)}>
								<Text style={StyleGCSiteLogin.buttonText}>
									No
								</Text>
							</TouchableOpacity>
							  
							  
						</View>
						<View style={StyleDashboard.pl_gridColumn}>   
						<TouchableOpacity style={StyleGCSiteLogin.buttonContainers} onPress={() => this.logoutDashApi()}>
								<Text style={StyleGCSiteLogin.buttonText}>
									Yes
								</Text>
							</TouchableOpacity>
							</View>
	</View> 
	  
  {/* <View style={StyleGCSiteLogin.bottomContainer}>   
			  <TouchableOpacity style={StyleGCSiteLogin.buttonContainer} onPress={() => this.logoutDashApi()}>
				  <Text style={StyleGCSiteLogin.buttonText}>
					  Logout
				  </Text>
			  </TouchableOpacity>
		  </View> */}
	  
	  
		{/* <Button title="Submit" onPress={() => this.()}/> */}
	  
	
	</View>   
                </View>

            </View>

          </View>
       
       
        </Modal>


			</View>
		);
	}




	async createNotificationListeners() {
		console.log("Received FCM");
	// 	this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification) => {
	// 		// alert(JSON.stringify(notification));
	// 	//   const { title, body } = notification;
	// 	  let GCM_TITLE = "mPulse";
	// 	//   alert("notification")
	// 	console.log("Received FCM 1");

		 
	//   });
		/*
		* Triggered when a particular notification has been received in foreground
		* */
		console.log("Received FCM 2");

		this.notificationListener = firebase.notifications().onNotification(async(notification) => {
			const { title, body ,data} = notification;
            const notification1 = new firebase.notifications.Notification()
            .setNotificationId("1") 
            .setTitle(title)
            .setBody(body)
            .setData(data)
            .android.setSmallIcon("@mipmap/ic_notification")
            .android.setLargeIcon("@mipmap/ic_notification")
            .android.setPriority(firebase.notifications.Android.Priority.High)
            .android.setChannelId('com.mobilecomm.tmoericsson')
            .android.setAutoCancel(false);
            if(data.image!="" && data.image!=null && data.image!=undefined)
            {
                notification1.android.setBigPicture(data.image);
            }
			await firebase.notifications().displayNotification(notification1);	
			this.showAlert(title, body);
		});
	  
		/*
		* If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
		* */
		this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
			 const action = notificationOpen.action;
			 const { title, body } = notificationOpen.notification;
			//  let GCM_TITLE = "Yellow  Rentals";;
			//  this.getGCSiteList(this.state.fromDate,this.state.toDate);
			//  this.showAlert(title,body);
			console.log("Received FCM Background");

		});
	  
		/*
		* If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
		* */
		const notificationOpen = await firebase.notifications().getInitialNotification();
		if (notificationOpen) 
		{
			// let GCM_TITLE = "mPulse";    
			const action = notificationOpen.action;
			const notification1 = notificationOpen.notification;
			const { title, body } = notificationOpen.notification;
			// this.showAlert(GCM_TITLE,notification1);
			console.log("Received FCM Background App is closed");

		}
		
		//  Triggered for data only payload in foreground
		this.messageListener = firebase.messaging().onMessage((message) => {
		
		  Toast.show('Message Listener  ! ', Toast.LONG);
		  console.log(JSON.stringify(message));
		  console.log("Received FCM Extra Payload");

		});
	}

	showAlert = (title, body) => {
		Alert.alert(
		  title, body,
		  [
			  { text: 'OK'},
		  ],
		  { cancelable: true },
		);
	  }

	//   callApiWithoutLoader = () =>
	//   {
		
	//   }
	

	// buildNotification = () => {
	// 	const title = Platform.OS === 'android' ? 'Daily Reminder' : '';
	// 	const notification = new firebase.notifications.Notification()
	// 	  .setNotificationId('1')
	// 	  .setTitle(title)
	// 	  .setBody('This is a notification')
	// 	  .android.setPriority(firebase.notifications.Android.Priority.High)
	// 	  .android.setChannelId('reminder')
	// 	  .android.setAutoCancel(true);
	
	// 	return notification;
	//   };
}