
import React, { PureComponent } from 'react';
import { View, ImageBackground, Image,Text,TouchableOpacity } from 'react-native';
import StatusBarComponent from '../../Components/StatusBar';
import { StyleSplash } from '../../Styles/PageStyle';
import AppImages from '../../Images';
import Loader from '../../Components/Loader';
import I18n from '../../Utils/i18n';
import AsyncStorage from '@react-native-community/async-storage'; 
import Constant from '../../Utils/Constant';
import { GlobalVariable } from '../../Utils/Utils'
import firebase from 'react-native-firebase';

export default class SplashScreen extends PureComponent {
	constructor(props) {
		super(props);
		 
	}


	componentDidMount() {
		
		this.checkPermission();
		firebase.messaging().onTokenRefresh((token) => {
			this._onChangeToken(token) // language
		});
		
		this.autoLogin(); 
	}

	autoLogin=async()=>{
		const userDetails = await AsyncStorage.getItem(Constant.LOCAL_STORAGE.LOGINRESPONSE);
		try{
			let userDetailsObject=JSON.parse(userDetails);
			if(userDetailsObject){  
				GlobalVariable.USERID=userDetailsObject.id; 
				this.props.navigation.navigate("MainApp"); 
			} 
		}catch(err){

		}
	}


	_onChangeToken = (token) => { //,language
		if (token!=null) {
		   AsyncStorage.setItem('fcmToken', token);
		   
	  }
	}

	async checkPermission() 
	{
		const enabled = await firebase.messaging().hasPermission();
		if (enabled) 
		{
			this.getToken();
		} 
		else 
		{
			this.requestPermission();
		}
	}


	async getToken()
	{
		let fcmToken = await AsyncStorage.getItem('fcmToken');
		if (!fcmToken)
		{
			fcmToken = await firebase.messaging().getToken();
			
			if (fcmToken) 
			{
				// user has a device token
				await AsyncStorage.setItem('fcmToken', fcmToken);
			}
		}
		console.log("FCm Token - "+fcmToken);
		// alert(fcmToken);

	}

	async requestPermission() 
	{
		try 
		{
			await firebase.messaging().requestPermission();
			// User has authorised
			this.getToken();
		} 
		catch (error) 
		{
			// User has rejected permissions
			console.log('permission rejected');
		}
	}
		


	gotoScreen=(pageName)=>{
		this.props.navigation.navigate(pageName);
	}

	render() {
		return (
			<View style={StyleSplash.container}>
				<StatusBarComponent isHidden={true} /> 
				<View style={StyleSplash.contentContainer}> 
					<Image
						resizeMode="contain"
						style={StyleSplash.logoImageStyle}
						source={AppImages.Logo_Login_Image}
					/>
					{/* <Text style={StyleSplash.logoText}>
						{I18n.t('SPLASHPAGE.COMPANYNAME')}
					</Text>
					<Text style={StyleSplash.logoText}>
						{I18n.t('SPLASHPAGE.COMPANYNAME1')}
					</Text>  */}
					<View style={StyleSplash.bottomContainer}>
						<TouchableOpacity style={StyleSplash.LoginButton} onPress={() => this.gotoScreen('LoginScreen')}>
							<Text style={StyleSplash.LoginButtonText}>
								{I18n.t('SPLASHPAGE.LOGIN')}
							</Text>
						</TouchableOpacity>
						{/* <TouchableOpacity style={[StyleSplash.SignupButton,StyleSplash.LoginButton]} onPress={() => this.gotoScreen('SignupScreen')}>
							<Text style={[StyleSplash.LoginButtonText,StyleSplash.SignupButtonText]}>
								{I18n.t('SPLASHPAGE.SIGNUP')}
							</Text>
						</TouchableOpacity> */}
					</View>
				</View>
				<ImageBackground fadeDuration={100} source={AppImages.SplashBackground} style={StyleSplash.bgImgContainer} resizeMode={'cover'}>
				</ImageBackground>
			</View>
		);
	}






	
}