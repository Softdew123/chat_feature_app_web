import React, { Component } from 'react';
import ToolbarComponent from "../../Components/Toolbar";
import I18n from '../../Utils/i18n';
import { DeviceInfo} from '../../Utils/Utils'
import {WebView,AppState, Alert, View,FlatList,RefreshControl,TouchableOpacity,Text, Modal, TextInput, Image } from 'react-native';
import { StylePreCheck } from '../../Styles/PageStyle';
import { StyleDashboard } from '../../Styles/PageStyle';
import StatusBarComponent from '../../Components/StatusBar';
import Constant from '../../Utils/Constant';
import {PostRequest} from '../../Utils/ServiceCalls';
import { Dialog,GlobalVariable} from '../../Utils/Utils';
import HTMLView from 'react-native-htmlview';  
import AsyncStorage from '@react-native-community/async-storage';
export default class PostCheckFormScreen extends Component {
  apiRetryAttempt = 0;

  constructor(props) {
    super(props);
    this.state = {
		cressIdds: "",
	  postCheckUrl:"",
	  postData: [],
	  isAppDataAvailable: false,
	  postRequestButton: false
    };
  }

  componentDidMount() {
	this.getUsersLocalDetails()
   try
   {
	console.log("post_check newwww ","start")
    let params= this.props.navigation.state.params;
	let postCheckUrl = params.siteId; 
	console.log("post_check newwww ",postCheckUrl)
	this.state.postCheckUrl = postCheckUrl
	// this.setState({postCheckUrl: postCheckUrl});
	console.log(this.state.postCheckUrl)
	this.getPostData()
   }
   catch(error)
   {
       alert(err)
   }

  
  }



  getUsersLocalDetails=async()=>{
    console.log("Main 1")
    const userDetails = await AsyncStorage.getItem(Constant.LOCAL_STORAGE.LOGINRESPONSE);

    try
    {
      console.log(JSON.parse(userDetails))
      let userDetailsObject=JSON.parse(userDetails);
      let idss= userDetailsObject.id
      this.setState({cressIdds: idss});
    //   this.setState({contactName:contactName,contactEmail:contactEmail,contactNumber:contactNumber},()=>{
    //       this.callGetSiteApi(); 
    //   })
    }
    catch(err)
    {
       
    }
  }

  getPostData(){
	let id = this.state.postCheckUrl
	let postStatus = this.state.postCheckUrl
	console.log("post_check newwww ",postStatus)
	if(postStatus.postStatus == 0){
		console.log(postStatus)
		this.setState({postRequestButton: true})
	}
	// this.
	console.log("post_check newwww", id.Id)
	let paramObj={"id":id.Id};
    let paramObject = {
      path: Constant.Api.POSTCHECKREQUEST.GetPostCheckInfo,
      showErrorMsg: true,
      params: paramObj 
    }

    
	console.log("post_check newwww  "  ,paramObj)
    PostRequest(paramObject).then((responseApisuccess) => {
		console.log("post_check newwww "  ,responseApisuccess)
		this.setState({isLoadervisible:false},()=>{ 
			let response=responseApisuccess.data; 
			if(response==null || response==""){ 
				let alertTitle= I18n.t('GCSITELOGINPAGE.NO_PosCHCKDETAIL_TITLE');
				let alertMessage= I18n.t('GCSITELOGINPAGE.NO_PosCHCKDETAIL_MSG');
				let buttonStr= I18n.t('OK_BUTTON');
				Dialog.alertDialog(alertTitle, alertMessage, buttonStr, true, null); 
				return;
			}
			if(response!=null && response!=""){   
				console.log(response)
				this.state.postData =response.data.Table
				this.setState({isAppDataAvailable: true})
				// let alertTitle= I18n.t('GCSITELOGINPAGE.NO_PosCHCKDETAIL_TITLE');
				// let alertMessage= I18n.t('GCSITELOGINPAGE.NO_PosCHCKDETAIL_MSG');
				// let buttonStr= I18n.t('OK_BUTTON');
				// Dialog.alertDialog(alertTitle, alertMessage, buttonStr, true, null); 
			}
		})  
	})
  }


  displayAlertMsg=(title,message,callback)=>{
	Dialog.alertDialog(title,message,I18n.t('OK_BUTTON'),false,null).then(()=>{
		if(callback){
			callback();
		}
	})
} 


  postCheckRequest(){
	let paramObj={"id":this.state.postCheckUrl,
		"crewId":this.state.cressIdds
};
console.log("pppppppppp",paramObj)   
    let paramObject = {
      path: Constant.Api.POSTCHECKREQUEST.PostCheckReq,
      showErrorMsg: true,
      params: paramObj 
    }

    

    PostRequest(paramObject).then((responseApisuccess) => {
		this.setState({isLoadervisible:false},()=>{ 
			let response=responseApisuccess.data; 
			if(response==null || response==""){ 
				let alertTitle= I18n.t('GCSITELOGINPAGE.NO_PosCHCKDETAIL_TITLE');
				let alertMessage= I18n.t('GCSITELOGINPAGE.NO_PosCHCKDETAIL_MSG');
				let buttonStr= I18n.t('OK_BUTTON');
				Dialog.alertDialog(alertTitle, alertMessage, buttonStr, true, null); 
				return;
			}
			if(response!=null && response!=""){
				console.log(response)
				this.displayAlertMsg('PostCheckRequest Status',response.message);
			}
		})  
	})
  }
  gotoWebView_Screen=(data)=>{
	console.log("webview",data)
let params = { item:data}; 
 this.props.navigation.navigate("WebView_Screen",params); 



}


  renderAppDataListItem=(item,index)=>{ 
	console.log(item)
	
	const htmlContent = item.mailInfo+"";  
	let flg=true
	if(!item.mailInfo || item.mailInfo=="" )
	flg=false  
			
			
			return( 
				<View style={StyleDashboard.card}> 
					{/* <View >  */}
{/* 						
						<View > */}
							
							{/* <Text style={StylePreCheck.preText}>Site Id: {item.site_id}</Text>
								<Text style={StylePreCheck.preText}>Work Order No: {item.Work_Order_No}</Text>
								<Text style={StylePreCheck.preText}>POR: {item.POR}</Text>
								<Text style={StylePreCheck.preText}>Postcheck Engineer: {item.postcheck_engineer_name}</Text>
								<Text style={StylePreCheck.preText}>Postcheck Request Date: {item.postmobileLoginTime}</Text>
								<Text style={StylePreCheck.preText}>Postcheck Deliver Date: {item.postcheck_deliver_date}</Text>
								{/* <Text style={StylePreCheck.preText}>id: {item.id}</Text>  */}
								{/* <Text style={StylePreCheck.preText}>postcheck_engineer: {item.postcheck_engineer}</Text> */}
								{/* <Text style={StylePreCheck.preText}>logout_time_postcheck:{item.logout_time_postcheck}</Text> */}
								

								
								
								{/* <Text style={StylePreCheck.preText}>5G Downtime: {item.fivegdownTime}</Text> */}
								{/* <Text style={StylePreCheck.preText}>5G UP: {item.five_g_up}</Text>
								<Text style={StylePreCheck.preText}>5G Alarm: {item.five_g_alarm}</Text>
								<Text style={StylePreCheck.preText}>GSM Downtime: {item.gsmdowntime}</Text>
								<Text style={StylePreCheck.preText}>GSM UP: {item.gsm_up}</Text>
								<Text style={StylePreCheck.preText}>GSM Alarm: {item.gsm_alarm}</Text>
								<Text style={StylePreCheck.preText}>UMTS Downtime: {item.umtsdowntime}</Text>
								<Text style={StylePreCheck.preText}>UMTS UP: {item.umts_up}</Text>
								<Text style={StylePreCheck.preText}>UMTS Alarm: {item.umts_alarm}</Text>


								<Text style={StylePreCheck.preText}>LTE Downtime: {item.ltedowntime}</Text>
								<Text style={StylePreCheck.preText}>LTE UP: {item.lte_up}</Text>
							
	
								<Text style={StylePreCheck.preText}>LTE Alarm: {item.lte_alarm}</Text>
								
								<Text style={StylePreCheck.preText}>Activity Status: {item.activity_status_site_name}</Text>
								<Text style={StylePreCheck.preText}>Activity StatusPOR: {item.activity_status_por}</Text>
								{/* <Text style={StylePreCheck.preText}>post_check_alarms: {item.post_check_alarms}</Text>  */}
								{/* <Text style={StylePreCheck.preText}>cancelled_reason: {item.cancelled_reason}</Text>
								<Text style={StylePreCheck.preText}>Was TS Scripting Was Required?: {item.was_scripting}</Text>
								<Text style={StylePreCheck.preText}>Engineer Responsible For Scripting:{item.engineer_responsible_forname}</Text>
								<Text style={StylePreCheck.preText}>Site Up Time: {item.site_up_time}</Text>
								<Text style={StylePreCheck.preText}>Activity Notes: {item.activity_notes}</Text>
								<Text style={StylePreCheck.preText}>Mail Info:   </Text> */} 
								{/* <HTMLView
        value={htmlContent}
	 
		 
      /> */} 
   <View style={{flex: 1, flexDirection:'column'}}>
   
   <Text style={StylePreCheck.preText}>Site Id: {item.site_id}</Text>
   								<Text style={StylePreCheck.preText}>Project Code: {item.Work_Order_No}</Text>
								{/* <Text style={StylePreCheck.preText}>Work Order No: {item.Work_Order_No}</Text> */}
								<Text style={StylePreCheck.preText}>Technology: {item.POR}</Text>
								<Text style={StylePreCheck.preText}>Postcheck Engineer: {item.postcheck_engineer_name}</Text>
								<Text style={StylePreCheck.preText}>Postcheck Request Date: {item.postmobileLoginTime}</Text>
								<Text style={StylePreCheck.preText}>Postcheck Deliver Date: {item.postcheck_deliver_date}</Text>
								{/* <Text style={StylePreCheck.preText}>id: {item.id}</Text>  */}
								{/* <Text style={StylePreCheck.preText}>postcheck_engineer: {item.postcheck_engineer}</Text> */}
								{/* <Text style={StylePreCheck.preText}>logout_time_postcheck:{item.logout_time_postcheck}</Text> */}
								

								
								
								<Text style={StylePreCheck.preText}>5G Downtime: {item.fivegdownTime}</Text>
								<Text style={StylePreCheck.preText}>5G UP: {item.five_g_up}</Text>
								<Text style={StylePreCheck.preText}>5G Alarm: {item.five_g_alarm}</Text>
								<Text style={StylePreCheck.preText}>GSM Downtime: {item.gsmdowntime}</Text>
								<Text style={StylePreCheck.preText}>GSM UP: {item.gsm_up}</Text>
								<Text style={StylePreCheck.preText}>GSM Alarm: {item.gsm_alarm}</Text>
								<Text style={StylePreCheck.preText}>UMTS Downtime: {item.umtsdowntime}</Text>
								<Text style={StylePreCheck.preText}>UMTS UP: {item.umts_up}</Text>
								<Text style={StylePreCheck.preText}>UMTS Alarm: {item.umts_alarm}</Text>


								<Text style={StylePreCheck.preText}>LTE Downtime: {item.ltedowntime}</Text>
								<Text style={StylePreCheck.preText}>LTE UP: {item.lte_up}</Text>
							
	
								<Text style={StylePreCheck.preText}>LTE Alarm: {item.lte_alarm}</Text>
								
			     					<Text style={StylePreCheck.preText}>Activity Status: {item.activity_status_site_name}</Text>
								{/* <Text style={StylePreCheck.preText}>Activity StatusPOR: {item.activity_status_por}</Text> */}
								<Text style={StylePreCheck.preText}>Activity StatusTechnology: {item.activity_status_por}</Text>
								{/* <Text style={StylePreCheck.preText}>post_check_alarms: {item.post_check_alarms}</Text>  */}
								<Text style={StylePreCheck.preText}>cancelled_reason: {item.cancelled_reason}</Text>
								<Text style={StylePreCheck.preText}>Was TS Scripting Was Required?: {item.was_scripting}</Text>
								<Text style={StylePreCheck.preText}>Engineer Responsible For Scripting:{item.engineer_responsible_forname}</Text>
								<Text style={StylePreCheck.preText}>Site Up Time: {item.site_up_time}</Text>
								<Text style={StylePreCheck.preText}>Activity Notes: {item.activity_notes}</Text>
								<Text style={StylePreCheck.preText}>Mail Info:   </Text>
								{flg && 	<TouchableOpacity style={StyleDashboard.buttonContainer} onPress={() => this.gotoWebView_Screen(htmlContent)}>
						<Text style={[StyleDashboard.buttonText]}>
						    View Mail Info >>
					
						</Text>
	</TouchableOpacity>    }


</View>  
	    				   
								{/* <Text style={StylePreCheck.preText}>tt_opend:{item.tt_opend}</Text>
								<Text style={StylePreCheck.preText}>tt_id:{item.tt_id}</Text> */}
								
								{/* <Text style={StylePreCheck.preText}>activity_went_out:{item.activity_went_out}</Text>

								<Text style={StylePreCheck.preText}>time_after_approved:{item.time_after_approved}</Text>
								
								
								<Text style={StylePreCheck.preText}>ttidstatus:{item.ttidstatus}</Text>
								
								
								<Text style={StylePreCheck.preText}>activity_status_site: {item.activity_status_site}</Text>
								
								
								<Text style={StylePreCheck.preText}>site_up_time_formatted: {item.site_up_time_formatted}</Text> */}
								
	
						{/* </View>  */}
					{/* </View> */}
					
					
				</View> 
			); 
		}


  render() {
    let url = Constant.PRE_POST_CHECK_URL.URL+this.state.postCheckUrl ;

    return(
        <View style={StylePreCheck.container}>
          <StatusBarComponent isHidden={false} />
          {DeviceInfo.isAndroid() &&
          <ToolbarComponent
            title={I18n.t('POSTCHECKPAGE.PAGETITLE')}
            navIcon={Constant.ICONS.BACK_ICON}
            navClick={() => this.props.navigation.goBack()}
          />
        }
          <View style={StylePreCheck.flatListContainer}>  
		  	{this.state.isAppDataAvailable &&
				<FlatList 
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this.onDragRefresh}
//                                 colors={[Colors.orange]}
                                progressViewOffset={20}
                                enabled={true}
                            />
                        } 
//                         ListEmptyComponent={this.appDataList} 
                        data={this.state.postData}
                        extraData={this.state.listRefresh}
                        initialNumToRender={20}
                        keyExtractor={(item, index) => index+""+new Date().toLocaleString()}
					renderItem={({item, index}) => this.renderAppDataListItem(item, index)}
                    /> 
			}

					{this.state.postRequestButton &&
					
					<View style={StylePreCheck.btnCont}>
							<TouchableOpacity style={StylePreCheck.buttonContainer}  onPress={() => this.postCheckRequest()}>
								<Text style={StylePreCheck.buttonText}>
									Post Check Request
								</Text>
							</TouchableOpacity>
					</View>	
					}
					

          </View>

{/* <View style={StyleDashboard.searchButtonContainer, StylePreCheck.formContainer }>
							<TouchableOpacity style={StyleDashboard.searchButton} onPress={() => this.searchGCSites()}>
								<Text style={StyleDashboard.searchButtonText}>
								    {I18n.t('DASHBOARDPAGE.Search')}
								</Text>
							</TouchableOpacity>
						</View> */}
        
		
		       
							
		
		</View>     
    );

  
  }
}