 
export default {
  white:'#ffffff', 
  dimWhite:"rgba(255,255,255,0.7)",
  colorStatusBar:"#ce1d04",
  modelBackground:"rgba(0,0,0,0.5)",
  transparent:"rgba(0,0,0,0)",
  darkBlue:"#5DBCD2",
  lightBlueInput:"rgba(10,97,155,0.5)",
  lightBlue:"#2980b6",
  red:"#b52424",
  orange:"#e54f3b",
  lightOrange:"#ed462d",
  mainBackgroun:"#e5e5e5",
  cardHeading:"#333333",
  cardDivideLineColor:"#d9dce0",
  lightGrey:"#fefefe",
  darkGrey:"#8d8e8e",
  blackLightShade:"#39393a",
  green:"#006400",
  lightGreen: "#0DD000",
  teal: "#66FFFF",
  purple:'#bf00ff'
}


