
import { StyleSheet,Dimensions } from 'react-native'
import { FontSize, FontName, Colors } from '../Themes'
const win = Dimensions.get('window');
export default StyleSheet.create({ 
	container: {
		flex: 1 
	},
	formContainer: {
		height:win.height,
		width:win.width, 
		position:'absolute', 
		justifyContent:'flex-start',
		alignItems:'center', 
		zIndex:1
	},
	formContainerInner:{ 
		justifyContent:'center',
		alignItems: 'center',
		width:"100%", 
		flex:1, 
		position:'relative', 
		flexDirection: 'column', 
	},
	bgImgContainer:{
		flex:1,		 
		justifyContent: 'center',
		alignItems: 'center',
	},
	logoContainer:{		 
		height:250,
		width:"100%",
		maxWidth:300, 
		position:'relative', 		 
		alignItems:'center' 
	},
	logoImageStyle:{
		width:150, 
		height:150,
		marginBottom:10,
		marginTop:20 
	},	
	logoText:{
		color: Colors.white,
		textAlign: 'center', 
		marginTop: 1,
		fontSize:FontSize.h4
	}, 
	middleContainer:{		 		 
		flex:1,
		width:"100%"
	},
	scrollView:{
		width:"100%" 
	},
	scrollViewInnerContainer:{
		width:"100%", 
		justifyContent:'center',
		flex:1,
		alignItems:'center', 
	},
	scrollViewContentContainer:{
		width:"100%",
		maxWidth:300
	},
	inputHeadingTitle:{
		fontSize:FontSize.h8,
		textAlign: 'left', 
		color: Colors.dimWhite,
		marginTop:10
	},
	inputField: {
		height: 45,
		color: Colors.white, 
		borderBottomWidth:0.5,
		borderColor: Colors.white,
		fontSize:FontSize.Standard,
	},
	dropdown:{  
		fontSize:FontSize.Standard,
		color: Colors.dimWhite
	},
	buttonContainer: {
		borderRadius:25,
		backgroundColor:Colors.orange,
		maxWidth:300,
		width:"100%",
		padding:12,
	},
	buttonText: {
		color: Colors.white,
		textAlign: 'center', 
		fontSize:FontSize.h7,
		fontWeight:'500'
	},
	addMarginBottom:{
		marginBottom:30
	},
	addMarginBottom1:{
		marginBottom:10
	},
	bottomContainer:{ 
		width:"100%",
		maxWidth:300,
		height:60,
		alignItems:'center'
	}, 
	gridContainer:{
		flex:1,
		flexDirection:'row'
	},
	gridColumn:{
		flex:1 
	},
	dividerLine:{
		borderBottomWidth:0.5,
		borderColor: Colors.white,
		height:1,
		flex:1
	},
	addMarginLeft:{
		marginLeft: 10
	}
	,dropdownItem:{
		padding:0
	}  ,




	autocontainer: {
		backgroundColor:'red',
		flex: 1,
		paddingTop: 25
	  },
	  autocompleteContainer: {
		  
		flex: 1,
		left: 0,
		position: 'absolute',
		right: 0,
		top: 0,
		zIndex: 1
	  },
	  autoitemText: {
		fontSize: 15,
		margin: 2
	  },
	  descriptionContainer: {
		// `backgroundColor` needs to be set otherwise the
		// autocomplete input will disappear on text input.
		backgroundColor: '#F5FCFF',
		marginTop: 25
	  },
	  autoinfoText: {
		textAlign: 'center'
	  },
	  autotitleText: {
		fontSize: 18,
		fontWeight: '500',
		marginBottom: 10,
		marginTop: 10,
		textAlign: 'center'
	  },
	  autodirectorText: {
		color: 'grey',
		fontSize: 12,
		marginBottom: 10,
		textAlign: 'center'
	  },
	  autoopeningText: {
		textAlign: 'center'
	  }
})