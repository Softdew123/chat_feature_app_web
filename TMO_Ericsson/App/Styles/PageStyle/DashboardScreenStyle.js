
import {Dimensions, StyleSheet} from 'react-native'
import { FontSize, FontName, Colors } from '../Themes'

const win = Dimensions.get('window');
const dateConaterinWidth=(win.width/2)
export default StyleSheet.create({
	reasonText: {
		color:'red',
		paddingBottom: 5,
		fontSize: 15
	},
	container: {
		flex: 1,
		alignItems: 'stretch',
		backgroundColor: Colors.mainBackgrounAndroid
	},
	scrollContainer: {
		flex: 1,
		flexDirection:'column' 
	},
	pl_gridContainer:{ 
		flexDirection:'row' 
	},
	pl_BorderTop:{
		borderTopWidth:1,
		borderColor: Colors.cardDivideLineColor,
	},
	pl_BorderLeft:{
		borderLeftWidth:1,
		borderColor: Colors.cardDivideLineColor,
	},
	pl_gridColumn:{
		flex:1,
		justifyContent: 'center'
		
	},
	pl_gridColumnSingle:{
		marginLeft:10,
		marginRight:10
	},
	lineDateTime:{
		height:25,
		width:'100%'
	},
	lineDateTimeButton:{
		height:60,
		width:'100%'
	},
	pl_gridColumnDate:{
		width:65,
		marginLeft:15
	},
	card:{
		backgroundColor:Colors.lightGrey,
		marginBottom:20,
		elevation: 2 
	},
	cardTitle:{
		fontWeight: '500',
		color:Colors.cardHeading,
		fontSize:FontSize.h5, 
		marginBottom:10,
	},
	additionMargin:{
		marginTop:10
	}, 
	additionMargin1:{
		marginTop:0
	},
	buttonContainer: {
		 paddingBottom:15,
		 paddingTop:15
	}, 
	circle:{
		width:50,
		height:50, 
		borderRadius:40,
		justifyContent: 'center',
		alignItems: 'center',
	},
	dateMain:{
		fontWeight: '500',
		color:Colors.orange,
		fontSize:FontSize.h3, 
		marginBottom:2,
		marginTop:2,
		fontStyle:'italic'
	},
	dateOther:{
		color:Colors.darkGrey,
		fontSize:FontSize.h6, 
		marginBottom:5, 
	},
	time:{
		color:Colors.darkGrey,
		fontSize:FontSize.h8, 
		fontWeight: '300',
		paddingBottom:5
	},
	ad_PaddingBottom:{
		paddingBottom:30
	},
	dateConainerParent:{
		height:80,
		borderBottomWidth:1,
		borderColor: Colors.cardDivideLineColor,
		flexDirection:'row',
		paddingTop:10,
		paddingBottom:10
	},
	dateConainerChild:{
		flex:1,
		width:'50%'
	},
	flatListContainer:{
		flex:1
	},
	dateTitle:{
		color: Colors.blackLightShade, 
		fontWeight:'600',
		marginBottom:2, 
		fontSize:FontSize.h8,
	},
	dateMarginLeft:{
		marginLeft:5
	},
	datePickerContainerStyle:{
		width:dateConaterinWidth-55
	},
	setDateMargin:{
		marginLeft:5,
		marginRight:5
	},
	datepickerIcon:{
		position: 'absolute',
		left: 0,
		top: 4,
		marginLeft: 0,
		width:30
	},
	datepickerInput:{
		 
	},
 	searchButtonContainer:{
		width:90, 
		alignItems: 'center',
		justifyContent:"center"
	},
	searchButton:{
		borderRadius:2,  
		paddingTop:10,
		paddingBottom:10,
		backgroundColor:Colors.orange,
		marginTop:22,
		width:80  
	},

	searchButtonText:{
		color: Colors.white,
		textAlign: 'left', 
		fontSize:FontSize.h8,
		fontWeight:'500', 
		alignSelf: 'stretch',  
	},
	buttonText:{
		color: Colors.orange,
		textAlign: 'center', 
		fontSize:FontSize.h8,
		fontWeight:'500'
	},
	buttonTexts:{
		color: Colors.green,
		textAlign: 'center', 
		fontSize:FontSize.h8,
		fontWeight:'500'
	},
	noGCSiteContainer:{
		flex:1,
		alignItems:"center",
		justifyContent:"center"
	},
	noGCSiteText:{
		color: Colors.blackLightShade,
		textAlign: 'center', 
		fontSize:FontSize.h8,
		fontWeight:'500'
	},
	noSiteButton:{
		borderRadius:5,
		backgroundColor:Colors.orange, 
		width:150,
		padding:12,
		marginTop:10
	},
	noSiteBUttonText:{
		color: Colors.white,
		textAlign: 'center', 
		fontSize:FontSize.h8,
		fontWeight:'500'
	},
	 upload_button: {
		backgroundColor: Colors.orange ,
		padding: 10, 
          width:200,            
		margin: 5,                        
		height: 40,  
		textAlign:'center',   
	 },      
	   upload_text:{
		color: 'white',
		textAlign:'center',   
		
	 }
})