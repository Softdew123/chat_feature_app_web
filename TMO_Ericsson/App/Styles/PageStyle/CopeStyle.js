

import {StyleSheet,Dimensions} from 'react-native'
import { FontSize, Colors } from '../Themes'

const win = Dimensions.get('window');
const autocompletePopHeight=win.height-100
export default StyleSheet.create({
  
	container: {      
		paddingTop: 23 ,
		flex:1,
	 },
	  
	 input: {
		 flex:1,
		// margin: 15,      
		 height: 40,
	    // width:'60%',    
		borderColor: '#7a42f4',
		borderWidth: 1
	 },
	 hv:{
		 margin:10,
		 display:'flex',
		 flexDirection:'row',
		 justifyContent: 'space-between',
		 flexWrap:'wrap'
	 },     
	  img_pic: {
	
		// margin: 5,
		height: 40,
		 width:40 ,
		// borderColor: '#7a42f4',
		// borderWidth: 1
	 },
	 img_view: {
	
		// margin: 5,
		height: 200,
		 width:200 ,
		// borderColor: '#7a42f4',
		// borderWidth: 1
	 },
	 img_add: {
	   marginLeft:5,
		// margin: 5,
		height: 40,
		 width:40 ,
		// borderColor: '#7a42f4',
		// borderWidth: 1
	 },
	 img_add1: {
		marginRight:5,
		 // margin: 5,
		 height: 40,
		  width:40 ,
		 // borderColor: '#7a42f4',
		 // borderWidth: 1
	  },
	 textAreaContainer: {
		margin: 15,    
		borderColor: Colors.grey20,
		borderWidth: 1,
		padding: 2
	  },
	  textArea: {
		height: 100,
		justifyContent: "flex-start"
	  }, 
	 submitButton: {
		backgroundColor: Colors.orange ,
		padding: 10,
		margin: 15,
		height: 40,
	 },
	 submitButtonText:{
		color: 'white',
		textAlign:'center'
	 },
	 upload_button: {
		backgroundColor: Colors.orange ,
		padding: 10, 
          width:200,            
		margin: 5,                        
		height: 40,  
		textAlign:'center',   
	 },      
	   upload_text:{
		color: 'white',
		textAlign:'center',   
		
	 } 
	 ,
	 opt:{
	
		padding: 7,
		margin: 15,
		 

	 }
})