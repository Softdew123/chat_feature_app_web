

import {Dimensions, StyleSheet} from 'react-native'
import { FontSize, FontName, Colors } from '../Themes'

const win = Dimensions.get('window');
export default StyleSheet.create({
	container: {
		flex: 1,
		height:win.height,
		width:win.width
	},
	bgImgContainer:{
		flex:1,
		height:win.height,
		width:win.width,
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'column'
	},
	contentContainer:{
		position:'absolute',
		top:0,
		left:0,
		right:0,
		bottom:0, 
		justifyContent:'center',
		alignItems:'center', 
		zIndex:1, 
	},
	logoText:{
		color: Colors.white,
		textAlign: 'center', 
		marginTop: 1,
		fontSize:FontSize.h4
	},
	logoImageStyle:{
		width:170, 
		height:170,
		marginTop:50,
		marginBottom:10
	},
	bottomContainer:{
		flex: 1,
		justifyContent: 'flex-end',
		marginBottom: 36,
		width:'100%',
		alignItems: 'center',
	},
	LoginButton:{
		borderRadius:25,
		borderWidth: 1,
		borderColor: Colors.white,
		padding:12,
		maxWidth:350,
		width:"100%" 
	},
	LoginButtonText:{
		color: Colors.white,
		textAlign: 'center', 
		fontSize:FontSize.h7,
		fontWeight:'500'
	},
	SignupButtonText:{ 
		color: Colors.orange 
	},
	SignupButton:{
		backgroundColor:Colors.white, 
		marginTop:10
	} 
})