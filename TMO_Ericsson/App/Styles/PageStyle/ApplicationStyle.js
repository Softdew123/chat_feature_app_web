
import { StyleSheet } from 'react-native';
import { Colors } from '../Themes';

export default StyleSheet.create({
  applicationView: {
    flex: 1
  },
  safeArea: {
    flex: 1,
    backgroundColor: Colors.white
  }
});