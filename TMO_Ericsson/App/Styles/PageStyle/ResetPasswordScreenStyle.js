
import { StyleSheet} from 'react-native'
import { FontSize, FontName, Colors } from '../Themes'
export default StyleSheet.create({ 
	container: {
		flex: 1 
	},
	bgImgContainer:{
		flex:1,		 
		justifyContent: 'center',
		alignItems: 'center',
	},
	formContainer: {
        paddingTop: 50,
		height:"100%",
		width:"100%", 
		alignItems:'center', 
		zIndex:1, 
	},
 
	inputHeadingTitle:{
		fontSize:FontSize.h8,
		textAlign: 'left', 
		maxWidth:300,
		width:"100%",
		color: Colors.darkGrey,
		marginTop:10
	},
	inputField: {
		height: 45,
		color: Colors.blackLightShade,
		maxWidth:300,
		width:"100%", 
		borderBottomWidth:0.5,
		borderColor: Colors.blackLightShade,
		fontSize:FontSize.Standard,
	},
	buttonContainer: {
		borderRadius:25,
		backgroundColor:Colors.orange,
		maxWidth:300,
		width:'100%',
		padding:12,
	},
	buttonText: {
		color: Colors.white,
		textAlign: 'center', 
		fontSize:FontSize.h7,
		fontWeight:'500'
	},
	addMarginBottom:{
		marginBottom:30
	}
})