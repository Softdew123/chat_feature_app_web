

import StyleApp from './ApplicationStyle';
import StyleSplash from './SplashScreenStyle';
import StyleLogin from './LoginScreenStyle';
import StyleSignup from './SignupScreenStyle';
import StyleDashboard from './DashboardScreenStyle';
import StyleGCSiteLogin from './GC_Site_LoginScreenStyle';
import StylePreCheck from './PreCheckScreenStyle';
import ResetPasswordStyles from './ResetPasswordScreenStyle';
import TimelineStyle from './TimelineStyle'
import CopeStyle  from './CopeStyle'
export {
  StyleApp,
  StyleSplash,
  StyleLogin,
  StyleSignup,
  StyleDashboard,
  StyleGCSiteLogin,
  StylePreCheck,
  ResetPasswordStyles,
  TimelineStyle,
  CopeStyle   
};
