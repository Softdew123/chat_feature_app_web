
import {StyleSheet,Dimensions} from 'react-native'
import { FontSize, Colors } from '../Themes'

const win = Dimensions.get('window');
const autocompletePopHeight=win.height-100
export default StyleSheet.create({
	container: {
		flex: 1 
	},
	formContainer:{
		flex: 1,
		backgroundColor:Colors.mainBackgroun
	},
	formContainerInner:{
		flex:1,
		paddingLeft:10,
		paddingRight:10,
		paddingBottom:10,
		paddingTop:10
	},
	gridContainer:{
		flexDirection:'row'
	},
	gridColumn:{
		flex:1,
		marginLeft:10,
		marginRight:10 
	},
	gridColumn1:{
		flex:1,
		marginTop: 10,
		marginLeft:10,
		marginRight:10 
	},
	gridColumn2:{
		flexDirection: "row",
		marginTop: 10,
		marginRight:10,
		alignSelf: "flex-end"
	},
	dropdownContainerParent:{
		marginLeft:10,
		marginRight:10 
	},
	inputTitle:{
		color: Colors.blackLightShade, 
		paddingHorizontal:10,
		fontWeight:'600',
		marginBottom:6, 
		fontSize:FontSize.h8,
	},
	dropdownContainer:{
		backgroundColor:Colors.white,
		borderColor: Colors.darkGrey,
		borderWidth:0.5
	},
	dropdown:{  
		fontSize:FontSize.h7,
		color: Colors.darkGrey,
		height: 40,
	},
	inputField:{
		height: 40,
		color: Colors.darkGrey, 
		borderWidth:0.5,
		paddingLeft:10,
		borderColor: Colors.darkGrey,
		fontSize:FontSize.h7,
		backgroundColor:Colors.white
	},
	inputField1:{
		minHeight: 40,
		color: Colors.darkGrey, 
		borderWidth:0.5,
		paddingLeft:10,
		borderColor: Colors.darkGrey,
		fontSize:FontSize.h7,
		backgroundColor:Colors.white
	},
	AC_InputLookLike:{
		paddingBottom:10,
		paddingTop:10,
		color: Colors.darkGrey, 
		borderWidth:0.5,
		paddingLeft:10,
		borderColor: Colors.darkGrey,
		fontSize:FontSize.h7,
		backgroundColor:Colors.white	
	},
	AC_Container: {
		flex: 1,
		left: 0,
		position: 'absolute',
		right: 0,
		top: 6,
		zIndex: 1, 
	},
	AC_ListContainer:{
		padding:0,
		margin:0 
	},
	AC_List:{
		maxHeight:autocompletePopHeight,
		paddingTop:5,
		paddingBottom:5,
		marginLeft:-0.3,
		marginRight:-0.5
	},
	AC_ListItem:{ 
		paddingTop:10,
		paddingBottom:10,
		borderBottomWidth:0.5,
		borderColor: Colors.darkGrey
	},
	AC_DropdownText: {
		fontSize:FontSize.h7,
		color: Colors.darkGrey, 
		paddingLeft: 10,
		paddingRight:10, 
		paddingTop:13, 
		paddingBottom:13, 
		backgroundColor:Colors.white,
		borderBottomWidth:0.5,
		borderColor: Colors.darkGrey
	},
	additionMarginTop:{
		marginTop:10
	},
	buttonContainer: {
		borderRadius:25, 
		width:"100%", 
		paddingTop:13,
		paddingBottom:13,
		backgroundColor:Colors.orange
	},
	buttonContainers: {
		borderRadius:25, 
		width:"80%", 
		marginTop:25,
		paddingTop:10,
		paddingBottom:10,
		backgroundColor:Colors.orange
	},
	buttonText: {
		color: Colors.white,
		textAlign: 'center', 
		fontSize:FontSize.h7,
		fontWeight:'500'
	},
	uploadButtonContainer: {
		borderRadius:3, 
		width:"100%", 
		justifyContent: "center",
		alignItems: "center",
	},
	uploadButtonText: {
		color: Colors.white,
		textAlign: 'center', 
		fontSize:FontSize.h8,
		fontWeight:'500',
		padding: 10
	},
	snapshotFileText: {
		color: Colors.blackLightShade,
		fontSize:FontSize.h8,
		fontWeight:'300',
		padding: 3
	},
	bottomContainer:{  
		marginTop:20,
		marginBottom:20,
		marginLeft:5,
		marginRight:5,  
		alignItems:'center' 
	},
	bottomContainers:{  
		marginTop:20,
		marginBottom:20,
		marginLeft:5,
		marginRight:5,  
		alignItems:'center',
	},
	searchBarContainerer:{
		flexDirection:"row",
		position:'relative', 
		backgroundColor:Colors.colorStatusBar,
		height:56,
		borderColor: Colors.darkGrey,
		borderBottomWidth:0.5
	},
	backIconContainer:{
		width:50,
		justifyContent: 'center',
		paddingLeft:15
	},
	searchBoxContainer:{
		flex:1,
		position:"relative",
	},
	crossIconContainer:{
		width:55,
		justifyContent: 'center',
		paddingLeft:15  
	}, 
	popupContainerParent:{
		flex: 1,
		backgroundColor:Colors.mainBackgroun,
		flexDirection:'column'
	}, 
	flextListContainer:{
		flex:1
	},
	autoCompleteInput:{
		marginTop:8,
		height: 40,
		color: Colors.darkGrey, 
		paddingLeft:10, 
		fontSize:FontSize.h7,
		backgroundColor:Colors.white
	},
	lineDateTimeButton:{
		height:30,
		width:'100%'
	}, 
})