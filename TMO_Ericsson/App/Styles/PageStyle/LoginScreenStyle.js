
import { StyleSheet,Dimensions } from 'react-native'
import { FontSize, FontName, Colors } from '../Themes'
const win = Dimensions.get('window');
export default StyleSheet.create({ 
	container: {
		flex: 1 
	},
	bgImgContainer:{
		flex:1,		 
		justifyContent: 'center',
		alignItems: 'center',
	},
	formContainer: {
		height:win.height,
		width:win.width, 
		position:'absolute', 
		justifyContent:'center',
		alignItems:'center', 
		zIndex:1, 
	},
	formContainerInner:{ 
		marginTop:-100,
		width:"100%", 
		alignItems:'center',  
		position:'relative' 
	},
	logoImageStyle:{
		width:150, 
		height:150,
		marginBottom:10 
	},
	logoText:{
		color: Colors.white,
		textAlign: 'center', 
		marginTop: 1,
		fontSize:FontSize.h4
	}, 
	inputHeadingTitle:{
		fontSize:FontSize.h8,
		textAlign: 'left', 
		maxWidth:300,
		width:"100%",
		color: Colors.dimWhite,
		marginTop:10
	},
	inputField: {
		height: 45,
		color: Colors.white,
		maxWidth:300,
		width:"100%", 
		borderBottomWidth:0.5,
		borderColor: Colors.white,
		fontSize:FontSize.Standard,
	},
	buttonContainer: {
		borderRadius:25,
		backgroundColor:Colors.orange,
		maxWidth:300,
		width:'100%',
		padding:12,
	},
	buttonText: {
		color: Colors.white,
		textAlign: 'center', 
		fontSize:FontSize.h7,
		fontWeight:'500'
	},
	addMarginBottom:{
		marginBottom:30
	}
})