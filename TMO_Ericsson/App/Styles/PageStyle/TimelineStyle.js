

import {StyleSheet,Dimensions} from 'react-native'
import { FontSize, Colors } from '../Themes'
import { auth } from 'react-native-firebase';

const win = Dimensions.get('window');
const autocompletePopHeight=win.height-100
export default StyleSheet.create({
  
	container: {
		paddingTop: 23,
		marginBottom:20 
	 },
	 input: {
		 flex:1,
		borderColor: '#7a42f4',
		borderWidth: 1,
		height:40
	 },
	 textAreaContainer: {
		margin: 15,    
		borderColor: Colors.grey20,
		borderWidth: 1,
		padding: 2
	  },
	  textArea: {
		height: 100,
		justifyContent: "flex-start"
	  }, 
	 submitButton: {
		backgroundColor: Colors.orange ,
		padding: 10,
		margin: 15,
		height: 40,
	 },
	 submitButton1: {
		 
		backgroundColor: Colors.white ,
		
		padding: 10,
		margin: 15,
		height: 40,
	 },
	 submitButtonText:{
		color: 'white',
		textAlign:'center'
	 },
	 img_add: {
		 
		
		 // margin: 5,
		 height: 40,
		  width:40 ,
		 // borderColor: '#7a42f4',
		 // borderWidth: 1
	  },
	  hv:{
		margin:10,
		display:'flex',
		flexDirection:'column',
		justifyContent: 'space-between',
		flexWrap:'wrap'
	},     
	hv1:{
		margin:10,
		display:'flex',
		flexDirection:'row',
		justifyContent: 'space-between',
		flexWrap:'wrap'
	},     
	input1: {
		flex:2,
	   // margin: 15,
	   // width:'60%',    
	   borderColor: '#7a42f4',
	   borderWidth: 1
	},
	hv2:{
		margin:10,
		display:'flex',
		flexDirection:'row',
		justifyContent: 'space-between',
		flexWrap:'wrap'
	},     
})