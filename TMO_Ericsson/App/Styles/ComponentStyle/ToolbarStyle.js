 
import { StyleSheet} from 'react-native'
import {Colors, FontSize} from '../Themes'
export default StyleSheet.create({
	toolbar: {
		backgroundColor: Colors.lightOrange,
		height: 56,
	},
	 
	toolbar_IOS_InnerCon: {
		flex:1,
		flexDirection:'row',	
		alignItems: 'center',		
	},
	toolbar_IOS_Back:{
		 paddingLeft:15,
	},
	toolbar_IOS_buttonCon:{
		width:70,
		height:'100%', 
		justifyContent:'flex-start' 
	},
	toolbar_IOS_TextCon:{
		flex:1,
		alignItems: 'center', 
		justifyContent:'center' 
	},
	iosBack_button:{
		width:'100%',
		height:'100%'
	},
	 
	iosTitle:{
		color:Colors.black, 
		fontSize:FontSize.h4, 
		fontWeight:'600'
	},
	iosToolbarActionText:{
		color:Colors.darkBlue, 
		fontSize:FontSize.h4, 
		fontWeight:'600',
	},
	iosToolbarAction:{
		alignItems:'center',
		justifyContent: 'center',
		paddingRight:15
	}
})
