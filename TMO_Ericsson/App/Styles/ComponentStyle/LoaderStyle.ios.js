 
import { StyleSheet } from 'react-native'
import { FontSize, FontName, Colors } from '../Themes'
export default StyleSheet.create({
	overlay: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: Colors.modelBackground
	},
	parentContainer: {
		backgroundColor: Colors.white,
		height: 130,
		width: 130,
		borderRadius: 8
	},
	conentContainer: {
		flex: 1,
		flexDirection: 'column',
		alignItems: 'center'
	},
	loaderText: {
		paddingLeft: 5,
		marginRight: 5,
		fontSize: FontSize.large,
		fontFamily: FontName.ROBOTO_MEDIUM
	},
	loaderIcon: {
		paddingTop: 15,
		paddingBottom: 10,
	}
})