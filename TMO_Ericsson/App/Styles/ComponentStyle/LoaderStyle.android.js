 
import { StyleSheet } from 'react-native'
import { FontSize, FontName, Colors } from '../Themes'
export default StyleSheet.create({
	overlay: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: Colors.modelBackground
	},
	parentContainer: {
		backgroundColor: Colors.white,
		height: 80,
		width: '85%',
		maxWidth: 370
	},
	conentContainer: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center'
	},
	loaderText: {
		paddingLeft: 20,
		marginRight: 70,
		fontSize: FontSize.h5,
		fontFamily: FontName.ROBOTO_MEDIUM
	},
	loaderIcon: {
		paddingLeft: 20,
	}	 
})