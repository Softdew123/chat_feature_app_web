 
import { StyleSheet,Dimensions } from 'react-native'
import { FontSize, FontName, Colors } from '../Themes'
const { width } = Dimensions.get('window');
export default StyleSheet.create({

	offlineContainer: {
		backgroundColor: Colors.red,
		height: 30,
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'row',
		width,
		position: 'absolute',
		top: 30
	},
	offlineText: {
		color: Colors.white,
		fontSize: FontSize.Standard,
		fontFamily: FontName.ROBOTO_MEDIUM
	}
})