
import axios from 'axios';
import I18n from './i18n';
import {Dialog} from './Utils';
import Constant from './Constant';
 
/**	   
* @desc make get request service call
* @param: apiObject	: Json object with all the params 
* @return promise
*/
var GetRequest=(apiObject)=>{
	let configObject={
		method: "GET",
		url:Constant.Api.ServerDomain+apiObject.path
	}

	//append parameters to object
	if(apiObject.params){
		configObject.params=apiObject.params;
	}

	let timeout=null;
	let CancelToken = axios.CancelToken;
	let source = CancelToken.source();
	configObject.cancelToken=source.token;


	let clearTimeoutCheck=()=>{
		if(timeout){
			clearTimeout(timeout);
		}
	}

	return new Promise((resolve, reject) => {  
		axios(configObject).then(response => {
			clearTimeoutCheck();		
			resolve(response);
		}).catch((error) => {  
			if (axios.isCancel(error)) { 
				clearTimeoutCheck();
			}
			handleError(error,reject,apiObject); 
		});
			 
		//terminate api call after specified time duration	to set timeout
		timeout = setTimeout(()=>{
			source.cancel("Timeout");
		},Constant.Api.apiTimeoutDuration); 
	});
}



var PostRequest=(apiObject)=>{ 

	let configObject={
		method: "POST",
		url:Constant.Api.ServerDomain+apiObject.path,   
		headers:{
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	}

	// alert(Constant.Api.ServerDomain+apiObject.path)
	//append parameters to object
	if(apiObject.params){
		configObject.data=apiObject.params;
	}
	console.log("API= ",Constant.Api.ServerDomain+apiObject.path);

	let timeout=null;
	let CancelToken = axios.CancelToken;
	let source = CancelToken.source();
	configObject.cancelToken=source.token;


	let clearTimeoutCheck=()=>{
		if(timeout){
			clearTimeout(timeout);
		}
	}

	return new Promise((resolve, reject) => {  
		axios(configObject).then(response => { 


			clearTimeoutCheck();		
			resolve(response);
			// alert(response)
		}).catch((error) => {  
			if (axios.isCancel(error)) { 
				clearTimeoutCheck();
			}
			handleError(error,reject,apiObject); 
		});
			 
		//terminate api call after specified time duration	to set timeout
		timeout = setTimeout(()=>{
			source.cancel("Timeout");
		},Constant.Api.apiTimeoutDuration); 
	});
}

/**	   
* @desc handles different sort of server erros
* @param: error	: Json object of error thrown 
* @return none
*/
function handleError(error,reject,config){  
	//default error message
	let errorTitle=I18n.t('ERROR_MESSAGES.GENERAL_ERROR_TITLE');
	let errorMsg=I18n.t('ERROR_MESSAGES.GENERAL_ERROR_MSG');
	try{
		
		const {message} = error;
		//no active network error
		if(message==="Network Error"){			
			errorTitle='Error';
	  		errorMsg='Server not reachable';	 
		} 
		 
		// get error status code
		let status=null;
		if(error.response && error.response.status){
			status=error.response.status
		}
	 
		if(status){
			//unauthorize error
			if(status==401){
				errorTitle=I18n.t('ERROR_MESSAGES.UNAUTHORIZED_TITLE');
				errorMsg=I18n.t('ERROR_MESSAGES.UNAUTHORIZED_MSG');	 
			}else if(status==500 || status==501 || status==502 || status==503 || status==504){
				//server error
				errorTitle=I18n.t('ERROR_MESSAGES.SERVER_ERROR_TITLE');
				errorMsg=I18n.t('ERROR_MESSAGES.SERVER_ERROR_MSG');	
			}
		}  
	}catch(catchError){
		
	}
	
	reject(error);
	// alert(error);
	// config.hideErrorMsg is true dont show any error msg
	if(config.showErrorMsg){  
		Dialog.alertDialog(errorTitle,errorMsg,I18n.t('OK_BUTTON'),false,null).then(()=>{ 
			reject(error);
		})  
	} 
};

export { GetRequest, PostRequest};