
import { createStackNavigator, createSwitchNavigator, createAppContainer } from 'react-navigation';
import SplashScreen from '../Pages/Splash/Splash';
import LoginScreen from '../Pages/Login/Login';
import TimelineScreen from '../Pages/Timeline/Timeline';
import   CopPostScreen    from '../Pages/CopPost/CopPost';
import   CopPreScreen    from '../Pages/CopPre/CopPre';
import     UploadScreen    from '../Pages/Upload/Upload';
 
import SignupScreen from '../Pages/Signup/Signup';
import DashboardScreen from  '../Pages/Dashboard/Dashboard';
import GC_SITE_LOGIN_Screen from  '../Pages/GC_Site_Login/GC_Site_Login';
import EOS_Form from  '../Pages/EOS_Form/EOS_Form';
import GC_SITE_Req from  '../Pages/GC_Site_Req/GC_Site_Req';
import PreCheck_Screen from  '../Pages/PreCheck/PreCheck';
import PostCheckFormScreen from  '../Pages/PostCheck/PostCheck';
import ResetPasswordScreen from  '../Pages/ResetPassword/ResetPassword';
import CaptureNFSDSnapShot from  '../Pages/Camera/CaptureNFSDSnapShot';
import MultipleCapture from  '../Pages/Camera/MultipleCapture';
import Webviews  from  '../Pages/WebView/WebView';
import SlidingImageView  from  '../Pages/SlidingImageView/SlidingImageView';
import ChatRegForm  from  '../Pages/ChatSignup/SignUpChat';
import ChatLoginForm  from  '../Pages/ChatLogin/LoginChat';
import HomeScreenWindow  from  '../Pages/ChatHome/HomeScreen';
import AddnewRoom  from  '../Pages/AddNewRoom/AddRoom';
import AllRooms  from  '../Pages/RoomLists/RoomList';
import ChatWindow  from  '../Pages/ChatScreen/ChatRoom';
import IntegratedChatWindow  from  '../Pages/ChatScreen/IntegratedChatRoom';
import FileManager from '../Pages/FileHandler/FileManager';
import MessageImage from '../Pages/MessageImageViewer/MessageImage';
     
/**	   
login flow of application
* @param: none	 
*/
 const loginFlow=  createStackNavigator({ 
		SplashScreen: { screen: SplashScreen },
		LoginScreen: { screen: LoginScreen, navigationOptions:{ gesturesEnabled:false}}, 
		SignupScreen: { screen: SignupScreen, navigationOptions:{ gesturesEnabled:false}} ,
		PostCheckFormScreen: { screen: PostCheckFormScreen,navigationOptions:{ gesturesEnabled:false}} 
	},
	{ 
		headerMode: 'none',
		cardStyle: { shadowColor: 'transparent' },
		initialRouteName: 'SplashScreen',
	}
) 

/**	   
After successful login flow of application 
* @param: none	 
*/
 const mainFlow= createStackNavigator({




	DashboardScreen: 		{ screen: DashboardScreen,navigationOptions:{ gesturesEnabled:false}},
	GC_SITE_LOGIN_Screen: 	{ screen: GC_SITE_LOGIN_Screen,navigationOptions:{ gesturesEnabled:false}},
	GC_SITE_Req: 			{ screen: GC_SITE_Req,navigationOptions:{ gesturesEnabled:false}},
	TimelineScreen: 		{ screen:TimelineScreen ,navigationOptions:{ gesturesEnabled:false}},   
     CopPostScreen: 		{ screen:CopPostScreen ,navigationOptions:{ gesturesEnabled:false}},     
	 CopPreScreen: 			{ screen: CopPreScreen ,navigationOptions:{ gesturesEnabled:false}},   
	 UploadScreen: 			{ screen:  UploadScreen ,navigationOptions:{ gesturesEnabled:false}},       
	WebView_Screen: 		{ screen: Webviews,navigationOptions:{ gesturesEnabled:false}} ,
	PreCheck_Screen: 		{ screen: PreCheck_Screen,navigationOptions:{ gesturesEnabled:false}} ,
	PostCheckFormScreen: 	{ screen: PostCheckFormScreen,navigationOptions:{ gesturesEnabled:false}}, 
	ResetPasswordScreen: 	{ screen: ResetPasswordScreen,navigationOptions:{ gesturesEnabled:false}},
	CaptureNFSDSnapShot: 	{ screen: CaptureNFSDSnapShot,navigationOptions:{ gesturesEnabled:false}} ,
	EOS_Form: 				{ screen: EOS_Form,navigationOptions:{ gesturesEnabled:false}},
	SlidingImageView: 		{ screen: SlidingImageView,navigationOptions:{ gesturesEnabled:false}},
	MultipleCapture: 		{ screen: MultipleCapture,navigationOptions:{ gesturesEnabled:false}},
	ChatRegForm: 			{ screen: ChatRegForm,navigationOptions:{ gesturesEnabled:false}},
	ChatLoginForm: 			{ screen: ChatLoginForm,navigationOptions:{ gesturesEnabled:false}},
	HomeScreenWindow: 		{ screen: HomeScreenWindow,navigationOptions:{ gesturesEnabled:false}},
	AddnewRoom: 			{ screen: AddnewRoom,navigationOptions:{ gesturesEnabled:false}},
	AllRooms: 				{ screen: AllRooms,navigationOptions:{ gesturesEnabled:false}},
	ChatWindow: 			{ screen: ChatWindow,navigationOptions:{ gesturesEnabled:false}},
	IntegratedChatWindow: 	{ screen: IntegratedChatWindow,navigationOptions:{ gesturesEnabled:false}},
	FileManager:			{ screen: FileManager, navigationOptions:{gesturesEnabled:false}},
	MessageImage:			{ screen: MessageImage, navigationOptions:{gesturesEnabled:false}}
},
	
//    },
//    {	
// 	   initialRouteName: 'PreCheck_Screen',
// 	   headerMode: 'none',
// 	   cardStyle: { shadowColor: 'transparent' }
//    }

	{ 
		headerMode: 'none',
		cardStyle: { shadowColor: 'transparent' },
		initialRouteName: 'DashboardScreen',
	}
)  

/**	   
Root navigation
* @param: none	 
*/
const RootStack = createSwitchNavigator({ 
	AuthProcess: loginFlow,	
	MainApp:mainFlow  
} 	 
);

export default  AppNavigation = createAppContainer(RootStack);