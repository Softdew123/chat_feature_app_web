
// const PRODUCTION = "http://166.62.32.167/mPulseAPI/";
// const PRODUCTION = "https://mpulsenet.com/mPulseAPI/";
const PRODUCTION = "https://entmomcps.mpulsenet.com/mPulseAPI/"; // "http://localhost:14234/mPulseAPI/";
const IMAGEBASE   = "https://entmomcps.mpulsenet.com/";  
const CHAT_FILEUPLOAD_PRODUCTION = "https://entmomcps.mpulsenet.com/"; // "http://localhost:14234/mPulseAPI/";
// "http://35.190.152.223/mPulseAPI/";
// const PRE_POST_CHECK_URL = "https://mpulsenet.com";
const PRE_POST_CHECK_URL = "http://35.190.152.223";
const SIT = "http://demo.mpulsepag.com/mPulseAPI/";
const DEVELOPMENT = "http://demo.mpulsepag.com/mPulseAPI/";
export default {
	ICONS:{
		BACK_ICON: "md-arrow-back",
		CROSS_ICON: "md-close",
		THREEDOTS:"md-more",
		PLUSICON:"md-add",
		DATE:"md-calendar",
		TIME:"md-time",
		LOGOUT:"md-log-out",
		CHANGEPASSWORD:"md-unlock"
	}, 
	LOCAL_STORAGE: {
		LOGINRESPONSE:"LOGINRESPONSE"
	},
	PRE_POST_CHECK_URL: {
		URL:PRE_POST_CHECK_URL
	},
	Api: {
		apiTimeoutDuration:40000,
		ServerDomain: PRODUCTION,
		ChatFileUpload: CHAT_FILEUPLOAD_PRODUCTION,
		LOGINPAGE: {
			LOGIN: "GCLogin",
			ACAprrove:"AccountApproveReq"
		},
		SIGNUP:{
			GETCREWCOMP: "GetCrewCompany",
			GETACCOUNT:"GetAccount",
			GETMARKET:"GetMarket",  
			SIGNUP_API:"GCRegistration",
			GETVENDOR:"GetVendor",
			GetCrewCompany: "GetCrewCompany"
		},
		GetSites:"GetSites",
		GetTmoEngineer:"GetTmoEngineer",
		PreCheckReqNew:"PreCheckReqNew",
		PostCheckReq:"PostCheckReq",
		PreCheckReq:"PreCheckReq",
		GetWonPor: "GetWonPor",
		SiteLoginReq: "SiteLoginReq",
		SiteLogoutReq: "SiteLogoutReq",
		GetSitesNew:"GetSitesNew",
		PASSWORD_RESET:{
			RESET: "GCChangePassword"
		},
		DASHBAORD:{
			AppDashboard: "AppDashboard",
			GCSITELIST:"GetGCSiteLoginDetails",
			UPLOADCREWIMAGE:"UploadCrewImage"
		},
		LCLOGIN:{
			GCSITELOGIN:"GCSiteLogin",
			GETSITES:"GetSites",
			GETSOW:"GetPlannedSOW",
			GETSOWDetail: "GetSowDetails",
			ENGINEERLIST:"GetEngineer"
		},
		PRECHECK:{
			GETPRECHECK:"GetPrecheckDetails",
			
		}
		,
		POSTCHECKFORMDATA:{
			POSTCHECKAPI:"GetPostcheckDetails"
		},
		GETENGINEER:{
			GETENGINEERLIST:"GetEngineer"
		},

		SUBMITPOSTCHECKFORMDATA:{
			SUBMITPOSTCHECKFORM:"SaveUpdatePostcheckDetails"
		},

		POSTCHECKREQUEST:{
			POSTCHECKREQUESTAPI:"PostcheckRequest",
			GetPostCheckInfo: "GetPostCheckInfo",
			PostCheckReq: "PostCheckReq",
			GetPreCheckInfo: "GetPreCheckInfo",
			PreChekReq:"PreChekReq"
		},
		COPE:{
			SUBMITCOP:"SubmitCop1",
			COPLIST:"GetCop",
			PREPOSTCOP:"GetPrePostCop",
			IMAGEBASE:IMAGEBASE
			
		} ,
		Time:{
			SUBMITTIMELINE:"SubmitTimeline",
			
		},
		EOSFORM: {
			SUBMIT: "SaveEOSFilesDetails",
			GETDATA: "GetEOSFilesDetails",
		},
		CHAT: {
			FILEUPLOAD: "api/chat/UploadFile",
		}

	}
};