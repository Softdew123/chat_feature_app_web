
import { Platform, Alert, Linking } from "react-native";
import call from 'react-native-phone-call';
import I18n from './i18n';
/**	   
* @desc deviceInfo object contains all functions related to device API
*/
var DeviceInfo = {
	/**	   
	* @desc Check if application is installed in IOS device
	* @param none
	* @return boolean true if ios device 	 
	*/
	isiOS: function () {
		let isIOS_OS = false;
		if (Platform.OS == "ios") {
			isIOS_OS = true;
		}
		return isIOS_OS;
	},
	/**	   
	* @desc Check if application is installed in android device
	* @param none
	* @return boolean true if android device 	 
	*/
	isAndroid: function () {
		let isAndroid_OS = false;
		if (Platform.OS === "android") {
			isAndroid_OS = true;
		}
		return isAndroid_OS;
	},

	/**	   
	   return true if internet is connected
	   @param none
     * @return connectionInfo object of network state 	 
     */
	validateConnectionType: (connectionInfo) => {
		let states = {
			none: "none",
			wifi: "wifi",
			mobile: "cellular", // all mobile network like 2g/3g
			error: "unknown"
		};

		let isConnected = false;
		if (connectionInfo.type == states.wifi || connectionInfo.type == states.mobile) {
			isConnected = true;
		}
		return isConnected;
	},
};

/**	   
* @desc Logger object contains functions of all type of log. Disable log from here only for production
* build.
*/
var Logger = {
	/**	   
     * @param string:log : string message that to be logged
     */
	normal: (log) => {
		console.log(log);
	},
	debug: (log) => {
		console.debug(log);
	},
	error: (log) => {
		console.error(log);
	}
}
/**	   
* @desc Dialog object contains functions of to display alert, confirm dialog box.
*/
var Dialog = {
	/**
	   Display alert box and callback will be executed after pressing button in calling function
		* @param title:alert box title 	
		* @param msg: alertbox message  
		* @param buttonLabel: button label 
		* @param isCancelable: boolean to allow user to hide alert on clicking outside
		* @param param if want to return anything after clicking button 		 
		* @return Promise object 		
     */
	alertDialog: (title, msg, buttonLabel, isCancelable, param) => {
		return new Promise((resolve, reject) => {
			Alert.alert(
				title,
				msg,
				[
					{ text: buttonLabel, onPress: () => { resolve(param) } }
				],
				{ cancelable: isCancelable }
			)
		});
	},

	/**
	   Display confirm box
		* @param title:alert box title 	
		* @param msg: alertbox message  
		* @param buttonLabelArray: This will be an array of label for both cancel and ok button
		* @param isCancelable: boolean to allow user to hide alert on clicking outside
		* @param param if want to return anything after clicking button 
		* @return none	
     */
	confirmDialog: (title, msg, buttonLabelArray, isCancelable, param) => {
		return new Promise((resolve, reject) => {
			Alert.alert(
				title,
				msg,
				[
					{ text: buttonLabelArray[0], onPress: () => { resolve(true, param) } },
					{ text: buttonLabelArray[1], onPress: () => { resolve(false) } }
				],
				{ cancelable: isCancelable }
			)
		});
	}

}

var CommonMethods = {
	/**
	    * @desc Invoke native phone dialer facility for both Android & IOS.
		* @param phoneNumber: string	
		* @return none.	
    */
	invokeDialer: (phoneNumber) => {
		if (phoneNumber) {
			let args = {
				number: phoneNumber,
				prompt: false
			}
			call(args).catch()
		} else {
			Dialog.alertDialog(
				I18n.t('INVALID_PHONE_TITLE'),
				I18n.t('INVALID_PHONE_MESSAGE'),
				I18n.t('ALERT_MESSAGES.OK_BUTTTON'),
				false, null
			);
		}
	},
	/**
	    * @desc Invoke native email facility for both Android & IOS.
		* @param emailAddress: string	
		* @return none.	
    */
	invokeEmail: (emailAddress) => {
		if (CommonMethods.validateEmailAddress(emailAddress)) {
			Linking.openURL("mailto:?to=" + emailAddress).catch((err) => { console.error('An error occurred', err) }
			);
		} else {
			Dialog.alertDialog(
				I18n.t('INVALID_EMAIL_TITLE'),
				I18n.t('INVALID_EMAIL_MESSAGE'),
				I18n.t('ALERT_MESSAGES.OK_BUTTTON'),
				false, null
			);
		}
	},
	/**
	    * @desc Invoke native sms facility for both Android & IOS.
		* @param phoneNumber: string	
		* @param messageStr: string
		* @return none.	
    */
	invokeSMS: (phoneNumber, messageStr) => {
		if (phoneNumber) {
			if (!messageStr) {
				messageStr = "";
			}
			let seperator = "?";
			if (DeviceInfo.isiOS()) {
				seperator = "&";
			}
			Linking.openURL('sms:' + phoneNumber + '' + seperator + 'body=' + messageStr);
		} else {
			Dialog.alertDialog(
				I18n.t('INVALID_PHONE_TITLE'),
				I18n.t('INVALID_PHONE_MESSAGE'),
				I18n.t('ALERT_MESSAGES.OK_BUTTTON'),
				false, null
			);
		}
	},
	/**
	    * @desc Invoke Phone activity for both Android & IOS.
		* @param phoneNumber: string	
		* @return none.	
    */
	invokeFaceTime: (phoneNumber) => {
		if (phoneNumber) {
			Linking.openURL('facetime://' + phoneNumber);
		}else {
			Dialog.alertDialog(
				I18n.t('INVALID_PHONE_TITLE'),
				I18n.t('INVALID_PHONE_MESSAGE'),
				I18n.t('ALERT_MESSAGES.OK_BUTTTON'),
				false, null
			);
		}
	},
	/**
	    * @desc Capital first character of sentance.
		* @param str:string	
		* @return string with first letter capital.	
    */
	capitalize: (str) => {
		if (!str) {
			return "";
		}
		let words = str.toLowerCase().split(' ');
		let retVal = (words.length > 0) ? words.map(letter => letter.charAt(0).toUpperCase() + letter.slice(1)).join(' ') : '';

		return retVal;
	},
	/**
	    * @desc validate if provided email id is valid or not.
		* @param emailID:string	email id string
		* @return bool true if email id is valid.	
    */
	validateEmailAddress: (emailID) => {
		let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		if (reg.test(emailID) == false) {
			return false;
		}
		return true;
	},
	/**
	    * @desc convert date in dd/mm/yy format.
		* @param date:string date in another format
		* @return date in dd/mm/yy format
    */
	chaangeDateFormat:(date)=>{
		if(date){
			let today = new Date(date);
			let dd = today.getDate();
			let mm = today.getMonth() + 1; 
			let yyyy = today.getFullYear();

			if (dd < 10) {
				dd = '0' + dd;
			} 
			if (mm < 10) {
				mm = '0' + mm;
			} 
			return today = mm + '-' + dd + '-' + yyyy;
		}
		return "";
	},
	/**
	    * @desc convert time to am/pm format.
		* @param time:string time in another format
		* @return date in 12pm format
    */
	changeTimetoAmPMFormat:(time)=>{
		if(time){
			time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

			if (time.length > 1) { // If time format correct
				time = time.slice (1);  // Remove full string match value
				time[5] = +time[0] < 12 ? ' am'  : ' pm '; // Set AM/PM
				time[0] = +time[0] % 12 || 12; // Adjust hours

				return time[0]+" "+time[1]+time[2]+time[5]; // return adjusted time or original string
			}
			
		}
		return "";
	}
}

/**
All Global Variables must be declared here
*/
var GlobalVariable = {
	isInternetConnected: false,
	networkType: "",
	USERID:"",
	PASSWORD: "",
	MCPSMarketId:""
}

export { DeviceInfo, GlobalVariable, Dialog, Logger, CommonMethods };