
import I18n from 'react-native-i18n';
import en from './i18n/en.json';
import fr from './i18n/fr.json';
 
// Should the app fallback to English if user locale doesn't exists
I18n.fallbacks = true;
  
//Define the supported translations
I18n.translations = {
  en,fr
};

export default I18n;