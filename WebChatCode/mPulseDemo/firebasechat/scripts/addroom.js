﻿function createroom(roomName) {
    if (roomName.length > 0) {
        var usersRef = firebase.firestore().collection('SITES').doc(roomName);

        usersRef.get()
            .then((docSnapshot) => {
                if (docSnapshot.exists) {
                    usersRef.onSnapshot((doc) => {
                        // do stuff with the data
                    });
                } else {
                    usersRef.set({
                        name: roomName,
                        latestMessage: {
                            text: "You have joined the room " + roomName,
                            createdAt: new Date().getTime()
                        }
                    }) // create the document

                    firebase.firestore().collection("SITES").doc(roomName).collection("MESSAGES").add({
                        text: "You have joined the room " + roomName,
                        createdAt: new Date().getTime(),
                        system: true
                    });
                }
            });
        /*firebase.firestore()
            .collection("THREADS")
            .add({
                name: roomName,
                latestMessage: {
                    text: "You have joined the room " + roomName,
                    createdAt: new Date().getTime()
                }
            })
            .then(function () {
                console.log("doc successful");
            })
            .catch(function (error) {
                console.error("error writing doc", error);
            });*/
    }
    return roomName;
}
window.createroom = createroom;

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

var foo = getParameterByName('id');
console.log("id -> ", foo);
createroom(foo);

