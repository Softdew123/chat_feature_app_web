/**
 * Copyright 2018 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/*$(document).ready(function () {
    var a = $("#hdnChatValue").val();
    alert(a);
    //alert("Hi");
    // Handler for .ready() called.
});*/

'use strict';

// Use below website link to work upon for integration with web
// https://firebase.google.com/docs/web/setup#default-hosting-site 
// https://firebase.google.com/docs/web/setup#with-npm_1
// Firebase App (the core Firebase SDK) is always required and must be listed first
//import firebase from "firebase/app";
// If you are using v7 or any earlier version of the JS SDK, you should import firebase using namespace import
// import * as firebase from "firebase/app"

// If you enabled Analytics in your project, add the Firebase SDK for Analytics
//import "firebase/analytics";

// Add the Firebase products that you want to use
//import "firebase/auth";
//import "firebase/firestore";
//import "firebase/storage";
// Signs-in Friendly Chat.
// function signIn() {
//   //alert('TODO: Implement Google Sign-In');
//   // TODO 1: Sign in Firebase with credential from the Google user.
//    // Sign into Firebase using popup auth & Google as the identity provider.
//    var provider = new firebase.auth.GoogleAuthProvider();
//    firebase.auth().signInWithPopup(provider);
// }

// Signs-out of Friendly Chat.
// function signOut() {
//   // TODO 2: Sign out of Firebase.
//   // Sign out of Firebase.
//   firebase.auth().signOut();
// }

// Initiate firebase auth.
// function initFirebaseAuth() {
//   // TODO 3: Initialize Firebase.
//   // Listen to auth state changes.
//   firebase.auth().onAuthStateChanged(authStateObserver);
// }

// Returns the signed-in user's profile Pic URL.
function getProfilePicUrl() {
  // TODO 4: Return the user's profile pic URL.
  //return '/images/profile_placeholder.png';// firebase.auth().currentUser.photoURL || 
}

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

var userid = parseInt(getParameterByName('userid'));
var username = getParameterByName('username');
var useremail = getParameterByName('useremail');
var messageDeviceToken = "";
var deviceTokenSet = new Set();

saveMessagingDeviceToken();

// Returns the signed-in user's display name.
// function getUserName() {
//   // TODO 5: Return the user's display name.
//   //return firebase.auth().currentUser.displayName;
// }

// Returns true if a user is signed-in.
// function isUserSignedIn() {
//   // TODO 6: Return true if a user is signed-in.
//   return !!firebase.auth().currentUser;
// }

// name: getUserName(),
//     text: messageText,
//     profilePicUrl: getProfilePicUrl(),
//     createdAt: firebase.firestore.FieldValue.serverTimestamp()

// Saves a new message on the Firebase DB.
function saveMessage(messageText) {
    console.log("messageText ", messageDeviceToken);
  // TODO 7: Push a new message to Firebase.
    return firebase.firestore().collection('SITES')
        .doc(getParameterByName('siteid'))//this.state.thread._id
  .collection('MESSAGES')
  .add({
    text: messageText,
    createdAt: new Date().getTime(),
    user: {
        _id: userid,//firebase.auth().currentUser.uid,
        name: username,
        email: useremail,//firebase.auth().currentUser.email
        deviceId: messageDeviceToken
    }
  }).catch(function(error) {
    console.error('Error writing new message to database', error);
  });
}

// Loads chat messages history and listens for upcoming ones.
function loadMessages() {

    var usersRef = firebase.firestore().collection('SITES').doc(getParameterByName('siteid'));

    usersRef.get()
        .then((docSnapshot) => {
            if (docSnapshot.exists) {
                usersRef.onSnapshot((doc) => {
                    // do stuff with the data
                });
            } else {
                usersRef.set({
                    name: getParameterByName('siteid'),
                    latestMessage: {
                        text: "You have joined the room " + getParameterByName('siteid'),
                        createdAt: new Date().getTime()
                    }
                }) // create the document

                firebase.firestore().collection("SITES").doc(getParameterByName('siteid')).collection("MESSAGES").add({
                    text: "You have joined the room " + getParameterByName('siteid'),
                    createdAt: new Date().getTime(),
                    system: true
                });
            }
        });

  // TODO 8: Load and listens for new messages.
    // Create the query to load the last 12 messages and listen for new ones.
    var query = firebase.firestore()
        .collection('SITES')
        .doc(getParameterByName('siteid')).collection('MESSAGES').orderBy('createdAt', 'asc');

// Start listening to the query.
query.onSnapshot(function(snapshot) {
snapshot.docChanges().forEach(function(change) {
if (change.type === 'removed') {
deleteMessage(change.doc.id);
} else {
var message = change.doc.data();
var userdoc = change.doc.data().user;

/*console.log("change.doc.id ",change.doc.id);
console.log("message.createdAt ",message.createdAt);
console.log("message.email ",emaildoc);
console.log("message.text ",message.text);
console.log("message.img ",message.img);
console.log("message.video ",message.video);
console.log("message.filename ",message.filename);
console.log("message.application ",message.application);*/
    if (userdoc && userdoc.email) {
        console.log("my deviceId ", change.doc.data());//deviceId
        if (userid != userdoc._id) {
            deviceTokenSet.add(userdoc.deviceId);
        console.log("deviceTokenSet ", deviceTokenSet);
        console.log("Array set ", Array.from(deviceTokenSet));
        }
        displayMessage(change.doc.id, message.createdAt, userdoc.name,
    message.text, message.img, message.video, message.filename, message.application);//, message.profilePicUrl, message.imageUrl
}

}
});
});
}

function sendNotifications(message) {
    console.log("sendNotifications", Array.from(deviceTokenSet));
    var notification = {
        'title': getParameterByName('siteid'),
        'body': message,
        'icon': '~/Images/Logo.png'
    };
fetch('https://fcm.googleapis.com/fcm/send', {
    method: 'POST',
    headers: {
        Authorization: 'key=' + 'AAAABHositI:APA91bEuweKrYFSl6Gbx8viHnNCTweYUhWmwUsyQ_5Tbcfmqc-IFdBPE2i0QLI76F3Z5wOj8A8xckaFuFhnIFn3JGxMPDwdxFeuT8add96Xjw-q7lBZ_zFoxM-LGgzS8YoP4M8o18ef4',
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        'notification': notification,
        'registration_ids': Array.from(deviceTokenSet)
    })
}).then(function (response) {
    console.log(response);
/*    "e6pcoumjt0Y:APA91bFHkZisgSqKNEzG59I8AoI40IPWxMjrROy-Z3rbkFBm-Vq3c-s9LifA2lqJkMReQLr14-Zys2orLeKaushvHkpD_nvzCZnMTh8Tn127gHEaDA59Z3xoyUlkx9JY9dUcTTuYSFrA"*/
}).catch(function (error) {
    console.error(error);
})
}

// Saves a new message containing an image in Firebase.
// This first saves the image in Firebase storage.
/*function saveImageMessage(file, filetype) {
      firebase.storage().ref()
      .child(`uploads/${new Date().getTime()}-${file.name}`)
      .put(file)
      .then(function(filesnapshot){
          return filesnapshot.ref.getDownloadURL().then((url) => {
            console.log("img url "+ url, " filetype - "+ filetype);
            saveDocumentUrl(url, filetype, file.name);
          })
      }).catch(function(error) {
          console.error('Error writing new message to database', error);
      });

}*/
async function saveImageMessage(file, filetype) {
    var formData = new FormData(); // Currently empty
    formData.append('file', file)
    console.log(formData);
    console.log(file);
    const response = await fetch('http://localhost:14186/api/chat/UploadFile', {
        method: 'POST',
        body: formData
    });
    var data = await response.json();
    console.log(data);
    console.log(await response);
    console.log(response.headers);
    saveDocumentUrl(data.data, filetype, data.message);
}
function saveDocumentUrl(documentUrl, filetype, fileName) {
    console.log("messageDeviceToken ", messageDeviceToken);
    var doctype = "New Document received";
  var document = "";
    if (filetype.match('video.*')) {
        doctype = "New Video Received";
    document = {
      video: documentUrl,
      createdAt: new Date().getTime(),
      user: {
          _id: userid,//firebase.auth().currentUser.uid,
          name: username,
          email: useremail,//firebase.auth().currentUser.email
          deviceId: messageDeviceToken
      }
    };
    } else if (filetype.match('image.*')) {
        doctype = "New Image Received";
    document = {
      img: documentUrl,
      createdAt: new Date().getTime(),
      user: {
          _id: userid,//firebase.auth().currentUser.uid,
          name: username,
          email: useremail,//firebase.auth().currentUser.email
          deviceId: messageDeviceToken
      }
    };
    } else{
        doctype = "New Document Received";
    document = {
      application: documentUrl,
      filename: fileName,
      createdAt: new Date().getTime(),
      user: {
          _id: userid,//firebase.auth().currentUser.uid,
          name: username,
          email: useremail,//firebase.auth().currentUser.email
          deviceId: messageDeviceToken
      }
    };
  }
  console.log("document ", document);
    firebase.firestore().collection('SITES')
        .doc(getParameterByName('siteid'))//this.state.thread._id
  .collection('MESSAGES')
        .add(document);

    sendNotifications(doctype);
}

// Saves the messaging device token to the datastore.
function saveMessagingDeviceToken() {
  // TODO 10: Save the device token in the realtime datastore
    firebase.messaging().getToken().then(function (currentToken) {
        if (currentToken) {
            console.log('Got FCM device token:', currentToken)
            messageDeviceToken = currentToken;
        } else {
            // Need to request permissions to show notifications.
            requestNotificationsPermissions();
        }
    }).catch(function (error) {
        console.error('Unable to get messaging token.', error);
    });
}

// Requests permissions to show notifications.
function requestNotificationsPermissions() {
  // TODO 11: Request permissions to send notifications.
    console.log('Requesting notifications permission...');
    firebase.messaging().requestPermission().then(function () {
        // Notification permission granted.
        saveMessagingDeviceToken();
    }).catch(function (error) {
        console.error('Unable to get permission to notify.', error);
    });
}

function getSavedDeviceId() {
    console.log("got saved token", window.localStorage.getItem('deviceId'));
    return window.localStorage.getItem('deviceId');
}

// Triggered when a file is selected via the media picker.
function onMediaFileSelected(event) {
  event.preventDefault();
  var file = event.target.files[0];
  console.log("File Type -> ",file.type);

  // Clear the selection in the file picker input.
  imageFormElement.reset();

  // Check if the file is an image.
  // if (!file.type.match('video.*')) {
  //   var data = {
  //     message: 'You can only share images',
  //     timeout: 2000
  //   };
  //   signInSnackbarElement.MaterialSnackbar.showSnackbar(data);
  //   return;
  // }
  // Check if the user is signed-in
  //if (checkSignedInWithMessage()) {
    saveImageMessage(file,file.type);
  //}
}

// Triggered when the send new message form is submitted.
function onMessageFormSubmit(e) {
  e.preventDefault();
  // Check that the user entered a message and is signed in.
  //if (messageInputElement.value && checkSignedInWithMessage()) {
    saveMessage(messageInputElement.value).then(function() {
      // Clear message text field and re-enable the SEND button.
      resetMaterialTextfield(messageInputElement);
      toggleButton();
    });
  //}
    console.log("sending 2");
    sendNotifications(messageInputElement.value);
}

// Triggers when the auth state change for instance when the user signs-in or signs-out.
// function authStateObserver(user) {
//   if (user) { // User is signed in!
//     // Get the signed-in user's profile pic and name.
//     var profilePicUrl = getProfilePicUrl();
//     var userName = "Hello world";//getUserName();

//     // Set the user's profile pic and name.
//     userPicElement.style.backgroundImage = 'url(' + addSizeToGoogleProfilePic(profilePicUrl) + ')';
//     userNameElement.textContent = userName;

//     // Show user's profile and sign-out button.
//     userNameElement.removeAttribute('hidden');
//     userPicElement.removeAttribute('hidden');
//     signOutButtonElement.removeAttribute('hidden');

//     // Hide sign-in button.
//     signInButtonElement.setAttribute('hidden', 'true');

//     // We save the Firebase Messaging Device token and enable notifications.
//     saveMessagingDeviceToken();
//   } else { // User is signed out!
//     // Hide user's profile and sign-out button.
//     userNameElement.setAttribute('hidden', 'true');
//     userPicElement.setAttribute('hidden', 'true');
//     signOutButtonElement.setAttribute('hidden', 'true');

//     // Show sign-in button.
//     signInButtonElement.removeAttribute('hidden');
//   }
// }

// Returns true if user is signed-in. Otherwise false and displays a message.
// function checkSignedInWithMessage() {
//   // Return true if the user is signed in Firebase
//   if (isUserSignedIn()) {
//     return true;
//   }

//   // Display a message to the user using a Toast.
//   var data = {
//     message: 'You must sign-in first',
//     timeout: 2000
//   };
//   signInSnackbarElement.MaterialSnackbar.showSnackbar(data);
//   return false;
// }

// Resets the given MaterialTextField.
function resetMaterialTextfield(element) {
  element.value = '';
  element.parentNode.MaterialTextfield.boundUpdateClassesHandler();
}

// Template for messages.
var MESSAGE_TEMPLATE =
    '<div class="message-container">' +
      '<div class="spacing"><div class="pic"></div></div>' +
      '<div class="message" style="color:black";></div>' +
      '<div class="name" style="color:blue";></div>' +
      '<div class="date" style="font-size:80%; text-align:right; padding-right:30px;"></div>' +
    '</div>';

// Adds a size to Google Profile pics URLs.
// function addSizeToGoogleProfilePic(url) {
//   if (url.indexOf('googleusercontent.com') !== -1 && url.indexOf('?') === -1) {
//     return url + '?sz=150';
//   }
//   return url;
// }

// A loading image URL.
var LOADING_IMAGE_URL = 'https://www.google.com/images/spin-32.gif?a';

// Delete a Message from the UI.
function deleteMessage(id) {
  var div = document.getElementById(id);
  // If an element for that message exists we delete it.
  if (div) {
    div.parentNode.removeChild(div);
  }
}

function createAndInsertMessage(id, createdAt) {
  const container = document.createElement('div');
  container.innerHTML = MESSAGE_TEMPLATE;
  const div = container.firstChild;
  div.setAttribute('id', id);

  // If createdAt is null, assume we've gotten a brand new message.
  // https://stackoverflow.com/a/47781432/4816918
  createdAt = createdAt ? new Date(createdAt) : Date.now();
  div.setAttribute('createdAt', createdAt);

  // figure out where to insert new message
  const existingMessages = messageListElement.children;
  if (existingMessages.length === 0) {
    messageListElement.appendChild(div);
  } else {
    let messageListNode = existingMessages[0];

    while (messageListNode) {
      const messageListNodeTime = messageListNode.getAttribute('createdAt');

      if (!messageListNodeTime) {
        throw new Error(
          `Child ${messageListNode.id} has no 'createdAt' attribute`
        );
      }

      if (messageListNodeTime > createdAt) {
        break;
      }

      messageListNode = messageListNode.nextSibling;
    }

    messageListElement.insertBefore(div, messageListNode);
  }

  return div;
}

// Displays a Message in the UI.
function displayMessage(id, createdAt, name, text, imageUrl, videoUrl, fileName, fileName_application) {// , picUrl
  var div = document.getElementById(id) || createAndInsertMessage(id, createdAt);

  // profile picture
  // if (picUrl) {
  //   div.querySelector('.pic').style.backgroundImage = 'url(' + addSizeToGoogleProfilePic(picUrl) + ')';
  // }

    div.querySelector('.name').textContent = name;
    let d = new Date(createdAt);
/*    let ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
    let mo = new Intl.DateTimeFormat('en', { month: 'short' }).format(d);
    let da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
    let hr = new Intl.DateTimeFormat('en', { hour: 'numeric' }).format(d);
    let min = new Intl.DateTimeFormat('en', { minute: '2-digit' }).format(d);*/
    div.querySelector('.date').textContent = `${d.getMonth() + 1}-${d.getDate()}-${d.getFullYear()}  ${d.getHours()}:${d.getMinutes()}`;
  var messageElement = div.querySelector('.message');

  if (text) { // If the message is text.
    messageElement.textContent = text;
    // Replace all line breaks by <br>.
    messageElement.innerHTML = messageElement.innerHTML.replace(/\n/g, '<br>');
  } else if (imageUrl) { // If the message is an image.
    var image = document.createElement('img');
    image.addEventListener('load', function() {
      messageListElement.scrollTop = messageListElement.scrollHeight;
    });
      image.src = imageUrl;
    messageElement.innerHTML = '';
    messageElement.appendChild(image);
  } else if(videoUrl){
    var video = document.createElement('video');
    video.setAttribute("type" , "application/x-shockwave-flash");
    video.setAttribute("width", "300");
    video.setAttribute("height", "300");
    video.setAttribute("name", "wmode");
    video.setAttribute("src", videoUrl);
    video.setAttribute("controls", true);
    video.addEventListener('load', function() {
      messageListElement.scrollTop = messageListElement.scrollHeight;
    });
    messageElement.appendChild(video);
  } else if(fileName_application){             
    var form = document.createElement("form");
    var label = document.createElement("label");
    var blanklabel = document.createElement("label");
    var download = document.createElement("button");
    label.textContent = fileName;
    blanklabel.textContent = " ";
    download.type = "button";
    download.innerHTML = "Download";
    
    form.append(label);
    form.append(blanklabel);
    form.append(download); 
    messageElement.appendChild(form);

    download.onclick = function () {
      downloadfile(messageElement,fileName, fileName_application);
    };
    }
    console.log("deviceTokenSet ", deviceTokenSet);
  // Show the card fading-in and scroll to view the new message.
  setTimeout(function() {div.classList.add('visible')}, 1);
  messageListElement.scrollTop = messageListElement.scrollHeight;
  messageInputElement.focus();
}

function downloadfile(messageElement, fileName, fileName_application){
  var element = document.createElement('a');
  element.setAttribute('href', fileName_application);
  element.setAttribute('download', fileName);

  messageElement.appendChild(element);
            
  element.click();
}

// Enables or disables the submit button depending on the values of the input
// fields.
function toggleButton() {
  if (messageInputElement.value) {
    submitButtonElement.removeAttribute('disabled');
  } else {
    submitButtonElement.setAttribute('disabled', 'true');
  }
}

// Checks that the Firebase SDK has been correctly setup and configured.
function checkSetup() {
  if (!window.firebase || !(firebase.app instanceof Function) || !firebase.app().options) {
    window.alert('You have not configured and imported the Firebase SDK. ' +
        'Make sure you go through the codelab setup instructions and make ' +
        'sure you are running the codelab using `firebase serve`');
  }
}

// Checks that Firebase has been imported.
//checkSetup();

// Shortcuts to DOM Elements.
var messageListElement = document.getElementById('messages');
var messageFormElement = document.getElementById('message-form');
var messageInputElement = document.getElementById('message');
var submitButtonElement = document.getElementById('submit');
var imageButtonElement = document.getElementById('submitImage');
var imageFormElement = document.getElementById('image-form');
var mediaCaptureElement = document.getElementById('mediaCapture');

// Saves message on form submit.
messageFormElement.addEventListener('submit', onMessageFormSubmit);

// Toggle for the button.
messageInputElement.addEventListener('keyup', toggleButton);
messageInputElement.addEventListener('change', toggleButton);
messageInputElement.addEventListener('change', toggleButton);

// Events for image upload.
imageButtonElement.addEventListener('click', function(e) {
  e.preventDefault();
  mediaCaptureElement.click();
});
mediaCaptureElement.addEventListener('change', onMediaFileSelected);
document.title = "Site-ID : " +getParameterByName('siteid');
document.getElementById('site_name').innerHTML = "Site-ID : "+getParameterByName('siteid');
// initialize Firebase
//initFirebaseAuth();
//getSavedDeviceId();
// TODO: Enable Firebase Performance Monitoring.

// We load currently existing chat messages and listen to new ones.
loadMessages();

// initFirebaseAuth - 465
