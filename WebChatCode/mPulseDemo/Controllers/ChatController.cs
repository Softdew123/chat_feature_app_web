﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.IO;
using AttributeRouting.Web.Mvc;
using System.Threading.Tasks;

namespace NEDA.Controllers
{
    public class ChatController : ApiController
    {
        [HttpPost]
        [Route("api/Chat/UploadFile")]
        public async Task<HttpResponseMessage> UploadFile()
        {
            var ctx = HttpContext.Current;
            var root = ctx.Server.MapPath("~/Chat_Files");
            string webpath = "http://localhost:14186/Chat_Files/";
            var provider =
                new MultipartFormDataStreamProvider(root);
            var webname="name";
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                APIMessage message = new APIMessage();
                message.status = 0;
                await
                    Request.Content.ReadAsMultipartAsync(provider);
                var name = "";
                foreach (var file in provider.FileData)
                {
                    name = file.Headers.ContentDisposition.FileName;

                    name = name.Trim('"');
                    string extension = Path.GetExtension(name);
                    string newFileName = Guid.NewGuid() + extension;
                    webname = newFileName;
                    webpath = webpath + newFileName;
                    var localfilenaame = file.LocalFileName;
                    var filepath = Path.Combine(root, newFileName);
                    File.Move(localfilenaame, filepath);
                }
                message.status = 1;
                message.message = name;// "file uploaded successfully";
                message.data = webpath;
                response = Request.CreateResponse(HttpStatusCode.OK, message);
            }
            catch(Exception e)
            {
                response = Request.CreateResponse(HttpStatusCode.ServiceUnavailable, "Error");
            }
            return response; 
        }
    }
}
