﻿var GCDashboardControllerNew = function ($scope, $http, GCDashboardNewService, $location, $timeout, $filter, $q) {
    $scope.currentPage = 1;

    $scope.tablerowMainId = '';
    $scope.IsAuthonticated = true;
    $scope.IsTmobUser = false;

    $scope.UserType = $('#hdnUserType').val();
    $scope.UserScope = $('#hdnUserScope').val();
    if ($scope.UserType != 1) {
        $scope.IsAuthonticated = false;
        if ($scope.UserScope.indexOf('TMOBUser') > -1)
            $scope.IsTmobUser = true;
        else if ($scope.UserScope.indexOf('RAN') > -1)
            $scope.IsAuthonticated = true;
    }

    $scope.copPreId = 0;
    $scope.copPostId = 0;
    $scope.timelineId = 0;
    $scope.crewSiteId = "";
    $scope.crewImgId = 0;
    $scope.Copflag = 0;
    $scope.modalCop = false;
    $scope.SelectedAccount = '';
    $scope.SelectedMarket = '';
    $scope.SelectedPOR = '';

    ////filter site
    $scope.SearchSiteListNew = [];
    $scope.GetSiteList = function () {
        GCDashboardNewService.GetSiteIDList().then(function (d) {
            $scope.SiteList = d.data;
            angular.forEach(d.data, function (item) {
                var has = $scope.SearchSiteListNew.indexOf(item.site_id) > -1;
                if (!has) $scope.SearchSiteListNew.push(item.site_id)

            });
        }, function (err) {
        });
    }
    $scope.GetSiteList();

    $scope.filterSiteListNew = function (userInput) {
        $scope.SelectedSitenee = userInput;
        var filter = $q.defer();
        var normalisedInput = userInput.toLowerCase();

        var filteredArray = $scope.SearchSiteListNew.filter(function (obj) {
            return obj.toLowerCase().indexOf(normalisedInput) === 0;
        });
        filter.resolve(filteredArray);
        return filter.promise;
    };

    $scope.SiteListSelectedNew = function (item) {
        $scope.Selected_Main_ID = item;
        $scope.ddl_change_main_id()
    };

    $scope.GetAccountList = function () {
        GCDashboardNewService.GetAccountList().then(function (d) {
            $scope.AccountList = d.data;
        }, function (err) {
            console.log(err);
        });
    };
    $scope.GetAccountList();

    $scope.ddl_account_Change = function () {
        $scope.SelectedMarket = '';
        if ($scope.SelectedAccount != '' && $scope.SelectedAccount != undefined) {
            GCDashboardNewService.GetMarketList($scope.SelectedAccount).then(function (d) {
                $scope.MarketList = d.data;
            }, function (err) {
                console.log(err);
            });
        }
        else
            $scope.MarketList = [];
    }

    $scope.GetEngineerList = function () {
        GCDashboardNewService.GetEngineerList().then(function (d) {
            $scope.EngineerList = d.data;
        }, function (err) {
            console.log(err);
        });
    };
    $scope.GetEngineerList();

    $scope.GetMarketCurrentTime = function () {
        GCDashboardNewService.GetMarketCurrentTime().then(function (d) {
            $scope.MarketTimeList = d.data;
            $scope.updateTime();
        }, function (err) {
            console.log(err);
        });
    }
    var d;

    $scope.updateTime = function () {
        $timeout(function () {
            angular.forEach($scope.MarketTimeList, function (obj) {
                var date = moment(obj.current_time, 'MM/DD/YYYY HH:mm:ss').add(1, 'seconds').format('MM/DD/YYYY HH:mm:ss');;
                obj.current_time = date;
            });
            $scope.$apply();
            $scope.updateTime();
        }, 1000);
    };

    $scope.GetMarketCurrentTime();

    //upload excel
    $scope.uploadFile = function (file) {
        var fileExtension = ['xlsx', 'xls'];
        if ($.inArray(file[0].name.split('.').pop().toLowerCase(), fileExtension) == -1) {
            jAlert('Upload excel file only.');
            $('#FileUpload1').val('');
            $scope.UploadFile = '';
        }
        else {
            $scope.UploadFile = file[0];
        }
        $scope.$apply();
    }
    $scope.SaveUploadFile = function (IsValid) {
        $scope.loading = true;
        if (IsValid) {

            var fileData = new FormData();
            fileData.append('UploadFile', $scope.UploadFile);
            fileData.append('remarks', $scope.txt_remarks);
            fileData.append('m_upload_type_id', 7);
            $http.post('/Home/UploadScheduleData', fileData, {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            }).then(function (d) {
                if (d.data.Status == 1) {
                    jAlert(d.data.Message, 'Message');
                    $scope.getRandashboarddata();
                    $scope.loading = false;
                }
                else if (d.data.Status == 2)
                    jAlert(d.data.Message, 'Alert');
                else if (d.data.Status == 4)
                    jAlert(d.data.Message, 'Alert');
                else if (d.data.Status == 3) {
                    jConfirm1(d.data.Message, 'Confirm', function (r) {
                        if (r) {
                            for (var i = 0; i < d.data.PaceExistList.length; i++) {
                                d.data.PaceExistList[i]["RF_Approved_MW_Time"] = moment(d.data.PaceExistList[i]["RF_Approved_MW_Time"]).format('YYYY-MM-DD H:mm');
                                d.data.PaceExistList[i]["RF_Approved_End_Time"] = moment(d.data.PaceExistList[i]["RF_Approved_End_Time"]).format('YYYY-MM-DD H:mm');
                            }
                            GCDashboardNewService.OverwriteScheduleDetails(d.data.PaceExistList).then(function (d) {
                                $scope.getRandashboarddata();
                            }, function (err) {
                                $scope.loading = false;
                            });
                        }
                    }, 'Overwrite', 'Cancel');
                }
                else jAlert(d.data.Message, 'Alert');

                $scope.getRandashboarddata();
                $('#FileUpload1').val('');
                $scope.UploadFile = '';
                $scope.txt_remarks = '';
                $scope.FormUpload.$setPristine();

                $scope.loading = false;
            }, function (result) {
                jAlert(result);
            });
        }
    }

    $scope.workOrderMultiList = []
    $scope.SelectedworkOrderMultiList = []

    $scope.selectGroup = [];

    var sowarr = [];
    $scope.SelectedsowMultiList = [];

    sowarr = [{ id: "4", label: "E911-Call Test" }, { id: "1", label: "CX" }, { id: "2", label: "IX" }, { id: "3", label: "Troubleshooting" }];

    $scope.sowMultiList = [];
    $scope.sowMultiList = sowarr;


    $scope.multi = function () {

        $("#example-optgroup-buttonText").multiselect('dataprovider', $scope.selectGroup);

        $('#example-optgroup-buttonText').multiselect({
            buttonText: function (options, select) {

                // First consider the simple cases, i.e. disabled and empty.
                if (this.disabledText.length > 0
                        && (this.disableIfEmpty || select.prop('disabled'))
                        && options.length == 0) {

                    return this.disabledText;
                }
                else if (options.length === 0) {
                    return this.nonSelectedText;
                }

                var $select = $(select);
                var $optgroups = $('optgroup', $select);
                //console.log("textgrp", $optgroups)
                var delimiter = this.delimiterText;
                var text = '';
                var $slt = [];
                // Go through groups.
                $optgroups.each(function () {
                    var $selectedOptions = $('option:selected', this);
                    var $options = $('option', this);
                    var key = $(this).attr('label');
                    var val = ""
                    $selectedOptions.each(function () {
                        val += $(this).text() + delimiter;
                        //console.log("text", text)
                    });

                    $slt.push({ [key]: val });
                    //console.log("text", $slt)

                    if ($selectedOptions.length == $options.length) {
                        text += $(this).attr('label') + delimiter;
                    }
                    else {
                        $selectedOptions.each(function () {
                            text += $(this).text() + delimiter;
                            //console.log("text", text)
                        });
                    }
                });

                var $remainingOptions = $('option:selected', $select).not('optgroup option');
                $remainingOptions.each(function () {
                    text += $(this).text() + delimiter;
                });

                return text.substr(0, text.length - 2);
            }
        });
    }

    $scope.yourEvents = {
        onItemSelect: function (item) {
            var site_id = $scope.Selected_Main_ID
            $.ajax({
                type: 'get',
                url: 'Home/getPOR?mainid=' + site_id + '&&workId=' + item.id,
                contentType: 'application/json',
                dataType: 'json',
                success: function (d) {
                    let d1 = []
                    if (Array.isArray(d.Table)) {
                        d.Table.map((itm, i) => {
                            d1.push({ label: itm.por, value: item.id + "," + itm.por })
                        })
                    }
                    $scope.selectGroup.push({
                        label: item.id, children: d1
                    })
                    $scope.multi()
                }, error: function (error) {
                }
            });
        }, onItemDeselect: function (item1) {
            let temp = $scope.selectGroup;
            $scope.selectGroup = []
            temp.map((item, i) => {
                if ((item1.id) != (item.label)) {
                    $scope.selectGroup.push(item)
                }
            })
            $scope.multi()
        }
    };

    ///end
    //------------------------------------start by Shiv-------------------------------------------

    $scope.ddl_change_main_id = function () {
        $scope.Scheduled_Account = '';
        $scope.Scheduled_Market = '';
        var site_id = $scope.Selected_Main_ID;
        debugger;
        $scope.workOrderMultiList = []
        $scope.selectGroup = []
        $scope.multi()
        $scope.SelectedworkOrderMultiList = []
        //$scope.loading = true;

        $.ajax({
            type: 'GET',
            url: '/Home/getdataonSiteId',
            data: { mainid: site_id },
            contentType: 'application/json',
            dataType: 'json',
            success: function (d) {
                marketacc = JSON.parse(d["marketacc"]);
                workorderlist = JSON.parse(d["workorderlist"]);
                porlist = JSON.parse(d["porlist"]);

                let SearchGCList = []
                let arr = [];
                if (Array.isArray(workorderlist.Table)) {
                    workorderlist.Table.map((item, i) => {
                        var has = SearchGCList.indexOf(item.work_order_no) > -1;
                        if (!has) {
                            SearchGCList.push(item.work_order_no)
                            arr.push({ id: item.work_order_no, label: item.work_order_no })
                        }
                    })
                }
                $scope.workOrderMultiList = arr;
                $('#txt_Market').val(marketacc.Table[0]["market"]);
                $('#txt_Account').val(marketacc.Table[0]["account"]);
                $('#txt_Cabinet_IDs').val(marketacc.Table[0]["cabinetid"]);


                $scope.workorderlist = workorderlist.Table;
                $scope.porlist = porlist.Table;

                $scope.loading = false;
            }, error: function (error) {
                console.log("error=>", error);
                $scope.loading = false;
            }
        });
    }

    $scope.ddl_Cxvendor_change = function () {
        var vendorId = $('#ddl_Cxvendor_ID').val();
        if (vendorId != "") {
            $scope.loading = true;
            $.ajax({
                type: 'GET',
                url: '/Home/getvendorDetails',
                data: { vendorId: vendorId },
                contentType: 'application/json',
                dataType: 'json',
                success: function (d) {
                    data = d.Table;
                    if (data.length > 0) {
                        $('#txt_CX_Crew_Ld_Cont_No').val(data[0]["crew_lead_contact_no"]);
                        $('#txt_CX_Crew_Ld_Name').val(data[0]["crew_lead_name"]);
                        $('#Scheduled_CX_Crew_Ld_Company').val(data[0]["crew_lead_company"]);
                        $('#txt_CX_Crew_Ld_E_mail_ID').val(data[0]["crew_lead_email_id"]);
                    }
                    else {
                        $('#txt_CX_Crew_Ld_Cont_No').val('');
                        $('#txt_CX_Crew_Ld_Name').val('');
                        $('#Scheduled_CX_Crew_Ld_Company').val('');
                        $('#txt_CX_Crew_Ld_E_mail_ID').val('');
                    }
                }, error: function (error) {
                    alert("error");
                }
            });
            $scope.loading = false;
        }
    }

    $scope.ddl_Ixvendor_change = function () {
        var vendorId = $('#ddl_Ixvendor_ID').val();
        if (vendorId != "") {
            $scope.loading = true;
            $.ajax({
                type: 'GET',
                url: '/Home/getvendorDetails',
                data: { vendorId: vendorId },
                contentType: 'application/json',
                dataType: 'json',
                success: function (d) {
                    data = d.Table;
                    if (data.length > 0) {
                        $('#txt_IX_Crew_Ld_Contact_No').val(data[0]["crew_lead_contact_no"]);
                        $('#txt_IX_Crew_Lead_Name').val(data[0]["crew_lead_name"]);
                        $('#txt_IX_Crew_Ld_Company').val(data[0]["crew_lead_company"]);
                        $('#IX_Crew_Ld_E_mail_ID').val(data[0]["crew_lead_email_id"]);
                    }
                    else {
                        $('#txt_IX_Crew_Ld_Contact_No').val('');
                        $('#txt_IX_Crew_Lead_Name').val('');
                        $('#txt_IX_Crew_Ld_Company').val('');
                        $('#IX_Crew_Ld_E_mail_ID').val('');
                    }
                }, error: function (error) {
                    alert("error");
                }
            });
            $scope.loading = false;
        }
    }

    $scope.example1model = [];
    $scope.example1data = [];
    $scope.example1model22 = [];
    $scope.example1data22 = [];

    $scope.example1model = [];
    $scope.example1data = [];
    $scope.example1model22 = [];
    $scope.example1data22 = [];
    $scope.currentSchedule_ID = ''

    $scope.openModelCrew = function (id, copPreId, copPostId, timeLineId, siteId, crewImgId,POR) {

        $scope.loading = true;
        GCDashboardNewService.getcxixinfo(id).then(function (d) {
            $scope.copPreId = copPreId;
            $scope.copPostId = copPostId;
            $scope.timelineId = timeLineId;
            $scope.crewSiteId = siteId;
            $scope.crewImgId = crewImgId;
            $scope.SelectedPOR = POR;
            console.log("crewImgId", $scope.crewImgId);
            let cxdata = JSON.parse(d.data.cxdata);
            let ixdata = JSON.parse(d.data.ixdata);

            $scope.cxData = cxdata.Table;
            $scope.ixdata = ixdata.Table;
            $scope.loading = false;
        }, function (err) {
            $scope.loading = false;
        });
        $scope.currentSchedule_ID = id
        $scope.modalcrew = true;
    }
    $scope.openModelNewsite = function () {
        $scope.modelAddnewsite = true;

        //bind Main Id
        $.ajax({
            type: 'POST',
            url: '/Home/GetMain_IDListNew',
            data: {},
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                $scope.Main_IDList = data;
            }, error: function (error) {
                alert("error");
            }
        });

        $scope.TMO_Deployment_Manager_List = [];
        $.ajax({
            type: 'POST',
            url: '/Home/mobile_deployment_managers',
            data: {},
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                debugger;
                if (Array.isArray(data)) {
                    data.map((item, i) => {
                        $scope.TMO_Deployment_Manager_List.push({ readableName: item.name, id: item.Id })
                    })
                }
            }, error: function (error) {
                console.log("error");
            }
        });

        $.ajax({
            type: 'GET',
            url: '/Home/getvendorList',
            data: {},
            contentType: 'application/json',
            dataType: 'json',
            success: function (d) {
                $scope.VendorList = d.Table;
                //console.log(d.Table);
            }, error: function (error) {
                alert("error");
            }
        });

        $scope.Selected_Main_ID = '';
        $scope.Selected_Work_Order_Number = '';
        $scope.Selected_POR = '';
        $('#reasonMOP').hide();
    }
    //Divyam added 07/22
    $scope.btn_chat = function (id, siteid) {

        GCDashboardNewService.GetChatDetails().then(function (d) {
            //debugger;
            var SessionVal = d.data.split("#");
            if (d.data == "ex") {
                jAlert("Error occured.", 'Alert');
            }
            else {
                //jAlert(id + " " + d.data, 'Alert');
                window.open("http://localhost:14186/firebasechat/chatroom.html?id=" + id + "&siteid=" + siteid + "&userid=" + SessionVal[0] + "&username=" + SessionVal[1] + "&useremail=" + SessionVal[2], '_blank', '');
            }

        }, function (err) {
            jAlert(id + " " + err, 'Alert');
        });

    }
    //Divyam added 07 / 22
    $scope.SelectedManagerNew = "";
    $scope.managerListSelected = function (item) {
        $scope.SelectedManagerNew = item.readableName;
        $scope.Selected_TMO_Deployment_Manager = item.id;
    };

    $scope.filterManagerList = function (userInput) {
        debugger;
        var filter = $q.defer();
        var normalisedInput = userInput.toLowerCase();

        var filteredArray = $scope.TMO_Deployment_Manager_List.filter(function (obj) {
            //console.log("filter", obj)
            return obj.readableName.toLowerCase().indexOf(normalisedInput) === 0;
        });
        filter.resolve(filteredArray);
        return filter.promise;
    };

    //------------------------------------end by Shiv---------------------------------------------

    $scope.Postcheck_Click = function (ObjLogin) {

        if (ObjLogin.postcheck_request_date == null) {
            var obj = ObjLogin;
            $scope.loading = true;
            GCDashboardNewService.SavePostcheckRequestDate(ObjLogin.id).then(function (d) {
                if (d.data.Status == 1) {
                    $location.url('/postcheck/?LoggedID=' + obj.id);
                    if (!$scope.$$phase) $scope.$apply()
                }
                $scope.loading = false;
            }, function (err) {
                console.log(err);
            });
        }
        else {
            $location.url('/postcheck/?LoggedID=' + ObjLogin.id);
            if (!$scope.$$phase) $scope.$apply()
        }
    }

    $scope.Export_Click = function () {
        $scope.modalShownDownloadDisclaimer = true;
        ////var table = $('#table').clone();
        ////table.find('tr.row-items').remove();
        ////var table1 = table.tableToJSON({ ignoreHiddenRows: false });
        //var uri = 'data:application/vnd.ms-excel;base64,'
        //   , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
        //   , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        //   , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }

        //var table = document.getElementById("table");
        //var filters = $('.ng-table-filters').remove();
        //var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML };
        //$('.ng-table-sort-header').after(filters);
        //var url = uri + base64(format(template, ctx));
        //var a = document.createElement('a');
        //a.href = url;
        //a.download = 'RAN_Support_Dashboard_Report.xls';
        //a.click();
    }

    $scope.Export_Click_RANDAR = function () {
        $scope.loading = true;
        $scope.PageUrl = $location.absUrl();
        $scope.ControllerName = "gcdashboardNew";
        $scope.ActivityType = "Export to Excel";
        $scope.PageName = "RAN Support Dashboard";
        $scope.MailSend = 1;
        $scope.SearchFor = $scope.Account + "," + $scope.market;// "NA";
        $scope.PageRemarks = "mPulse Log Tracking - RAN Support Dashboard";
        GCDashboardNewService.Export_logTracker($scope.ControllerName, $scope.ActivityType, $scope.PageName, $scope.MailSend, $scope.PageUrl, $scope.SearchFor, $scope.PageRemarks).then(function (d) {
            $scope.modalShownDownloadDisclaimer = false;
            $scope.loading = false;
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }

            var table = document.getElementById("table");
            var filters = $('.ng-table-filters').remove();
            var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML };
            $('.ng-table-sort-header').after(filters);
            var url = uri + base64(format(template, ctx));
            var a = document.createElement('a');
            a.href = url;
            a.download = 'RAN_Support_Dashboard_Report.xls';
            a.click();
        });
    }

    //$scope.Export_Click = function () {
    //    var table1 = [];
    //    angular.forEach($scope.ranDashboarddataForExcel, function (x) {
    //        var Item = {
    //            "Scheduled Date": x.Scheduled_Date,
    //            "Entry Mode": x.status == 1 && x.RF_Approval == 3 ? "Scheduled/Login Awaited" : x.status == 1 && x.RF_Approval == 2 ? "Scheduled/Cancelled" :
    //               x.status == 0 && x.RF_Approval == 3 ? "On the Fly/Login Awaited" : x.status == 1 && x.RF_Approval == 1 ? "Scheduled/Ongoing" :
    //               x.status == 0 && x.RF_Approval == 1 ? "On the Fly/Ongoing" : x.status == 0 && x.RF_Approval == 2 ? "On the Fly/Cancelled" : "",
    //            "Service Affecting": x.Service_Affecting == 1 ? "Yes" : x.Service_Affecting == 2 ? "No" : "",
    //            "RF Approval": x.RF_Approval == 1 ? "Approved" : x.RF_Approval == 2 ? "Not Approved" : x.RF_Approval == 3 ? "Pending" : "",
    //            "Main ID": x.site_id,
    //            "PAG Trouble Ticket ": x.ttid == 0 ? "NA" : x.ttid,
    //            "Planned SoW": $scope.splitSow(x.Planned_SoW),
    //            "DAY MOP or NIGHT MOP": x.Day_MOP_Night_MOP == 1 ? "Day MOP" : x.Day_MOP_Night_MOP == 2 ? "Night MOP" : "",
    //            "RF Approved NW Time": x.RF_Approved_MW_Time,
    //            "Login Time PreChecks Requested(Mob-App)": x.mobileLoginTime,
    //            "Log in time PreCheck Delivered": x.precheck_deliver_date,
    //            "Logout Time PostChecks Requested(Phone e-mail or Mobile-App)": x.mobileLogoutTime,
    //            "Log out time Postchecks delivered": x.postcheck_deliver_date

    //        }
    //        table1.push(Item);
    //    });

    //    var opts = [{ sheetid: 'RAN Support Dashboard', header: true }];
    //    var res = alasql.promise('SELECT INTO XLSX("RAN_Support_Dashboard.xlsx",?) FROM ?', [opts, [table1]])
    //     .then(function (data) {

    //     }).catch(function (err) {
    //         console.log(err);
    //     });
    //}

    $scope.searchData = function () {
        $scope.loading = true;
        $scope.i = 1;
        $scope.getRandashboarddata();
        $scope.pageChanged(1);
    };

    $scope.i = 0;
    $scope.getRandashboarddata = function () {
        var account = 0;
        var market = 0;
        var startdate = 0;
        var enddate = 0;

        if ($scope.i == 1) {
            account = $scope.SelectedAccount;
            market = $scope.SelectedMarket;
            startdate = $scope.txt_start_date;
            enddate = $scope.txt_end_date;
        }
        $scope.loading = true;
        $.ajax({
            type: 'GET',
            url: '/Home/getRandashboarddata',
            data: { "account": account, "market": market, "startdate": startdate, "enddate": enddate },
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                if (data != "0") {
                    $scope.ranDashboarddata = data.Table;
                    $scope.$broadcast('refreshFixedColumns');
                    setTimeout(function () {
                        $scope.$broadcast('refreshFixedColumns');
                    }, 200);
                    $scope.ranDashboarddataForExcel = data.Table;
                }
                $scope.loading = false;
            }, error: function (error) {
                $scope.loading = false;
                console.log("Error-->", error);
            }
        });
    };

    $scope.CreateRandashboardJson = function (data) {
        var mainArr = [];
        var nestedArr = [];
        var currentId = 0;

        for (var i = 0; i < data.length; i++) {
            mainId = data[i]["site_id"];

            if (currentId != mainId) {
                if (nestedArr.length > 0) {
                    mainArr[mainArr.length - 1]["child"] = nestedArr;
                    nestedArr = [];
                }
                mainArr.push(data[i]);

            } else {
                nestedArr.push(data[i]);
            }
            currentId = data[i]["site_id"];
        }
        if (nestedArr.length > 0) {
            mainArr[mainArr.length - 1]["child"] = nestedArr;
            nestedArr = [];
        }
        $scope.ranDashboarddata = mainArr;
    }
    $scope.pageChanged = function (page) {
        $scope.$broadcast('refreshFixedColumns');
    }


    //$scope.currentDatetime = new Date();
    var currentdate = new Date();

    $scope.currenttimeHr = currentdate.getHours();

    $scope.currenttimeHr1 = $scope.currenttimeHr + 6;
    if ($scope.currenttimeHr1 > 24) {
        $scope.currenttimeHr1 = $scope.currenttimeHr1 - 24;
    }

    $scope.currenttimeHr2 = $scope.currenttimeHr1 + 6;
    if ($scope.currenttimeHr2 > 24) {
        $scope.currenttimeHr2 = $scope.currenttimeHr2 - 24;
    }
    $scope.scheduleId = "";
    $scope.selectedmainId = "0";

    $scope.GetSiteAllDetails = function (site_id, scheduleid, mainid) {
        $scope.scheduleId = scheduleid;
        $scope.selectedmainId = mainid;
        $scope.loading = true;
        $scope.siteDetails = 1;
        GCDashboardNewService.GetSiteAllDetailsNew(site_id).then(function (d) {
            if (d.data && Array.isArray(d.data) && d.data.length > 0) {
                $scope.SiteAllDetails = d.data;
                $scope.newId = d.data[0].ob_gc_login.tbl_site_id;
                $scope.modalShown = true;
                $scope.loading = false;
            }
            else {
                $scope.loading = false;
                alert("No Record Found");
            }
        }, function (err) {
            console.log(err);
        });
    }

    $scope.precheckNew = function (schedule_id) {
        $location.url('/precheckNew/?schedule_id=' + schedule_id + '&&type=0');
    };
    $scope.crewNew = function () {
        $location.url('/precheckNew/?schedule_id=' + $scope.currentSchedule_ID + '&&type=1');
    };

    $scope.calltestNew = function (schedule_id) {
        $location.url('/addcalltest/?schedule_id=' + schedule_id);
    };

    $scope.postcheckNew = function (schedule_id) {
        $location.url('/precheckNew/?schedule_id=' + schedule_id + '&&type=2');
    };

    //View Map for all sites
    $scope.ViewAllSiteOnMap = function (flag) {
        var serviceAff = $('#ddlimpact').val();

        var check = []
        var allSite = "";
        for (var i = 0; i < $scope.ranDashboarddataForExcel.length; i++) {
            allSite = allSite + $scope.ranDashboarddataForExcel[i].schedule_id + ",";
        }

        var allsites = [];
        if (allSite != "") {
            $scope.modelMapAll = true;
            GCDashboardNewService.getMapSiteInfo(allSite).then(function (d) {
                allsites = d.data.Table;

                $scope.Markers = [];
                for (j = 0; j < allsites.length; j++) {
                    var items = { "id": "", "left_hrs": 0, "status": 0 };
                    if (flag == 0 && serviceAff == 1) {
                        if (allsites[j].siflag == 1) {
                            $scope.Markers.push({
                                "item": items,
                                "title": allsites[j].site_id + "-" + allsites[j].site_address,
                                "lat": allsites[j].latitude,
                                "lng": allsites[j].longitude,
                                "description": allsites[j].site_address,
                                "loginFlag": allsites[j].loginFlag,
                                "siteStatus": allsites[j].siteStatus,
                                "siflag": allsites[j].siflag,
                                "nsiflag": allsites[j].nsiflag,
                                "ttFlag": allsites[j].ttFlag
                            });
                        }
                    }
                    else if (flag == 0 && serviceAff == 2) {
                        if (allsites[j].nsiflag == 1) {
                            $scope.Markers.push({
                                "item": items,
                                "title": allsites[j].site_id + "-" + allsites[j].site_address,
                                "lat": allsites[j].latitude,
                                "lng": allsites[j].longitude,
                                "description": allsites[j].site_address,
                                "loginFlag": allsites[j].loginFlag,
                                "siteStatus": allsites[j].siteStatus,
                                "siflag": allsites[j].siflag,
                                "nsiflag": allsites[j].nsiflag,
                                "ttFlag": allsites[j].ttFlag
                            });
                        }
                    }
                    else if (flag == 1 && serviceAff == 1) {
                        if (allsites[j].siteStatus != 1 && allsites[j].siflag == 1) {
                            $scope.Markers.push({
                                "item": items,
                                "title": allsites[j].site_id + "-" + allsites[j].site_address,
                                "lat": allsites[j].latitude,
                                "lng": allsites[j].longitude,
                                "description": allsites[j].site_address,
                                "loginFlag": allsites[j].loginFlag,
                                "siteStatus": allsites[j].siteStatus,
                                "siflag": allsites[j].siflag,
                                "nsiflag": allsites[j].nsiflag,
                                "ttFlag": allsites[j].ttFlag
                            });
                        }
                    }
                    else if (flag == 1 && serviceAff == 2) {
                        if (allsites[j].siteStatus != 1 && allsites[j].nsiflag == 2) {
                            $scope.Markers.push({
                                "item": items,
                                "title": allsites[j].site_id + "-" + allsites[j].site_address,
                                "lat": allsites[j].latitude,
                                "lng": allsites[j].longitude,
                                "description": allsites[j].site_address,
                                "loginFlag": allsites[j].loginFlag,
                                "siteStatus": allsites[j].siteStatus,
                                "siflag": allsites[j].siflag,
                                "nsiflag": allsites[j].nsiflag,
                                "ttFlag": allsites[j].ttFlag
                            });
                        }
                    }
                    else if (flag == 2 && serviceAff == 1) {
                        if (allsites[j].siteStatus == 1 && allsites[j].siflag == 1) {
                            $scope.Markers.push({
                                "item": items,
                                "title": allsites[j].site_id + "-" + allsites[j].site_address,
                                "lat": allsites[j].latitude,
                                "lng": allsites[j].longitude,
                                "description": allsites[j].site_address,
                                "loginFlag": allsites[j].loginFlag,
                                "siteStatus": allsites[j].siteStatus,
                                "siflag": allsites[j].siflag,
                                "nsiflag": allsites[j].nsiflag,
                                "ttFlag": allsites[j].ttFlag
                            });
                        }
                    }
                    else if (flag == 2 && serviceAff == 2) {
                        if (allsites[j].siteStatus == 1 && allsites[j].nsiflag == 1) {
                            $scope.Markers.push({
                                "item": items,
                                "title": allsites[j].site_id + "-" + allsites[j].site_address,
                                "lat": allsites[j].latitude,
                                "lng": allsites[j].longitude,
                                "description": allsites[j].site_address,
                                "loginFlag": allsites[j].loginFlag,
                                "siteStatus": allsites[j].siteStatus,
                                "siflag": allsites[j].siflag,
                                "nsiflag": allsites[j].nsiflag,
                                "ttFlag": allsites[j].ttFlag
                            });
                        }
                    }
                    else if (flag == 2) {
                        if (allsites[j].siteStatus == 1) {
                            $scope.Markers.push({
                                "item": items,
                                "title": allsites[j].site_id + "-" + allsites[j].site_address,
                                "lat": allsites[j].latitude,
                                "lng": allsites[j].longitude,
                                "description": allsites[j].site_address,
                                "siteStatus": allsites[j].siteStatus,
                                "loginFlag": allsites[j].loginFlag,
                                "siflag": allsites[j].siflag,
                                "nsiflag": allsites[j].nsiflag,
                                "ttFlag": allsites[j].ttFlag
                            });
                        }
                    }
                    else if (flag == 1) {
                        if (allsites[j].siteStatus != 1) {
                            $scope.Markers.push({
                                "item": items,
                                "title": allsites[j].site_id + "-" + allsites[j].site_address,
                                "lat": allsites[j].latitude,
                                "lng": allsites[j].longitude,
                                "description": allsites[j].site_address,
                                "siteStatus": allsites[j].siteStatus,
                                "loginFlag": allsites[j].loginFlag,
                                "siflag": allsites[j].siflag,
                                "nsiflag": allsites[j].nsiflag,
                                "ttFlag": allsites[j].ttFlag
                            });
                        }
                    }
                    else {
                        $scope.Markers.push({
                            "item": items,
                            "title": allsites[j].site_id + "-" + allsites[j].site_address,
                            "lat": allsites[j].latitude,
                            "lng": allsites[j].longitude,
                            "description": allsites[j].site_address,
                            "siteStatus": allsites[j].siteStatus,
                            "loginFlag": allsites[j].loginFlag,
                            "siflag": allsites[j].siflag,
                            "nsiflag": allsites[j].nsiflag,
                            "ttFlag": allsites[j].ttFlag
                        });
                    }
                }
                console.log("mapdata", $scope.Markers);
                //Setting the Map options
                if ($scope.Markers != 0) {
                    $scope.MapOptions = {
                        center: new google.maps.LatLng($scope.Markers[0].lat, $scope.Markers[0].lng),
                        zoom: 8,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                }
                else {
                    $scope.InfoWindow = new google.maps.InfoWindow();
                    //$scope.Latlngbounds = new google.maps.LatLngBounds();
                    $scope.Map = new google.maps.Map(document.getElementById("map_regionNew"), $scope.MapOptions);
                }
                //Initializing the InfoWindow, Map and LatLngBounds objects.
                $scope.InfoWindow = new google.maps.InfoWindow();
                //$scope.Latlngbounds = new google.maps.LatLngBounds();
                $scope.Map = new google.maps.Map(document.getElementById("map_regionNew"), $scope.MapOptions);

                //Looping through the Array and adding Markers.                
                for (var i = 0; i < $scope.Markers.length; i++) {
                    var data = $scope.Markers[i];
                    var myLatlng = new google.maps.LatLng(data.lat, data.lng);

                    //Initializing the Marker object.
                    marker = (data.siteStatus == 1 && data.ttFlag == 0) ? new google.maps.Marker({
                        icon: 'https://maps.google.com/mapfiles/ms/icons/green-dot.png',
                        position: myLatlng,
                        map: $scope.Map,
                        title: data.title + ""
                    }) : (data.siteStatus == 1 && data.ttFlag == 1) ? new google.maps.Marker({
                        icon: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                        position: myLatlng,
                        map: $scope.Map,
                        title: data.title + ""
                    }) : (data.loginFlag == 1 && data.nsiflag == 1) ? new google.maps.Marker({
                        icon: 'https://maps.google.com/mapfiles/ms/icons/orange-dot.png',
                        position: myLatlng,
                        map: $scope.Map,
                        title: data.title + ""
                    }) : new google.maps.Marker({
                        position: myLatlng,
                        map: $scope.Map,
                        title: data.title + ""
                    });

                    //Adding InfoWindow to the Marker.
                    (function (marker, data) {
                        google.maps.event.addListener(marker, "click", function (e) {
                            $scope.InfoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.description + "</div>");
                            $scope.InfoWindow.open($scope.Map, marker);
                        });
                    })(marker, data);
                }
            });
        }
        else
            jAlert("No Site Details Found", "message");
    }

    $scope.ddl_MapStatus_Change = function () {
        flag = $('#ddlMapstatus').val();
        console.log("flag", flag.value);
        $scope.ViewAllSiteOnMap(flag);
    }

    $scope.example1model = [];
    $scope.example1data = [];
    $scope.example1model22 = [];

    $scope.example1data22 = [{ id: "All", label: "All" }, { id: "L700", label: "L700" }, { id: "L850", label: "L850" }, { id: "L1900", label: "L1900" }, { id: "L2100", label: "L2100" }, { id: "AWS3", label: "AWS3" }, { id: "U1900", label: "U1900" }, { id: "G1900", label: "G1900" }, { id: "4th Sector", label: "4th Sector" }, { id: "NSD", label: "NSD" }, { id: "NSD G1900", label: "NSD G1900" }, { id: "NSD U1900", label: "NSD U1900" }, { id: "NSD U2100", label: "NSD U2100" }, { id: "NSD L700", label: "NSD L700" }, { id: "NSD L850", label: "NSD L850" }, { id: "NSD L1900", label: "NSD L1900" }, { id: "NSD L2100", label: "NSD L2100" }, { id: "NSD AWS3", label: "NSD AWS3" }, { id: "MicroWave", label: "MicroWave" }, { id: "Small Cell", label: "Small Cell" }, { id: "5G", label: "5G" }];

    $scope.Selected_Main_ID = '';
    $scope.Selected_Work_Order_Number = '';
    $scope.Selected_POR = '';

    $scope.AddNewSiteClick = function () {

        var curDate = new Date();
        $scope.loading = true;
        if ($('#start1').val() == "" || $('#start1').val() == 'undefined') {
            $('#start1').val("00");
        }
        if ($('#start2').val() == "" || $('#start2').val() == 'undefined') {
            $('#start2').val("00");
        }
        if ($('#end1').val() == "" || $('#end1').val() == 'undefined') {
            $('#end1').val("00");
        }
        if ($('#end2').val() == "" || $('#end2').val() == 'undefined') {
            $('#end2').val("00");
        }

        var startTime = $('#start1').val() + ':' + $('#start2').val();
        var endTime = $('#end1').val() + ':' + $('#end2').val();


        var sow = $scope.SelectedsowMultiList;
        var sowlist = "";

        if (Array.isArray(sow)) {
            sow.map((item, i) => {
                if (sowlist != "") {
                    sowlist = sowlist + ',' + item["id"];
                }
                else {
                    sowlist = item["id"];
                }
            })
        }
        if (sowlist == "") {
            jAlert("Select Planned SOW");
            $scope.loading = false;
            return;
        }
        var ontheFlylist = [];
        if ($('#por_txt').val() != "") {
            let porList = JSON.parse($('#por_txt').val());
            if (Array.isArray(porList)) {
                porList.map((itm1, i) => {
                    if (itm1 && Array.isArray(itm1[itm1.key])) {
                        itm1[itm1.key].map((itm2, j) => {
                            let portemp = ""
                            let arr = itm2.split(",");
                            if (Array.isArray(arr) && arr.length == 2) {
                                portemp = arr[1]
                            }

                            var items = {
                                Id: 0,
                                Scheduled_Date: $('#txt_Scheduled_Date').val(),
                                site_id: $scope.Selected_Main_ID,
                                Work_Order_No: itm1.key,
                                POR: portemp,
                                T_Mobile_TicketID: $('#txt_T_Mobile_Ticket_Id').val(),
                                Account: $('#txt_Account').val(),
                                Market: $('#txt_Market').val(),
                                Cabinet_IDs: $('#txt_Cabinet_IDs').val(),
                                CX_Crew_Vendor: $('#ddl_Cxvendor_ID').val(),
                                CX_Crew_Ld_Cont_No: $scope.changeNumber($('#txt_CX_Crew_Ld_Cont_No').val()),
                                CX_Crew_Ld_Name: $('#txt_CX_Crew_Ld_Name').val(),
                                CX_Crew_Ld_Company: $('#Scheduled_CX_Crew_Ld_Company').val(),
                                CX_Crew_Ld_E_mail_ID: $('#txt_CX_Crew_Ld_E_mail_ID').val(),

                                IX_Crew_Vendor: $('#ddl_Ixvendor_ID').val(),
                                IX_Crew_Ld_Cont_No: $scope.changeNumber($('#txt_IX_Crew_Ld_Contact_No').val()),
                                IX_Crew_Ld_Name: $('#txt_IX_Crew_Lead_Name').val(),
                                IX_Crew_Ld_Company: $('#txt_IX_Crew_Ld_Company').val(),
                                IX_Crew_Ld_E_mail_ID: $('#IX_Crew_Ld_E_mail_ID').val(),
                                Service_Affecting: $('#ddl_Service_Affecting').val(),
                                Day_MOP_Night_MOP: $('#ddl_Day_MOP_or_Night_MOP').val(),
                                Reason_for_Day_MOP: $('#txt_Reason_for_Day_MOP').val(),
                                RF_Approval: $('#ddl_RF_Approval').val(),
                                RF_Approved_MW_Time: startTime,
                                RF_Approved_End_Time: endTime,
                                TMO_Deployment_Manager: $scope.Selected_TMO_Deployment_Manager,
                                Planned_SoW: sowlist,
                            }
                            ontheFlylist.push(items);
                        })
                    }
                });
            }

            if (ontheFlylist.length > 0) {
                GCDashboardNewService.SaveUpdateschedulingNew(ontheFlylist).then(function (d) {
                    if (d.data.Status == 1) {
                        jAlert(d.data.Message, "Message");
                        $scope.getRandashboarddata();
                        $scope.ClearAddMarket();
                        $scope.modelAddnewsite = false;
                        $scope.loading = false;
                    }
                    else if (d.data.Status == 2) {
                        jAlert(d.data.Message, "Message");
                        $scope.getRandashboarddata();
                        $scope.ClearAddMarket();
                        $scope.modelAddnewsite = false;
                        $scope.loading = false;
                    }
                    else if (d.data.Status == 0) {
                        jAlert("Site Not Scheduled", "Alert");
                        $scope.loading = false;
                    }
                }, function (err) {
                    console.log(err);
                    $scope.loading = false;
                });
            }
        }
        else {
            jAlert("Please Select MainID,Work Order No and POR", 'Alert');
            $scope.loading = false;
        }
    }

    $scope.ClearAddMarket = function () {

        $('#txt_Scheduled_Date').val(''),
        $('#ddl_Main_ID').val(''),
        $('#workorderid').val(''),
        $('#porid').val(''),
        $('#txt_T_Mobile_Ticket_Id').val(''),
        $('#txt_Account').val(''),
        $('#txt_Market').val(''),
        $('#txt_Cabinet_IDs').val(''),
        $('#txt_CX_Crew_Vendor').val(''),
        $('#txt_CX_Crew_Ld_Cont_No').val(''),
        $('#txt_CX_Crew_Ld_Name').val(''),
        $('#Scheduled_CX_Crew_Ld_Company').val(''),
        $('#txt_CX_Crew_Ld_E_mail_ID').val(''),

        $('#txt_IX_Crew_Vendor').val(''),
        $('#txt_IX_Crew_Ld_Contact_No').val(''),
        $('#txt_IX_Crew_Lead_Name').val(''),
        $('#txt_IX_Crew_Ld_Company').val(''),
        $('#IX_Crew_Ld_E_mail_ID').val(''),
        $('#ddl_Service_Affecting').val(''),
        $('#ddl_Day_MOP_or_Night_MOP').val(''),
        $('#txt_Reason_for_Day_MOP').val(''),
        $('#ddl_RF_Approval').val(''),
        $('#ddl_RF_Approved_MW_Time').val(''),
        $('#TMO_Deployment_Manager').val(''),
        $('#Planned_SoW').val(''),
        $('#ddl_Cxvendor_ID').val(''),
        $('#ddl_Ixvendor_ID').val('')
    }

    $scope.getDatediff = function (start, end, nestingtime) {

        var start1 = new Date(start);
        var end1 = new Date(end);
        var currtime = new Date();
        var diff = end1 - start1;
        var difffromcurtime = currtime - start1;

        $scope.hourdiff = Math.round((diff / 3600000), 2);
        difffromcurtime = Math.round((difffromcurtime / 3600000), 2);
        $scope.percentage = Math.round((difffromcurtime * 100) / nestingtime, 2);
        if ($scope.percentage > 100) {
            $scope.percentage = 100;
        }
    }

    //-------start of crew ---------------------///
    $scope.SearchGCList = [];
    $scope.SearchGCList1 = [];
    $scope.changedVal = 1;
    $scope.SearchPaceHolder = "Enter  Contact Number";

    $scope.changedVal1 = 1;
    $scope.SearchPaceHolder1 = "Enter  Contact Number";
    $scope.getSearchVal = function (changedVal) {
        $scope.SearchGCList = [];
        $scope.changedVal = changedVal
        if ($scope.changedVal == 1) {
            $scope.SearchPaceHolder = "Enter Contact Number";
            angular.forEach($scope.GCList, function (item) {
                var has = $scope.SearchGCList.indexOf(item.crew_lead_contact_no) > -1;
                if (!has) $scope.SearchGCList.push(item.crew_lead_contact_no)
            });

        }
        else if ($scope.changedVal == 2) {
            $scope.SearchPaceHolder = "Enter  Email ID";
            angular.forEach($scope.GCList, function (item) {
                var has = $scope.SearchGCList.indexOf(item.crew_lead_email_id) > -1;
                if (!has) $scope.SearchGCList.push(item.crew_lead_email_id)
            });
        }
        else if ($scope.changedVal == 3) {
            $scope.SearchPaceHolder = "Enter Lead Name";
            angular.forEach($scope.GCList, function (item) {
                var has = $scope.SearchGCList.indexOf(item.crew_lead_name) > -1;
                if (!has) $scope.SearchGCList.push(item.crew_lead_name)
            });
        }
        var ddlsearchGC = document.getElementById("ddl_gc");
        setTimeout(function () {

            ddlsearchGC.children[0].firstChild.value = "SOFTDEW#";
            var event = new Event('change');
            ddlsearchGC.children[0].firstChild.dispatchEvent(event);
        }, 100);



    }
    $scope.getSearchVal1 = function (changedVal) {
        $scope.SearchGCList1 = [];
        $scope.changedVal1 = changedVal
        if ($scope.changedVal1 == 1) {
            $scope.SearchPaceHolder1 = "Enter Contact Number";
            angular.forEach($scope.GCList, function (item) {
                var has = $scope.SearchGCList1.indexOf(item.crew_lead_contact_no) > -1;
                if (!has) $scope.SearchGCList1.push(item.crew_lead_contact_no)
            });

        }
        else if ($scope.changedVal1 == 2) {
            $scope.SearchPaceHolder1 = "Enter  Email ID";
            angular.forEach($scope.GCList, function (item) {
                var has = $scope.SearchGCList1.indexOf(item.crew_lead_email_id) > -1;
                if (!has) $scope.SearchGCList1.push(item.crew_lead_email_id)
            });
        }
        else if ($scope.changedVal1 == 3) {
            $scope.SearchPaceHolder1 = "Enter Lead Name";
            angular.forEach($scope.GCList, function (item) {
                var has = $scope.SearchGCList1.indexOf(item.crew_lead_name) > -1;
                if (!has) $scope.SearchGCList1.push(item.crew_lead_name)
            });
        }
        var ddlsearchGC = document.getElementById("ddl_gc1");
        setTimeout(function () {

            ddlsearchGC.children[0].firstChild.value = "SOFTDEW#";
            var event = new Event('change');
            ddlsearchGC.children[0].firstChild.dispatchEvent(event);
        }, 100);



    }
    $scope.GetGCList = function () {
        GCDashboardNewService.GetGCList().then(function (d) {
            $scope.GCList = d.data.Table;
            angular.forEach(d.data.Table, function (item) {
                var has = $scope.SearchGCList.indexOf(item.crew_lead_contact_no) > -1;
                if (!has) $scope.SearchGCList.push(item.crew_lead_contact_no)
                var has1 = $scope.SearchGCList1.indexOf(item.crew_lead_contact_no) > -1;
                if (!has1) $scope.SearchGCList1.push(item.crew_lead_contact_no)
            });
        }, function (err) {
            alert(err);
        });
    };

    $scope.GetGCList();
    $scope.filterGCList = function (userInput) {
        $scope.ShowGCDetails = '';
        $scope.SelectedGCID = '0';
        $scope.GCUpdated = false;
        $scope.IsNewGC = true;
        $scope.SelectedGC = userInput;
        var filter = $q.defer();
        var normalisedInput = userInput.toLowerCase();

        var filteredArray = $scope.SearchGCList.filter(function (obj) {
            return obj.toLowerCase().indexOf(normalisedInput) === 0;
        });
        filter.resolve(filteredArray);
        return filter.promise;
    };
    $scope.filterGCList1 = function (userInput) {
        $scope.ShowGCDetails = '';
        $scope.SelectedGCID1 = '0';
        $scope.GCUpdated = false;
        $scope.IsNewGC = true;
        $scope.SelectedGC = userInput;
        var filter = $q.defer();
        var normalisedInput = userInput.toLowerCase();

        var filteredArray = $scope.SearchGCList1.filter(function (obj) {
            return obj.toLowerCase().indexOf(normalisedInput) === 0;
        });
        filter.resolve(filteredArray);
        return filter.promise;
    };
    function GetGCDetails(contact, type) {
        var anArray = $scope.GCList;
        for (var i = 0; i < anArray.length; i += 1) {
            var aid = anArray[i].crew_lead_contact_no;
            if (type == 2)
                aid = anArray[i].crew_lead_email_id;
            else if (type == 3)
                aid = anArray[i].crew_lead_name;

            if (aid == contact) {
                return anArray[i];
            }
        }
        return '0';
    }
    function GetGCID(contact, type) {
        var anArray = $scope.GCList;
        for (var i = 0; i < anArray.length; i += 1) {
            var aid = anArray[i].crew_lead_contact_no;
            if (type == 2)
                aid = anArray[i].crew_lead_email_id;
            else if (type == 3)
                aid = anArray[i].crew_lead_name;
            if (aid == contact) {
                return anArray[i].id;
            }
        }
        return '0';
    }

    $scope.changeNumber = function (data, reverse) {
        let st = ""
        if (data != "") {
            let tmp = data.split('-');
            if (Array.isArray(tmp)) {
                tmp.map((item, i) => {
                    st = st + item;
                })
            }
        }

        if (reverse) {
            let first = reverse.substring(0, 3);
            let second = reverse.substring(3, 6);
            let end = reverse.substring(6, 10);

            st = first + "-" + second + "-" + end;
        }

        return st;
    }
    $scope.GCListSelected = function (item) {
        $scope.SelectedGC = item;
        let ShowGCDetails = GetGCDetails(item, $scope.changedVal);
        $scope.SelectedGCID = GetGCID($scope.SelectedGC, $scope.changedVal);

        $scope.Scheduled_CX_Crew_Ld_Cont_No = $scope.changeNumber("", ShowGCDetails.crew_lead_contact_no);
        $scope.Scheduled_CX_Crew_Ld_Name = ShowGCDetails.crew_lead_name;
        $scope.Scheduled_CX_Crew_Ld_Company = ShowGCDetails.crew_lead_company;
        $scope.Scheduled_CX_Crew_Ld_E_mail_ID = ShowGCDetails.crew_lead_email_id;
        $scope.Selected_Cxvendor_ID = ShowGCDetails.vendor_id;
        $scope.IsNewGC = false;
    };
    $scope.GCListSelected1 = function (item) {
        $scope.SelectedGC1 = item;
        let ShowGCDetails = GetGCDetails(item, $scope.changedVal1);
        $scope.SelectedGCID1 = GetGCID($scope.SelectedGC, $scope.changedVal1);

        $scope.Scheduled_IX_Crew_Ld_Contact_No = $scope.changeNumber("", ShowGCDetails.crew_lead_contact_no);
        $scope.Scheduled_IX_Crew_Lead_Name = ShowGCDetails.crew_lead_name;
        $scope.Scheduled_IX_Crew_Ld_Company = ShowGCDetails.crew_lead_company;
        $scope.Scheduled_IX_Crew_Ld_E_mail_ID = ShowGCDetails.crew_lead_email_id;
        $scope.Selected_Ixvendor_ID = ShowGCDetails.vendor_id;
        $scope.IsNewGC = false;
    };


    $scope.splitSow = function (item) {
        var str = [];

        if (item != "" && item != null) {
            str = item.split(',');

            $scope.finalsow = "";

            if (Array.isArray(str)) {
                str.map((item, i) => {
                    if (item == 1) {
                        if ($scope.finalsow != "") {
                            $scope.finalsow = $scope.finalsow + ',' + 'CX';
                        }
                        else {
                            $scope.finalsow = 'CX'
                        }
                    }
                    else if (item == 2) {
                        if ($scope.finalsow != "") {
                            $scope.finalsow = $scope.finalsow + ',' + 'IX';
                        }
                        else {
                            $scope.finalsow = 'IX'
                        }
                    }
                    else if (item == 3) {

                        if ($scope.finalsow != "") {
                            $scope.finalsow = $scope.finalsow + ',' + 'TroubleShooting';
                        }
                        else {
                            $scope.finalsow = 'TroubleShooting'
                        }
                    }
                    else if (item == 4) {
                        if ($scope.finalsow != "") {
                            $scope.finalsow = $scope.finalsow + ',' + 'E911-Call Test';
                        }
                        else {
                            $scope.finalsow = 'E911-Call Test'
                        }
                    }
                });
            }
        }
        return $scope.finalsow;
    }

    $scope.showsiteinfo = 0;
    $scope.globalscheduleid = 0;
    $scope.getsiteDetailsWithMap = function (scheduleid) {

        $scope.globalscheduleid = scheduleid;
        $scope.modalShown = true;
        $scope.showsiteinfo = 1;
        $scope.getsitedetails($scope.globalscheduleid);
    };

    $scope.getsitedetails = function (scheduleid) {
        $scope.loading = true;
        if (scheduleid == 0) {
            scheduleid = $scope.globalscheduleid;
            $scope.showsiteinfo = 1;
            $scope.modalShown = false;
            $scope.modalShown = true;
        }
        $scope.getPrecheckdata(scheduleid);
        $scope.getPostcheckdata(scheduleid);
        $scope.getSiteschedulingData(scheduleid);
        $scope.loading = false;
    }

    $scope.getPrecheckdata = function (scheduleid) {
        GCDashboardNewService.getPrecheckdata(scheduleid).then(function (d) {
            $scope.precheckdata = d.data.Table;
        }, function (err) {
            console.log(err);
        });
    };

    $scope.getPostcheckdata = function (scheduleid) {
        GCDashboardNewService.getPostcheckdata(scheduleid).then(function (d) {
            $scope.postcheckdata = d.data.Table;
        }, function (err) {
            console.log(err);
        });
    };
    $scope.getSiteschedulingData = function (scheduleid) {
        GCDashboardNewService.GetOntheflyDetails(scheduleid).then(function (d) {
            $scope.scheduling = d.data.Table;
        }, function (err) {
            console.log(err);
        });
    };

    $scope.getRanhistoricaldata = function () {
        $scope.modalShown = false;
        $scope.showsiteinfo = 3;
        $scope.modalShown = true;

        scheduleid = $scope.globalscheduleid;

        GCDashboardNewService.getRanhistoricaldata(scheduleid).then(function (d) {
            $scope.Ranhistoricaldata = d.data.Table;
        }, function (err) {
            console.log(err);
        });
    };

    //show map for a single site
    $scope.ViewSiteOnMap = function () {
        $scope.modalShown = false;
        $scope.showsiteinfo = 2;
        $scope.modalShown = true;

        $scope.siteDetails = 2;

        scheduleid = $scope.globalscheduleid;
        GCDashboardNewService.getMapSiteInfo(scheduleid).then(function (d) {
            var MapSiteList = d.data;

            if (MapSiteList.Table.length > 0) {

                var latitude = MapSiteList.Table[0].latitude;
                var longitude = MapSiteList.Table[0].longitude;
                var siteID = MapSiteList.Table[0].site_id;
                var address = MapSiteList.Table[0].site_address;
                var siteStatus = MapSiteList.Table[0].siteStatus;

                if (latitude != '' && longitude != '') {
                    $scope.MapmodalShown = true;
                    $scope.ModalHeader = siteID;
                    $scope.Markers = [
                    {
                        "title": siteID + '-' + address,
                        "lat": latitude,
                        "lng": longitude,
                        "description": siteID + '-' + address,
                        "siteStatus": siteStatus
                    }];

                    //Setting the Map options.
                    $scope.MapOptions = {
                        center: new google.maps.LatLng($scope.Markers[0].lat, $scope.Markers[0].lng),
                        zoom: 8,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };

                    //Initializing the InfoWindow, Map and LatLngBounds objects.
                    $scope.InfoWindow = new google.maps.InfoWindow();
                    //$scope.Latlngbounds = new google.maps.LatLngBounds();
                    $scope.Map = new google.maps.Map(document.getElementById("map_region"), $scope.MapOptions);

                    //Looping through the Array and adding Markers.
                    for (var i = 0; i < $scope.Markers.length; i++) {
                        var data = $scope.Markers[i];
                        var myLatlng = new google.maps.LatLng(data.lat, data.lng);

                        //Initializing the Marker object.
                        var marker = (data.siteStatus == 1) ? new google.maps.Marker({
                            icon: 'https://maps.google.com/mapfiles/ms/icons/green-dot.png',
                            position: myLatlng,
                            map: $scope.Map,
                            title: data.title
                        }) : new google.maps.Marker({
                            position: myLatlng,
                            map: $scope.Map,
                            title: data.title
                        });

                        //Adding InfoWindow to the Marker.
                        (function (marker, data) {
                            google.maps.event.addListener(marker, "click", function (e) {
                                $scope.InfoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.description + "</div>");
                                $scope.InfoWindow.open($scope.Map, marker);
                            });
                        })(marker, data);
                    }
                }
                else
                    jAlert('Latitude and longitude is not available.', 'Alert');
            }
            else
                jAlert('Latitude and longitude is not available.', 'Alert');
            $scope.loading = false;
        }, function (err) {
            alert(err);
        });
    }

    $scope.HideRow = function (rowsiteid) {
        //alert(rowsiteid);
        $('#rowid').hide();
    }

    $scope.ChangeMOP = function () {
        var MOP = $('#ddl_Day_MOP_or_Night_MOP').val();
        if (MOP == 1) {
            $('#reasonMOP').show();
        }
        else {
            $('#reasonMOP').hide();
        }
    }
    $scope.uploadFile2 = function (file) {
        var fileExtension = ['xlsx', 'xls'];
        if ($.inArray(file[0].name.split('.').pop().toLowerCase(), fileExtension) == -1) {
            jAlert('Upload excel file only.');
            $('#FileUpload1').val('');
            $scope.UploadFile2 = '';
        }
        else {
            $scope.UploadFile2 = file[0];
        }
        $scope.$apply();
    }
    $scope.is_file = 1;
    $scope.UploadpreviousData = function (IsValid) {
        $scope.loading = true;
        if (IsValid) {
            var fileData = new FormData();
            fileData.append('UploadFile', $scope.UploadFile2);
            fileData.append('remarks', $scope.txt_remarks);
            fileData.append('m_upload_type_id', $scope.is_file);

            $http.post('/Home/generatePreviousData', fileData, {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            }).then(function (d) {
                jAlert(d.data.Message, "Alert");
                $scope.loading = false;
            }, function (result) {
                console.log(result);
                $scope.loading = false;
            });
        }
    }

    $scope.ddlMapstatus = 0;
    $scope.ddlimpact = 0;

    $scope.openLoginPage = function (schId) {
        $location.url('/precheckNew/?schedule_id=' + schId + '&&type=4');
    }

    $scope.BtnDeleteClick = function (id) {
        jConfirm("Are You Sure To Delele This Record", 'Confirm', function (r) {
            if (r) {
                $scope.loading = true;
                GCDashboardNewService.deleteRanRecord(id).then(function (d) {
                    if (d.data == 1)
                        jAlert("Record Deleted sucessfully", "Message");
                    else if (d.data == 2)
                        jAlert("No Record Deleted", "Alert");
                    else if (d.data == 0)
                        jAlert("some error occured", "Alert");

                    $scope.getRandashboarddata();
                    $scope.loading = false;
                }, function (err) {
                    $scope.loading = false;
                    console.log("Error-->", err);
                });

            }
        });
    }

    $scope.sowDetailslist = [];
    $scope.mobsowDetails = [];

    $scope.mobileSchId = 0;
    $scope.mobilesiteId = 0;
    $scope.mobileFlag = 0;
    $scope.mobileReq = function (schId, siteId, flag) {
        $scope.mobileSchId = schId;
        $scope.mobilesiteId = siteId;
        $scope.mobileFlag = flag;
        if (flag == 2) {
            $scope.submitMobileReq(2);
        }
        else {
            $scope.mobsowDetails = [];
            $scope.sowDetailslist = [];
            $scope.GetsowDetails = function () {
                GCDashboardNewService.GetsowDetails().then(function (d) {
                    $scope.sowDetailslist = d.data;
                }, function (err) {
                });
            };
            $scope.GetsowDetails();
            $scope.modalMobileReq = true;
        }
    }

    $scope.submitMobileReq = function (flag) {
        var nestingTime = "";
        var sowCommment = "";
        var sowlist = "";

        if (flag == 1) {
            nestingTime = $('#mobNestingTime').val();
            sowCommment = $('#mobsowComment').val();
            var sowDetails = $scope.mobsowDetails;

            if (Array.isArray(sowDetails)) {
                sowDetails.map((item, i) => {
                    if (sowlist != "") {
                        sowlist = sowlist + ',' + item["id"];
                    }
                    else {
                        sowlist = item["id"];
                    }
                })
            }

            if (nestingTime == "") {
                jAlert("Enter Nesting Time", "Alert");
                return;
            }
            else if (sowCommment == "") {
                jAlert("Enter SOW Comment", "Alert");
                return;
            }
            else if (sowlist == "") {
                jAlert("Select SOW Details", "Alert");
                return;
            }
            else {
                jConfirm('Are you sure to request precheck', 'Confirm', function (r) {
                    if (r) {
                        $scope.loading = true;
                        GCDashboardNewService.submitMobReq($scope.mobileSchId, $scope.mobilesiteId, $scope.mobileFlag, nestingTime, sowCommment, sowlist).then(function (d) {
                            if (d.data == "1") {
                                jAlert("Precheck Requested", "Message");
                                $scope.modalMobileReq = false;
                                $scope.loading = false;
                                $scope.getRandashboarddata();

                            }
                        }, function (err) {
                            $scope.loading = false;
                        });
                    }
                });
            }
        }
        else {
            jConfirm('Are you sure to request postcheck', 'Confirm', function (r) {
                if (r) {
                    $scope.loading = true;
                    GCDashboardNewService.submitMobReq($scope.mobileSchId, $scope.mobilesiteId, $scope.mobileFlag, nestingTime, sowCommment, sowlist).then(function (d) {
                        if (d.data == "1") {
                            jAlert("Post check Requested", "message");
                            $scope.getRandashboarddata();
                            $scope.loading = false;
                        }
                    }, function (err) {
                        $scope.loading = false;
                    });
                }
            });
        }
    }

    $scope.CancelMobileReq = function (schedule_id, site_id, flag) {
        var f = 'precheck';
        if (flag == 2)
            f = 'postcheck';
        jConfirm('Are you sure to cancel ' + f + ' request.', 'Confirm', function (r) {
            if (r) {
                $scope.loading = true;
                GCDashboardNewService.submitCancelMobReq(schedule_id, site_id, flag).then(function (d) {
                    if (d.data == "1") {
                        jAlert("Requested has been canclled", "Message");
                        $scope.modalMobileReq = false;
                        $scope.loading = false;
                        $scope.getRandashboarddata();

                    }
                }, function (err) {
                    $scope.loading = false;
                });
            }
        });
    }
    $scope.selectedHeader = [];
    $scope.headerList = [];
    $scope.allcolumns = [];

    $scope.allcolumns = [{ id: "Scheduled_Date", label: "Schedule Date" }, { id: "status", label: "Entry Mode" },
            { id: "Service_Affecting", label: "Service Affecting" }, { id: "RF_Approval", label: "RF Approval" },
            { id: "site_id", label: "Main Id" }, { id: "Work_Order_No", label: "Project Code" },
            { id: "POR", label: "Technology" },
            { id: "ttid", label: "PAG Trouble Ticket" }, { id: "Planned_SoW", label: "Planned SOW" },
            { id: "Day_MOP_Night_MOP", label: "Day/Night Mop" },
            { id: "RF_Approved_MW_Time", label: "RF Approved MW Time" }, { id: "manualPre", label: "Manual Precheck Request" },
            { id: "mobileLoginTime", label: "Login Time Precheck Requested(Mobile App)" }, { id: "preCheck", label: "Pre-Check" },
            { id: "preDelivered", label: "Login Time Precheck Delivered" },
            { id: "mestingTime", label: "Nesting Time" },
            { id: "progress", label: "Progress" }, { id: "crew", label: "Crew" },
            { id: "callTest", label: "Call Test Activity" }, { id: "manualPost", label: "Manual Postcheck Request" },
            { id: "logoutTime", label: "Logout Time Postcheck Requested(Mobile App)" },
            { id: "postCheck", label: "Post-Check" },
            { id: "postDelivered", label: "Logout Time Postcheck Delivered" },
            { id: "pierUpdated", label: "Pier Updated" },
            { id: "siteType", label: "Site Type" }];

    $scope.headerList = $scope.allcolumns;
    $scope.submitselectedHeader = function () {
        $scope.loading = true;
        var selectedheader = $scope.selectedHeader;
        var selectedheader2 = "";

        if (Array.isArray(selectedheader)) {
            selectedheader.map((item, i) => {
                if (selectedheader2 != "") {
                    selectedheader2 = selectedheader2 + ',' + item["id"];
                }
                else {
                    selectedheader2 = item["id"];
                }
            })
        }
        if (selectedheader2 != "") {
            var obj = {
                id: 0,
                dashboardName: "RAN",
                column_no: selectedheader2
            }
            GCDashboardNewService.submitselectedHeader(obj).then(function (d) {
                if (d.data.Status == 1) {
                    jAlert("Slected Column Updated", "Message");
                    $scope.getRandashboarddata();
                    $scope.Getfilterdashboard();
                    $scope.modalfilterdashboard = false;
                    $scope.loading = false;
                }
                else if (d.data.Status == 0) {
                    jAlert("Session Time out", "Alert");
                    $scope.loading = false;
                }
                else
                    $scope.loading = false;
            });
        }
        else {
            jAlert("Please Select Column", "Alert");
            $scope.loading = false;
        }
    }

    $scope.modelArr = [];
    $scope.selectedHeader = [];

    $scope.btnFilterdashboard = function () {
        $scope.modalfilterdashboard = true;
        $scope.modelArr = [];
        GCDashboardNewService.getselectedHeader("RAN").then(function (d) {
            var output = d.data.Table[0];
            if (output != undefined && output != "") {
                let column = output.column_no;
                if (column != null && column != "") {
                    var nameArr = column.split(',');
                    if (Array.isArray(nameArr)) {
                        nameArr.map((item, i) => {
                            $scope.modelArr.push({ id: item })
                        })
                    }
                    $scope.selectedHeader = $scope.modelArr;
                }
            }
        });
    }

    $scope.Getfilterdashboard = function () {
        $scope.loading = true;
        $scope.getRandashboarddata();
        GCDashboardNewService.getselectedHeader("RAN").then(function (d) {
            if (d.data == "") {
                $scope.ShowheaderOnly = [];
            }
            else if (d.data.Table.length == 0) {
                $scope.ShowheaderOnly = [];
            }
            else if (d.data.Table[0].column_no == "") {
                $scope.ShowheaderOnly = [];
            }
            else {
                $scope.SelectedCol = d.data.Table[0].column_no.toString().split(',');
                if ($scope.SelectedCol.length > 0) {
                    var SearchResult = [];
                    var SearchResultlebal = [];
                    var Filter_list = $scope.allcolumns;
                    var TicketList = $scope.SelectedCol;
                    for (var i = 0; i < Filter_list.length; i += 1) {
                        for (var j = 0; j < TicketList.length; j += 1) {
                            if (Filter_list[i].id != null) {
                                if (Filter_list[i].id == TicketList[j]) {
                                    SearchResult.push(Filter_list[i]);
                                    SearchResultlebal.push(Filter_list[i].label);
                                }
                            }
                        }
                    }
                    $scope.ShowheaderOnly = SearchResult;
                    $scope.selectlable = SearchResultlebal;
                }
            }
            $scope.loading = false;
        });
    };

    $scope.getRandashboarddata();
    $scope.Timer = function () {
        var id = setInterval(frame, 120000);
        function frame() {
            $scope.getRandashboarddata();
            $scope.$apply();
        }
    }
    $scope.Timer();

    $scope.GetCopList = function () {
        $scope.modalCop = true;
        $scope.loading = true;
        $scope.Copflag = 1;
        GCDashboardNewService.GetCopList($scope.copPreId, $scope.copPostId, $scope.crewSiteId).then(function (d) {
            if (d.data != "") {
                $scope.CopPreList = JSON.parse(d.data.pre)
                $scope.CopPostList = JSON.parse(d.data.post)
                $scope.GetMailSender();
            }
            $scope.loading = false;
        }, function (err) {
            console.log(err);
            $scope.loading = false;
        });
    };

    $scope.GetTimeLineList = function () {
        $scope.modalCop = true;
        $scope.loading = true;
        $scope.Copflag = 2;
        GCDashboardNewService.GetTimeLineList($scope.timelineId).then(function (d) {
            $scope.TimeLineList = d.data;
            $scope.loading = false;
        }, function (err) {
            console.log(err);
            $scope.loading = false;
        });
    };

    $scope.pageChanged(1);
    $scope.$broadcast('refreshFixedColumns');

    $scope.BtnReset_Click = function () {
        $scope.i = 0;
        $scope.SelectedAccount = "";
        $scope.SelectedMarket = "";
        $scope.txt_start_date = "";
        $scope.txt_end_date = "";
        $scope.getRandashboarddata();
    }

    $scope.GetMailSender = function () {
        GCDashboardNewService.GetSenderMails().then(function (d) {
            $scope.MailSenderList = d.data;
        }, function (err) {

        });
    }

    $scope.sendCopMail = function () {
        var fromMail = $('#mailFromId').val();
        var toMail = $('#txtTomail').val();

        if (fromMail == "" || fromMail == "0") {
            jAlert("Select Sender Mail Id", "Alert");
        }
        else if (toMail == "") {
            jAlert("Enter Receiver Emails", "Alert");
        }
        else {
            $scope.loading = true;
            GCDashboardNewService.sendCopMail(fromMail, toMail, $scope.copPreId, $scope.copPostId, $scope.timelineId, $scope.crewSiteId, $scope.SelectedPOR).then(function (d) {
                jAlert(d.data.Message, "Alert");
                $scope.loading = false;
            }, function (err) {
                console.log("Error In send Mail->", err);
            });
        }
    }
    $scope.clearCopMail = function () {
        $('#txtTomail').val('');
        $('#mailFromId').val('');
    }
}

GCDashboardControllerNew.$inject = ['$scope', '$http', 'GCDashboardNewService', '$location', '$timeout', '$filter', '$q'];

var GCDashboardNewService = function ($http, $q) {
    var fac = {};
    fac.GetPOR = function (mainId, workId) {
        return $http({
            url: "/Home/getPOR",
            method: 'POST',
            data: { "mainid": mainId, "workId": workId },
            headers: { 'content-type': 'application/json' }
        });
    };

    fac.getRanhistoricaldata = function (id) {
        return $http({
            url: "/Home/getRanhistoricaldata",
            method: 'POST',
            data: { "id": id },
            headers: { 'content-type': 'application/json' }
        });
    };

    fac.GetAllCIQSiteDetails = function (AllsiteID) {
        return $http({
            url: "/Home/GetAllCIQSiteDetails",
            method: 'POST',
            data: { AllsiteID: AllsiteID },
            headers: { 'content-type': 'application/json' }
        });
    };
    fac.getdataonSiteId = function (siteid) {
        return $http({
            url: "/Home/getdataonSiteId",
            method: 'POST',
            data: { mainid: siteid },
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.GetMain_IDList = function () {
        return $http({
            url: "/Home/GetMain_IDListNew",
            method: 'POST',
            data: {},
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.GetSiteIDList = function () {
        return $http({
            url: "/Home/GetSiteIDList",
            method: 'POST',
            data: {},
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.GetWork_Order_NumberList = function () {
        return $http({
            url: "/Home/GetWork_Order_NumberListNew",
            method: 'POST',
            data: {},
            headers: { 'content-type': 'application/json' }
        });
    };

    fac.GetPOR_List = function () {
        return $http({
            url: "/Home/GetPOR_List_New",
            method: 'POST',
            data: {},
            headers: { 'content-type': 'application/json' }
        });
    };
    fac.BindVendorList = function () {
        return $http({
            url: "/Home/getvendorList",
            method: 'POST',
            data: {},
            headers: { 'content-type': 'application/json' }
        });
    };
    fac.getvendorDetails = function (vendorId) {
        return $http({
            url: "/Home/getvendorDetails",
            method: 'POST',
            data: { vendorId: vendorId },
            headers: { 'content-type': 'application/json' }
        });
    };

    fac.SaveUpdateschedulingNew = function (arrList) {
        return $http({
            url: "/Home/SaveUpdateschedulingNew",
            method: 'POST',
            data: { arrList: arrList },
            headers: { 'content-type': 'application/json' }
        });
    };

    fac.GetGCLoginDetails = function () {
        return $http({
            url: "/Home/GetGCLoginDetails",
            method: 'POST',
            data: {},
            headers: { 'content-type': 'application/json' }
        });
    };
    fac.getRandashboarddata = function () {
        return $http({
            url: "/Home/getRandashboarddata",
            method: 'POST',
            data: {},
            headers: { 'content-type': 'application/json' }
        });
    };
    fac.GetEngineerList = function () {
        return $http({
            url: "/Home/GetEngineerList",
            method: 'POST',
            data: {},
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.GetAccountList = function () {
        return $http({
            url: "/Home/GetAccountList",
            method: 'POST',
            data: {},
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.GetMarketList = function (account_id) {
        return $http({
            url: "/Home/GetMarketList",
            method: 'POST',
            data: { AccountID: account_id },
            headers: { 'content-type': 'application/json' }
        });
    }

    fac.getdatabyMailid = function (Main_id) {
        return $http({
            url: "/Home/GetMain_idList",
            method: 'POST',
            data: { Main_id: site_id },
            headers: { 'content-type': 'application/json' }
        });
    }

    fac.SaveEngineerDetails = function (CGLoggedID, eng, type) {
        return $http({
            url: "/Home/UpdateRANDashboardEngineer",
            method: 'POST',
            data: { GCLoginID: CGLoggedID, EngID: eng, EngType: type },
            headers: { 'content-type': 'application/json' }
        });
    };
    fac.GetMarketCurrentTime = function () {
        return $http({
            url: "/Home/GetMarketCurrentTime",
            method: 'POST',
            data: {},
            headers: { 'content-type': 'application/json' }
        });
    };
    fac.GetSiteAllDetails = function (CGLoggedID) {
        return $http({
            url: "/Home/GetSiteAllDetails",
            method: 'POST',
            data: { CGLoggedID: CGLoggedID },
            headers: { 'content-type': 'application/json' }
        });
    };
    fac.OverwriteScheduleDetails = function (data) {
        return $http({
            url: "/Home/OverwriteScheduleDetails",
            method: 'POST',
            data: JSON.stringify({ ObjPaceExist: data }),
            headers: { 'content-type': 'application/json' }
        });
    };


    fac.GetSiteAllDetailsNew = function (site_id) {
        return $http({
            url: "/Home/GetSiteAllDetailsNew",
            method: 'POST',
            data: { site_id: site_id },
            headers: { 'content-type': 'application/json' }
        });
    };

    fac.SavePrecheckRequestDate = function (CGLoggedID) {
        return $http({
            url: "/Home/SavePrecheckRequestDate",
            method: 'POST',
            data: { GCLoginID: CGLoggedID },
            headers: { 'content-type': 'application/json' }
        });
    };
    fac.SavePostcheckRequestDate = function (CGLoggedID) {
        return $http({
            url: "/Home/SavePostcheckRequestDate",
            method: 'POST',
            data: { GCLoginID: CGLoggedID },
            headers: { 'content-type': 'application/json' }
        });
    };
    //Divyam added 07/22
    fac.GetChatDetails = function () {
        return $http({
            url: "/Home/GetChatDetails",
            method: 'POST',
            data: {},
            headers: { 'content-type': 'application/json' }
        });
    }
   //Divyam added 07 / 22
    fac.GetTMO_Deployment_Manager_List = function () {
        return $http({
            url: "/Home/GetTMO_Deployment_Manager_List_new",
            method: 'POST',
            data: {},
            headers: { 'content-type': 'application/json' }
        });
    };

    fac.GetCIQSiteDetails = function (newId) {
        return $http({
            url: "/Home/GetCIQSiteDetailsNew",
            method: 'POST',
            data: { newId: newId },
            headers: { 'content-type': 'application/json' }
        });
    }

    fac.getMapSiteInfo = function (scheduleId) {
        return $http({
            url: "/Home/getMapSiteInfo",
            method: 'POST',
            data: { scheduleId: scheduleId },
            headers: { 'content-type': 'application/json' }
        });
    }

    fac.getcxixinfo = function (newId) {
        return $http({
            url: "/Home/getcxixinfo",
            method: 'POST',
            data: { schedule_id: newId },
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.GetPlannedSOW = function () {
        return $http({
            url: "/Home/GetPlannedSOW",
            method: 'POST',
            data: {},
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.GetGCList = function () {
        return $http({
            url: "/Home/GetManagecrewList",
            method: 'GET',

            headers: { 'content-type': 'application/json' }
        });
    }
    fac.Binddataonbutton = function (siteID) {
        return $http({
            url: "/Home/GetOntheflydata",
            method: 'POST',
            data: { siteID: siteID },
            headers: { 'content-type': 'application/json' }
        });
    }
    //get precheck data
    fac.getPrecheckdata = function (schedule_id) {
        return $http({
            url: "/Home/GetPrecheckDetailsNew",
            method: 'POST',
            data: { schedule_id: schedule_id },
            headers: { 'content-type': 'application/json' }
        });
    }

    //get postcheck data
    fac.getPostcheckdata = function (schedule_id) {
        return $http({
            url: "/Home/GetPostcheckDetailsNew",
            method: 'POST',
            data: { schedule_id: schedule_id },
            headers: { 'content-type': 'application/json' }
        });
    }

    fac.GetOntheflyDetails = function (schedule_id) {
        return $http({
            url: "/Home/GetOntheflyDetails",
            method: 'POST',
            data: { schedule_id: schedule_id },
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.deleteRanRecord = function (id) {
        return $http({
            url: "/Home/deleteTrackerRecord",
            method: 'POST',
            data: { id: id, "trackerName": "RAN" },
            headers: { 'content-type': 'application/json' }
        });
    };

    fac.submitMobReq = function (schId, siteId, flag, nestingTime, sowCommment, sowDetails) {
        return $http({
            url: "/Home/manualMobReq",
            method: 'POST',
            data: { "schId": schId, "siteId": siteId, "flag": flag, "nestingTime": nestingTime, "sowCommment": sowCommment, "sowDetails": sowDetails },
            headers: { 'content-type': 'application/json' }
        });
    };
    fac.submitCancelMobReq = function (schId, siteId, flag) {
        return $http({
            url: "/Home/CancelmanualMobReq",
            method: 'POST',
            data: { "schId": schId, "siteId": siteId, "flag": flag },
            headers: { 'content-type': 'application/json' }
        });
    };
    fac.GetsowDetails = function () {
        return $http({
            url: "/Home/getsowDetails",
            method: 'POST',
            data: {},
            headers: { 'content-type': 'application/json' }
        });
    };
    fac.getselectedHeader = function (dashboardName) {
        return $http({
            url: "/Home/GetFilterdataList",
            method: 'POST',
            data: { dashboardName: dashboardName },
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.submitselectedHeader = function (obj) {
        return $http({
            url: "/Home/SaveFilterdashboardDetailsdata",
            method: 'POST',
            data: { filter: obj },
            headers: { 'content-type': 'application/json' }
        });
    };

    fac.GetCopList = function (pre, post, siteId) {
        return $http({
            url: "/Home/GetCopList",
            method: 'POST',
            data: { schPreId: pre, schPostId: post, siteId: siteId },
            headers: { 'content-type': 'application/json' }
        });
    };
    fac.GetTimeLineList = function (Id) {
        return $http({
            url: "/Home/GetsiteTimeLine",
            method: 'POST',
            data: { timlineId: Id },
            headers: { 'content-type': 'application/json' }
        });
    };
    fac.GetSenderMails = function () {
        return $http({
            url: "/Home/GetMailSender",
            method: 'POST',
            data: {},
            headers: { 'content-type': 'application/json' }
        });
    }
    fac.sendCopMail = function (fromId, toEmail, copPreId, copPostId, timelineId, crewSiteId,por) {
        return $http({
            url: "/Home/sendCopMail",
            method: 'POST',
            data: { mailFromId: fromId, preCopId: copPreId, postCopId: copPostId, timeLineId: timelineId, siteId: crewSiteId, toMailId: toEmail,POR:por },
            headers: { 'content-type': 'application/json' }
        });
    };

    fac.Export_logTracker = function (ControllerName, ActivityType, PageName, MailSend, PageUrl, SearchFor, Remarks) {
        return $http({
            url: "/Home/Track_Log_Activity_SendMail",
            method: 'POST',
            data: { ControllerName: ControllerName, ActivityType: ActivityType, PageName: PageName, MailSend: MailSend, PageUrl: PageUrl, SearchFor: SearchFor, Remarks: Remarks },
            headers: { 'content-type': 'application/json' }
        });
    };
    return fac;
}
GCDashboardNewService.$inject = ['$http', '$q'];